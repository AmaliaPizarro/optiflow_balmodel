* The value 0 or white space implies no bound. The value 'eps' restricts to zero.


* The capacity is implemented as a bound on a Flow.
* The Flow relates processes PROC (index 3) and IPROC (index 4);
* the value of IFLOWINOUT determines whether the Flow is from PROC to IPROC or the other way.
* Hence, with 'IFLOWINOUT_OUT' the bound will be on the Flow from PROC  (index 3) to IPROC (index 4),
* while  with 'IFLOWINOUT_IN'  the bound will be on the Flow from IPROC (index 4) to  PROC (index 3).
* This way, the bound always relates PROC (index 3) and FLOW, and the sequence of PROC and IPROC in the data rows may be used as is convenient.
* Capacity cannot be given for entering Flow with IMANYONE in PROCINOUTFLOW nor for leaving Flow with IONEMANY.



*$ifi  %invSmallDom%==yes $goto largeDomain

* ADDITIONAL COMMENT TO ABOVE:
* Proc with capacity bounds are restricted to have only one independent Flow bundle (TO BE defined..).


* OBS:  These data were changed ONLY WHEN SYNTAX ERRORS OCCUR DUE TO change in domain in declaration.  Hence, there are still some buffers that bave values that probably whould not have.
* Moreover, I may have made errors in the revision. In particular, search "57.6000" which is on ElecBuffer with value 57.6000 many palces - should that not be on 'IFLOWINOUT_IN' ?
* More errors obs: see errors.out
* the good message is that now there are no syntas errors :)

PARAMETER PROCKAPFX(YYY,AAA,PROC,FLOW,IFLOWINOUT) "Capacity of Process (U/h)"
/

* $ontext  !! this is small data set for testing: almost no capacity:
*This file: For debug: interchanged  00.00 with EPS and vice versa
*If the capacity of a plant in an area is 0, this number does not have to be stated here (unless wanted)


* W A S T E  -  T O  -  E N E R G Y

2012.DK_CA_KBH . BiogasCHP                   . BIOGAS_TOCHP_GJ        .IFLOWINOUT_IN       00.00
2012.DK_CA_KBH . Biogas_Upgrading            . UPGRADED_BIOGAS_GJ     .IFLOWINOUT_IN       00.00

2012.DK_CA_KBH . MSW_chp_small            . MSW_FUELCHP_SMALL_TON           .IFLOWINOUT_IN     00.00
2012.DK_CA_KBH . MSW_chp_medium           . MSW_FUELCHP_MEDIUM_TON          .IFLOWINOUT_IN     00.00
2012.DK_CA_KBH . MSW_chp_large            . MSW_FUELCHP_LARGE_TON           .IFLOWINOUT_IN     00.00
2012.DK_CA_KBH . MSW_ho                   . MSW_FUELHO_TON                  .IFLOWINOUT_IN     00.00
2012.DK_CA_KBH . RDF_chp                  . RDF_FUELCHP_TON                 .IFLOWINOUT_IN     00.00
2012.DK_CA_KBH . RDF_ho                   . RDF_FUELHO_TON                  .IFLOWINOUT_IN     00.00
/;





























































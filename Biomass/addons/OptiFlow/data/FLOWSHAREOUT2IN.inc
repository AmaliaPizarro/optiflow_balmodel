* Use EPS for the value 0. Entering 0 or nothing will be interpreted as 'not relevant'.


TABLE FLOWSHAREOUT2IN(AAA,IPROCFROM,IPROCTO,FLOW/*,SSS,TTT*/,ILOUPFXSET) "Bounds on shares of split outflow to PROC ()"

                                                                                                    ILOUPFX_LO  ILOUPFX_UP  ILOUPFX_FX

DK_CA_KBH . Biogas_SplitCHPorTransp   .ProcBiogasCHPLinkStorage           .BIOGAS_TOCHP_GJ                       1
DK_CA_KBH . Biogas_SplitCHPorTransp  . ProcBiogasUpgradeLinkStorage       .BIOGAS_TOUPGRADE_GJ                   1

DK_CA_KBH .   SourceSegreg_pre         . SourceSegregOF_50           . RESIDUALWASTEHH_SEP_PEOPLE_1
DK_CA_KBH .   SourceSegreg_pre         . SourceSegregOF_No           . RESIDUALWASTEHH_SEP_PEOPLE_3                              0.1


;





* For convenience assigning same data to all other relevant areas (then overwriting where needed).
* However, note that areas that are exclusively for transit with respect to transport should not be part of this:

*DISPLAY IA, RRRAAA, ICA;
*DISPLAY "EFTER DEF, F�R ASSIGN:", FLOWSHAREOUT2IN ;
FLOWSHAREOUT2IN(IA,IPROCFROM,IPROCTO,FLOW,ILOUPFXSET)$(ICA('DENMARK',IA) AND (NOT AAATRANSIT(IA)) AND FLOWFROMTOPROC(IA,IPROCFROM,IPROCTO,FLOW)) = FLOWSHAREOUT2IN('DK_CA_KBH',IPROCFROM,IPROCTO,FLOW,ILOUPFXSET);
*DISPLAY "EFTER ASSIGN:", FLOWSHAREOUT2IN ;


SET  PROCKAPDATA(PROC,FLOW,IFLOWINOUT)    "Process data: capacity relative to FLOW and in/out direction ()"
/

*BiogasCHP                . ELECFLOW                        .IFLOWINOUT_OUT
*Biogas_Upgrading         . BIOGAS_TOUPGRADE_GJ              .IFLOWINOUT_IN
ProcHeatGenLinkStorage   . HEATFLOW                        . IFLOWINOUT_IN

RDF_Storage              . RDF_HHW_TOTAL                   . IFLOWINOUT_IN

MSW_chp_small            . MSW_FUELCHP_SMALL_TON           .IFLOWINOUT_IN
MSW_chp_medium           . MSW_FUELCHP_MEDIUM_TON          .IFLOWINOUT_IN
MSW_chp_large            . MSW_FUELCHP_LARGE_TON           .IFLOWINOUT_IN
MSW_ho                   . MSW_FUELHO_TON                  .IFLOWINOUT_IN
RDF_chp                  . RDF_FUELCHP_TON                 .IFLOWINOUT_IN
RDF_ho                   . RDF_FUELHO_TON                  .IFLOWINOUT_IN

/
;

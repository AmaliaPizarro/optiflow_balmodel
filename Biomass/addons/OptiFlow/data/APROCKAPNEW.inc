SET APROCKAPNEW(AAA,PROC)     'Areas for possible location of new Proc or capacity expansion of existing Proc'
*This is stablished according to economy of scale, where large plants can only be located when heat demand is high

/

DK_CA_KBH . MSW_chp_large
DK_CA_KBH . MSW_ho

DK_CA_KBH . RDF_chp
DK_CA_KBH . RDF_ho

DK_CA_Kal . MSW_chp_medium
DK_CA_Kal . MSW_ho

DK_CA_Kal . RDF_chp
DK_CA_Kal . RDF_ho

DK_CA_Aab . MSW_chp_small
DK_CA_Aab . MSW_ho

DK_E_DTU .  MSW_ho


/

;

* Areas with high heat demand (Large) can invest in the same technologies than DK_CA_KBH

APROCKAPNEW('DK_CA_Aarhus',PROC)=APROCKAPNEW('DK_CA_KBH',PROC);
APROCKAPNEW('DK_CA_Odense',PROC)=APROCKAPNEW('DK_CA_KBH',PROC);


*Areas with medium heat demand (Medium) can invest in the same technologies than DK_CA_Kal



APROCKAPNEW('DK_CA_Aal',PROC)=APROCKAPNEW('DK_CA_KBH',PROC)+APROCKAPNEW('DK_CA_Kal',PROC)$(NOT APROCKAPNEW('DK_CA_KBH',PROC));
APROCKAPNEW('DK_CA_TVIS',PROC)=APROCKAPNEW('DK_CA_KBH',PROC)+APROCKAPNEW('DK_CA_Kal',PROC)$(NOT APROCKAPNEW('DK_CA_KBH',PROC));

*Areas with medium-small heat demand (Medium-Small) can invest in the same technologies than DK_CA_Aab
APROCKAPNEW('DK_MA_Sndborg',PROC)=APROCKAPNEW('DK_CA_Aab',PROC);
APROCKAPNEW('DK_MA_Grenaa',PROC)=APROCKAPNEW('DK_CA_Aab',PROC);
APROCKAPNEW('DK_MA_Horsens',PROC)=APROCKAPNEW('DK_CA_Aab',PROC);
APROCKAPNEW('DK_MAM_Nyk',PROC)=APROCKAPNEW('DK_CA_Aab',PROC);
APROCKAPNEW('DK_MAM_Naestved',PROC)=APROCKAPNEW('DK_CA_Aab',PROC);
APROCKAPNEW('DK_MAM_Thisted',PROC)=APROCKAPNEW('DK_CA_Aab',PROC);
APROCKAPNEW('DK_MAM_Frdhavn',PROC)=APROCKAPNEW('DK_CA_Aab',PROC);
APROCKAPNEW('DK_MAM_Nyborg',PROC)=APROCKAPNEW('DK_CA_Aab',PROC);
APROCKAPNEW('DK_MAM_Svend',PROC)=APROCKAPNEW('DK_CA_Aab',PROC);
APROCKAPNEW('DK_MAM_Slagelse',PROC)=APROCKAPNEW('DK_CA_Aab',PROC);
APROCKAPNEW('DK_CA_Hern',PROC)=APROCKAPNEW('DK_CA_Kal',PROC)+APROCKAPNEW('DK_CA_Aab',PROC)$(NOT APROCKAPNEW('DK_CA_Kal',PROC));
APROCKAPNEW('DK_CA_Esb',PROC)=APROCKAPNEW('DK_CA_Kal',PROC)+APROCKAPNEW('DK_CA_Aab',PROC)$(NOT APROCKAPNEW('DK_CA_Kal',PROC));

APROCKAPNEW('DK_CA_Randers',PROC)=APROCKAPNEW('DK_CA_Kal',PROC)+APROCKAPNEW('DK_CA_Aab',PROC)$(NOT APROCKAPNEW('DK_CA_Kal',PROC));
APROCKAPNEW('DK_MA_NrdOstSj',PROC)=APROCKAPNEW('DK_CA_Kal',PROC)+APROCKAPNEW('DK_CA_Aab',PROC)$(NOT APROCKAPNEW('DK_CA_Kal',PROC));
APROCKAPNEW('DK_MA_Holst',PROC)=APROCKAPNEW('DK_CA_Kal',PROC)+APROCKAPNEW('DK_CA_Aab',PROC)$(NOT APROCKAPNEW('DK_CA_Kal',PROC));
APROCKAPNEW('DK_MA_Hil',PROC)=APROCKAPNEW('DK_CA_Kal',PROC)+APROCKAPNEW('DK_CA_Aab',PROC)$(NOT APROCKAPNEW('DK_CA_Kal',PROC));
APROCKAPNEW('DK_MA_Silk',PROC)=APROCKAPNEW('DK_CA_Kal',PROC)+APROCKAPNEW('DK_CA_Aab',PROC)$(NOT APROCKAPNEW('DK_CA_Kal',PROC));
APROCKAPNEW('DK_MA_Hjoerring',PROC)=APROCKAPNEW('DK_CA_Kal',PROC)+APROCKAPNEW('DK_CA_Aab',PROC)$(NOT APROCKAPNEW('DK_CA_Kal',PROC));
APROCKAPNEW('DK_MA_Viborg',PROC)=APROCKAPNEW('DK_CA_Kal',PROC)+APROCKAPNEW('DK_CA_Aab',PROC)$(NOT APROCKAPNEW('DK_CA_Kal',PROC));


APROCKAPNEW('DK_CA_Kal',PROC)=APROCKAPNEW('DK_CA_Kal',PROC)+APROCKAPNEW('DK_CA_KBH',PROC)$(NOT APROCKAPNEW('DK_CA_Kal',PROC));

*Areas with small heat demand (small) can invest in the same technologies than DK_E_DTU
APROCKAPNEW('DK_MAM_Had',PROC)=APROCKAPNEW('DK_E_DTU',PROC);
APROCKAPNEW('DK_MAM_Aars',PROC)=APROCKAPNEW('DK_E_DTU',PROC);
APROCKAPNEW('DK_MAM_Hammel',PROC)=APROCKAPNEW('DK_E_DTU',PROC);
APROCKAPNEW('DK_MAM_Skagen',PROC)=APROCKAPNEW('DK_E_DTU',PROC);
APROCKAPNEW('DK_MAM_Hobro',PROC)=APROCKAPNEW('DK_E_DTU',PROC);
APROCKAPNEW('DK_CA_Roenne',PROC)=APROCKAPNEW('DK_E_DTU',PROC);
APROCKAPNEW('DK_MAM_NrAlslev',PROC)=APROCKAPNEW('DK_E_DTU',PROC);
*APROCKAPNEW('DK_CA_Vestfrb',PROC)=APROCKAPNEW('DK_E_DTU',PROC);

*Decentral areas for Anaerobic Digestion with manure
APROCKAPNEW('DK_CA_Aal_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_CA_Aarhus_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_CA_Esb_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_CA_Hern_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_CA_Kal_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_CA_KBH_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_CA_Odense_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_CA_Randers_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_CA_Roenne_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_CA_TVIS_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_CA_Vestfrb_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_E_DTU_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MA_Grenaa_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MA_Hil_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MA_Hjoerring_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MA_Holst_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MA_Horsens_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MA_NrdOstSj_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MA_Silk_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MA_Sndborg_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MA_Viborg_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MAM_Aars_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MAM_Frdhavn_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MAM_Had_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MAM_Hammel_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MAM_Hobro_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MAM_Naestved_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MAM_NrAlslev_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MAM_Nyborg_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MAM_Nyk_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MAM_Skagen_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MAM_Slagelse_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MAM_Svend_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);
APROCKAPNEW('DK_MAM_Thisted_DEC',PROC)=APROCKAPNEW('DK_CA_Aab_DEC',PROC);


* File Balmorel.gms
$TITLE Balmorel version 3.03 (January 2014; latest 20150430)
*$TITLE Balmorel version 3.03 (January 2014; latest 20141016)   This is Title line from the file from which the present file is originally taken.

* Dynamic partial equilibrium model for the electricity and CHP sector.

* Efforts have been made to make a good model.
* However, most probably the model is incomplete and subject to errors.
* It is distributed with the idea that it will be usefull anyway,
* and with the purpose of getting the essential feedback,
* which in turn will permit the development of improved versions
* to the benefit of other user.
* Hopefully it will be applied in that spirit.

* Description, further documentation, examples of applications of the model,
* conditions of use, and contact for feedback may be found at www.Balmorel.com.


SCALAR IBALVERSN 'This version of Balmorel' /303.21041016/;
* (Internal version id BC07)
* Internal info: last revisions: 20140829; 21041016;



*-------------------------------------------------------------------------------
*-------------------------------------------------------------------------------
*-------------------------------------------------------------------------------

* GAMS options are $included from file balgams.opt.
* In order to make them apply globally, the option $ONGLOBAL will first be set here:
$ONGLOBAL

* The balgams file holds control settings for GAMS.
* Use local file if it exists, otherwise use the one in folder  ../../base/model/.
$ifi     exist 'balgams.opt'  $include  'balgams.opt';
$ifi not exist 'balgams.opt'  $include  '../../base/model/balgams.opt'

* The balopt file holds option (control) settings for the Balmorel model.
* Use local file if it exists, otherwise use the one in folder  ../../base/model/.


$ifi     exist 'balopt.opt'  $include                  'balopt.opt';
$ifi not exist 'balopt.opt'  $include '../../base/model/balopt.opt';

*-------------------------------------------------------------------------------
*-------------------------------------------------------------------------------
* In case of Model Balbase4 the following included file substitutes the remaining part of the present file Balmorel.gms:
$ifi %BB4%==yes $include '../../base/model/BalmorelBB4.inc';
$ifi %BB4%==yes $goto ENDOFMODEL
*-------------------------------------------------------------------------------
*-------------------------------------------------------------------------------



*-------------------------------------------------------------------------------
*-------------------------------------------------------------------------------
* Declarations and inclusion of data files:
*-------------------------------------------------------------------------------
*-------------------------------------------------------------------------------


*-------------------------------------------------------------------------------
*---- User defined SETS and ACRONYMS needed for data entry: --------------------
*-------------------------------------------------------------------------------

* Declarations: ----------------------------------------------------------------

* SETS:
SET CCCRRRAAA         'All geographical entities (CCC + RRR + AAA)';
SET CCC(CCCRRRAAA)    'All Countries';
SET RRR(CCCRRRAAA)    'All regions';
SET AAA(CCCRRRAAA)    'All areas';
SET CCCRRR(CCC,RRR)   'Regions in countries';
SET RRRAAA(RRR,AAA)   'Areas in regions';
SET YYY               'All years';
SET SSS               'All seasons';
SET TTT               'All time periods';
SET GGG               'All generation technologies';
SET GDATASET          'Generation technology data';
SET FFF               'Fuels';
SET FDATASET          'Characteristics of fuels';
SET HYRSDATASET       'Characteristics of hydro reservoirs';
SET DF_QP             'Quantity and price information for elastic demands';
SET DEF               'Steps in elastic electricity demand';
SET DEF_D1(DEF)       'Downwards steps in elastic el. demand, relative data format';
SET DEF_U1(DEF)       'Upwards steps in elastic el. demand, relative data format';
SET DEF_D2(DEF)       'Downwards steps in elastic el. demand, absolute Money and MW-incremental data format';
SET DEF_U2(DEF)       'Upwards steps in elastic el. demand, absolute Money and MW-incremental data format';
SET DEF_D3(DEF)       'Downwards steps in elastic el. demand, absolute Money and fraction of nominal demand data format';
SET DEF_U3(DEF)       'Upwards steps in elastic el. demand, absolute Money and fraction of nominal demand data format';
SET DHF               'Steps in elastic heat demand';
SET DHF_D1(DHF)       'Downwards steps in elastic heat demand, relative data format';
SET DHF_U1(DHF)       'Upwards steps in elastic heat demand, relative data format';
SET DHF_D2(DHF)       'Downwards steps in elastic heat demand, absolute Money and MW-incremental data format';
SET DHF_U2(DHF)       'Upwards steps in elastic heat demand, absolute Money and MW-incremental data format';
SET DHF_D3(DHF)       'Downwards steps in elastic heat demand, absolute Money and fraction of nominal demand data format';
SET DHF_U3(DHF)       'Upwards steps in elastic heat demand, absolute Money and fraction of nominal demand data format';
SET MPOLSET           'Emission and other policy data';
SET C(CCC)            'Countries in the simulation';
SET G(GGG)            'Generation technologies in the simulation';
SET AGKN(AAA,GGG)     'Areas for possible location of new technologies';
SET Y(YYY)            'Years in the simulation';
SET S(SSS)            'Seasons in the simulation';
SET T(TTT)            'Time periods within the season in the simulation';
*SET DAYTYPE           'Types of days within the week, typically workdays/weekend';
SET TWORKDAY(TTT)     'Time segments, T, in workdays';
SET TWEEKEND(TTT)     'Time segments, T, in weekends';
SET ECONSET           'Set for econ'
SET ECONSETCATEGORY   'Set for econ'

* Internal set IGGGALIAS may be used in the $included data files:
ALIAS(GGG,IGGGALIAS);
* Set CCCRRRAAAALIAS may be used in the $included data files:
ALIAS(CCCRRRAAA,CCCRRRAAAALIAS);

* ACRONYMS:
* ACRONYMS for technology types
* Each of the following ACRONYMS symbolise a technology type,
* They correspond in a one-to-one way with the internal sets IGCND, IGBRP etc. below.
* They should generally not be changed.
* New technology types may be added only if also code specifying their properties are added.
ACRONYMS   GCND, GBPR, GEXT, GHOB, GETOH, GHSTO, GESTO, GHYRS, GHYRR, GWND, GSOLE, GSOLH, GWAVE, GHSTO_S, GH2STO, GETOH2, GCH4TOH2, GH2FUEL;

* ACRONYMS for user defined fuels will be given in a $included file FFFACRONYMS.inc.


*-------------------------------------------------------------------------------
*----- Definitions of SETS and ACRONYMS that are given in the $included files:
*-------------------------------------------------------------------------------
* Printing of data to the list file controlled by %ONOFFDATALISTING% and %ONOFFCODELISTING%
* (see file balgams.opt):


%ONOFFDATALISTING%

SET CCCRRRAAA          'All geographical entities (CCC + RRR + AAA)' %semislash%
$if     EXIST '../data/CCCRRRAAA.inc' $INCLUDE      '../data/CCCRRRAAA.inc';
$if not EXIST '../data/CCCRRRAAA.inc' $INCLUDE '../../base/data/CCCRRRAAA.inc';
%semislash%;

SET CCC(CCCRRRAAA)       'All Countries' %semislash%
$if     EXIST '../data/CCC.inc' $INCLUDE      '../data/CCC.inc';
$if not EXIST '../data/CCC.inc' $INCLUDE '../../base/data/CCC.inc';
%semislash%;

SET RRR(CCCRRRAAA)       'All regions' %semislash%
$if     EXIST '../data/RRR.inc' $INCLUDE      '../data/RRR.inc';
$if not EXIST '../data/RRR.inc' $INCLUDE '../../base/data/RRR.inc';
%semislash%;

SET AAA(CCCRRRAAA)       'All areas' %semislash%
$if     EXIST '../data/AAA.inc' $INCLUDE      '../data/AAA.inc';
$if not EXIST '../data/AAA.inc' $INCLUDE '../../base/data/AAA.inc';
%semislash%;

SET CCCRRR(CCC,RRR)      'Regions in countries' %semislash%
$if     EXIST '../data/CCCRRR.inc' $INCLUDE      '../data/CCCRRR.inc';
$if not EXIST '../data/CCCRRR.inc' $INCLUDE '../../base/data/CCCRRR.inc';
%semislash%;

SET RRRAAA(RRR,AAA)      'Areas in regions' %semislash%
$if     EXIST '../data/RRRAAA.inc' $INCLUDE      '../data/RRRAAA.inc';
$if not EXIST '../data/RRRAAA.inc' $INCLUDE '../../base/data/RRRAAA.inc';
%semislash%;

SET CCCAAA(CCC,AAA);
CCCAAA(CCC,AAA)=YES$(SUM(RRR$ (RRRAAA(RRR,AAA) AND CCCRRR(CCC,RRR)),1) GT 0);


SET YYY                'All years'  %semislash%
$if     EXIST '../data/YYY.inc' $INCLUDE      '../data/YYY.inc';
$if not EXIST '../data/YYY.inc' $INCLUDE '../../base/data/YYY.inc';
%semislash%;

SET SSS                'All seasons' %semislash%
$if     EXIST '../data/SSS.inc' $INCLUDE      '../data/SSS.inc';
$if not EXIST '../data/SSS.inc' $INCLUDE '../../base/data/SSS.inc';
%semislash%;

SET TTT                'All time periods' %semislash%
$if     EXIST '../data/TTT.inc' $INCLUDE      '../data/TTT.inc';
$if not EXIST '../data/TTT.inc' $INCLUDE '../../base/data/TTT.inc';
%semislash%;

SET GGG                'All generation technologies'   %semislash%
$if     EXIST '../data/GGG.inc' $INCLUDE      '../data/GGG.inc';
$if not EXIST '../data/GGG.inc' $INCLUDE '../../base/data/GGG.inc';
%semislash%;

SET GDATASET           'Generation technology data' %semislash%
$if     EXIST '../data/GDATASET.inc' $INCLUDE      '../data/GDATASET.inc';
$if not EXIST '../data/GDATASET.inc' $INCLUDE '../../base/data/GDATASET.inc';
%semislash%;

SET FFF                'Fuels'  %semislash%
$if     EXIST '../data/FFF.inc' $INCLUDE      '../data/FFF.inc';
$if not EXIST '../data/FFF.inc' $INCLUDE '../../base/data/FFF.inc';
%semislash%;

* ACRONYMS for fuels:
$ifi %semislash%=="/" ACRONYMS
$if     EXIST '../data/FFFACRONYM.inc' $INCLUDE      '../data/FFFACRONYM.inc';
$if not EXIST '../data/FFFACRONYM.inc' $INCLUDE '../../base/data/FFFACRONYM.inc';
;

SET FDATASET           'Characteristics of fuels ' %semislash%
$if     EXIST '../data/FDATASET.inc' $INCLUDE      '../data/FDATASET.inc';
$if not EXIST '../data/FDATASET.inc' $INCLUDE '../../base/data/FDATASET.inc';
%semislash%;

SET HYRSDATASET  'Characteristics of hydro reservoirs' %semislash%
$if     EXIST '../data/HYRSDATASET.inc' $INCLUDE      '../data/HYRSDATASET.inc';
$if not EXIST '../data/HYRSDATASET.inc' $INCLUDE '../../base/data/HYRSDATASET.inc';
%semislash%;

SET DF_QP  'Quantity and price information for elastic demands' %semislash%
$if     EXIST '../data/DF_QP.inc' $INCLUDE      '../data/DF_QP.inc';
$if not EXIST '../data/DF_QP.inc' $INCLUDE '../../base/data/DF_QP.inc';
%semislash%;

SET DEF  'Steps in elastic electricity demand'  %semislash%
$if     EXIST '../data/DEF.inc' $INCLUDE      '../data/DEF.inc';
$if not EXIST '../data/DEF.inc' $INCLUDE '../../base/data/DEF.inc';
%semislash%;

SET DEF_D1(DEF)   'Downwards steps in elastic el. demand, relative data format' %semislash%
$if     EXIST '../data/DEF_D1.inc' $INCLUDE      '../data/DEF_D1.inc';
$if not EXIST '../data/DEF_D1.inc' $INCLUDE '../../base/data/DEF_D1.inc';
%semislash%;

SET DEF_U1(DEF)   'Upwards steps in elastic el. demand, relative data format' %semislash%
$if     EXIST '../data/DEF_U1.inc' $INCLUDE      '../data/DEF_U1.inc';
$if not EXIST '../data/DEF_U1.inc' $INCLUDE '../../base/data/DEF_U1.inc';
%semislash%;

SET DEF_D2(DEF)   'Downwards steps in elastic el. demand, absolute Money and MW-incremental data format'%semislash%
$if     EXIST '../data/DEF_D2.inc' $INCLUDE      '../data/DEF_D2.inc';
$if not EXIST '../data/DEF_D2.inc' $INCLUDE '../../base/data/DEF_D2.inc';
%semislash%;

SET DEF_U2(DEF)   'Upwards steps in elastic el. demand, absolute Money and MW-incremental data format' %semislash%
$if     EXIST '../data/DEF_U2.inc' $INCLUDE      '../data/DEF_U2.inc';
$if not EXIST '../data/DEF_U2.inc' $INCLUDE '../../base/data/DEF_U2.inc';
%semislash%;

SET DEF_D3(DEF)   'Downwards steps in elastic el. demand, absolute Money and fraction of nominal demand data format' %semislash%
$if     EXIST '../data/DEF_D3.inc' $INCLUDE      '../data/DEF_D3.inc';
$if not EXIST '../data/DEF_D3.inc' $INCLUDE '../../base/data/DEF_D3.inc';
%semislash%;

SET DEF_U3(DEF)   'Upwards steps in elastic el. demand, absolute Money and fraction of nominal demand data format' %semislash%
$if     EXIST '../data/DEF_U3.inc' $INCLUDE      '../data/DEF_U3.inc';
$if not EXIST '../data/DEF_U3.inc' $INCLUDE '../../base/data/DEF_U3.inc';
%semislash%;

SET DHF  'Steps in elastic heat demand' %semislash%
$if     EXIST '../data/DHF.inc' $INCLUDE      '../data/DHF.inc';
$if not EXIST '../data/DHF.inc' $INCLUDE '../../base/data/DHF.inc';
%semislash%;

SET DHF_D1(DHF)    'Downwards steps in elastic heat demand, relative data format' %semislash%
$if     EXIST '../data/DHF_D1.inc' $INCLUDE      '../data/DHF_D1.inc';
$if not EXIST '../data/DHF_D1.inc' $INCLUDE '../../base/data/DHF_D1.inc';
%semislash%;

SET DHF_U1(DHF)    'Upwards steps in elastic heat demand, relative data format' %semislash%
$if     EXIST '../data/DHF_U1.inc' $INCLUDE      '../data/DHF_U1.inc';
$if not EXIST '../data/DHF_U1.inc' $INCLUDE '../../base/data/DHF_U1.inc';
%semislash%;

SET DHF_D2(DHF)    'Downwards steps in elastic heat demand, absolute Money and MW-incremental data format' %semislash%
$if     EXIST '../data/DHF_D2.inc' $INCLUDE      '../data/DHF_D2.inc';
$if not EXIST '../data/DHF_D2.inc' $INCLUDE '../../base/data/DHF_D2.inc';
%semislash%;

SET DHF_U2(DHF)    'Upwards steps in elastic heat demand, absolute Money and MW-incremental data format' %semislash%
$if     EXIST '../data/DHF_U2.inc' $INCLUDE      '../data/DHF_U2.inc';
$if not EXIST '../data/DHF_U2.inc' $INCLUDE '../../base/data/DHF_U2.inc';
%semislash%;

SET DHF_D3(DHF)    'Downwards steps in elastic heat demand, absolute Money and fraction of nominal demand data format' %semislash%
$if     EXIST '../data/DHF_D3.inc' $INCLUDE      '../data/DHF_D3.inc';
$if not EXIST '../data/DHF_D3.inc' $INCLUDE '../../base/data/DHF_D3.inc';
%semislash%;

SET DHF_U3(DHF)    'Upwards steps in elastic heat demand, absolute Money and fraction of nominal demand data format' %semislash%
$if     EXIST '../data/DHF_U3.inc' $INCLUDE      '../data/DHF_U3.inc';
$if not EXIST '../data/DHF_U3.inc' $INCLUDE '../../base/data/DHF_U3.inc';
%semislash%;

SET MPOLSET  'Emission and other policy data'   %semislash%
$if     EXIST '../data/MPOLSET.inc' $INCLUDE      '../data/MPOLSET.inc';
$if not EXIST '../data/MPOLSET.inc' $INCLUDE '../../base/data/MPOLSET.inc';
%semislash%;

SET C(CCC)    'Countries in the simulation'  %semislash%
$if     EXIST '../data/C.inc' $INCLUDE      '../data/C.inc';
$if not EXIST '../data/C.inc' $INCLUDE '../../base/data/C.inc';
%semislash%;

SET G(GGG)    'Generation technologies in the simulation'  %semislash%
$if     EXIST '../data/G.inc' $INCLUDE      '../data/G.inc';
$if not EXIST '../data/G.inc' $INCLUDE '../../base/data/G.inc';
%semislash%;

SET AGKN(AAA,GGG)   'Areas for possible location of new technologies'   %semislash%
$if     EXIST '../data/AGKN.inc' $INCLUDE      '../data/AGKN.inc';
$if not EXIST '../data/AGKN.inc' $INCLUDE '../../base/data/AGKN.inc';
%semislash%;

SET Y(YYY)    'Years in the simulation'   %semislash%
$if     EXIST '../data/Y.inc' $INCLUDE      '../data/Y.inc';
$if not EXIST '../data/Y.inc' $INCLUDE '../../base/data/Y.inc';
%semislash%;

SET S(SSS)    'Seasons in the simulation'   %semislash%
$if     EXIST '../data/S.inc' $INCLUDE      '../data/S.inc';
$if not EXIST '../data/S.inc' $INCLUDE '../../base/data/S.inc';
%semislash%;

SET T(TTT)    'Time periods within the season in the simulation'  %semislash%
$if     EXIST '../data/T.inc' $INCLUDE         '../data/T.inc';
$if not EXIST '../data/T.inc' $INCLUDE '../../base/data/T.inc';
%semislash%;

*SET DAYTYPE       'Types of days within the week, typically workdays/weekend' %semislash%
*$if     EXIST '../data/DAYTYPE.inc' $INCLUDE         '../data/DAYTYPE.inc';
*$if not EXIST '../data/DAYTYPE.inc' $INCLUDE '../../base/data/DAYTYPE.inc';
*%semislash%

SET TWORKDAY(TTT)   'Time segments, T, in workdays'  %semislash%
$if     EXIST '../data/TWORKDAY.inc' $INCLUDE         '../data/TWORKDAY.inc';
$if not EXIST '../data/TWORKDAY.inc' $INCLUDE '../../base/data/TWORKDAY.inc';
%semislash%

SET TWEEKEND(TTT)   'Time segments, T, in weekends'   %semislash%
$if     EXIST '../data/TWEEKEND.inc' $INCLUDE         '../data/TWEEKEND.inc';
$if not EXIST '../data/TWEEKEND.inc' $INCLUDE '../../base/data/TWEEKEND.inc';
%semislash%

/*
SET ECONSET         'Set for econ'  %semislash%
$if     EXIST '../data/ECONSET.inc' $INCLUDE         '../data/ECONSET.inc';
$if not EXIST '../data/ECONSET.inc' $INCLUDE '../../base/data/ECONSET.inc';
%semislash%

SET ECONSETCATEGORY 'Set for econ'  %semislash%
$if     EXIST '../data/ECONSETCATEGORY.inc' $INCLUDE         '../data/ECONSETCATEGORY.inc';
$if not EXIST '../data/ECONSETCATEGORY.inc' $INCLUDE '../../base/data/ECONSETCATEGORY.inc';
%semislash%
*/

$ifi not %X3V%==yes $goto X3V_label1

$if     EXIST '../data/X3VPLACE0.inc' $INCLUDE      '../data/X3VPLACE0.inc';
$if not EXIST '../data/X3VPLACE0.inc' $INCLUDE      '../../base/data/X3VPLACE0.inc';

$if     EXIST '../data/X3VPLACE.inc' $INCLUDE      '../data/X3VPLACE.inc';
$if not EXIST '../data/X3VPLACE.inc' $INCLUDE      '../../base/data/X3VPLACE.inc';

$if     EXIST '../data/X3VSTEP0.inc' $INCLUDE      '../data/X3VSTEP0.inc';
$if not EXIST '../data/X3VSTEP0.inc' $INCLUDE      '../../base/data/X3VSTEP0.inc';

$if     EXIST '../data/X3VSTEP.inc' $INCLUDE      '../data/X3VSTEP.inc';
$if not EXIST '../data/X3VSTEP.inc' $INCLUDE      '../../base/data/X3VSTEP.inc';

$if     EXIST '../data/X3VX.inc' $INCLUDE      '../data/X3VX.inc';
$if not EXIST '../data/X3VX.inc' $INCLUDE      '../../base/data/X3VX.inc';
*$ifi %X3V%==yes   $INCLUDE '../../base/addons/X3V/data/X3VSETS.inc';
$label X3V_label1


* Printing of data to the list file controlled by %ONOFFDATALISTING% and %ONOFFCODELISTING%
* (see file balgams.opt):
%ONOFFCODELISTING%

*------------------------------------------------------------------------------
* Declaration of internal sets:
*------------------------------------------------------------------------------
* Internal sets and aliases in relation to geography.
* Note: although it is possible, you should generally not (i.e., don't!) use
* the following internal sets for data entry or data assignments
* (it may be impossible in future versions).
SET IR(RRR)            'Regions in the simulation';
SET IA(AAA)            'Areas in the simulation';
SET ICA(CCC,AAA)       'Assignment of areas to countries in the simulation';
* The simple way to limit the geographical scope of the model is to specify in the input data
* the set C as a proper subset of the set CCC,
* because the following mechanism will automatically exclude all regions
* and areas not in any country in C.
ICA(CCC,AAA) = YES$(SUM(RRR$ (RRRAAA(RRR,AAA) AND CCCRRR(CCC,RRR)),1) GT 0);
IR(RRR) = YES$(SUM(C,CCCRRR(C,RRR)));
IA(AAA) = YES$(SUM(C,ICA(C,AAA)));

SET IS3(S)   'Present season simulated in balbase3';


*-------------------------------------------------------------------------------
*--- End: Definitions of SETS and ACRONYMS that are given in the $included files
*-------------------------------------------------------------------------------

*-------------------------------------------------------------------------------
*----- Definitions of ALIASES that are needed for data entry:
*-------------------------------------------------------------------------------

* Duplication of sets for describing electricity exchange between regions:
ALIAS (RRR,IRRRE,IRRRI);
ALIAS (IR,IRE,IRI);
* Duplication of sets for describing heat exchange between areas: /*varmetrans*/
ALIAS (AAA,IAAAE,IAAAI);
ALIAS (IA,IAE,IAI);
* Duplication of sets for fuel.
ALIAS (FFF,IFFFALIAS);

ALIAS(S,IS3LOOPSET);

* Duplication of sets for seasons and hours:
ALIAS (Y,IYALIAS);
ALIAS (S,ISALIAS);
ALIAS (T,ITALIAS);


*-------------------------------------------------------------------------------
*----- End: Definitions of ALIASES that are needed for data entry
*-------------------------------------------------------------------------------

*-------------------------------------------------------------------------------
*----- Any declarations and definitions of sets, aliases and acronyms for addon:
*-------------------------------------------------------------------------------


$ifi %COMBTECH%==yes
$if     EXIST '../data/ggcomb.inc' $INCLUDE         '../data/ggcomb.inc';
$ifi %COMBTECH%==yes
$if not EXIST '../data/ggcomb.inc' $INCLUDE '../../base/data/ggcomb.inc';

*** Davide: The following geographical sets are included in the model whether
*   the relative statement is 'YES' in balopt.opt

SET AIND(AAA)  'Individual heating areas' %semislash%
$if     EXIST '../data/AIND.inc' $INCLUDE      '../data/AIND.inc';
$if not EXIST '../data/AIND.inc' $INCLUDE '../../base/data/AIND.inc';
%semislash%;


SET AAAOFFSHORE(AAA) 'Areas with offshore wind' %semislash%
$if     EXIST '../data/AAAOFFSHORE.inc' $INCLUDE      '../data/AAAOFFSHORE.inc';
$if not EXIST '../data/AAAOFFSHORE.inc' $INCLUDE '../../base/data/AAAOFFSHORE.inc';
%semislash%;


SET ENSOFFSHORE(AAA)  'Offshore wind areas based on ENS' %semislash%
$if     EXIST '../data/ENSOFFSHORE.inc' $INCLUDE      '../data/ENSOFFSHORE.inc';
$if not EXIST '../data/ENSOFFSHORE.inc' $INCLUDE '../../base/data/ENSOFFSHORE.inc';
%semislash%;


SET RETRANS(RRR)  'Electric transport regions' %semislash%
$if     EXIST '../data/RETRANS.inc' $INCLUDE      '../data/RETRANS.inc';
$if not EXIST '../data/RETRANS.inc' $INCLUDE '../../base/data/RETRANS.inc';
%semislash%;


SET ABF(AAA)  'Biofuel areas' %semislash%
$if     EXIST '../data/ABF.inc' $INCLUDE      '../data/ABF.inc';
$if not EXIST '../data/ABF.inc' $INCLUDE '../../base/data/ABF.inc';
%semislash%;


SET RBF(RRR)  'Biofuel areas' %semislash%
$if     EXIST '../data/RBF.inc' $INCLUDE      '../data/RBF.inc';
$if not EXIST '../data/RBF.inc' $INCLUDE '../../base/data/RBF.inc';
%semislash%;

* Do not include the below regions and areas, (AIND,RETRANS,RBF,ABF) if is not 'YES' in balopt.opt

$ifi not %FV%==yes IA(AIND)              =no;
$ifi not %ETrans%==yes IR(RETRANS)       =no;
$ifi not %Biofuel%==yes IR(RBF)          =no;
$ifi not %Biofuel%==yes IA(ABF)          =no;



* Addon AGKNDISC: discrete size investment in technologies:
$ifi not %AGKNDISC%==yes  $goto label_AGKNDISC1

SET AGKNDISCAG(AAA,GGG)   'Areas for possible location of discrete capacity investments in technologies' %semislash%
$if     EXIST '../data/AGKNDISCAG.inc' $INCLUDE         '../data/AGKNDISCAG.inc';
$if not EXIST '../data/AGKNDISCAG.inc' $INCLUDE '../../base/data/AGKNDISCAG.inc';
%semislash%

SET AGKNDISCGSIZESET    'Set of possible sizes for discrete capacity investments in technologies' %semislash%
$if     EXIST '../data/AGKNDISCGSIZESET.inc' $INCLUDE         '../data/AGKNDISCGSIZESET.inc';
$if not EXIST '../data/AGKNDISCGSIZESET.inc' $INCLUDE '../../base/data/AGKNDISCGSIZESET.inc';
%semislash%

SET AGKNDISCGDATASET    'Technology investment data types for discrete capacity size investments' %semislash%
$if     EXIST '../data/AGKNDISCGDATASET.inc' $INCLUDE         '../data/AGKNDISCGDATASET.inc';
$if not EXIST '../data/AGKNDISCGDATASET.inc' $INCLUDE '../../base/data/AGKNDISCGDATASET.inc';
%semislash%


PARAMETER AGKNDISCGDATA(GGG,AGKNDISCGSIZESET,AGKNDISCGDATASET) 'Technology investment data for discrete capacity size investments' %semislash%
$if     EXIST '../data/AGKNDISCGDATA.inc' $INCLUDE         '../data/AGKNDISCGDATA.inc';
$if not EXIST '../data/AGKNDISCGDATA.inc' $INCLUDE '../../base/data/AGKNDISCGDATA.inc';
%semislash%

$label label_AGKNDISC1
* End: Addon AGKNDISC: discrete size investment in technologies.


$ifi not %HYRSBB123%==none    $include "..\..\base\addons\hyrsbb123\hyrsbb123internals.inc";

*-------------------------------------------------------------------------------
* End: Any declarations and definitions of sets, aliases and acronyms for addon
*-------------------------------------------------------------------------------

*Raffaele

Parameter INSCAPWINDON(YYY,C)'Maximum WIND installed capacity in every country in every year' %semislash%
$if     EXIST '../data/INSCAPWINDON.inc' $INCLUDE         '../data/INSCAPWINDON.inc';
$if not EXIST '../data/INSCAPWINDON.inc' $INCLUDE '../../base/data/INSCAPWINDON.inc';
%semislash%

Parameter INSCAPWINDON_Y(C) 'Maximum wind installed capacity for the year of simulation' %semislash%
INSCAPWINDON_Y(C)=SMAX(Y,INSCAPWINDON(Y,C));
%semislash%

Parameter INSCAPWINDOFF(YYY,C)'Maximum WIND installed capacity in every country in every year' %semislash%
$if     EXIST '../data/INSCAPWINDOFF.inc' $INCLUDE         '../data/INSCAPWINDOFF.inc';
$if not EXIST '../data/INSCAPWINDOFF.inc' $INCLUDE '../../base/data/INSCAPWINDOFF.inc';
%semislash%

Parameter INSCAPWINDOFF_Y(C) 'Maximum wind installed capacity for the year of simulation' %semislash%
INSCAPWINDOFF_Y(C)=SMAX(Y,INSCAPWINDOFF(Y,C));
%semislash%

Parameter INSCAPSOLE(YYY,C)'Maximum solar PV installed capacity in every country in every year' %semislash%
$if     EXIST '../data/INSCAPSOLE.inc' $INCLUDE         '../data/INSCAPSOLE.inc';
$if not EXIST '../data/INSCAPSOLE.inc' $INCLUDE '../../base/data/INSCAPSOLE.inc';
%semislash%

Parameter INSCAPSOLE_Y(C) 'Maximum solar PV installed capacity for the year of simulation' %semislash%
INSCAPSOLE_Y(C)=SMAX(Y,INSCAPSOLE(Y,C));
%semislash%

Parameter INSCAPSOLH(YYY,C)'Maximum solar thermal installed capacity in every country in every year' %semislash%
$if     EXIST '../data/INSCAPSOLH.inc' $INCLUDE         '../data/INSCAPSOLH.inc';
$if not EXIST '../data/INSCAPSOLH.inc' $INCLUDE '../../base/data/INSCAPSOLH.inc';
%semislash%

Parameter INSCAPSOLH_Y(C) 'Maximum solar thermal installed capacity for the year of simulation' %semislash%
INSCAPSOLH_Y(C)=SMAX(Y,INSCAPSOLH(Y,C));
%semislash%

*Raffaele Note that the heat pumps (following code) are not anymore constrained in terms of capacity, while the electric fuel consumption was fixed instead.
$ontext
Parameter INSCAPHP(YYY,C)'Maximum heat pumps installed capacity in every country in every year' %semislash%
$if     EXIST '../data/INSCAPHP.inc' $INCLUDE         '../data/INSCAPHP.inc';
$if not EXIST '../data/INSCAPHP.inc' $INCLUDE '../../base/data/INSCAPHP.inc';
%semislash%

Parameter INSCAPHP_Y(C) 'Maximum heat pumps installed capacity for the year of simulation' %semislash%
INSCAPHP_Y(C)=SMAX(Y,INSCAPHP(Y,C));
%semislash%
$offtext

*-------------------------------------------------------------------------------
* The choice of model made in the balopt.opt fil is checked here,
* and basic sets concerning time within the year are reconstructed accordingly if needed:

$ifi %bb3%==yes $KILL T
$ifi %bb3%==yes SET T(TTT)   /T001*T168/;
$ifi %bb3%==yes $KILL TWORKDAY
$ifi %bb3%==yes $KILL TWEEKEND
$ifi %bb3%==yes SET TWORKDAY(TTT)   /T001*T120/;
$ifi %bb3%==yes SET TWEEKEND(TTT)   /T121*T168/;
*-------------------------------------------------------------------------------




*-------------------------------------------------------------------------------
*----- Any internal sets for addon to be placed here: --------------------------


$ifi %FV%==yes $include '../../base/addons/Fjernvarme/sets_fv.inc';



*----- End: Any internal sets for addon to be placed here: ---------------------
*-------------------------------------------------------------------------------



*------------------------------------------------------------------------------
* End: Declaration of internal sets
*------------------------------------------------------------------------------



*------------------------------------------------------------------------------
* Declaration and definition of numerical data: PARAMETERS and SCALARS:
*------------------------------------------------------------------------------


*------------------------------------------------------------------------------
*---- Technology data: ---------------------------------------------------------
*------------------------------------------------------------------------------
PARAMETER GDATA(GGG,GDATASET)    'Technologies characteristics' %semislash%
* Printing of data to the list file controlled by %ONOFFDATALISTING% and %ONOFFCODELISTING%:
%ONOFFDATALISTING%
$if     EXIST '../data/gdata.inc' $INCLUDE         '../data/gdata.inc';
$if not EXIST '../data/gdata.inc' $INCLUDE '../../base/data/gdata.inc';
%ONOFFCODELISTING%


*-------------------------------------------------------------------------------
* Definitions of internal sets relative to technologies,

* The following are convenient internal subsets of generation technologies:

SET IGCND(G)               'Condensing technologies';
SET IGBPR(G)               'Back pressure technologies';
SET IGEXT(G)               'Extraction technologies';
SET IGHOB(G)               'Heat-only boilers';
SET IGETOH(G)              'Electric heaters, heatpumps, electrolysis plants';
SET IGHSTO(G)              'Heat seasonal storage technologies';
SET IGHSTO_S(G)            'Long-time heat seasonal storage technologies';
SET IGHSTOALL(G)           'All heat seasonal storage technologies';
SET IGESTO(G)              'Electricity seasonal storage technologies';
SET IGHYRS(G)              'Hydropower with reservoir';
SET IGHYRR(G)              'Hydropower run-of-river no reservoir';
SET IGWND(G)               'Wind power technologies';
SET IGSOLE(G)              'Solar electrical power technologies';
SET IGSOLH(G)              'Solar heat technologies';
SET IGWAVE(G)              'Wave power technologies';
SET IGHH(G)                'Technologies generating heat-only';
SET IGHHNOSTO(G)           'Technologies generating heat-only, except storage';
SET IGNOTETOH(G)           'Technologies excluding electric heaters, heat pumps,electrolysis plants';
SET IGKH(G)                'Technologies with capacity specified on heat';
SET IGKHNOSTO(G)           'Technologies with capacity specified on heat, except storage';
SET IGKE(G)                'Technologies with capacity specified on electricity';
SET IGKENOSTO(G)           'Technologies with capacity specified on electricity, except storage';
SET IGDISPATCH(G)          'Dispatchable technologies';
SET IGEE(G)                'Technologies generating electricity only';
SET IGEENOSTO(G)           'Technologies generating electricity only, except storage';
SET IGKKNOWN(G)            'Technoloies for which the capacity is specified by the user';
SET IGKFIND(G)             'Technologies for which the capacity is found by algorithm';
SET IGEH(G)                'Technologies generating electricity and heat';
SET IGE(G)                 'Technologies generating electricity';
SET IGH(G)                 'Technologies generating heat';

$ifi %H2%==yes $include '../../base/addons/hydrogen/H2g.inc';


* The following sets concern combination technologies.

* The following describes the main applications for the subsets:
*
* The sets:
* IGCND,IGBPR,IGEXT,IGHOB,IGETOH,IGHSTO,IGESTO,IGHYRS,IGHYRR,IGWND,IGSOLE,IGSOLH, IGWAVE
* are directly extracted from GDATA(G,'GDTYPE') and are used to reference
* technology types.
*
* IGHH and IGEE are used in the balance equations and for output control.
*
* IGHHNOSTO and IGEENOSTO is not currently used for anything.
*
* IGNOTETOH is used for O&M cost in the objective function, in the balance
* equation, in heat storage balance equations and in various output functions.
*
* IGKH and IGKE are used for capacity restrictions and for certain output files.
*
* IGKHNOSTO and IGKENOSTO are used for output.
*
* IGDISPATCH is used capacity restrictions and output.
*
* IGKKNOWN is used to exclude investment options.
*
* IGKFIND is used to single out investment options.
*
* IGEH is used for CHP specific taxes.
*
* IGE and IGH are used all over.


* The sets are defined based on information in PARAMETER GDATA.


* The specifications of technology type 'GDTYPE' in GDATA may use an acronym or a number.
* Under most circumstances an acronym is preferable, the list is given above.
* If an acroym can not be used, use the numbers consistently as seen below.

   IGCND(G)    = YES$((GDATA(G,'GDTYPE') EQ 1)  OR (GDATA(G,'GDTYPE') EQ GCND));
   IGBPR(G)    = YES$((GDATA(G,'GDTYPE') EQ 2)  OR (GDATA(G,'GDTYPE') EQ GBPR));
   IGEXT(G)    = YES$((GDATA(G,'GDTYPE') EQ 3)  OR (GDATA(G,'GDTYPE') EQ GEXT));
   IGHOB(G)    = YES$((GDATA(G,'GDTYPE') EQ 4)  OR (GDATA(G,'GDTYPE') EQ GHOB));
   IGETOH(G)   = YES$((GDATA(G,'GDTYPE') EQ 5)  OR (GDATA(G,'GDTYPE') EQ GETOH));
   IGHSTO(G)   = YES$((GDATA(G,'GDTYPE') EQ 6)  OR (GDATA(G,'GDTYPE') EQ GHSTO));
   IGESTO(G)   = YES$((GDATA(G,'GDTYPE') EQ 7)  OR (GDATA(G,'GDTYPE') EQ GESTO));
   IGHYRS(G)   = YES$((GDATA(G,'GDTYPE') EQ 8)  OR (GDATA(G,'GDTYPE') EQ GHYRS));
   IGHYRR(G)   = YES$((GDATA(G,'GDTYPE') EQ 9)  OR (GDATA(G,'GDTYPE') EQ GHYRR));
   IGWND(G)    = YES$((GDATA(G,'GDTYPE') EQ 10) OR (GDATA(G,'GDTYPE') EQ GWND));
   IGSOLE(G)   = YES$((GDATA(G,'GDTYPE') EQ 11) OR (GDATA(G,'GDTYPE') EQ GSOLE));
   IGSOLH(G)   = YES$((GDATA(G,'GDTYPE') EQ 12) OR (GDATA(G,'GDTYPE') EQ GSOLH));
   IGWAVE(G)   = YES$((GDATA(G,'GDTYPE') EQ 22) OR (GDATA(G,'GDTYPE') EQ GWAVE));
   IGHSTO_S(G) = YES$((GDATA(G,'GDTYPE') EQ 23) OR (GDATA(G,'GDTYPE') EQ GHSTO_S));

   IGHHNOSTO(G) = NO;   IGHHNOSTO(IGHOB)   = YES;  IGHHNOSTO(IGSOLH)= YES;

   IGHSTOALL(G) =       +IGHSTO(G)
                        +IGHSTO_S(G);

   IGHH(G)      =       IGHHNOSTO(G)
                       +IGHSTOALL(G);

   IGNOTETOH(G)= NOT IGETOH(G);

   IGDISPATCH(G)    =   IGCND(G)
                         +IGBPR(G)
                         +IGEXT(G)
                         +IGHOB(G)
                         +IGESTO(G)
                         +IGHSTO(G)
                         +IGHSTO_S(G)
                         +IGETOH(G)
                         +IGHYRS(G);

   IGEE(G)          =   IGCND(G)
                         +IGHYRS(G)
                         +IGHYRR(G)
                         +IGWND(G)
                         +IGSOLE(G)
                         +IGWAVE(G)
                         +IGESTO(G);

   IGEENOSTO(G)     =   IGCND(G)
                         +IGHYRS(G)
                         +IGHYRR(G)
                         +IGWND(G)
                         +IGSOLE(G)
                         +IGWAVE(G);

   IGKHNOSTO(G)     =   IGETOH(G)
                         +IGHHNOSTO(G);

   IGKH(G)          =     IGKHNOSTO(G)
                         +IGHSTOALL(G);

   IGKE(G)          =   IGCND(G)
                         +IGBPR(G)
                         +IGEXT(G)
                         +IGESTO(G)
                         +IGHYRS(G)
                         +IGHYRR(G)
                         +IGWND(G)
                         +IGSOLE(G)
                         +IGWAVE(G);

   IGKENOSTO(G)     =   IGKE(G)
                         -IGESTO(G);

   IGKKNOWN(G) = YES$(GDATA(G,'GDKVARIABL') EQ 0);
   IGKFIND(G) = NOT IGKKNOWN(G);

   IGEH(G) = IGBPR(G)+IGEXT(G)+IGETOH(G);
   IGE(G)=IGEE(G)+IGEH(G);
   IGH(G)=IGHH(G)+IGEH(G);


* End: Rev.3.01: The following definitions are not standardised in Balmorel.

*Raffaele

SET IGWNDON(G)    'Onshore wind power technologies' %semislash%
$if     EXIST '../data/IGWNDON.inc' $INCLUDE      '../data/IGWNDON.inc';
$if not EXIST '../data/IGWNDON.inc' $INCLUDE '../../base/data/IGWNDON.inc';
%semislash%;

SET IGWNDOFF(G)    'Offshore wind power technologies' %semislash%
$if     EXIST '../data/IGWNDOFF.inc' $INCLUDE      '../data/IGWNDOFF.inc';
$if not EXIST '../data/IGWNDOFF.inc' $INCLUDE '../../base/data/IGWNDOFF.inc';
%semislash%;

$ontext
SET IGHP(G)    'Heat pumps technologies' %semislash%
$if     EXIST '../data/IGHP.inc' $INCLUDE      '../data/IGHP.inc';
$if not EXIST '../data/IGHP.inc' $INCLUDE '../../base/data/IGHP.inc';
%semislash%;
$offtext

*-------------------------------------------------------------------------------
*----- Any internal sets assignments for addon to be placed here: --------------
*-------------------------------------------------------------------------------
* NOTE: When making new generation technology types. Some add-ons may already
* be using values of GDATA(G,'GDTYPE'). Check addons referenced here to maximize
* consistency and avoid conflicts. It is encouraged to avoid using the '-' set
* operator and instead use the '+' operator for compound assignments.




$ifi %COMBTECH%==yes   SET IGCOMB1(G)              'Combination technologies, primary';
$ifi %COMBTECH%==yes   SET IGCOMB2(G)              'Combination technologies, secondary with primary in G';
$ifi %COMBTECH%==yes   SET GGCOMB(GGG,IGGGALIAS)   'Combination techologies in the same combination';
$ifi %COMBTECH%==yes   IGCOMB1(G) =no;
$ifi %COMBTECH%==yes   IGCOMB2(G) =no;
$ifi %COMBTECH%==yes   $include '../../base/addons/combtech/combGassign.inc';

* These internal parameters pertain to hydrogen technologies.

$ifi %H2%==yes $include '../../base/addons/hydrogen/H2Gassign.inc';
*---- Unit commitment --------------------------------------------------------------

$ifi %UnitComm%==yes $include '../../base/addons/unitcommitment/datadecl.inc';



$ifi %POLICIES%==yes  $ifi exist 'pol_sets.inc' $INCLUDE 'pol_sets.inc';
$ifi %POLICIES%==yes  $ifi not exist 'pol_sets.inc' $INCLUDE '../../base/addons/Policies/pol_sets.inc';

$ifi %SYSTEMCONST%==yes  $ifi exist 'sets_syscon.inc' $INCLUDE 'sets_syscon.inc';
$ifi %SYSTEMCONST%==yes  $ifi not exist 'sets_syscon.inc' $INCLUDE '../../base/addons/SystemConst/sets_syscon.inc';

*-------------------------------------------------------------------------------
*----- End: Any internal sets assignments for addon to be placed here ----------
*-------------------------------------------------------------------------------

table helperDH(yyy,aaa)

               SE_N_Rural       SE_M_Urban       SE_M_Rural       SE_S_Rural       SE_S_Bio         SE_M_Bio         SE_N_Bio
2010             4512371          16429546         8422092          12927550         4161914          4161914          4161914
2011             4488906          16344113         8378296          12860327         4140272          4140272          4140272
2012             4465564          16259124         8334729          12793453         4118742          4118742          4118742
2013             4442344          16174576         8291389          12726927         4097325          4097325          4097325
2014             4419243          16090468         8248274          12660746         4076019          4076019          4076019
2015             4396448          16007470         8205728          12595442         4054994          4054994          4054994
2016             4392887          15994505         8199082          12585239         4051709          4051709          4051709
2017             4389329          15981548         8192440          12575045         4048428          4048428          4048428
2018             4385773          15968604         8185804          12564859         4045148          4045148          4045148
2019             4382221          15955670         8179174          12554681         4041872          4041872          4041872
2020             4378245          15941194         8171754          12543292         4038205          4038205          4038205
2021             4366116          15897032         8149116          12508543         4027018          4027018          4027018
2022             4353987          15852870         8126477          12473794         4015831          4015831          4015831
2023             4341858          15808708         8103839          12439045         4004644          4004644          4004644
2024             4329729          15764546         8081201          12404297         3993457          3993457          3993457
2025             4317599          15720384         8058562          12369548         3982270          3982270          3982270
2026             4305470          15676221         8035924          12334799         3971082          3971082          3971082
2027             4293341          15632059         8013286          12300050         3959895          3959895          3959895
2028             4281212          15587897         7990647          12265301         3948708          3948708          3948708
2029             4269083          15543735         7968009          12230552         3937521          3937521          3937521
2030             4256954          15499573         7945371          12195804         3926334          3926334          3926334
2031             4244825          15455411         7922733          12161055         3915147          3915147          3915147
2032             4232696          15411249         7900094          12126306         3903960          3903960          3903960
2033             4220567          15367087         7877456          12091557         3892773          3892773          3892773
2034             4208437          15322925         7854818          12056808         3881586          3881586          3881586
2035             4196308          15278763         7832179          12022059         3870399          3870399          3870399
2036             4184179          15234601         7809541          11987311         3859212          3859212          3859212
2037             4172050          15190438         7786903          11952562         3848024          3848024          3848024
2038             4159921          15146276         7764264          11917813         3836837          3836837          3836837
2039             4147792          15102114         7741626          11883064         3825650          3825650          3825650
2040             4135663          15057952         7718988          11848315         3814463          3814463          3814463
2041             4123534          15013790         7696349          11813566         3803276          3803276          3803276
2042             4111405          14969628         7673711          11778818         3792089          3792089          3792089
2043             4099276          14925466         7651073          11744069         3780902          3780902          3780902
2044             4087146          14881304         7628434          11709320         3769715          3769715          3769715
2045             4075017          14837142         7605796          11674571         3758528          3758528          3758528
2046             4062888          14792980         7583158          11639822         3747341          3747341          3747341
2047             4050759          14748818         7560520          11605073         3736154          3736154          3736154
2048             4038630          14704655         7537881          11570324         3724966          3724966          3724966
2049             4026501          14660493         7515243          11535576         3713779          3713779          3713779
2050             4014372          14616331         7492605          11500827         3702592          3702592          3702592
;
FILE TESTFILE /TESTFILE_dh.out/;
put   TESTFILE;
LOOP(YYY, LOOP(AAA$helperDH(yyy,aaa),
PUT YYY.TL:6 ".  " AAA.TL:20  helperDH(yyy,aaa):20:0 /;
));
*$GOTO ENDOFMODEL


*-------------------------------------------------------------------------------
*---- End: Technology data -----------------------------------------------------
*-------------------------------------------------------------------------------



*-------------------------------------------------------------------------------
*---- Annually specified generation capacities: --------------------------------
*-------------------------------------------------------------------------------

* Printing of data to the list file controlled by %ONOFFDATALISTING% and %ONOFFCODELISTING%:
%ONOFFDATALISTING%

PARAMETER GKFX(YYY,AAA,GGG)    'Capacity of existing generation technologies (MW)' %semislash%
$if     EXIST '../data/gkfx.inc' $INCLUDE      '../data/gkfx.inc';
$if not EXIST '../data/gkfx.inc' $INCLUDE '../../base/data/gkfx.inc';
%semislash%;



* Printing of data to the list file controlled by %ONOFFDATALISTING% and %ONOFFCODELISTING%:
%ONOFFCODELISTING%

*-------------------------------------------------------------------------------
*---- End: Annually specified generation capacities ----------------------------
*-------------------------------------------------------------------------------

SET IAGK(AAA,G) 'Technologies with exogenous capacity in a simulation year';
* Add all technologies with positive generation capacity within a simulation year.
IAGK(IA,G)$(SUM(Y,GKFX(Y,IA,G)))=yes;
$ifi %COMBTECH%==yes IAGK(IA,IGCOMB2)$SUM(IGCOMB1$(IAGK(IA,IGCOMB1) and GGCOMB(IGCOMB1,IGCOMB2)),1)=yes;



*-------------------------------------------------------------------------------
*---- Geographically specific values: -----------------------------------------
*-------------------------------------------------------------------------------




PARAMETER YVALUE(YYY)                  'Numerical value of the years labels';
PARAMETER FDATA(FFF,FDATASET)          'Fuel specific values';

PARAMETER FMAXINVEST(CCC,FFF)          'Maximum investment (MW) by fuel type for each year simulated';
PARAMETER GROWTHCAP(C,GGG)             'Maximum model generated capacity increase (MW) from one year to the next';

PARAMETER DISLOSS_E(RRR)               'Loss in electricity distribution (fraction)';
PARAMETER DISLOSS_H(AAA)               'Loss in heat distribution (fraction)';
PARAMETER DISCOST_E(RRR)               'Cost of electricity distribution (Money/MWh)';
PARAMETER DISCOST_H(AAA)               'Cost of heat distribution (Money/MWh)';
PARAMETER FKPOT(CCCRRRAAA,FFF)         'Fuel potential restriction by geography (MW)';
PARAMETER FGEMIN(CCCRRRAAA,FFF)        'Minimum electricity generation by fuel (MWh)';
PARAMETER FGEMAX(CCCRRRAAA,FFF)        'Maximum electricity generation by fuel (MWh)';
$ifi %GMINF_DOL%==CCCRRRAAA_FFF         PARAMETER GMINF(CCCRRRAAA,FFF)             'Minimum fuel use (GJ) per year';
$ifi %GMINF_DOL%==YYY_CCCRRRAAA_FFF     PARAMETER GMINF(YYY,CCCRRRAAA,FFF)         'Minimum fuel use (GJ) per year';
$ifi %GMAXF_DOL%==CCCRRRAAA_FFF         PARAMETER GMAXF(CCCRRRAAA,FFF)             'Maximum fuel use (GJ) per year';
$ifi %GMAXF_DOL%==YYY_CCCRRRAAA_FFF     PARAMETER GMAXF(YYY,CCCRRRAAA,FFF)         'Maximum fuel use (GJ) per year';
$ifi %GEQF_DOL%== CCCRRRAAA_FFF         PARAMETER GEQF(CCCRRRAAA,FFF)              'Required fuel use (GJ) per year';
$ifi %GEQF_DOL%== YYY_CCCRRRAAA_FFF     PARAMETER GEQF(YYY,CCCRRRAAA,FFF)          'Required fuel use (GJ) per year';
$ifi %GEQF_DOL%== CCCRRRAAA_FFF         PARAMETER GEQF_FLEX(CCCRRRAAA,FFF)         'Required fuel use (GJ) per week - flexibility of the required fuel use per year';
$ifi %GEQF_DOL%== YYY_CCCRRRAAA_FFF     PARAMETER GEQF_FLEX(YYY,CCCRRRAAA,FFF)     'Required fuel use (GJ) per week - flexibility of the required fuel use per year';

PARAMETER WTRRSFLH(AAA)                'Full load hours for hydro reservoir plants (hours)';
PARAMETER WTRRRFLH(AAA)                'Full load hours for hydro run-of-river plants (hours)';
PARAMETER WNDFLH(AAA)                  'Full load hours for wind power (hours)';
PARAMETER SOLEFLH(AAA)                 'Full load hours for solarE power (hours)';
PARAMETER SOLHFLH(AAA)                 'Full load hours for solarH power (hours)';
PARAMETER WAVEFLH(AAA)                 'Full load hours for wave power'
PARAMETER HYRSDATA(AAA,HYRSDATASET,SSS)'Data for hydro with storage';
PARAMETER TAX_FHO(YYY,AAA,G)           'Fuel taxes on heat-only units';
PARAMETER TAX_FHO_C(FFF,CCC)           'Fuel taxes on heat-only units';
PARAMETER TAX_GH(YYY,AAA,G)            'Heat taxes on generation units';
PARAMETER TAX_FCHP_C(FFF,CCC)          'Fuel taxes on CHP units';
PARAMETER TAX_HHO_C(FFF,CCC)           'Fuel taxes on HO units';
PARAMETER TAX_F(FFF,CCC)               'Fuel taxes for heat and electricity production (Money/GJ)';
PARAMETER TAX_DE(CCC)                  'Consumers tax on electricity consumption (Money/MWh)';
PARAMETER TAX_DH(CCC)                  'Consumers tax on heat consumption (Money/MWh)';
PARAMETER ANNUITYC(CCC)                'Transforms investment to annual payment (fraction)';
PARAMETER GINVCOST(AAA,GGG)            'Investment cost for new technology (MMoney/MW)';
PARAMETER GOMVCOST(AAA,GGG)            'Variable operating and maintenance costs (Money/MWh)';
PARAMETER GOMFCOST(AAA,GGG)            'Annual fixed operating costs (kMoney/MW)';
PARAMETER GEFFRATE(AAA,GGG)            "Rating of fuel efficiency (fraction)";
PARAMETER DEFP_BASE(RRR)               'Nominal annual average consumer electricity price (Money/MWh)';
PARAMETER DHFP_BASE(AAA)               'Nominal annual average consumer heat price (Money/MWh)';

PARAMETER OHEATECON(YYY,CCC,RRR,AAA,GGG,ECONSET,ECONSETCATEGORY)         'Parameter for econ';



PARAMETER DE(YYY,RRR)                  'Annual electricity consumption (MWh)';
PARAMETER DH(YYY,AAA)                  'Annual heat consumption (MWh)';

*---- Transmission data: -------------------------------------------------------
PARAMETER XKINI(YYY,IRRRE,IRRRI)       'Initial transmission capacity between regions (MW)';
PARAMETER XINVCOST(IRRRE,IRRRI)        'Investment cost in new transmission capacity (Money/MW)';
PARAMETER XCOST(IRRRE,IRRRI)           'Transmission cost between regions (Money/MWh)';
PARAMETER XLOSS(IRRRE,IRRRI)           'Transmission loss between regions (fraction)';
$ifi %XKRATE_DOL%==IRRRE_IRRRI          PARAMETER XKRATE(IRRRE,IRRRI)         "Transmission capacity rating (share; non-negative, typically close to 1; default/1/, use eps for 0)";
$ifi %XKRATE_DOL%==IRRRE_IRRRI_SSS      PARAMETER XKRATE(IRRRE,IRRRI,SSS)     "Transmission capacity rating (share; non-negative, typically close to 1; default/1/, use eps for 0)";
$ifi %XKRATE_DOL%==IRRRE_IRRRI_SSS_TTT  PARAMETER XKRATE(IRRRE,IRRRI,SSS,TTT) "Transmission capacity rating (share; non-negative, typically close to 1; default/1/, use eps for 0)";



*---- Third regions:
PARAMETER X3FX(YYY,RRR)                'Annual net electricity export to third regions';
$ifi %X3V%==yes PARAMETER X3VPIM(YYY,RRR,X3VPLACE0,X3VSTEP0,SSS,TTT)    'Price (Money/MWh) of price dependent imported electricity';
$ifi %X3V%==yes PARAMETER X3VPEX(YYY,RRR,X3VPLACE0,X3VSTEP0,SSS,TTT)    'Price (Money/MWh) of price dependent exported electricity';
$ifi %X3V%==yes PARAMETER X3VQIM(RRR,X3VPLACE0,X3VSTEP0,SSS,TTT)        'Limit (MW) on price dependent electricity import';
$ifi %X3V%==yes PARAMETER X3VQEX(RRR,X3VPLACE0,X3VSTEP0,SSS,TTT)        'Limit (MW) on price dependent electricity export';

* Fuel prices: -----------------------------------------------------------------

PARAMETER M_POL(YYY,MPOLSET,CCC)                   'Emission policy data';
* Time series on (SSS,TTT):
PARAMETER WEIGHT_S(SSS)                            'Weight (relative length) of each season';
PARAMETER WEIGHT_T(TTT)                            'Weight (relative length) of each time period';
$ifi %GKRATE_DOL%==AAA_GGG_SSS_TTT                 PARAMETER GKRATE(AAA,GGG,SSS,TTT)     "Rating of technology capacities (fraction)";
$ifi %GKRATE_DOL%==AAA_GGG_SSS                     PARAMETER GKRATE(AAA,GGG,SSS)         "Rating of technology capacities (fraction)";
PARAMETER DE_VAR_T(RRR,SSS,TTT)                    'Variation in electricity demand';
PARAMETER DH_VAR_T(AAA,SSS,TTT)                    'Variation in heat demand';
PARAMETER WTRRSVAR_S(AAA,SSS)                      'Variation of the water inflow to reservoirs';
PARAMETER WTRRRVAR_T(AAA,SSS,TTT)                  'Variation of generation of hydro run-of-river';
PARAMETER WND_VAR_T(AAA,SSS,TTT)                   'Variation of the wind generation';
PARAMETER SOLE_VAR_T(AAA,SSS,TTT)                  'Variation of the solar generation';
PARAMETER WAVE_VAR_T(AAA,SSS,TTT)                  'Variation of the wave generation'
PARAMETER X3FX_VAR_T(RRR,SSS,TTT)                  'Variation in fixed exchange with 3. region';
PARAMETER HYPPROFILS(AAA,SSS)                      'Hydro with storage seasonal price profile';
PARAMETER DEF_STEPS(RRR,SSS,TTT,DF_QP,DEF)         'Elastic electricity demands';
$ifi %DEFPCALIB%==yes PARAMETER DEFP_CALIB(RRR,SSS,TTT)  'Calibrate the price side of electricity demand';
PARAMETER DHF_STEPS(AAA,SSS,TTT,DF_QP,DHF)         'Elastic heat demands';
$ifi %DHFPCALIB%==yes PARAMETER DHFP_CALIB(AAA,SSS,TTT)  'Calibrate the price side of heat demand';
$ifi %YIELDREQUIREMENT%==yes  PARAMETER YIELDREQ(GGG) 'Differentiates yield requirements for different technologies';


SCALAR PENALTYQ  'Penalty on violation of equation';


* Data for handling of annual hydro constraints in BB3, to be saved by BB1/BB2 and used by BB3:
PARAMETER HYRSG(YYY,AAA,SSS)        'Water (hydro) generation quantity of the seasons to be transferred to model Balbase3 (MWh)';
PARAMETER VHYRS_SL(Y,AAA,SSS)       'To be saved for comparison with BB1/BB2 solution value for VHYRS_S.L (initial letter is V although declared as a parameter) (MWh)'
PARAMETER WATERVAL(YYY,AAA,SSS)     'Water value (in input Money) to be transferred to model Balbase3 (input-Money/MWh)';


* Printing of data to the list file controlled by %ONOFFDATALISTING% and %ONOFFCODELISTING%:
%ONOFFDATALISTING%

PARAMETER YVALUE(YYY)   'Numerical value of the years labels'  %semislash%
$if     EXIST '../data/YVALUE.inc' $INCLUDE      '../data/YVALUE.inc';
$if not EXIST '../data/YVALUE.inc' $INCLUDE '../../base/data/YVALUE.inc';
%semislash%;

PARAMETER FDATA(FFF,FDATASET)    'Fuel specific values'  %semislash%
$if     EXIST '../data/FDATA.inc' $INCLUDE      '../data/FDATA.inc';
$if not EXIST '../data/FDATA.inc' $INCLUDE '../../base/data/FDATA.inc';
%semislash%;


* The two specifications of fuel type 'FDACRONYM' and 'FDNB', have relative merits.
* Under most circumstances 'FDACRONYM' is preferable, and it is used internally in the GAMS code.
* Under these circumstances assign no value for 'FDNB'.
* If 'FDACRONYM' can not be used, use 'FDNB'.
* Always subsitute 'FDACRONYM' values by 'FDNB' values if the latter are non-zero:
FDATA(FFF,'FDACRONYM')$(NOT FDATA(FFF,'FDACRONYM')) = FDATA(FFF,'FDNB');

PARAMETER FMAXINVEST(CCC,FFF)   'Maximum investment (MW) by fuel type for each year simulated'    %semislash%
$if     EXIST '../data/FMAXINVEST.inc' $INCLUDE      '../data/FMAXINVEST.inc';
$if not EXIST '../data/FMAXINVEST.inc' $INCLUDE '../../base/data/FMAXINVEST.inc';
%semislash%;

PARAMETER GROWTHCAP(C,GGG)    'Maximum model generated capacity increase (MW) from one year to the next'   %semislash%
$if     EXIST '../data/GROWTHCAP.inc' $INCLUDE      '../data/GROWTHCAP.inc';
$if not EXIST '../data/GROWTHCAP.inc' $INCLUDE '../../base/data/GROWTHCAP.inc';
%semislash%;


PARAMETER DISLOSS_E(RRR)    'Loss in electricity distribution (fraction)'   %semislash%
$if     EXIST '../data/DISLOSS_E.inc' $INCLUDE      '../data/DISLOSS_E.inc';
$if not EXIST '../data/DISLOSS_E.inc' $INCLUDE '../../base/data/DISLOSS_E.inc';
%semislash%;

PARAMETER DISLOSS_H(AAA)    'Loss in heat distribution (fraction)'  %semislash%
$if     EXIST '../data/DISLOSS_H.inc' $INCLUDE      '../data/DISLOSS_H.inc';
$if not EXIST '../data/DISLOSS_H.inc' $INCLUDE '../../base/data/DISLOSS_H.inc';
%semislash%;

PARAMETER DISCOST_E(RRR)    'Cost of electricity distribution (Money/MWh)'  %semislash%
$if     EXIST '../data/DISCOST_E.inc' $INCLUDE      '../data/DISCOST_E.inc';
$if not EXIST '../data/DISCOST_E.inc' $INCLUDE '../../base/data/DISCOST_E.inc';
%semislash%;

PARAMETER DISCOST_H(AAA)    'Cost of heat distribution (Money/MWh)'     %semislash%
$if     EXIST '../data/DISCOST_H.inc' $INCLUDE      '../data/DISCOST_H.inc';
$if not EXIST '../data/DISCOST_H.inc' $INCLUDE '../../base/data/DISCOST_H.inc';
%semislash%;

PARAMETER FKPOT(CCCRRRAAA,FFF)    'Fuel potential restricted by geography (MW)'   %semislash%
$if     EXIST '../data/FKPOT.inc' $INCLUDE      '../data/FKPOT.inc';
$if not EXIST '../data/FKPOT.inc' $INCLUDE '../../base/data/FKPOT.inc';
%semislash%;


PARAMETER FGEMIN(CCCRRRAAA,FFF)    'Minimum electricity generation by fuel (MWh)'  %semislash%
$if     EXIST '../data/FGEMIN.inc' $INCLUDE      '../data/FGEMIN.inc';
$if not EXIST '../data/FGEMIN.inc' $INCLUDE '../../base/data/FGEMIN.inc';
%semislash%;

PARAMETER FGEMAX(CCCRRRAAA,FFF)     'Maximum electricity generation by fuel (MWh)'   %semislash%
$if     EXIST '../data/FGEMAX.inc' $INCLUDE      '../data/FGEMAX.inc';
$if not EXIST '../data/FGEMAX.inc' $INCLUDE '../../base/data/FGEMAX.inc';
%semislash%;

$ifi %GMINF_DOL%==CCCRRRAAA_FFF      PARAMETER GMINF(CCCRRRAAA,FFF)             'Minimum fuel use (GJ) per year' %semislash%
$ifi %GMINF_DOL%==YYY_CCCRRRAAA_FFF  PARAMETER GMINF(YYY,CCCRRRAAA,FFF)         'Minimum fuel use (GJ) per year' %semislash%
$if     EXIST '../data/GMINF.inc' $INCLUDE      '../data/GMINF.inc';
$if not EXIST '../data/GMINF.inc' $INCLUDE '../../base/data/GMINF.inc';
%semislash%;

$ifi %GMAXF_DOL%==CCCRRRAAA_FFF      PARAMETER GMAXF(CCCRRRAAA,FFF)             'Maximum fuel use (GJ) per year' %semislash%
$ifi %GMAXF_DOL%==YYY_CCCRRRAAA_FFF  PARAMETER GMAXF(YYY,CCCRRRAAA,FFF)         'Maximum fuel use (GJ) per year' %semislash%
$if     EXIST '../data/GMAXF.inc' $INCLUDE      '../data/GMAXF.inc';
$if not EXIST '../data/GMAXF.inc' $INCLUDE '../../base/data/GMAXF.inc';
%semislash%;

$ifi %GEQF_DOL%==CCCRRRAAA_FFF       PARAMETER GEQF(CCCRRRAAA,FFF)              'Required fuel use (GJ) per year' %semislash%
$ifi %GEQF_DOL%== YYY_CCCRRRAAA_FFF  PARAMETER GEQF(YYY,CCCRRRAAA,FFF)          'Required fuel use (GJ) per year' %semislash%
$if     EXIST '../data/GEQF.inc' $INCLUDE      '../data/GEQF.inc';
$if not EXIST '../data/GEQF.inc' $INCLUDE '../../base/data/GEQF.inc';
%semislash%;


Parameter GEQF_FLEX(YYY,CCCRRRAAA,FFF) 'Required fuel use (GJ) per week , flexibility of the required fuel use per year' %semislash%
Parameter GEQF_FLEX_Y(CCCRRRAAA,FFF) 'Required fuel use (GJ) in the year of simulation' %semislash%
$if     EXIST '../data/GEQF_FLEX.inc' $INCLUDE      '../data/GEQF_FLEX.inc';
$if not EXIST '../data/GEQF_FLEX.inc' $INCLUDE '../../base/data/GEQF_FLEX.inc';
%semislash%;


* Maximum capacity at new technologies
PARAMETER GKNMAX(YYY,AAA,GGG)   'Maximum capacity at new technologies (MW)'  %semislash%
$if     EXIST '../data/GKNMAX.inc' $INCLUDE      '../data/GKNMAX.inc';
$if not EXIST '../data/GKNMAX.inc' $INCLUDE '../../base/data/GKNMAX.inc';
%semislash%;


PARAMETER WTRRSFLH(AAA)    'Full load hours for hydro reservoir plants (hours)'  %semislash%
$if     EXIST '../data/WTRRSFLH.inc' $INCLUDE      '../data/WTRRSFLH.inc';
$if not EXIST '../data/WTRRSFLH.inc' $INCLUDE '../../base/data/WTRRSFLH.inc';
%semislash%;

PARAMETER WTRRRFLH(AAA)    'Full load hours for hydro run-of-river plants (hours)'  %semislash%
$if     EXIST '../data/WTRRRFLH.inc' $INCLUDE      '../data/WTRRRFLH.inc';
$if not EXIST '../data/WTRRRFLH.inc' $INCLUDE '../../base/data/WTRRRFLH.inc';
%semislash%;

PARAMETER WNDFLH(AAA)    'Full load hours for wind power (hours)'  %semislash%
$if     EXIST '../data/WNDFLH.inc' $INCLUDE      '../data/WNDFLH.inc';
$if not EXIST '../data/WNDFLH.inc' $INCLUDE '../../base/data/WNDFLH.inc';
%semislash%;

PARAMETER SOLEFLH(AAA)    'Full load hours for solar power (hours)'  %semislash%
$if     EXIST '../data/SOLEFLH.inc' $INCLUDE      '../data/SOLEFLH.inc';
$if not EXIST '../data/SOLEFLH.inc' $INCLUDE '../../base/data/SOLEFLH.inc';
%semislash%;


PARAMETER SOLHFLH(AAA)    'Full load hours for solar power (hours)'  %semislash%
$if     EXIST '../data/SOLHFLH.inc' $INCLUDE      '../data/SOLHFLH.inc';
$if not EXIST '../data/SOLHFLH.inc' $INCLUDE '../../base/data/SOLHFLH.inc';
%semislash%;

PARAMETER WAVEFLH(AAA)    'Full load hours for wave power (hours)'  %semislash%
$if     EXIST '../data/WAVEFLH.inc' $INCLUDE      '../data/WAVEFLH.inc';
$if not EXIST '../data/WAVEFLH.inc' $INCLUDE '../../base/data/WAVEFLH.inc';
%semislash%;

PARAMETER HYRSDATA(AAA,HYRSDATASET,SSS)    'Data for hydro with storage'  %semislash%
$if     EXIST '../data/HYRSDATA.inc' $INCLUDE         '../data/HYRSDATA.inc';
$if not EXIST '../data/HYRSDATA.inc' $INCLUDE '../../base/data/HYRSDATA.inc';
%semislash%;

PARAMETER TAX_GF(YYY,AAA,G)    'Fuel taxes on heat-only units'  %semislash%
$if     EXIST '../data/TAX_GF.inc' $INCLUDE      '../data/TAX_GF.inc';
$if not EXIST '../data/TAX_GF.inc' $INCLUDE '../../base/data/TAX_GF.inc';
%semislash%;

PARAMETER TAX_GE(YYY,AAA,G)    'Electricity taxes on generation units and subsidies(-) (EURO 90/MWh)'  %semislash%
$if     EXIST '../data/TAX_GE.inc' $INCLUDE      '../data/TAX_GE.inc';
$if not EXIST '../data/TAX_GE.inc' $INCLUDE '../../base/data/TAX_GE.inc';
%semislash%;

PARAMETER TAX_GH(YYY,AAA,G)    'Heat taxes on generation units'   %semislash%
$if     EXIST '../data/TAX_GH.inc' $INCLUDE      '../data/TAX_GH.inc';
$if not EXIST '../data/TAX_GH.inc' $INCLUDE '../../base/data/TAX_GH.inc';
%semislash%;

PARAMETER TAX_KN(YYY,AAA,G)    'Tax (neg. subsidy) for investment in electricity generation (mEUR90/MW)'  %semislash%
$if     EXIST '../data/TAX_KN.inc' $INCLUDE      '../data/TAX_KN.inc';
$if not EXIST '../data/TAX_KN.inc' $INCLUDE '../../base/data/TAX_KN.inc';
%semislash%;

PARAMETER TAX_F(FFF,CCC)   'Fuel taxes for heat and electricity production (Money/GJ)'  %semislash%
$if     EXIST '../data/TAX_F.inc' $INCLUDE      '../data/TAX_F.inc';
$if not EXIST '../data/TAX_F.inc' $INCLUDE '../../base/data/TAX_F.inc';
%semislash%;

PARAMETER TAX_DE(CCC)    'Consumers tax on electricity consumption (Money/MWh)'  %semislash%
$if     EXIST '../data/TAX_DE.inc' $INCLUDE      '../data/TAX_DE.inc';
$if not EXIST '../data/TAX_DE.inc' $INCLUDE '../../base/data/TAX_DE.inc';
%semislash%;

PARAMETER TAX_DH(CCC)    'Consumers tax on heat consumption (Money/MWh)'  %semislash%
$if     EXIST '../data/TAX_DH.inc' $INCLUDE      '../data/TAX_DH.inc';
$if not EXIST '../data/TAX_DH.inc' $INCLUDE '../../base/data/TAX_DH.inc';
%semislash%;



PARAMETER ANNUITYC(CCC)   'Transforms investment to annual payment (fraction)'  %semislash%
$if     EXIST '../data/ANNUITYC.inc' $INCLUDE         '../data/ANNUITYC.inc';
$if not EXIST '../data/ANNUITYC.inc' $INCLUDE '../../base/data/ANNUITYC.inc';
%semislash%;

PARAMETER GINVCOST(AAA,GGG)    'Investment cost for new technology (MMoney/MW)'  %semislash%
$if     EXIST '../data/GINVCOST.inc' $INCLUDE         '../data/GINVCOST.inc';
$if not EXIST '../data/GINVCOST.inc' $INCLUDE '../../base/data/GINVCOST.inc';
%semislash%;

PARAMETER GOMVCOST(AAA,GGG)    'Variable operating and maintenance costs (Money/MWh)'  %semislash%
$if     EXIST '../data/GOMVCOST.inc' $INCLUDE         '../data/GOMVCOST.inc';
$if not EXIST '../data/GOMVCOST.inc' $INCLUDE '../../base/data/GOMVCOST.inc';
%semislash%;

PARAMETER GOMFCOST(AAA,GGG)    'Annual fixed operating costs (kMoney/MW)'  %semislash%
$if     EXIST '../data/GOMFCOST.inc' $INCLUDE         '../data/GOMFCOST.inc';
$if not EXIST '../data/GOMFCOST.inc' $INCLUDE '../../base/data/GOMFCOST.inc';
%semislash%;

PARAMETER GEFFRATE(AAA,GGG)    "Rating of fuel efficiency (fraction)"  %semislash%
$if     EXIST '../data/GEFFRATE.inc' $INCLUDE         '../data/GEFFRATE.inc';
$if not EXIST '../data/GEFFRATE.inc' $INCLUDE '../../base/data/GEFFRATE.inc';
%semislash%;
GEFFRATE(IA,G)$((NOT GEFFRATE(IA,G)) AND SUM(Y,GKFX(Y,IA,G))) = 1; !! Insert default values. TODO: This is for BB1 and BB3 ONLY!

PARAMETER DEFP_BASE(RRR)    'Nominal annual average consumer electricity price (Money/MWh)'  %semislash%
$if     EXIST '../data/DEFP_BASE.inc' $INCLUDE         '../data/DEFP_BASE.inc';
$if not EXIST '../data/DEFP_BASE.inc' $INCLUDE '../../base/data/DEFP_BASE.inc';
%semislash%;

PARAMETER DHFP_BASE(AAA)    'Nominal annual average consumer heat price (Money/MWh)'  %semislash%
$if     EXIST '../data/DHFP_BASE.inc' $INCLUDE         '../data/DHFP_BASE.inc';
$if not EXIST '../data/DHFP_BASE.inc' $INCLUDE '../../base/data/DHFP_BASE.inc';
%semislash%;



*-------------------------------------------------------------------------------
*---- End: Geographically specific values --------------------------------------
*-------------------------------------------------------------------------------


*-------------------------------------------------------------------------------
*---- Annual electricity demand : ---------------------------------------------
*-------------------------------------------------------------------------------
PARAMETER DE(YYY,RRR)    'Annual electricity consumption (MWh)' %semislash%
$if     EXIST '../data/de.inc' $INCLUDE         '../data/de.inc';
$if not EXIST '../data/de.inc' $INCLUDE '../../base/data/de.inc';
%semislash%;


*-------------------------------------------------------------------------------
*---- Annual heat demand: -----------------------------------------------------
*-------------------------------------------------------------------------------
PARAMETER DH(YYY,AAA)    'Annual heat consumption (MWh)'  %semislash%
$if     EXIST '../data/dh.inc' $INCLUDE         '../data/dh.inc';
$if not EXIST '../data/dh.inc' $INCLUDE '../../base/data/dh.inc';
%semislash%;



*-------------------------------------------------------------------------------
*---- Transmission data: ------------------------------------------------------
*-------------------------------------------------------------------------------

PARAMETER XKINI(YYY,IRRRE,IRRRI)  'Initial transmission capacity between regions (MW)'   %semislash%
$if     EXIST '../data/XKINI.inc' $INCLUDE      '../data/XKINI.inc';
$if not EXIST '../data/XKINI.inc' $INCLUDE '../../base/data/XKINI.inc';
%semislash%;

PARAMETER XINVCOST(IRRRE,IRRRI)   'Investment cost in new transmission capacity (Money/MW)'    %semislash%
$if     EXIST '../data/XINVCOST.inc' $INCLUDE      '../data/XINVCOST.inc';
$if not EXIST '../data/XINVCOST.inc' $INCLUDE '../../base/data/XINVCOST.inc';
%semislash%;

PARAMETER XCOST(IRRRE,IRRRI)      'Transmission cost between regions (Money/MWh)'   %semislash%
$if     EXIST '../data/XCOST.inc' $INCLUDE      '../data/XCOST.inc';
$if not EXIST '../data/XCOST.inc' $INCLUDE '../../base/data/XCOST.inc';
%semislash%;

PARAMETER XLOSS(IRRRE,IRRRI)      'Transmission loss between regions (fraction)'  %semislash%
$if     EXIST '../data/XLOSS.inc' $INCLUDE         '../data/XLOSS.inc';
$if not EXIST '../data/XLOSS.inc' $INCLUDE '../../base/data/XLOSS.inc';
%semislash%;

$ifi %XKRATE_DOL%==IRRRE_IRRRI          PARAMETER XKRATE(IRRRE,IRRRI)         'Transmission capacity derating (fraction)'  %semislash%
$ifi %XKRATE_DOL%==IRRRE_IRRRI_SSS_TTT  PARAMETER XKRATE(IRRRE,IRRRI,SSS,TTT) 'Transmission capacity derating (fraction)'  %semislash%
$if     EXIST '../data/XKRATE.inc' $INCLUDE         '../data/XKRATE.inc';
$if not EXIST '../data/XKRATE.inc' $INCLUDE '../../base/data/XKRATE.inc';
%semislash%;

PARAMETER XMAXINV(IRRRE,IRRRI)   'Max investment in transmission capacity between two regions for each simulated year(each 5th year)'  %semislash%
$if     EXIST '../data/XMAXINV.inc' $INCLUDE         '../data/XMAXINV.inc';
$if not EXIST '../data/XMAXINV.inc' $INCLUDE '../../base/data/XMAXINV.inc';
%semislash%;

*-------------------------------------------------------------------------------
*---- Exchange with third countries: ------------------------------------------
*-------------------------------------------------------------------------------
* Fixed profile:
PARAMETER X3FX(YYY,RRR)    'Annual net electricity export to third regions'  %semislash%
$if     EXIST '../data/X3FX.inc' $INCLUDE      '../data/X3FX.inc';
$if not EXIST '../data/X3FX.inc' $INCLUDE '../../base/data/X3FX.inc';
%semislash%;


$ifi not %X3V%==yes $goto X3V_label3
*-------------------------------------------------------------------------------
* Price-dependant electricity exchange to places outside the simulated area
*-------------------------------------------------------------------------------

* Price (Money/MWh) for the price dependent electricity exchange.
* It will be assumed that prices should be positive.
* For import the prices should be increasing with ord(X3VSTEP0),
* for export the prices should be decreasing with ord(X3VSTEP0).


PARAMETER X3VPIM(YYY,RRR,X3VPLACE0,X3VSTEP0,SSS,TTT)   'Price (Money/MWh) of price dependent imported electricity'  %semislash%
$if     EXIST '../data/X3VPIM.inc' $INCLUDE      '../data/X3VPIM.inc';
$if not EXIST '../data/X3VPIM.inc' $INCLUDE '../../base/data/X3VPIM.inc';
%semislash%;


PARAMETER X3VPEX(YYY,RRR,X3VPLACE0,X3VSTEP0,SSS,TTT)   'Price (Money/MWh) of price dependent exported electricity'  %semislash%
$if     EXIST '../data/X3VPEX.inc' $INCLUDE      '../data/X3VPEX.inc';
$if not EXIST '../data/X3VPEX.inc' $INCLUDE '../../base/data/X3VPEX.inc';
%semislash%;


* Maximum quantity (MW) of price dependent electricity exchange per time segment:
PARAMETER X3VQIM(RRR,X3VPLACE0,X3VSTEP0,SSS,TTT)   'Limit (MW) on price dependent electricity import'  %semislash%
$if     EXIST '../data/X3VQIM.inc' $INCLUDE      '../data/X3VQIM.inc';
$if not EXIST '../data/X3VQIM.inc' $INCLUDE '../../base/data/X3VQIM.inc';
%semislash%;


PARAMETER X3VQEX(RRR,X3VPLACE0,X3VSTEP0,SSS,TTT)   'Limit (MW) on price dependent electricity export'  %semislash%
$if     EXIST '../data/X3VQEX.inc' $INCLUDE      '../data/X3VQEX.inc';
$if not EXIST '../data/X3VQEX.inc' $INCLUDE '../../base/data/X3VQEX.inc';
%semislash%;

$label X3V_label3

*-------------------------------------------------------------------------------
* Annually specified fuel prices: ---------------------------------------------
*-------------------------------------------------------------------------------

PARAMETER FUELPRICE(YYY,AAA,FFF)                   'Fuel prices (Money/GJ)'   %semislash%
$if     EXIST '../data/FUELPRICE.inc' $INCLUDE      '../data/FUELPRICE.inc';
$if not EXIST '../data/FUELPRICE.inc' $INCLUDE '../../base/data/FUELPRICE.inc';
%semislash%;


*-------------------------------------------------------------------------------
*---- Emission policy data: ---------------------------------------------------
*-------------------------------------------------------------------------------

PARAMETER M_POL(YYY,MPOLSET,CCC)    'Emission policy data'   %semislash%
$if     EXIST '../data/m_pol.inc' $INCLUDE         '../data/m_pol.inc';
$if not EXIST '../data/m_pol.inc' $INCLUDE '../../base/data/m_pol.inc';
%semislash%;

*-------------------------------------------------------------------------------
*---- Seasonal and daily variations: ------------------------------------------
*-------------------------------------------------------------------------------


* DISSE FILER INDHOLDER NaSTEN ALLESAMMEN KODE, CONDITIONALS, ETC! Skal revideres!

PARAMETER WEIGHT_S(SSS)                            'Weight (relative length) of each season'    %semislash%
$if     EXIST '../data/WEIGHT_S.inc' $INCLUDE      '../data/WEIGHT_S.inc';
$if not EXIST '../data/WEIGHT_S.inc' $INCLUDE '../../base/data/WEIGHT_S.inc';
%semislash%;

PARAMETER WEIGHT_T(TTT)                            'Weight (relative length) of each time period'   %semislash%
$if     EXIST '../data/WEIGHT_T.inc' $INCLUDE      '../data/WEIGHT_T.inc';
$if not EXIST '../data/WEIGHT_T.inc' $INCLUDE '../../base/data/WEIGHT_T.inc';
%semislash%;

* GKDERATE substituted by GKRATE
$ifi %GKRATE_DOL%==AAA_GGG_SSS_TTT  PARAMETER GKRATE(AAA,GGG,SSS,TTT)     "Rating of technology capacities (fraction)"; %semislash%
$ifi %GKRATE_DOL%==AAA_GGG_SSS      PARAMETER GKRATE(AAA,GGG,SSS)         "Rating of technology capacities (fraction)"; %semislash%
$if     EXIST '../data/GKRATE.inc' $INCLUDE      '../data/GKRATE.inc';
$if not EXIST '../data/GKRATE.inc' $INCLUDE '../../base/data/GKRATE.inc';
%semislash%;
*NOT HERE, SEARCH IGKRATE  $ifi %GKDERATE_DOL%==AAA_GGG_SSS_TTT  GKRATE(IA,G,S,T)$((NOT GKRATE(IA,G,S,T)) AND SUM(Y,GKFX(Y,IA,G))) = 1; !! Insert default values.  TODO: This is for BB1 and BB3 ONLY!
*NOT HERE, SEARCH IGKRATE  $ifi %GKDERATE_DOL%==AAA_GGG_SSS      GKRATE(IA,G,S)$((NOT GKRATE(IA,G,S)) AND SUM(Y,GKFX(Y,IA,G))) = 1;     !! Insert default values.  TODO: This is for BB1 and BB3 ONLY!


PARAMETER DE_VAR_T(RRR,SSS,TTT)                    'Variation in electricity demand'   %semislash%
$if     EXIST '../data/DE_VAR_T.inc' $INCLUDE      '../data/DE_VAR_T.inc';
$if not EXIST '../data/DE_VAR_T.inc' $INCLUDE '../../base/data/DE_VAR_T.inc';
%semislash%;



PARAMETER DH_VAR_T(AAA,SSS,TTT)                    'Variation in heat demand'   %semislash%
$if     EXIST '../data/DH_VAR_T.inc' $INCLUDE      '../data/DH_VAR_T.inc';
$if not EXIST '../data/DH_VAR_T.inc' $INCLUDE '../../base/data/DH_VAR_T.inc';
%semislash%;

PARAMETER WTRRSVAR_S(AAA,SSS)                      'Variation of the water inflow to reservoirs'   %semislash%
$if     EXIST '../data/WTRRSVAR_S.inc' $INCLUDE      '../data/WTRRSVAR_S.inc';
$if not EXIST '../data/WTRRSVAR_S.inc' $INCLUDE '../../base/data/WTRRSVAR_S.inc';
%semislash%;

PARAMETER WTRRRVAR_T(AAA,SSS,TTT)                  'Variation of generation of hydro run-of-river'    %semislash%
$if     EXIST '../data/WTRRRVAR_T.inc' $INCLUDE      '../data/WTRRRVAR_T.inc';
$if not EXIST '../data/WTRRRVAR_T.inc' $INCLUDE '../../base/data/WTRRRVAR_T.inc';
%semislash%;

PARAMETER WND_VAR_T(AAA,SSS,TTT)                   'Variation of the wind generation'    %semislash%
$if     EXIST '../data/WND_VAR_T.inc' $INCLUDE      '../data/WND_VAR_T.inc';
$if not EXIST '../data/WND_VAR_T.inc' $INCLUDE '../../base/data/WND_VAR_T.inc';
%semislash%;

PARAMETER SOLE_VAR_T(AAA,SSS,TTT)                  'Variation of the solarE generation'   %semislash%
$if     EXIST '../data/SOLE_VAR_T.inc' $INCLUDE      '../data/SOLE_VAR_T.inc';
$if not EXIST '../data/SOLE_VAR_T.inc' $INCLUDE '../../base/data/SOLE_VAR_T.inc';
%semislash%;

PARAMETER SOLH_VAR_T(AAA,SSS,TTT)                  'Variation of the solarH generation'   %semislash%
$if     EXIST '../data/SOLH_VAR_T.inc' $INCLUDE      '../data/SOLH_VAR_T.inc';
$if not EXIST '../data/SOLH_VAR_T.inc' $INCLUDE '../../base/data/SOLH_VAR_T.inc';
%semislash%;

PARAMETER WAVE_VAR_T(AAA,SSS,TTT)                  'Variation of the solarH generation'   %semislash%
$if     EXIST '../data/WAVE_VAR_T.inc' $INCLUDE      '../data/WAVE_VAR_T.inc';
$if not EXIST '../data/WAVE_VAR_T.inc' $INCLUDE '../../base/data/WAVE_VAR_T.inc';
%semislash%;

PARAMETER X3FX_VAR_T(RRR,SSS,TTT)                  'Variation in fixed exchange with 3. region'   %semislash%
$if     EXIST '../data/X3FX_VAR_T.inc' $INCLUDE      '../data/X3FX_VAR_T.inc';
$if not EXIST '../data/X3FX_VAR_T.inc' $INCLUDE '../../base/data/X3FX_VAR_T.inc';
%semislash%;


PARAMETER HYPPROFILS(AAA,SSS)                      'Hydro with storage seasonal price profile'   %semislash%
$if     EXIST '../data/HYPPROFILS.inc' $INCLUDE      '../data/HYPPROFILS.inc';
$if not EXIST '../data/HYPPROFILS.inc' $INCLUDE '../../base/data/HYPPROFILS.inc';
%semislash%;

PARAMETER DEF_STEPS(RRR,SSS,TTT,DF_QP,DEF)         'Elastic electricity demands'   %semislash%
$if     EXIST '../data/DEF_STEPS.inc' $INCLUDE      '../data/DEF_STEPS.inc';
$if not EXIST '../data/DEF_STEPS.inc' $INCLUDE '../../base/data/DEF_STEPS.inc';
%semislash%;

$ifi %DEFPCALIB%==yes PARAMETER DEFP_CALIB(RRR,SSS,TTT)                  'Calibrate the price side of electricity demand'   %semislash%
$ifi %DEFPCALIB%==yes $if     EXIST '../data/DEFP_CALIB.inc' $INCLUDE      '../data/DEFP_CALIB.inc';
$ifi %DEFPCALIB%==yes $if not EXIST '../data/DEFP_CALIB.inc' $INCLUDE '../../base/data/DEFP_CALIB.inc';
$ifi %DEFPCALIB%==yes %semislash%;

PARAMETER DHF_STEPS(AAA,SSS,TTT,DF_QP,DHF)         'Elastic heat demands'   %semislash%
$if     EXIST '../data/DHF_STEPS.inc' $INCLUDE      '../data/DHF_STEPS.inc';
$if not EXIST '../data/DHF_STEPS.inc' $INCLUDE '../../base/data/DHF_STEPS.inc';
%semislash%;

$ifi %DHFPCALIB%==yes PARAMETER DHFP_CALIB(AAA,SSS,TTT)                  'Calibrate the price side of heat demand'   %semislash%
$ifi %DHFPCALIB%==yes $if     EXIST '../data/DHFP_CALIB.inc' $INCLUDE      '../data/DHFP_CALIB.inc';
$ifi %DHFPCALIB%==yes $if not EXIST '../data/DHFP_CALIB.inc' $INCLUDE '../../base/data/DHFP_CALIB.inc';
$ifi %DHFPCALIB%==yes %semislash%;


$ifi %bb3%==yes WEIGHT_T(T)=1;


$ifi %YIELDREQUIREMENT%==yes  PARAMETER YIELDREQ(GGG)  'Differentiates yield requirements for different technologies'   %semislash%
$ifi %YIELDREQUIREMENT%==yes  $if     EXIST '../data/YIELDREQ.inc' $INCLUDE      '../data/YIELDREQ.inc';
$ifi %YIELDREQUIREMENT%==yes  $if not EXIST '../data/YIELDREQ.inc' $INCLUDE '../../base/data/YIELDREQ.inc';
$ifi %YIELDREQUIREMENT%==yes  %semislash%;


* Printing of data to the list file controlled by %ONOFFDATALISTING% and %ONOFFCODELISTING%:
%ONOFFCODELISTING%

*-------------------------------------------------------------------------------
*---- Investments: --------------------------------------------------------------
*-------------------------------------------------------------------------------

* Investments made in BB2 are loaded automatically in non BB2 simulations
* conditioned by the ADDINVEST global variable (see balopt.opt).
* The parameter declaration is not conditional as it is used also when saving
* investments in a BB2 simulation conditioned by the MAKEINVEST global variable.

PARAMETER GKVACCDECOM(Y,AAA,G)   'Investments in generation technology by BB2 up to and including year Y with subtraction of decommissioning';
PARAMETER GKVACC(Y,AAA,G)        'Investments in generation technology by BB2 up to and including year Y without subtraction of decommissioning';
PARAMETER GVKGN(YYY,AAA,G)       'Investments in generation technology by BB2 in year Y';
PARAMETER XKACC(Y,IRRRE,IRRRI)   'Investments in electricity transmission by BB2 up to and including year Y';

$ifi %ADDINVEST%==yes execute_load '../../base/data/GKVACCDECOM.gdx', GKVACCDECOM;
$ifi %ADDINVEST%==yes execute_load '../../base/data/GKVACC.gdx', GKVACC;
$ifi %ADDINVEST%==yes execute_load '../../base/data/GVKGN.gdx', GVKGN;
$ifi %ADDINVEST%==yes execute_load '../../base/data/XKACC.gdx', XKACC;



*-------------------------------------------------------------------------------
*---- Miscellaneous ------------------------------------------------------------
*-------------------------------------------------------------------------------

* SCALAR OMONEY specifies output currency name and its exchange rate to input currency.
* Example of declaration:  'SCALAR OMONEY "EUR07" / 1.389 /;'.
* Since the text string holding the output currency is part of the declaration,
* the declaration is not given here in Balmorel.gms but in the included file.
* (For this reason you can not here apply the 'Semislash-idea'.)

$if     EXIST '../data/OMONEY.inc' $INCLUDE         '../data/OMONEY.inc';
$if not EXIST '../data/OMONEY.inc' $INCLUDE '../../base/data/OMONEY.inc';


*-------------------------------------------------------------------------------
*----- Any parameters for addon to be placed here: -----------------------------
*-------------------------------------------------------------------------------
$ifi %X3V%==yes $INCLUDE '../../base/addons/x3v/data/x3vdata.inc';
$ifi %HEATTRANS%==yes $if     EXIST '../data/htrans.inc' $INCLUDE         '../data/htrans.inc';
$ifi %HEATTRANS%==yes $if not EXIST '../data/htrans.inc' $INCLUDE '../../base/data/htrans.inc';

$ifi %POLICIES%==yes   $if exist 'POLREQ.inc' $INCLUDE 'POLREQ.inc';
$ifi %POLICIES%==yes   $if not exist 'POLREQ.inc' $INCLUDE '../../base/addons/policies/data/POLREQ.inc';

$ifi %SYSTEMCONST%==yes   $if exist 'BASELOADSERVICE.inc' $INCLUDE 'BASELOADSERVICE.inc';
$ifi %SYSTEMCONST%==yes   $if not exist 'BASELOADSERVICE.inc' $INCLUDE '../../base/addons/SystemConst/data/BASELOADSERVICE.inc';

$ifi %H2%==yes   $if exist '../addons/Hydrogen/H2data.inc' $INCLUDE '../addons/Hydrogen/H2data.inc';
$ifi %H2%==yes   $if not exist '../../base/addons/Hydrogen/H2data.inc' $INCLUDE '../../base/addons/Hydrogen/H2data.inc';

* This file (if exists) contains:
* PARAMETER XHKINI(IAAAE,IAAAI)    'Initial heat transmission capacity between regions'
* PARAMETER XHINVCOST(IAAAE,IAAAI) 'Investment cost in new heat transmission cap'
* PARAMETER XHCOST(IAAAE,IAAAI)    'Heat transmission cost between countries'
* PARAMETER XHLOSS(IAAAE,IAAAI)    'Heat transmission loss between regions'

* Data for handling of annual hydro constraints in BB3:
$ifi %bb3%==yes $include '../../base/addons/hyrsbb123/hyrsbb123includebb12data.inc';


* Printing of data to the list file controlled by %ONOFFDATALISTING% and %ONOFFCODELISTING%:
%ONOFFDATALISTING%

SCALAR PENALTYQ  'Penalty on violation of equation'  %semislash%
$if     EXIST '../data/PENALTYQ.inc' $INCLUDE      '../data/PENALTYQ.inc';
$if not EXIST '../data/PENALTYQ.inc' $INCLUDE '../../base/data/PENALTYQ.inc';
%semislash%;

* Printing of data to the list file controlled by %ONOFFDATALISTING% and %ONOFFCODELISTING%:
%ONOFFCODELISTING%


*-------------------------------------------------------------------------------
*----- End: Any parameters for addon to be placed here -------------------------
*-------------------------------------------------------------------------------


*-------------------------------------------------------------------------------
* End: Declaration and definition of numerical data: PARAMETERS and SCALARS
*-------------------------------------------------------------------------------
DE_VAR_T(RRR,SSS,TTT)$(not IR(RRR)) = 0;
DE_VAR_T(RRR,SSS,TTT)$(not IR(RRR)) = 0;
DH_VAR_T(AAA,SSS,TTT)$(not IA(AAA)) = 0;
X3FX_VAR_T(RRR,SSS,TTT)$(not IR(RRR)) = 0;
WND_VAR_T(AAA,SSS,TTT)$(not IA(AAA)) = 0;
SOLE_VAR_T(AAA,SSS,TTT)$(not IA(AAA)) = 0;
SOLE_VAR_T(AAA,SSS,TTT)$(not IA(AAA)) = 0;
WAVE_VAR_T(AAA,SSS,TTT)$(not IA(AAA)) = 0;
WTRRRVAR_T(AAA,SSS,TTT)$(not IA(AAA)) = 0;

*-------------------------------------------------------------------------------
* Declaration and definition of (additional) internal sets, aliases and parameters:
*-------------------------------------------------------------------------------


PARAMETER IGKFXYMAX(AAA,G) "The maximum over years in Y of exogenously specified capacity (MW)" ;


*-------------------------------------------------------------------------------
* Application of default data:
*-------------------------------------------------------------------------------

* GOMVCOST, GOMFCOST and GINVCOST are given the default values in GDATA unless otherwise specified in data file:
GOMVCOST(IA,G)$((NOT GOMVCOST(IA,G)) AND (SUM(Y,GKFX(Y,IA,G)) OR AGKN(IA,G))) = GDATA(G,'GDOMVCOST0');
GOMFCOST(IA,G)$((NOT GOMFCOST(IA,G)) AND (SUM(Y,GKFX(Y,IA,G)) OR AGKN(IA,G))) = GDATA(G,'GDOMFCOST0');
GINVCOST(IA,G)$((NOT GINVCOST(IA,G)) AND (SUM(Y,GKFX(Y,IA,G)) OR AGKN(IA,G))) = GDATA(G,'GDINVCOST0');

*-------------------------------------------------------------------------------
* End of: Application of default data
*-------------------------------------------------------------------------------


* Time aggregation:
$ifi %timeaggr%==yes  $include '../../base/addons/TimeAggregation/timeaggr.inc';

* This file contains initialisations of printing of log and error messages:
$INCLUDE '../../base/logerror/logerinc/error1.inc';


* The following relates technology and fuel:
SET IGF(GGG,FFF)   'Relation between technology type and fuel type';

* Internal scalars:

* Convenient Factors, typically relating Output and Input:
SCALAR IOF1000    'Multiplier 1000'       /1000/;
SCALAR IOF1000000 'Multiplier 1000000'    /1000000/;
SCALAR IOF0001    'Multiplier 0.001'      /0.001/;
SCALAR IOF0000001 'Multiplier 0.000001'   /0.000001/;
SCALAR IOF3P6     'Multiplier 3.6'        /3.6/;
SCALAR IOF24      'Multiplier 24'         /24/;
SCALAR IOF8760    'Multiplier 8760'       /8760/;
SCALAR IOF8784    'Multiplier 8784'       /8784/;
SCALAR IOF365     'Multiplier 365'        /365/;
SCALAR IOF05      'Multiplier 0.5'        /0.5/;
SCALAR IOF1_      'Multiplier 1 (used with QOBJ and derivation of marginal values('   /1/;!! special, possibly to disappear in future versions
* Scalars for occational use, their meaning will be context dependent :
SCALAR ISCALAR1   '(Context dependent)';
SCALAR ISCALAR2   '(Context dependent)';
SCALAR ISCALAR3   '(Context dependent)';
SCALAR ISCALAR4   '(Context dependent)';
SCALAR ISCALAR5   '(Context dependent)';

*------------------------------------------------------------------------------
* Internal sets:


SET IAGK_Y(AAA,G)        'Area, technology with positive capacity current simulation year';
SET IXKN(IRRRE,IRRRI)   'Pair of regions that may get new transmission capacity';

* Specification of where new endogenous generation capacity may be located:

SET IAGKN(AAA,G)     'Area, technology where technology may be invested based on AGKN and implicit constraints ';
* Initialisation: equal to AGKN:
IAGKN(IA,G)=AGKN(IA,G);

* No investment in secondary combination technologies:
$ifi %COMBTECH%==yes    IAGKN(IA,IGCOMB2)=NO;

SET IPLUSMINUS "Violation of equation"  /IPLUS Violation of equation upwards, IMINUS  Violation of equation downwards/;
* Note: When placed on the left hand side of the equation
* the sign to the IMINUS and IPLUS terms should be - and +, respectively.
* This way the sign and the name will be intuitively consistent in equation listings.


*------------------------------------------------------------------------------
* Internal parameters and settings:
*------------------------------------------------------------------------------


* Set GDCV value to one for IGBPR and IGHOB units so that their fuel consumption
* can be found using the same formula as for IGCND and IGEXT:
GDATA(IGBPR,'GDCV') = 1;
GDATA(IGHOB,'GDCV') = 1;

* Specifying the relation between technology type and fuel type in IGF:

IGF(G,FFF)=YES$(GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'));
IGF(G,FFF)=YES$(GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDNB'));   /* This assignent line may in future versions become invalid, only fuels Acronyms will remain supported. */
IGF(G,FFF)$(GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))=YES;

PARAMETER IGKRATE(AAA,G,S,T)     "Rating of technology capacities (share)";
* Transfer data from data file and insert default 1 as needed.
$ifi %GKRATE_DOL%==AAA_GGG_SSS_TTT  IGKRATE(IA,G,S,T) = GKRATE(IA,G,S,T);
$ifi %GKRATE_DOL%==AAA_GGG_SSS_TTT  IGKRATE(IA,G,S,T)$((NOT GKRATE(IA,G,S,T)) AND SUM(Y,GKFX(Y,IA,G))) = 1; !! Insert default values.  TODO: This is for BB1 and BB3 ONLY!
$ifi %GKRATE_DOL%==AAA_GGG_SSS      IGKRATE(IA,G,S,T) = GKRATE(IA,G,S);
$ifi %GKRATE_DOL%==AAA_GGG_SSS      IGKRATE(IA,G,S,T)$((NOT GKRATE(IA,G,S)) AND SUM(Y,GKFX(Y,IA,G))) = 1;   !! Insert default values.  TODO: This is for BB1 and BB3 ONLY!

* The following parameters contain information about CO2 and SO2 emission
* from technology G based on the fuel used and its emission data:

PARAMETER   IM_CO2(G)  'CO2 emission coefficient for fuel';
PARAMETER   IM_SO2(G)  'SO2 emission coefficient for fuel';
PARAMETER   IM_N2O(G)  'NO2 emission coefficient for fuel';

LOOP(FFF,
     IM_CO2(G)$IGF(G,FFF) = FDATA(FFF,'FDCO2');
     IM_SO2(G)$IGF(G,FFF) =
             FDATA(FFF,'FDSO2')$(GDATA(G,'GDDESO2') EQ 0) +
            (FDATA(FFF,'FDSO2')*(1-GDATA(G,'GDDESO2')))$(GDATA(G,'GDDESO2') GT 0);
     IM_N2O(G)$IGF(G,FFF) = FDATA(FFF,'FDN2O');
    );



* Further declarations relative to variations:
* Parameters holding the total weight in the (arbitrary) units of the weights
* used in input for each season and time period.
* To be used in calculations below.


PARAMETER IWEIGHSUMS          'Weight of the time of each season in S';
PARAMETER IWEIGHSUMT          'Weight of the time of each time period in T ';

* The (arbitrary) units used in the input are converted to days and hours,
* respectively. Note that IHOURSIN24 is given in hours.
PARAMETER IHOURSINST(SSS,T)   'Length of time segment (hours)';

* Annual amounts as expressed in the units of the weights and demands used
* in input in the file var.inc:

PARAMETER IDE_SUMST(RRR)      'Annual amount of electricity demand';
PARAMETER IDH_SUMST(AAA)      'Annual amount of heat demand';
PARAMETER IX3FXSUMST(RRR)     'Annual amount of electricity exported to third countries';

* Sums for finding the wind and solar generated electricity generation
* as expressed in the units of the weights and demands used in input:

PARAMETER IWND_SUMST(AAA)  'Annual amount of wind generated electricity';
PARAMETER ISOLESUMST(AAA)  'Annual amount of solar generated electricity';
PARAMETER ISOLHSUMST(AAA)  'Annual amount of solar generated heat';
PARAMETER IWAVESUMST(AAA)  'Annual amount of wave generated electricity';
PARAMETER IWTRRSSUM(AAA)   'Annual amount of hydro from reservoirs generated electricity';
PARAMETER IWTRRRSUM(AAA)   'Annual amount of hydro-run-of-river generated electricity';

*-------------------------------------------------------------------------------
* Set the time weights depending on the model:
*-------------------------------------------------------------------------------
* To guard against division-by-zero and other errors make sure that the following sums do not have inappropriate values:
$ifi %BB1%==yes    IWEIGHSUMS = SUM(S, WEIGHT_S(S));
$ifi %BB1%==yes    IWEIGHSUMT = SUM(T, WEIGHT_T(T));
$ifi %BB1%==yes    IHOURSINST(S,T)=IOF8760*WEIGHT_S(S)*WEIGHT_T(T)/(IWEIGHSUMS*IWEIGHSUMT);
$ifi %BB1%==yes    IDE_SUMST(IR) = SUM((S,T), IHOURSINST(S,T)*DE_VAR_T(IR,S,T));
$ifi %BB1%==yes    IDH_SUMST(IA) = SUM((S,T), IHOURSINST(S,T)*DH_VAR_T(IA,S,T));
$ifi %BB1%==yes    IX3FXSUMST(IR) = SUM((S,T), IHOURSINST(S,T)*X3FX_VAR_T(IR,S,T));
$ifi %BB1%==yes    IWND_SUMST(IA)=SUM((S,T), IHOURSINST(S,T)*WND_VAR_T(IA,S,T));
$ifi %BB1%==yes    ISOLESUMST(IA)=SUM((S,T), IHOURSINST(S,T)*SOLE_VAR_T(IA,S,T));
$ifi %BB1%==yes    ISOLHSUMST(IA)=SUM((S,T), IHOURSINST(S,T)*SOLH_VAR_T(IA,S,T));
$ifi %BB1%==yes    IWAVESUMST(IA)=SUM((S,T), IHOURSINST(S,T)*WAVE_VAR_T(IA,S,T));
$ifi %BB1%==yes    IWTRRRSUM(IA)=SUM((S,T),  IHOURSINST(S,T)*WTRRRVAR_T(IA,S,T));
$ifi %BB1%==yes    IWTRRSSUM(IA)=SUM(S, (IOF365*WEIGHT_S(S)/IWEIGHSUMS)*WTRRSVAR_S(IA,S));

$ifi %BB2%==yes    IWEIGHSUMS = SUM(S, WEIGHT_S(S));
$ifi %BB2%==yes    IWEIGHSUMT = SUM(T, WEIGHT_T(T));
$ifi %BB2%==yes    IHOURSINST(S,T)=IOF8760*WEIGHT_S(S)*WEIGHT_T(T)/(IWEIGHSUMS*IWEIGHSUMT);
$ifi %BB2%==yes    IDE_SUMST(IR) = SUM((S,T), IHOURSINST(S,T)*DE_VAR_T(IR,S,T));
$ifi %BB2%==yes    IDH_SUMST(IA) = SUM((S,T), IHOURSINST(S,T)*DH_VAR_T(IA,S,T));
$ifi %BB2%==yes    IX3FXSUMST(IR) = SUM((S,T), IHOURSINST(S,T)*X3FX_VAR_T(IR,S,T));
$ifi %BB2%==yes    IWND_SUMST(IA)=SUM((S,T), IHOURSINST(S,T)*WND_VAR_T(IA,S,T));
$ifi %BB2%==yes    ISOLESUMST(IA)=SUM((S,T), IHOURSINST(S,T)*SOLE_VAR_T(IA,S,T));
$ifi %BB2%==yes    ISOLHSUMST(IA)=SUM((S,T), IHOURSINST(S,T)*SOLH_VAR_T(IA,S,T));
$ifi %BB2%==yes    IWAVESUMST(IA)=SUM((S,T), IHOURSINST(S,T)*WAVE_VAR_T(IA,S,T));
$ifi %BB2%==yes    IWTRRRSUM(IA)=SUM((S,T),  IHOURSINST(S,T)*WTRRRVAR_T(IA,S,T));
$ifi %BB2%==yes    IWTRRSSUM(IA)=SUM(S, (IOF365*WEIGHT_S(S)/IWEIGHSUMS)*WTRRSVAR_S(IA,S));

$ifi %BB3%==yes    IWEIGHSUMS = SUM(SSS, WEIGHT_S(SSS));
$ifi %BB3%==yes    IWEIGHSUMT = SUM(T, WEIGHT_T(T));
$ifi %BB3%==yes    IHOURSINST(SSS,T)=IOF8760*(WEIGHT_S(SSS)*WEIGHT_T(T))/(IWEIGHSUMS*IWEIGHSUMT);
$ifi %BB3%==yes    IDE_SUMST(IR) = SUM((SSS,T), IHOURSINST(SSS,T)*DE_VAR_T(IR,SSS,T));
$ifi %BB3%==yes    IDH_SUMST(IA) = SUM((SSS,T), IHOURSINST(SSS,T)*DH_VAR_T(IA,SSS,T));
$ifi %BB3%==yes    IX3FXSUMST(IR) = SUM((SSS,T), IHOURSINST(SSS,T)*X3FX_VAR_T(IR,SSS,T));
$ifi %BB3%==yes    IWND_SUMST(IA)=SUM((SSS,T), IHOURSINST(SSS,T)*WND_VAR_T(IA,SSS,T));
$ifi %BB3%==yes    ISOLESUMST(IA)=SUM((SSS,T), IHOURSINST(SSS,T)*SOLE_VAR_T(IA,SSS,T));
$ifi %BB3%==yes    ISOLHSUMST(IA)=SUM((SSS,T), IHOURSINST(SSS,T)*SOLH_VAR_T(IA,SSS,T));
$ifi %BB3%==yes    IWAVESUMST(IA)=SUM((SSS,T), IHOURSINST(SSS,T)*WAVE_VAR_T(IA,SSS,T));
$ifi %BB3%==yes    IWTRRRSUM(IA)=SUM((SSS,T),  IHOURSINST(SSS,T)*WTRRRVAR_T(IA,SSS,T));
$ifi %BB3%==yes    IWTRRSSUM(IA)=SUM(SSS, (IOF365*WEIGHT_S(SSS)/IWEIGHSUMS)*WTRRSVAR_S(IA,SSS));


$ifi not %H2%==yes $goto noh2timeweight
PARAMETER IDH2_SUMST(RRR) 'Variation profile of H2 demand per region';
$ifi %BB1%==yes    IDH2_SUMST(IR) = SUM((S,T), IHOURSINST(S,T)*DH2_VAR_T(IR,S,T));
$ifi %BB2%==yes    IDH2_SUMST(IR) = SUM((S,T), IHOURSINST(S,T)*DH2_VAR_T(IR,S,T));
$ifi %BB3%==yes    IDH2_SUMST(IR) = SUM((SSS,T), IHOURSINST(SSS,T)*DH2_VAR_T(IR,SSS,T));
$label noh2timeweight

*-------------------------------------------------------------------------------
* End of: Set the time weights depending on the model
*-------------------------------------------------------------------------------


* PARAMETER IDEFP_T holds the price levels of individual steps
* in the electricity demand function, transformed to be comparable with
* production costs (including fuel taxes) by subtraction of taxes
* and distribution costs.

* Unit: Money/MWh.


PARAMETER IDEFP_T(RRR,SSS,TTT,DEF)    'Prices on elastic electricity demand steps (Money/MWh)';

IDEFP_T(IR,S,T,DEF_D1) = DEF_STEPS(IR,S,T,'DF_PRICE',DEF_D1)*DEFP_BASE(IR)
- SUM(C$CCCRRR(C,IR),TAX_DE(C)) - DISCOST_E(IR)
$ifi %DEFPCALIB%==yes  + DEFP_CALIB(IR,S,T)
;

IDEFP_T(IR,S,T,DEF_U1) = DEF_STEPS(IR,S,T,'DF_PRICE',DEF_U1)*DEFP_BASE(IR)
- SUM(C$CCCRRR(C,IR),TAX_DE(C)) - DISCOST_E(IR)
$ifi %DEFPCALIB%==yes  + DEFP_CALIB(IR,S,T)
;

IDEFP_T(IR,S,T,DEF_D2) = DEF_STEPS(IR,S,T,'DF_PRICE',DEF_D2);

IDEFP_T(IR,S,T,DEF_U2) = DEF_STEPS(IR,S,T,'DF_PRICE',DEF_U2);

IDEFP_T(IR,S,T,DEF_D3) = DEF_STEPS(IR,S,T,'DF_PRICE',DEF_D3);

IDEFP_T(IR,S,T,DEF_U3) = DEF_STEPS(IR,S,T,'DF_PRICE',DEF_U3);

* PARAMETER IDHFP_T holds the price levels of individual steps
* in the electricity demand function, transformed to be comparable with
* production costs (including fuel taxes) by subtraction of consumer taxes
* and distribution costs.

* Unit: Money/MWh.


PARAMETER IDHFP_T(AAA,SSS,TTT,DHF)   'Prices on elastic heat demand steps (Money/MWh)';

IDHFP_T(IA,S,T,DHF)$(DHF_D1(DHF)+DHF_U1(DHF)+DHF_D2(DHF)+DHF_U2(DHF)+DHF_D2(DHF)+DHF_U2(DHF))=
   DHF_STEPS(IA,S,T,'DF_PRICE',DHF)*DHFP_BASE(IA) - SUM(C$ICA(C,IA),TAX_DH(C)) - DISCOST_H(IA)
$ifi %DHFPCALIB%==yes  + DHFP_CALIB(IA,S,T)
;




* Demand of electricity (MW) and heat (MW) current simulation year:
PARAMETER IDE_T_Y(RRR,S,T)      'Electricity demand (MW) time segment (S,T)   current simulation year',
          IDH_T_Y(AAA,S,T)      'Heat demand (MW) time segment (S,T) current simulation year';
* Generation capacity (MW) at the beginning of current simulation year,
* specified by GKFX and by accumulated endogeneous investments, respectively:
PARAMETER IGKFX_Y(AAA,GGG)      'Externally given generation capacity in year Y (MW)',
          IGKFX_Y_1(AAA,GGG)    'Externally given generation capacity in year -1 (MW)',
          IGKVACCTOY(AAA,G)     'Internally given generation capacity at beginning of year (MW)(cumulative investments minus decommissioning)',
*          IGKVACCTOYNODECOM(G,AAA)'Cumulative investments in generation capacity at beginning of year',
          IGKVACCEOY(AAA,G)     'Internally given generation capacity at end of year (MW)(cumulative investments minus decommissioning)';


* Emission policy data during current simulation year:
PARAMETER ITAX_CO2_Y(C)   'Tax on CO2 (Money/t)' ,
          ITAX_SO2_Y(C)  'Tax on SO2 (Money/t)',
          ITAX_NOX_Y(C)'Tax on NOX (Money/kg)';
PARAMETER ILIM_CO2_Y(C)   'CO2 emission limit',
          ILIM_SO2_Y(C)'SO2 emission limit',
          ILIM_NOX_Y(C)'NOX emission limit';

* Taxes differentiated by (AAA,G)
PARAMETER  ITAX_GF(AAA,G)   'Fuel taxes on heat-only units',
           ITAX_GH(AAA,G)   'Heat taxes on generation units',
           ITAX_GE(AAA,G)   'Electricity taxes on generation units and subsidies(-) (EURO 90/MWh)',
           ITAX_KN(AAA,G)   'Tax (neg. subsidy) for investment in electricity generation (mEUR90/MW)';
*          ITAX_IE(AAA,G)
* Fuel use differentiated by (CCCRRRAAA,FFF)
PARAMETER  IGMAXF_Y(CCCRRRAAA,FFF)     'Maximum fuel use (GJ) per year',
           IGMINF_Y(CCCRRRAAA,FFF)     'Minimum fuel use (GJ) per year',
           IGEQF_Y(CCCRRRAAA,FFF)      'Required fuel use (GJ) per year'
           IGEQF_Y_FLEX(CCCRRRAAA,FFF) 'Flexible required fuel consumption in a year (%)'
           IGEQF_Y_S(CCCRRRAAA,FFF,S)  'Required fuel use (GJ) per week';


* Transmission capacity (MW) between regions RE (exporting) and  RI (importing)
* at the beginning of current simulation year:
PARAMETER IXKINI_Y(IRRRE,IRRRI)      'Externally given transmission capacity between regions at beginning of year (MW)',
          IXKVACCTOY(IRRRE,IRRRI) 'Internally given transmission capacity at beginning of year (MW)(cumulative investments)';

* Possibilities for new transmission lines:

IXKN(IRE,IRI) = NO;

* Fixed exchange with third countries (MW) current simulation year:
PARAMETER IX3FX_T_Y(RRR,S,T)   'Export to third countries for each time segment (MW)';

PARAMETER IXKRATE(IRRRE,IRRRI,SSS,TTT) "Transmission capacity rating (share)";
* Transfer data from data file and insert default 1 as needed.
$ifi %XKRATE_DOL%==IRRRE_IRRRI          IXKRATE(IRE,IRI,S,T) = XKRATE(IRE,IRI);
$ifi %XKRATE_DOL%==IRRRE_IRRRI          IXKRATE(IRE,IRI,S,T)$((NOT XKRATE(IRE,IRI))     AND SMAX(Y, XKINI(Y,IRE,IRI))) = 1;
$ifi %XKRATE_DOL%==IRRRE_IRRRI_SSS      IXKRATE(IRE,IRI,S,T) = XKRATE(IRE,IRI,S);
$ifi %XKRATE_DOL%==IRRRE_IRRRI_SSS      IXKRATE(IRE,IRI,S,T)$((NOT XKRATE(IRE,IRI,S))   AND SMAX(Y, XKINI(Y,IRE,IRI))) = 1;
$ifi %XKRATE_DOL%==IRRRE_IRRRI_SSS_TTT  IXKRATE(IRE,IRI,S,T) = XKRATE(IRE,IRI,S,T);
$ifi %XKRATE_DOL%==IRRRE_IRRRI_SSS_TTT  IXKRATE(IRE,IRI,S,T)$((NOT XKRATE(IRE,IRI,S,T)) AND SMAX(Y, XKINI(Y,IRE,IRI))) = 1;


* Fuel price for current simulation year:
PARAMETER IFUELP_Y(AAA,FFF)   'Fuel price in the year simulated';


* Inflow in areas each season:
* The following is used in balbase1.sim and balbase2.sim
PARAMETER IHYINF_S(AAA,SSS)  'Water inflow to hydro reservoirs in areas in each season (MWh/MW)';

* In BB3 one of the following parameters are used to control the use of hydro:
* Hans: taget ud, erstattes af noget funktionelt lignende.

PARAMETER IGROWTHCAP(C,G)        'Limit on growth of technology dependant on years between optimisation';

PARAMETER IGKNMAX_Y(AAA,G)       'Maximum capacity at new technologies (MW)';

*-------------------------------------------------------------------------------
*----- Any internal sets and parameters for addon to be placed here: -----------
*-------------------------------------------------------------------------------

* These internal parameters pertain to price sensitivy electricity exchange with third countries.
$ifi %X3V%==yes $include '../../base/addons/x3v/model/x3vinternal.inc';
* These internal parameters and sets pertain to heat transmission.
$if %HEATTRANS% == yes $include '../../base/addons/heattrans/model/htinternal.inc';
* Aggregated heat demand profile used in data_fv.inv, therefore placed here.
$ifi %FV%==yes $INCLUDE '../../base/addons/Fjernvarme/data_fv.inc';
* Unit commitment add-on
$ifi %UnitComm%==yes $include '../../base/addons/unitcommitment/intern.inc';


$ifi %AGKNDISC%==yes   $include '../../base/addons/agkndisc/agkndiscinternal.inc';

$ifi %H2%==yes $include '../../base/addons/Hydrogen/H2InternalParameters.inc';
*-------------------------------------------------------------------------------
*----- End: Any internal sets and parameters for addon to be placed here -------
*-------------------------------------------------------------------------------


*-------------------------------------------------------------------------------
* End: Internal parameters and settings
*-------------------------------------------------------------------------------


*-------------------------------------------------------------------------------
* Possibly write input data, prepare output file possibilities:
*-------------------------------------------------------------------------------

* The following file contains definitions of logical file names
* and the associated names of files that may be used for printout of simulation results.
* It also contains various definitions that are useful for the production and layout of the output.

* Prepare some printing possibilities:
$INCLUDE '../../base/output/printout/printinc/print1.inc';

* Overview of input data in text format (see the description at the top of the file inputout):
$ifi %inputdatatxt%== yes        $INCLUDE '../../base/output/printout/printinc/inputout.inc';
$ifi %inputdatatxt%== yesnosolve $INCLUDE '../../base/output/printout/printinc/inputout.inc';
$ifi %inputdatatxt%== yesnosolve $goto ENDOFMODEL
* Unload input data to gdx file:
* Note that the execute_unload command will also identify variables and equations
$ifi %INPUTDATA2GDX%==yes execute_unload '"%relpathInputdata2GDX%INPUTDATAOUT.gdx"';
* Transfer inputdata a seperate Access file (only possible if %INPUTDATA2GDX%==yes):
$ifi %INPUTDATAGDX2MDB%==yes execute '=GDX2ACCESS "%relpathInputdata2GDX%INPUTDATAOUT.gdx"';
* Transfer to Excel, read the identifiers to be transferred from file inputdatagdx2xls.txt (only possible if %INPUTDATA2GDX%==yes):
* Note: presently not working (the relevant data is not set i file inputdatagdx2xls.txt):
* NOTWORKING (i.e. you MUST have '$setglobal INPUTDATAGDX2XLS'): Note:  GAMS version 22.7 and later have better support for this....
*$ifi %INPUTDATAGDX2XLS%==yes  execute  "GDXXRW.EXE Input=%relpathInputdata2GDX%INPUTDATAOUT.gdx  Output=%relpathInputdata2GDX%INPUTDATAOUT.xls  @%relpathModel%inputdatagdx2xls.txt";


*-------------------------------------------------------------------------------
* End: Possibly write input data, prepare output file possibilities
*-------------------------------------------------------------------------------


option FDATA:4:1:1  ; display FDATA ;


$ifi %REShareE%==yes $include '../addons/REShareE/demodata/RESEdata.inc';
$ifi %REShareE%==yes $include '../addons/REShareE/RESEintrn.inc';
$ifi %REShareE%==yes file REShareEPrt4 / '../printout/RESEprt4.out' /;

*$ifi %REShareEH%==yes $include '../addons/REShareEH/demodata/RESEHdata.inc';
*$ifi %REShareEH%==yes $include '../addons/REShareEH/RESEHintrn.inc';
*$ifi %REShareEH%==yes file REShareEHPrt4 / '../printout/RESEHprt4.out' /;

$ifi %REShareEH%==yes
$if     EXIST '../data/RESEHDATA.inc' $INCLUDE         '../data/RESEHDATA.inc';
$ifi %REShareEH%==yes
$if not EXIST '../data/RESEHDATA.inc' $INCLUDE '../../base/data/RESEHDATA.inc';
$ifi %REShareEH%==yes $include '../../base/addons/REShareEH/RESEHintrn.inc';
$ifi %REShareEH%==yes file REShareEHPrt4 / '../printout/RESEHprt4.out' /;

*------------------------------

* A number of input data IDs contain more information than is actually used with the set-up of the model.
* To reduce use of memory space and to reduce further processing and storage of unused data
* some of the larger data IDs are here reduced by resetting unused data items to default values.

DE_VAR_T(RRR,SSS,TTT)$((NOT IR(RRR)) OR (NOT S(SSS)) OR (NOT T(TTT))) = 0;
DH_VAR_T(AAA,SSS,TTT)$((NOT IA(AAA)) OR (NOT S(SSS)) OR (NOT T(TTT))) = 0;
WND_VAR_T(AAA,SSS,TTT)$((NOT IA(AAA)) OR (NOT S(SSS)) OR (NOT T(TTT))) = 0;
SOLE_VAR_T(AAA,SSS,TTT)$((NOT IA(AAA)) OR (NOT S(SSS)) OR (NOT T(TTT))) = 0;
SOLH_VAR_T(AAA,SSS,TTT)$((NOT IA(AAA)) OR (NOT S(SSS)) OR (NOT T(TTT))) = 0;
WTRRSVAR_S(AAA,SSS)$((NOT IA(AAA)) OR (NOT S(SSS))) = 0;
WTRRRVAR_T(AAA,SSS,TTT)$((NOT IA(AAA)) OR (NOT S(SSS)) OR (NOT T(TTT))) = 0;
WAVE_VAR_T(AAA,SSS,TTT)$((NOT IA(AAA)) OR (NOT S(SSS)) OR (NOT T(TTT))) = 0;
X3FX_VAR_T(RRR,SSS,TTT)$((NOT IR(RRR)) OR (NOT S(SSS)) OR (NOT T(TTT))) = 0;
DEF_STEPS(RRR,SSS,TTT,DF_QP,DEF)$((NOT IR(RRR)) OR (NOT S(SSS)) OR (NOT T(TTT))) = 0;
DEF_STEPS(IR,S,T,DF_QP,DEF)$((NOT DEF_D1(DEF) AND (NOT DEF_D2(DEF)) AND (NOT DEF_D3(DEF)) AND (NOT DEF_U1(DEF)) AND (NOT DEF_U2(DEF)) AND (NOT DEF_U3(DEF)))) = 0;
DHF_STEPS(AAA,SSS,TTT,DF_QP,DHF)$((NOT IA(AAA)) OR (NOT S(SSS)) OR (NOT T(TTT))) = 0;
DHF_STEPS(IA,S,T,DF_QP,DHF)$((NOT DHF_D1(DHF) AND (NOT DHF_D2(DHF)) AND (NOT DHF_D3(DHF)) AND (NOT DHF_U1(DHF)) AND (NOT DHF_U2(DHF)) AND (NOT DHF_U3(DHF)))) = 0;
$ifi %GKRATE_DOL%==AAA_GGG_SSS_TTT     GKRATE(AAA,GGG,SSS,TTT)$((NOT IA(AAA)) OR (NOT G(GGG)) OR (NOT S(SSS)) OR (NOT T(TTT))) = 0;
$ifi %GKRATE_DOL%==AAA_GGG_SSS         GKRATE(AAA,GGG,SSS)$((NOT IA(AAA)) OR (NOT G(GGG)) OR (NOT S(SSS))) = 0;
GKNMAX(YYY,AAA,GGG)$((NOT Y(YYY)) OR (NOT IA(AAA))) = 0;

*------------------------------

* Unload input data to gdx and possibly MDB files.
* Note that this is a compile time operation, such that only the 'direct' data
* definitions (and no assignments) are reflected:

execute_unload '%relpathModel%..\output\temp\1INPUT_B303.gdx';

* No need to proceed further if there are compilation errors in the data:
$ifi not errorfree $abort "Balmorel execution aborted because of data errors"


*------------------------------

$ifi not %H2%==yes $goto H2demand
Parameter DH2_Y (RRR);
DH2_Y(RRR)=sum(Y,DH2(Y,RRR))/card(Y);
IDH2_T_Y(RRR,SSS,TTT)=((DH2_Y(RRR)*DH2_VAR_T(RRR,SSS,TTT))/IDH2_SUMST(RRR))$IDH2_SUMST(RRR);
DISPLAY DH2_Y,DH2_VAR_T,IDH2_SUMST,IDH2_T_Y;
$label H2demand

*deltamarginal coding
*Implementation of deltamarginal application in order to calculate inflexible marginals related to electricity and heat

Parameter DELTA_E(RRR,SSS,TTT)   'Marginal increase in electricity production'   %semislash%
$if     EXIST '../data/DELTA_E.inc' $INCLUDE      '../data/DELTA_E.inc';
$if not EXIST '../data/DELTA_E.inc' $INCLUDE '../../base/data/DELTA_E.inc';
%semislash%;


Parameter DELTA_H(AAA,SSS,TTT) 'Marginal increase in heat production'   %semislash%
$if     EXIST '../data/DELTA_H.inc' $INCLUDE      '../data/DELTA_H.inc';
$if not EXIST '../data/DELTA_H.inc' $INCLUDE '../../base/data/DELTA_H.inc';
%semislash%;


*-------------------------------------------------------------------------------
*  Declaration of VARIABLES:
*-------------------------------------------------------------------------------


FREE     VARIABLE VOBJ                           'Objective function value (MMoney)';
POSITIVE VARIABLE VGE_T(AAA,G,S,T)               'Electricity generation (MW), existing units';
POSITIVE VARIABLE VGEN_T(AAA,G,S,T)              'Electricity generation (MW), new units';
POSITIVE VARIABLE VGH_T(AAA,G,S,T)               'Heat generation (MW), existing units';
POSITIVE VARIABLE VGF_T(AAA,G,S,T)               'Fuel consumption rate (MW), existing units'
POSITIVE VARIABLE VX_T(IRRRE,IRRRI,S,T)          'Electricity export from region IRRRE to IRRRI (MW)';
POSITIVE VARIABLE VGHN_T(AAA,G,S,T)              'Heat generation (MW), new units';
POSITIVE VARIABLE VGFN_T(AAA,G,S,T)              'Fuel consumption rate (MW), new units'
POSITIVE VARIABLE VGKN(AAA,G)                    'New generation capacity (MW)';
POSITIVE VARIABLE VXKN(IRRRE,IRRRI)              'New electricity transmission capacity (MW)';
POSITIVE VARIABLE VDECOM(AAA,G)                  'Decommissioned capacity(MW)';
POSITIVE VARIABLE VDEF_T(RRR,S,T,DEF)            'Flexible electricity demands (MW)';
POSITIVE VARIABLE VDHF_T(AAA,S,T,DHF)            'Flexible heat demands (MW)';
POSITIVE VARIABLE VGHYPMS_T(AAA,S,T)             'Contents of pumped hydro storage (MWh)';
POSITIVE VARIABLE VHYRS_S(AAA,S)                 'Hydro energy equivalent at the start of the season (MWh)';
POSITIVE VARIABLE VESTOLOADT(AAA,S,T)            'Loading of electricity storage (MW)';
POSITIVE VARIABLE VHSTOLOADT(AAA,S,T)            'Loading of heat storage (MW)';
POSITIVE VARIABLE VESTOVOLT(AAA,S,T)             'Electricity storage contents at beginning of time segment (MWh)';
POSITIVE VARIABLE VHSTOVOLT(AAA,S,T)             'Heat storage contents at beginning of time segment (MWh)';
POSITIVE VARIABLE VQEEQ(RRR,S,T,IPLUSMINUS)      'Feasibility in electricity balance equation QEEQ (MW)';
POSITIVE VARIABLE VQHEQ(AAA,S,T,IPLUSMINUS)      'Feasibility in heat balance equantion QHEQ (MW)';
POSITIVE VARIABLE VQESTOVOLT(AAA,S,T,IPLUSMINUS) 'Feasibility in electricity storage equation QESTOVOLT (MWh)';
POSITIVE VARIABLE VQHSTOVOLT(AAA,S,T,IPLUSMINUS) 'Feasibility in heat storage equation VQHSTOVOLT (MWh)';
POSITIVE VARIABLE VQHYRSSEQ(AAA,S,IPLUSMINUS)    'Feasibility of QHYRSSEQ (MWh)';
POSITIVE VARIABLE VQGEQCF(C,FFF,IPLUSMINUS)      'Feasibility in Requered fuel usage per country constraint (MWh)'
POSITIVE VARIABLE VQGMINCF(C,FFF)                'Feasibility in Minimum fuel usage per country constraint (MWh)'
POSITIVE VARIABLE VQGMAXCF(C,FFF)                'Feasibility in Maximum fuel usage per country constraint (MWh)'
POSITIVE VARIABLE VQGEQRF(RRR,FFF,IPLUSMINUS)    'Feasibility in Requered fuel usage per region constraint (MWh)'
POSITIVE VARIABLE VQGMAXRF(RRR,FFF)              'Feasibility in Minimum fuel usage per region constraint (MWh)'
POSITIVE VARIABLE VQGMINRF(RRR,FFF)              'Feasibility in Maximum fuel usage per region constraint (MWh)'
POSITIVE VARIABLE VQGEQAF(AAA,FFF,IPLUSMINUS)    'Feasibility in Required fuel usage per area constraint (MWh)'
POSITIVE VARIABLE VQGMAXAF(AAA,FFF)              'Feasibility in Minimum fuel usage per area constraint (MWh)'
POSITIVE VARIABLE VQGMINAF(AAA,FFF)              'Feasibility in Maximum fuel usage per area constraint (MWh)'
POSITIVE VARIABLE VQXK(IRRRE,IRRRI,S,T,IPLUSMINUS)'Feasibility in Transmission capacity constraint (MW)'

*-------------------------------------------------------------------------------
*----- Any variables for addon to be placed here: ------------------------------
*-------------------------------------------------------------------------------
* This file includes:
* [list equations]
* These add-on variables pertain to price sensitive electricity exchange with
* third countries.
$ifi %X3V%==yes $include '../../base/addons/x3v/model/x3vvariables.inc';
* These add-on variables pertain heat transmission
$ifi %HEATTRANS%==yes $include '../../base/addons/heattrans/model/htvariables.inc';
* These add-on variables pertain district heating
$ifi %FV%==yes $include '../../base/addons/Fjernvarme/var_fv.inc';

$ifi %UnitComm%==yes $include '../../base/addons/unitcommitment/vars.inc';
$ifi %UnitComm%==yes $include '../../base/addons/unitcommitment/eqns.inc';

$ifi %AGKNDISC%==yes  $include '../../base/addons/agkndisc/agkndiscvariables.inc';

$ifi %POLICIES%==yes $include '../../base/addons/policies/pol_var.inc';

$ifi %SYSTEMCONST%==yes $include '../../base/addons/SystemConst/var_syscon.inc';

$ifi %HYRSBB123%==quantprice  $include "..\..\base\addons\hyrsbb123\hyrsbb123variables.inc";

* These add-on variables pertain to hydrogen technology.
$ifi %H2%==yes $include '../../base/addons/Hydrogen/H2variables.inc';

*-------------------------------------------------------------------------------
*----- End: Any variables for addon to be placed here: -------------------------
*-------------------------------------------------------------------------------

* Note that intervals for variables may be set later (as .lo, .up  and/or .fx).

*-------------------------------------------------------------------------------
*  End: Declaration of VARIABLES
*-------------------------------------------------------------------------------



*-------------------------------------------------------------------------------
*  Declaration and definition of EQUATIONS:
*-------------------------------------------------------------------------------

* Equation declarations:

EQUATIONS
   QOBJ                   'Objective function'
   QEEQ(RRR,S,T)          'Electricity generation equals demand (MW)'
   QHEQ(AAA,S,T)          'Heat generation equals demand (MW)'
   QGFEQ(AAA,G,S,T)       'Calculate fuel consumption, existing units (MW)'
   QGFNEQ(AAA,G,S,T)      'Calculate fuel consumption, new units (MW)'
   QGCBGBPR(AAA,G,S,T)    'CHP generation (back pressure) limited by Cb-line (MW)'
   QGCBGEXT(AAA,G,S,T)    'CHP generation (extraction) limited by Cb-line (MW)'
   QGCVGEXT(AAA,G,S,T)    'CHP generation (extraction) limited by Cv-line (MW)'
   QGGETOH(AAA,G,S,T)     'Electric heat generation (MW)'
   QGNCBGBPR(AAA,G,S,T)   'CHP generation (back pressure) Cb-line, new (MW)'
   QGNCBGEXT(AAA,G,S,T)   'CHP generation (extraction) Cb-line, new (MW)'
   QGNCVGEXT(AAA,G,S,T)   'CHP generation (extraction) Cv-line, new (MW)'
   QGNGETOH(AAA,G,S,T)    'Electric heat generation, new (MW)'
   QGEKNT(AAA,G,S,T)      'Generation on new electricity cap, limited by capacity (MW)'
   QGHKNT(AAA,G,S,T)      'Generation on new IGHH cap, limited by capacity (MW)'
   QGKNHYRR(AAA,G,S,T)    'Generation on new hydro-ror limited by capacity and water (MW)'
   QGKNWND(AAA,G,S,T)     'Generation on new windpower limited by capacity and wind (MW)'
   QGKNSOLE(AAA,G,S,T)    'Generation on new solarpower limited by capacity and sun (MW)'
   QGKNSOLH(AAA,G,S,T)    'Generation on new solarheat limited by capacity and sun (MW)'
   QGKNWAVE(AAA,G,S,T)    'Generation on new wavepower limited by cap and waves (MW)'
   QHYRSSEQ(AAA,S)        'Hydropower with reservoir seasonal dynamic energy constraint (MWh)'
   QHYRSMINVOL(AAA,S)     'Hydropower reservoir - minimum level (MWh)'
   QHYRSMAXVOL(AAA,S)     'Hydropower reservoir - maximum level (MWh)'
   QHYMAXG(AAA,S,T)       'Regulated and unregulated hydropower production lower than capacity'
   QESTOVOLT(AAA,S,T)     'Electricty storage dynamic equation (MWh)'
   QHSTOVOLT(AAA,S,T)     'Heat storage dynamic equation (MWh)'
   QHSTOVOLT_S(AAA,S,T)   'Seasonal heat storage dynamic equation (MWh)'
   QHSTOLOADTLIM(AAA,S,T) 'Upper limit to heat storage loading (model Balbase2 only) (MW)'
   QESTOLOADTLIM(AAA,S,T) 'Upper limit to electricity storage loading (model Balbase2 only) (MW)'
   QHSTOVOLTLIM(AAA,S,T)  'Heat storage capacity limit (model Balbase2 only) (MWh)'
   QESTOVOLTLIM(AAA,S,T)  'Electricity storage capacity limit (model Balbase2 only) (MWh)'
   QKFUELC(C,FFF)         'Total capacity using fuel FFF is limited in country (MW)'
   QKFUELR(RRR,FFF)       'Total capacity using fuel FFF is limited in region (MW)'
   QKFUELA(AAA,FFF)       'Total capacity using fuel FFF is limited in area (MW)'
   QFGEMINC(C,FFF)         'Minimum electricity generation by fuel per country (MWh)'
   QFGEMAXC(C,FFF)         'Maximum electricity generation by fuel per country (MWh)'
   QFGEMINR(RRR,FFF)      'Minimum electricity generation by fuel per region (MWh)'
   QFGEMAXR(RRR,FFF)      'Maximum electricity generation by fuel per region (MWh)'
   QFGEMINA(AAA,FFF)      'Minimum electricity generation by fuel per area (MWh)'
   QFGEMAXA(AAA,FFF)      'Maximum electricity generation by fuel per area (MWh)'
   QGMINCF(C,FFF)         'Minimum fuel usage per country constraint (MWh)'
   QGMAXCF(C,FFF)         'Maximum fuel usage per country constraint (MWh)'
   QGEQCF(C,FFF)          'Required fuel usage per country constraint (MWh)'
   QGEQCF_S(C,FFF,S)      'Seasonal required fuel usage per country constraint(MWh)'
   QGMINRF(RRR,FFF)       'Minimum fuel usage per region constraint (MWh)'
   QGMAXRF(RRR,FFF)       'Maximum fuel usage per region constraint (MWh)'
   QGEQRF(RRR,FFF)        'Required fuel usage per region constraint (MWh)'
   QGEQRF_S(RRR,FFF,S)    'Seasonal required fuel usage per region constraint (MWh)'
   QGMINAF(AAA,FFF)       'Minimum fuel usage per area constraint (MWh)'
   QGMAXAF(AAA,FFF)       'Maximum fuel usage per area constraint (MWh)'
   QGEQAF(AAA,FFF)        'Required fuel usage per area constraint (MWh)'
   QGEQAF_S(AAA,FFF,S)    'Seasonal required fuel usage per area constraint (MWh)'
   QXK(IRRRE,IRRRI,S,T)   'Transmission capacity constraint (MW)'
   QXMAXINV(IRRRE,IRRRI)  'Limit of new transmission capacity (MW)'
   QFMAXINVEST(C,FFF)     'Limit on investment in capacity defined per fuel'
   QLIMCO2(C)             'Limit on annual CO2-emission (ton)'
   QLIMSO2(C)             'Limit on annual SO2 emission (ton)'
   QLIMNOX(C)             'Limit on annual NOx emission (kg)'
   QGMAXINVEST2(C,G)      'Maximum model generated capacity increase from one year to the next (MW)'
* Raffaele
   QONWIND(C)     'Onshore wind maximum installed capacity of the scenario'
   QOFFWIND(C)    'Offshore wind maximum installed capacity of the scenario'
   QSOLE(C)       'Solar PV maximum installed capacity of the scenario'
   QSOLH(C)       'Solar thermal maximum installed capacity of the scenario'
*QHP(C)         'Heat pumps maximum installed capacity of the scenario'

*-------------------------------------------------------------------------------
*----- Any equations declarations for addon to be placed here: -----------------
*-------------------------------------------------------------------------------



$ifi %PLANTCLOSURES%==yes   QGEKOT(AAA,G,S,T)      'Generation on old electricity capacity, limited by capacity';
$ifi %PLANTCLOSURES%==yes   QGHKOT(AAA,G,S,T)      'Generation on old electricity capacity, limited by capacity';

$ifi %PLANTCLOSURES%==yes   QGKOHYRR(AAA,G,S,T)    'Generation on old hydro-ror limited by capacity and water';
$ifi %PLANTCLOSURES%==yes   QGKOWND(AAA,G,S,T)     'Generation on old windpower limited by capacity and wind';
$ifi %PLANTCLOSURES%==yes   QGKOSOLE(AAA,G,S,T)    'Generation on old solarpower limited by capacity and sun';
$ifi %PLANTCLOSURES%==yes   QGKOWAVE(AAA,G,S,T)    'Generation on old wavepower limited by cap and waves';

$ifi %REShareE%==yes QREShareE(CCCREShareE)        'Minimum share of electricity production from renewable electricity';
$ifi %REShareEH%==yes QREShareEH(CCCREShareEH)     'Minimum share of electricity and heat production from renewable electricity';

$ifi %AGKNDISC%==yes QAGKNDISC01(AAA,G)            'At most one of the specified discrete capacity size investments is chosen (Addon AGKNDISC)';
$ifi %AGKNDISC%==yes QAGKNDISCCONT(AAA,G)          'The invested capacity must be one of the specified sizes or zero (MW) (Addon AGKNDISC)';

$ifi %BB3%==yes $ifi not %HYRSBB123%==none         $include "..\..\base\addons\hyrsbb123\hyrsbb123equations.inc";
;

*-------------------------------------------------------------------------------
*----- End: Any equations declarations for addon to be placed here -------------
*-------------------------------------------------------------------------------




* Equation definitions:
*----- The objective function QOBJ: --------------------------------------------
QOBJ ..

   VOBJ =E=


*IOF0000001*(Note: in final version there will be no factor, because scaling of the equation is used.
IOF1_*(

* Cost of fuel consumption during the year:

     SUM((IAGK_Y(IA,G),FFF)$(GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM')),
                 IFUELP_Y(IA,FFF) * IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGF_T(IA,G,IS3,T)))

    +SUM((IAGKN(IA,G),FFF)$(GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM')),
                 IFUELP_Y(IA,FFF) * IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGFN_T(IA,G,IS3,T)))


* Operation and maintainance cost:

   + SUM(IAGK_Y(IA,IGE), GOMVCOST(IA,IGE) * SUM((IS3,T), IHOURSINST(IS3,T)*
     VGE_T(IA,IGE,IS3,T)))

   + SUM(IAGK_Y(IA,IGNOTETOH(IGH)), GOMVCOST(IA,IGNOTETOH) * SUM((IS3,T), IHOURSINST(IS3,T)*
     GDATA(IGNOTETOH,'GDCV')*(VGH_T(IA,IGNOTETOH,IS3,T))))

   + SUM(IAGKN(IA,IGE), GOMVCOST(IA,IGE) * SUM((IS3,T), IHOURSINST(IS3,T)*
     VGEN_T(IA,IGE,IS3,T)))

   + SUM(IAGKN(IA,IGNOTETOH(IGH)), GOMVCOST(IA,IGNOTETOH) * SUM((IS3,T), IHOURSINST(IS3,T)*
     GDATA(IGNOTETOH,'GDCV')*(VGHN_T(IA,IGNOTETOH,IS3,T))))

$ifi not %PLANTCLOSURES%==yes + IOF1000*(SUM(IAGK_Y(IA,G),(IGKVACCTOY(IA,G) + IGKFX_Y(IA,G))*GOMFCOST(IA,G)))
$ifi %PLANTCLOSURES%==yes     + IOF1000*(SUM(IAGK_Y(IA,G),(IGKVACCTOY(IA,G) + IGKFX_Y(IA,G) - VDECOM(IA,G))*GOMFCOST(IA,G)))

   + IOF1000*(SUM(IAGKN(IA,G),VGKN(IA,G)*GOMFCOST(IA,G)))

*  Hydro with storage seasonal price profile:

   + SUM(IAGK_Y(IA,IGHYRS), SUM((IS3,T), HYPPROFILS(IA,IS3)* IHOURSINST(IS3,T)
     * VGE_T(IA,IGHYRS,IS3,T)))

   + SUM(IAGKN(IA,IGHYRS), SUM((IS3,T), HYPPROFILS(IA,IS3)* IHOURSINST(IS3,T)
     * VGEN_T(IA,IGHYRS,IS3,T)))

$ifi not %bb3%==yes $goto not_waterval
$ifi not %WATERVAL%==yes $goto not_waterval
   + SUM(IAGK_Y(IA,IGHYRS),SUM((IS3,T), IWATERVAL(IA,IS3)* IHOURSINST(IS3,T)
     * VGE_T(IA,IGHYRS,IS3,T)))

   + SUM(IAGKN(IA,IGHYRS), SUM((IS3,T), IWATERVAL(IA,IS3)* IHOURSINST(IS3,T)
     * VGEN_T(IA,IGHYRS,IS3,T)))
$label not_waterval


* Transmission cost:

   + SUM((IRE,IRI)$(IXKINI_Y(IRE,IRI) OR IXKN(IRE,IRI) OR IXKN(IRI,IRE)),
       SUM((IS3,T), IHOURSINST(IS3,T) * (VX_T(IRE,IRI,IS3,T) * XCOST(IRE,IRI))))

* Investment costs, new generation and storage capacity:

    + IOF1000000*(SUM((IAGKN(IA,G)), VGKN(IA,G)*GINVCOST(IA,G)*
$ifi %YIELDREQUIREMENT%==yes YIELDREQ(G)*
      (SUM(C$ICA(C,IA),ANNUITYC(C)))))


$ifi %AGKNDISC%==yes  $include '../../base/addons/agkndisc/agkndiscaddobj.inc';

* Investment tax (neg. subsidy), new electricity generation capacity:

    + IOF1000000*(SUM((IAGKN(IA,G)), VGKN(IA,G)*ITAX_KN(IA,G)*(SUM(C$ICA(C,IA),ANNUITYC(C)))))

* Investment costs, new transmission capacity
* (the average of the annuities for the two countries in question
* is used for international transmission):

    + SUM((IRE,IRI)$(IXKN(IRI,IRE) OR IXKN(IRE,IRI)), VXKN(IRE,IRI)*XINVCOST(IRE,IRI)*
      (IOF05*(SUM(C$CCCRRR(C,IRE),ANNUITYC(C))+SUM(C$CCCRRR(C,IRI),ANNUITYC(C)))))


* Emission taxes

    + SUM(C, SUM(IAGK_Y(IA,G)$ICA(C,IA), SUM((IS3,T), IHOURSINST(IS3,T) * (IM_CO2(G)*IOF0001) * IOF3P6 * VGF_T(IA,G,IS3,T))  * ITAX_CO2_Y(C)))
    + SUM(C, SUM(IAGKN(IA,G)$ICA(C,IA),  SUM((IS3,T), IHOURSINST(IS3,T) * (IM_CO2(G)*IOF0001) * IOF3P6 * VGFN_T(IA,G,IS3,T)) * ITAX_CO2_Y(C)))

    + SUM(C, SUM(IAGK_Y(IA,G)$ICA(C,IA), SUM((IS3,T), IHOURSINST(IS3,T) * (IM_SO2(G)*IOF0001) * IOF3P6 * VGF_T(IA,G,IS3,T))  * ITAX_SO2_Y(C)))
    + SUM(C, SUM(IAGKN(IA,G)$ICA(C,IA),  SUM((IS3,T), IHOURSINST(IS3,T) * (IM_SO2(G)*IOF0001) * IOF3P6 * VGFN_T(IA,G,IS3,T)) * ITAX_SO2_Y(C)))

    + SUM(C, SUM(IAGK_Y(IA,G)$ICA(C,IA), SUM((IS3,T), IHOURSINST(IS3,T) * (GDATA(G,'GDNOX')*IOF0000001)* IOF3P6 * VGF_T(IA,G,IS3,T))  * ITAX_NOX_Y(C)))
    + SUM(C, SUM(IAGKN(IA,G)$ICA(C,IA),  SUM((IS3,T), IHOURSINST(IS3,T) * (GDATA(G,'GDNOX')*IOF0000001)* IOF3P6 * VGFN_T(IA,G,IS3,T)) * ITAX_NOX_Y(C)))

* Fuel taxes

    + SUM((C,FFF,IS3,T), SUM(IAGK_Y(IA,G)$((GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))  AND ICA(C,IA)),
         IHOURSINST(IS3,T) * TAX_F(FFF,C) * IOF3P6 * VGF_T(IA,G,IS3,T)))

    + SUM((C,FFF,IS3,T), SUM(IAGKN(IA,G)$((GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM')) AND ICA(C,IA)),
         IHOURSINST(IS3,T) * TAX_F(FFF,C) * IOF3P6 * VGFN_T(IA,G,IS3,T)))

* More fuel taxes on technology types.

   + SUM((IA,G,IS3,T)$(IAGK_Y(IA,G) and ITAX_GF(IA,G)),
      ITAX_GF(IA,G)*IHOURSINST(IS3,T)  * IOF3P6 * VGF_T(IA,G,IS3,T))

   + SUM((IA,G,IS3,T)$(IAGKN(IA,G) and ITAX_GF(IA,G)),
      ITAX_GF(IA,G)*IHOURSINST(IS3,T)  * IOF3P6 * VGFN_T(IA,G,IS3,T))

* Electricity generation taxes (and subsidies).
   + SUM((IA,IGNOTETOH(IGE),IS3,T)$(IAGK_Y(IA,IGE) and ITAX_GE(IA,IGE)),
      ITAX_GE(IA,IGE)*IHOURSINST(IS3,T)  * VGE_T(IA,IGE,IS3,T))

   + SUM((IA,IGNOTETOH(IGE),IS3,T)$(IAGKN(IA,IGE) and ITAX_GE(IA,IGE)),
      ITAX_GE(IA,IGE)*IHOURSINST(IS3,T)  * VGEN_T(IA,IGE,IS3,T))

* Heat generation taxes.

   + SUM((IA,IGH,IS3,T)$(IAGK_Y(IA,IGH) and ITAX_GH(IA,IGH)),
      ITAX_GH(IA,IGH)*IOF3P6*VGH_T(IA,IGH,IS3,T)*IHOURSINST(IS3,T))

   + SUM((IA,IGH,IS3,T)$(IAGKN(IA,IGH) and ITAX_GH(IA,IGH)),
      ITAX_GH(IA,IGH)*IOF3P6*VGHN_T(IA,IGH,IS3,T)*IHOURSINST(IS3,T))


* Changes in consumers' utility relative to electricity consumption:

   + SUM(IR,
     SUM((IS3,T), IHOURSINST(IS3,T)
     * (SUM(DEF_D1, VDEF_T(IR,IS3,T,DEF_D1)* IDEFP_T(IR,IS3,T,DEF_D1)  )
     +  SUM(DEF_D2, VDEF_T(IR,IS3,T,DEF_D2)* IDEFP_T(IR,IS3,T,DEF_D2)  )
     +  SUM(DEF_D3, VDEF_T(IR,IS3,T,DEF_D3)* IDEFP_T(IR,IS3,T,DEF_D3)  )
     )

     )
     )

   - SUM(IR,
     SUM((IS3,T), IHOURSINST(IS3,T)
     * (SUM(DEF_U1, VDEF_T(IR,IS3,T,DEF_U1)* IDEFP_T(IR,IS3,T,DEF_U1)  )
     +  SUM(DEF_U2, VDEF_T(IR,IS3,T,DEF_U2)* IDEFP_T(IR,IS3,T,DEF_U2)  )
     +  SUM(DEF_U3, VDEF_T(IR,IS3,T,DEF_U3)* IDEFP_T(IR,IS3,T,DEF_U3)  )))
     )

* Changes in consumers' utility relative to heat consumption:

   + SUM(IA,
     SUM((IS3,T), IHOURSINST(IS3,T)
     * (SUM(DHF_D1, VDHF_T(IA,IS3,T,DHF_D1)* IDHFP_T(IA,IS3,T,DHF_D1)  )
     +  SUM(DHF_D2, VDHF_T(IA,IS3,T,DHF_D2)* IDHFP_T(IA,IS3,T,DHF_D2)  )
     +  SUM(DHF_D3, VDHF_T(IA,IS3,T,DHF_D3)* IDHFP_T(IA,IS3,T,DHF_D3)  )))
      )

   - SUM(IA,
     SUM((IS3,T), IHOURSINST(IS3,T)
     * (SUM(DHF_U1, VDHF_T(IA,IS3,T,DHF_U1)* IDHFP_T(IA,IS3,T,DHF_U1)  )
     +  SUM(DHF_D2, VDHF_T(IA,IS3,T,DHF_D2)* IDHFP_T(IA,IS3,T,DHF_D2)  )
     +  SUM(DHF_D3, VDHF_T(IA,IS3,T,DHF_D3)* IDHFP_T(IA,IS3,T,DHF_D3)  )))
     )


* Infeasibility penalties:
   + PENALTYQ*(
$ifi %BB1%==yes    +SUM((IA,IS3)$SUM(IGHYRS,IAGK_Y(IA,IGHYRS)),(VQHYRSSEQ(IA,IS3,'IMINUS')+VQHYRSSEQ(IA,IS3,'IPLUS')))
$ifi %BB2%==yes    +SUM((IA,IS3)$SUM(IGHYRS,IAGK_Y(IA,IGHYRS)),(VQHYRSSEQ(IA,IS3,'IMINUS')+VQHYRSSEQ(IA,IS3,'IPLUS')))
               +SUM((IA,IS3,T)$SUM(IGHSTOALL(G), IAGK_Y(IA,IGHSTOALL) or IAGKN(IA,IGHSTOALL)),(VQHSTOVOLT(IA,IS3,T,'IMINUS')+VQHSTOVOLT(IA,IS3,T,'IPLUS')))
               +SUM((IA,IS3,T)$SUM(IGESTO, IAGK_Y(IA,IGESTO) or IAGKN(IA,IGESTO)),(VQESTOVOLT(IA,IS3,T,'IMINUS')+VQESTOVOLT(IA,IS3,T,'IPLUS')))
               +SUM((IR,IS3,T),(VQEEQ(IR,IS3,T,'IMINUS')+VQEEQ(IR,IS3,T,'IPLUS')))
               +SUM((IA,IS3,T)$IDH_SUMST(IA),(VQHEQ(IA,IS3,T,'IMINUS')+VQHEQ(IA,IS3,T,'IPLUS')))

               +SUM((C,FFF)$IGEQF_Y(C,FFF) , VQGEQCF(C,FFF,'IPLUS')+VQGEQCF(C,FFF,'IMINUS')    )
               +SUM((C,FFF)$IGMINF_Y(C,FFF), VQGMINCF(C,FFF)      )
               +SUM((C,FFF)$IGMAXF_Y(C,FFF), VQGMAXCF(C,FFF)      )

               +SUM((IR,FFF)$IGEQF_Y(IR,FFF) , VQGEQRF(IR,FFF,'IPLUS')+VQGEQRF(IR,FFF,'IMINUS') )
               +SUM((IR,FFF)$IGMINF_Y(IR,FFF), VQGMAXRF(IR,FFF)    )
               +SUM((IR,FFF)$IGMAXF_Y(IR,FFF), VQGMINRF(IR,FFF)    )

               +SUM((IA,FFF)$IGEQF_Y(IA,FFF) , VQGEQAF(IA,FFF,'IPLUS')+VQGEQAF(IA,FFF,'IMINUS') )
               +SUM((IA,FFF)$IGMINF_Y(IA,FFF), VQGMAXAF(IA,FFF)    )
               +SUM((IA,FFF)$IGMAXF_Y(IA,FFF), VQGMINAF(IA,FFF)    )

               +SUM((IRE,IRI,S,T)$((IXKINI_Y(IRE,IRI) OR IXKN(IRI,IRE) OR IXKN(IRE,IRI)) AND (IXKINI_Y(IRE,IRI) NE INF) ), VQXK(IRE,IRI,S,T,'IPLUS')+VQXK(IRE,IRI,S,T,'IMINUS') )
              )

* Add-on objective function contributions

$ifi %X3V%==yes $include '../../base/addons/x3v/model/x3vobj.inc';
* This file contains Heat transmission induced additions to the objective function.
$ifi %HEATTRANS%==yes $include '../../base/addons/heattrans/model/htcosts.inc';
* This file contains district heating induced additions to the objective function.
$ifi %FV%==yes $include '../../base/addons/Fjernvarme/cost_fv.inc';
* This file contains hydrogen induced additions to the objective function.
$ifi %H2%==yes $include '../../base/addons/Hydrogen/H2costs.inc'

* Unit commitmen add-on
$ifi %UnitComm%==yes      $include '../../base/addons/unitcommitment/qobjadd.inc';

$ifi %POLICIES%==yes   $include '../../base/addons/policies/pol_cost.inc';
$ifi %SYSTEMCONST%==yes   $include '../../base/addons/SystemConst/cost_syscon.inc';
$ifi %UnitComm%==yes      $include '../../base/addons/unitcommitment/qobjadd.inc';

$ifi %BB3%==yes  $ifi %HYRSBB123%==price      $include  '../../base/addons/hyrsbb123/hyrsbb123addobj.inc'
$ifi %BB3%==yes  $ifi %HYRSBB123%==quantprice $include  '../../base/addons/hyrsbb123/hyrsbb123addobj.inc'


)
;
*----- End: The objective function QOBJ ----------------------------------------


*--------------------------Balance equations for electricity and heat ----------

QEEQ(IR,IS3,T) ..

      SUM(IAGK_Y(IA,IGE)$((RRRAAA(IR,IA)) AND IGNOTETOH(IGE)), VGE_T(IA,IGE,IS3,T))
    - SUM(IAGK_Y(IA,IGE)$((RRRAAA(IR,IA)) AND IGETOH(IGE)), VGE_T(IA,IGE,IS3,T))
    + SUM(IAGKN(IA,IGE)$((RRRAAA(IR,IA)) AND IGNOTETOH(IGE)), VGEN_T(IA,IGE,IS3,T))
    - SUM(IAGKN(IA,IGE)$((RRRAAA(IR,IA)) AND IGETOH(IGE)), VGEN_T(IA,IGE,IS3,T))
    + SUM(IRE$(IXKINI_Y(IRE,IR) OR IXKN(IRE,IR) OR IXKN(IR,IRE)), VX_T(IRE,IR,IS3,T)*(1-XLOSS(IRE,IR)))
    - SUM(IA$(RRRAAA(IR,IA) AND SUM(IGESTO,IAGK_Y(IA,IGESTO))),VESTOLOADT(IA,IS3,T))
$ifi %X3V%==yes + SUM(X3VPLACE$X3VX(IR,X3VPLACE),SUM(X3VSTEP,VX3VIM_T(IR,X3VPLACE,X3VSTEP,IS3,T)))
$ifi %H2%==yes - SUM(IAGK_Y(IA,IGETOH2)$(RRRAAA(IR,IA)), VGE_T(IA,IGETOH2,IS3,T))
$ifi %H2%==yes - SUM(IAGKN(IA,IGETOH2)$(RRRAAA(IR,IA)), VGEN_T(IA,IGETOH2,IS3,T))
    =E=
      IX3FX_T_Y(IR,IS3,T)
    + (   (IDE_T_Y(IR,IS3,T)

         + SUM(DEF_U1$IDEFP_T(IR,IS3,T,DEF_U1),VDEF_T(IR,IS3,T,DEF_U1) )
         - SUM(DEF_D1$IDEFP_T(IR,IS3,T,DEF_D1),VDEF_T(IR,IS3,T,DEF_D1) )
         + SUM(DEF_U2$IDEFP_T(IR,IS3,T,DEF_U2),VDEF_T(IR,IS3,T,DEF_U2) )
         - SUM(DEF_D2$IDEFP_T(IR,IS3,T,DEF_D2),VDEF_T(IR,IS3,T,DEF_D2) )
         + SUM(DEF_U3$IDEFP_T(IR,IS3,T,DEF_U3),VDEF_T(IR,IS3,T,DEF_U3) )
         - SUM(DEF_D3$IDEFP_T(IR,IS3,T,DEF_D3),VDEF_T(IR,IS3,T,DEF_D3) )
     )/(1-DISLOSS_E(IR)))
      + SUM(IRI$(IXKINI_Y(IR,IRI) OR IXKN(IR,IRI) OR IXKN(IRI,IR)),VX_T(IR,IRI,IS3,T))
$ifi %X3V%==yes + SUM(X3VPLACE$X3VX(IR,X3VPLACE),SUM(X3VSTEP,VX3VEX_T(IR,X3VPLACE,X3VSTEP,IS3,T)))
      - VQEEQ(IR,IS3,T,'IMINUS') + VQEEQ(IR,IS3,T,'IPLUS')
 ;


QHEQ(IA,IS3,T)$(IDH_SUMST(IA) NE 0) ..

     SUM(IGBPR$IAGK_Y(IA,IGBPR),VGH_T(IA,IGBPR,IS3,T))
   + SUM(IGBPR$IAGKN(IA,IGBPR),VGHN_T(IA,IGBPR,IS3,T))
   + SUM(IGEXT$IAGK_Y(IA,IGEXT),VGH_T(IA,IGEXT,IS3,T))
   + SUM(IGEXT$IAGKN(IA,IGEXT),VGHN_T(IA,IGEXT,IS3,T))
   + SUM(IGHH$IAGK_Y(IA,IGHH),VGH_T(IA,IGHH,IS3,T))
   + SUM(IGHH$IAGKN(IA,IGHH),VGHN_T(IA,IGHH,IS3,T))
   + SUM(IGETOH$IAGK_Y(IA,IGETOH),VGH_T(IA,IGETOH,IS3,T))
   + SUM(IGETOH$IAGKN(IA,IGETOH),VGHN_T(IA,IGETOH,IS3,T))
   + SUM(IGESTO$IAGK_Y(IA,IGESTO),(VGE_T(IA,IGESTO,IS3,T)/GDATA(IGESTO,'GDCB'))$GDATA(IGESTO,'GDCB'))  /* This may disappear */
   - VHSTOLOADT(IA,IS3,T)$SUM(IGHSTO$(IAGK_Y(IA,IGHSTO) or IAGKN(IA,IGHSTO)),1)
    =E=
     (IDH_T_Y(IA,IS3,T)
        + SUM(DHF_U1$IDHFP_T(IA,IS3,T,DHF_U1),VDHF_T(IA,IS3,T,DHF_U1) )
        - SUM(DHF_D1$IDHFP_T(IA,IS3,T,DHF_D1),VDHF_T(IA,IS3,T,DHF_D1) )
        + SUM(DHF_U2$IDHFP_T(IA,IS3,T,DHF_U2),VDHF_T(IA,IS3,T,DHF_U2) )
        - SUM(DHF_D2$IDHFP_T(IA,IS3,T,DHF_D2),VDHF_T(IA,IS3,T,DHF_D2) )
        + SUM(DHF_U3$IDHFP_T(IA,IS3,T,DHF_U3),VDHF_T(IA,IS3,T,DHF_U3) )
        - SUM(DHF_D3$IDHFP_T(IA,IS3,T,DHF_D3),VDHF_T(IA,IS3,T,DHF_D3) )
    )/(1-DISLOSS_H(IA))

* Adds heat transmission if selected in the gas add-on
$ifi %HEATTRANS%==yes $include '../../base/addons/heattrans/model/htheatbalance.inc';
        - VQHEQ(IA,IS3,T,'IMINUS') + VQHEQ(IA,IS3,T,'IPLUS')
* Adds district heating if selected
$ifi %FV%==yes $include '../../base/addons/Fjernvarme/heatbalance_fv.inc';
;


* Fuel consumption rate calculated on existing units.
QGFEQ(IA,G,IS3,T)$IAGK_Y(IA,G) ..
     VGF_T(IA,G,IS3,T)
  =E=
   (VGE_T(IA,G,IS3,T)/(GDATA(G,'GDFE')*GEFFRATE(IA,G)))$(IGNOTETOH(G) AND IGE(G))
    +(GDATA(G,'GDCV')*VGH_T(IA,G,IS3,T)/(GDATA(G,'GDFE')*GEFFRATE(IA,G)))$IGH(G)
$ifi %UnitComm%==yes $include '../../base/addons/unitcommitment/qgfeqadd.inc';
;

* Fuel consumption rate calculated on new units.
QGFNEQ(IA,G,IS3,T)$IAGKN(IA,G)..
     VGFN_T(IA,G,IS3,T)
 =E=
     (VGEN_T(IA,G,IS3,T)/(GDATA(G,'GDFE')*GEFFRATE(IA,G)))$(IGNOTETOH(G) AND IGE(G))
    +(GDATA(G,'GDCV')*VGHN_T(IA,G,IS3,T)/(GDATA(G,'GDFE')*GEFFRATE(IA,G)))$IGH(G)
$ifi %UnitComm%==yes $include '../../base/addons/unitcommitment/qgfneqadd.inc';
;


*------ Generation technologies' electricity/heat operating area: --------------

* Back pressure units:

QGCBGBPR(IAGK_Y(IA,IGBPR),IS3,T) ..
      VGE_T(IA,IGBPR,IS3,T) =E= VGH_T(IA,IGBPR,IS3,T) * GDATA(IGBPR,'GDCB');


QGNCBGBPR(IAGKN(IA,IGBPR),IS3,T) ..
      VGEN_T(IA,IGBPR,IS3,T) =E= VGHN_T(IA,IGBPR,IS3,T) * GDATA(IGBPR,'GDCB');


* Extraction units:

QGCBGEXT(IAGK_Y(IA,IGEXT),IS3,T) ..
   VGE_T(IA,IGEXT,IS3,T) =G= VGH_T(IA,IGEXT,IS3,T) * GDATA(IGEXT,'GDCB');


QGCVGEXT(IAGK_Y(IA,IGEXT),IS3,T)$
$ifi not %COMBTECH%==yes  1 ..
$ifi     %COMBTECH%==yes  (NOT IGCOMB2(IGEXT)) ..
* This will have a value if not a combination technology
     (IGKVACCTOY(IA,IGEXT)+IGKFX_Y(IA,IGEXT))*IGKRATE(IA,IGEXT,IS3,T)
         - VGH_T(IA,IGEXT,IS3,T) * GDATA(IGEXT,'GDCV')
$ifi %COMBTECH%==yes  * This will have a value in case of a combination technology.
$ifi %COMBTECH%==yes    + SUM(IGCOMB2$GGCOMB(IGEXT,IGCOMB2), VGH_T(IA,IGCOMB2,IS3,T) * GDATA(IGCOMB2,'GDCV'))$IGCOMB1(IGEXT)
    =G=
   VGE_T(IA,IGEXT,IS3,T)
$ifi %COMBTECH%==yes  * Add secondary generation if combination technologies are used
$ifi %COMBTECH%==yes  + SUM(IGCOMB2$GGCOMB(IGEXT,IGCOMB2), VGE_T(IA,IGCOMB2,IS3,T))$IGCOMB1(IGEXT)
;

QGNCBGEXT(IAGKN(IA,IGEXT),IS3,T) ..
   VGEN_T(IA,IGEXT,IS3,T) =G= VGHN_T(IA,IGEXT,IS3,T) * GDATA(IGEXT,'GDCB') ;


QGNCVGEXT(IAGKN(IA,IGEXT),IS3,T)$
$ifi not %COMBTECH%==yes  1 ..
$ifi     %COMBTECH%==yes  (NOT IGCOMB2(IGEXT)) ..

         VGKN(IA,IGEXT)*IGKRATE(IA,IGEXT,IS3,T)
       - VGHN_T(IA,IGEXT,IS3,T) * GDATA(IGEXT,'GDCV')
$ifi %COMBTECH%==yes  * This will have a value in case of a combination technology.
$ifi %COMBTECH%==yes         + SUM(IGCOMB2$GGCOMB(IGEXT,IGCOMB2), VGHN_T(IA,IGCOMB2,IS3,T) * GDATA(IGCOMB2,'GDCV'))$IGCOMB1(IGEXT)
    =G=
   VGEN_T(IA,IGEXT,IS3,T)
$ifi %COMBTECH%==yes  * Add secondary generation if combination technologies are used
$ifi %COMBTECH%==yes     + SUM(IGCOMB2$GGCOMB(IGEXT,IGCOMB2), VGEN_T(IA,IGCOMB2,IS3,T))$IGCOMB1(IGEXT)
;
* Electric heat pumps:

QGGETOH(IAGK_Y(IA,IGETOH),IS3,T) ..
       VGE_T(IA,IGETOH,IS3,T) =E= VGH_T(IA,IGETOH,IS3,T) / GDATA(IGETOH,'GDFE');

QGNGETOH(IAGKN(IA,IGETOH),IS3,T) ..
       VGEN_T(IA,IGETOH,IS3,T) =E= VGHN_T(IA,IGETOH,IS3,T) / GDATA(IGETOH,'GDFE');




*--------- Dispatchable generation technologies' operating area: ---------------

* Generation on new capacity is constrained by the capacity,
* modified by GKDERATE:

QGEKNT(IAGKN(IA,IGKE),IS3,T)$(IGDISPATCH(IGKE) AND
$ifi not %COMBTECH%==yes  1) ..
$ifi     %COMBTECH%==yes  (NOT IGCOMB2(IGKE))) ..
  VGKN(IA,IGKE)*IGKRATE(IA,IGKE,IS3,T)/(1$(not IGESTO(IGKE))+GDATA(IGKE,'GDSTOHUNLD')$IGESTO(IGKE))
    =G=
  VGEN_T(IA,IGKE,IS3,T)
$ifi %COMBTECH%==yes  * Add secondary generation if combination technologies are used
$ifi %COMBTECH%==yes    + SUM(IGCOMB2$GGCOMB(IGKE,IGCOMB2), VGEN_T(IA,IGCOMB2,IS3,T))$IGCOMB1(IGDISPATCH)
;


* Note: does not presently include COMBTECH.
QGHKNT(IAGKN(IA,IGKH),IS3,T)$IGDISPATCH(IGKH)..
  VGKN(IA,IGKH)*IGKRATE(IA,IGKH,IS3,T)/(1$(not IGHSTO(IGKH) or IGHSTO_S(IGKH)) + GDATA(IGKH,'GDSTOHUNLD')$IGHSTO(IGKH))
     =G=
  VGHN_T(IA,IGKH,IS3,T)
;


$ifi %PLANTCLOSURES%==yes QGEKOT(IAGK_Y(IA,IGDISPATCH(IGE)),IS3,T) ..
$ifi %PLANTCLOSURES%==yes  (IGKFX_Y(IA,IGDISPATCH) + IGKVACCTOY(IA,IGDISPATCH)-VDECOM(IA,IGDISPATCH))*IGKRATE(IA,IGDISPATCH,IS3,T)
$ifi %PLANTCLOSURES%==yes     =G=
$ifi %PLANTCLOSURES%==yes  VGE_T(IA,IGDISPATCH,IS3,T)
$ifi %PLANTCLOSURES%==yes ;

* Rev. 3.01: missing GSOLH, to be added.
$ifi %PLANTCLOSURES%==yes QGHKOT(IAGK_Y(IA,IGKH),IS3,T) ..
$ifi %PLANTCLOSURES%==yes  (IGKFX_Y(IA,IGKH) + IGKVACCTOY(IA,IGKH)-VDECOM(IA,IGKH))*IGKRATE(IA,IGKH,IS3,T)
$ifi %PLANTCLOSURES%==yes     =G=
$ifi %PLANTCLOSURES%==yes  VGH_T(IA,IGKH,IS3,T)
$ifi %PLANTCLOSURES%==yes ;


*-------------- New hydro run-of-river: cannot be dispatched: ------------------


QGKNHYRR(IAGKN(IA,IGHYRR),IS3,T)$IWTRRRSUM(IA)..
 WTRRRFLH(IA) * VGKN(IAGKN) * WTRRRVAR_T(IA,IS3,T) / IWTRRRSUM(IA)
   =E=
   VGEN_T(IA,IGHYRR,IS3,T)

$ifi %PLANTCLOSURES%==yes QGKOHYRR(IAGK_Y(IA,IGHYRR),IS3,T)..
$ifi %PLANTCLOSURES%==yes  WTRRRFLH(IA) * (IGKFX_Y(IA,IGHYRR) + IGKVACCTOY(IA,IGHYRR)-VDECOM(IA,IGHYRR)) * WTRRRVAR_T(IA,IS3,T) / IWTRRRSUM(IA)
$ifi %PLANTCLOSURES%==yes    =E=
$ifi %PLANTCLOSURES%==yes  VGE_T(IA,IGHYRR,IS3,T)
;


*-------------- New windpower: cannot be dispatched: ---------------------------


QGKNWND(IAGKN(IA,IGWND),IS3,T)$IWND_SUMST(IA)..
 WNDFLH(IA) * VGKN(IA,IGWND) * WND_VAR_T(IA,IS3,T) / IWND_SUMST(IA)
$ifi     %WNDSHUTDOWN%==yes =G=
$ifi not %WNDSHUTDOWN%==yes =E=
  VGEN_T(IA,IGWND,IS3,T);

$ifi %PLANTCLOSURES%==yes QGKOWND(IAGK_Y(IA,IGWND),IS3,T)$IWND_SUMST(IA)..
$ifi %PLANTCLOSURES%==yes  WNDFLH(IA) * (IGKFX_Y(IA,IGWND) + IGKVACCTOY(IA,IGWND)-VDECOM(IA,IGWND)) * WND_VAR_T(IA,IS3,T) / IWND_SUMST(IA)
$ifi %PLANTCLOSURES%==yes    =G=
$ifi %PLANTCLOSURES%==yes   VGE_T(IA,IGWND,IS3,T);


*-------------- New solar power and heat: cannot be dispatched: ----------------
QGKNSOLE(IAGKN(IA,IGSOLE),IS3,T)$ISOLESUMST(IA)..
SOLEFLH(IA) * VGKN(IA,IGSOLE) * SOLE_VAR_T(IA,IS3,T) / ISOLESUMST(IA)
 =E=
VGEN_T(IA,IGSOLE,IS3,T);


QGKNSOLH(IAGKN(IA,IGSOLH),IS3,T)$ISOLHSUMST(IA)..
SOLHFLH(IA) * VGKN(IA,IGSOLH) * SOLH_VAR_T(IA,IS3,T) / ISOLHSUMST(IA)
 =E=
VGHN_T(IA,IGSOLH,IS3,T);


$ifi %PLANTCLOSURES%==yes QGKOSOLE(IAGK_Y(IA,IGSOLE),IS3,T)..
$ifi %PLANTCLOSURES%==yes SOLEFLH(IA) * (IGKFX_Y(IA,IGSOLE) + IGKVACCTOY(IA,IGSOLE)-VDECOM(IA,IGSOLE)) * SOLE_VAR_T(IA,IS3,T) / ISOLESUMST(IA)
$ifi %PLANTCLOSURES%==yes  =E=
$ifi %PLANTCLOSURES%==yes VGE_T(IA,IGSOLE,IS3,T);

*-------------- New wave power: cannot be dispatched: -------------------------

QGKNWAVE(IAGKN(IA,IGWAVE),IS3,T)..
WAVEFLH(IA) * VGKN(IAGKN) * WAVE_VAR_T(IA,IS3,T) / IWAVESUMST(IA)
 =E=
VGEN_T(IAGKN,IS3,T);

$ifi %PLANTCLOSURES%==yes QGKOWAVE(IAGK_Y(IA,IGWAVE),IS3,T)..
$ifi %PLANTCLOSURES%==yes WAVEFLH(IA) * (IGKFX_Y(IA,IGWAVE) + IGKVACCTOY(IA,IGWAVE)-VDECOM(IA,IGWAVE)) * WAVE_VAR_T(IA,IS3,T) / IWAVESUMST(IA)
$ifi %PLANTCLOSURES%==yes  =E=
$ifi %PLANTCLOSURES%==yes VGE_T(IA,IGWAVE,IS3,T);



*-------------- Hydropower with reservoirs: ------------------------------------
*
* Hydro reservoir content - dynamic seasonal equation:

QHYRSSEQ(IA,S)$SUM(IGHYRS,IAGK_Y(IA,IGHYRS) or IAGKN(IA,IGHYRS))..
      VHYRS_S(IA,S)
      + SUM(IGHYRS$IAGK_Y(IA,IGHYRS),
        IHYINF_S(IA,S) * (IGKVACCTOY(IA,IGHYRS)+IGKFX_Y(IA,IGHYRS))
        -SUM(T,IHOURSINST(S,T)*(VGE_T(IA,IGHYRS,S,T))))
      + SUM(IGHYRS$IAGKN(IA,IGHYRS),
        IHYINF_S(IA,S) * VGKN(IA,IGHYRS)
        -SUM(T,IHOURSINST(S,T)*(VGEN_T(IA,IGHYRS,S,T))))

      - VQHYRSSEQ(IA,S,'IMINUS') + VQHYRSSEQ(IA,S,'IPLUS')

      =G=  VHYRS_S(IA,S++1);

* Regulated and unregulated hydropower production lower than capacity:

QHYMAXG(IA,IS3,T)$SUM(IGHYRS,IAGK_Y(IA,IGHYRS))..
      SUM(IGHYRS$IAGK_Y(IA,IGHYRS),VGE_T(IA,IGHYRS,IS3,T))
      + SUM(IGHYRS$IAGKN(IA,IGHYRS),VGEN_T(IA,IGHYRS,IS3,T))
      + SUM(IGHYRR$IAGK_Y(IA,IGHYRR),VGE_T(IA,IGHYRR,IS3,T))
      + SUM(IGHYRR$IAGKN(IA,IGHYRR),VGEN_T(IA,IGHYRR,IS3,T))
      =L=
      SUM(IGHYRS$IAGK_Y(IA,IGHYRS),(IGKVACCTOY(IA,IGHYRS)+IGKFX_Y(IA,IGHYRS))*IGKRATE(IA,IGHYRS,IS3,T))
      + SUM(IGHYRS$IAGKN(IA,IGHYRS),VGKN(IA,IGHYRS))
      ;

* Hydro storage within limits:

QHYRSMINVOL(IA,S)$(HYRSDATA(IA,'HYRSMINVOL',S) and SUM(IGHYRS$(IAGK_Y(IA,IGHYRS) or IAGKN(IA,IGHYRS)),1))..

      VHYRS_S(IA,S) =G= HYRSDATA(IA,'HYRSMINVOL',S) * WTRRSFLH(IA) *
        (SUM(IGHYRS,(IGKVACCTOY(IA,IGHYRS)+IGKFX_Y(IA,IGHYRS)+VGKN(IA,IGHYRS)$IAGKN(IA,IGHYRS)))
        -VQHYRSSEQ(IA,S,'IMINUS'));

* Hydro reservoir content - maximum level

QHYRSMAXVOL(IA,S)$(HYRSDATA(IA,'HYRSMAXVOL',S) and SUM(IGHYRS$(IAGK_Y(IA,IGHYRS) or IAGKN(IA,IGHYRS)),1))..

       HYRSDATA(IA,'HYRSMAXVOL',S) * WTRRSFLH(IA) *
        (SUM(IGHYRS,(IGKVACCTOY(IA,IGHYRS)+IGKFX_Y(IA,IGHYRS)+VGKN(IA,IGHYRS)$IAGKN(IA,IGHYRS)))
        -VQHYRSSEQ(IA,S,'IPLUS'))=G= VHYRS_S(IA,S);

*------------ Short term heat and electricity storages:-------------------------


QESTOVOLT(IA,S,T)$(SUM(IGESTO, IAGK_Y(IA,IGESTO)+IAGKN(IA,IGESTO)) AND IS3(S))..
    VESTOVOLT(IA,S,T++1) =E= VESTOVOLT(IA,S,T)
  + IHOURSINST(S,T)*(VESTOLOADT(IA,S,T)
  - SUM(IGESTO$IAGK_Y(IA,IGESTO), VGE_T(IA,IGESTO,S,T)/GDATA(IGESTO,'GDFE'))
  - SUM(IGESTO$IAGKN(IA,IGESTO),VGEN_T(IA,IGESTO,S,T)/GDATA(IGESTO,'GDFE'))
  )
  - VQESTOVOLT(IA,S,T,'IPLUS') + VQESTOVOLT(IA,S,T,'IMINUS')
;


QHSTOVOLT(IA,S,T)$(SUM(IGHSTO, IAGK_Y(IA,IGHSTO)+IAGKN(IA,IGHSTO)) AND IS3(S))..
    VHSTOVOLT(IA,S,T)
  + IHOURSINST(S,T)* (VHSTOLOADT(IA,S,T)
  - SUM(IGHSTO$IAGK_Y(IA,IGHSTO), VGH_T(IA,IGHSTO,S,T)/GDATA(IGHSTO,'GDFE'))
  - SUM(IGHSTO$IAGKN(IA,IGHSTO), VGHN_T(IA,IGHSTO,S,T)/GDATA(IGHSTO,'GDFE'))
  )
    =E=       VHSTOVOLT(IA,S,T++1)
  - VQHSTOVOLT(IA,S,T,'IPLUS') + VQHSTOVOLT(IA,S,T,'IMINUS')
;

QHSTOVOLT_S(IA,S,T)$SUM(IGHSTO_S, IAGK_Y(IA,IGHSTO_S)+IAGKN(IA,IGHSTO_S))..
    VHSTOVOLT(IA,S,T)
  + IHOURSINST(S,T)* (VHSTOLOADT(IA,S,T)
  - SUM(IGHSTO_S$IAGK_Y(IA,IGHSTO_S), VGH_T(IA,IGHSTO_S,S,T)/GDATA(IGHSTO_S,'GDFE'))
  - SUM(IGHSTO_S$IAGKN(IA,IGHSTO_S), VGHN_T(IA,IGHSTO_S,S,T)/GDATA(IGHSTO_S,'GDFE')))
* The following line may be active or not, depending on the intention:
*$ifi %QSTOVOLT_OneCycleInS%==yes   /IDAYSIN_S(S)
    =E=       VHSTOVOLT(IA,S,T+1)+SUM(ITALIAS$(ORD(ITALIAS)=1),VHSTOVOLT(IA,S++1,ITALIAS)$(ORD(T)=CARD(T)))
   - VQHSTOVOLT(IA,S,T,'IPLUS') + VQHSTOVOLT(IA,S,T,'IMINUS')
;
* The following constraints limits when investments in storage facilities are allowed.
* They pertain only to BB2 and to areas with an option to invest in storage facilities.
* Simple bounds cover all other situations.

QHSTOLOADTLIM(IA,IS3,T)$SUM(IGHSTOALL,IAGKN(IA,IGHSTOALL)) ..
* Existing or accumulated capacity
   SUM(IGHSTOALL$IAGK_Y(IA,IGHSTOALL),  (IGKFX_Y(IA,IGHSTOALL)+IGKVACCTOY(IA,IGHSTOALL))/GDATA(IGHSTOALL,'GDSTOHLOAD')
   )
* New investments
   +SUM(IGHSTOALL$IAGKN(IA,IGHSTOALL),   VGKN(IA,IGHSTOALL)/GDATA(IGHSTOALL,'GDSTOHLOAD')
   )
         =G=
   VHSTOLOADT(IA,IS3,T)
;

QESTOLOADTLIM(IA,IS3,T)$SUM(IGESTO,IAGKN(IA,IGESTO)) ..
* Existing or accumulated capacity
   SUM(IGESTO$IAGK_Y(IA,IGESTO),  (IGKFX_Y(IA,IGESTO)+IGKVACCTOY(IA,IGESTO))/GDATA(IGESTO,'GDSTOHLOAD')
   )
* New investments
   +SUM(IGESTO$IAGKN(IA,IGESTO),  VGKN(IA,IGESTO)/GDATA(IGESTO,'GDSTOHLOAD')   )
         =G=
   VESTOLOADT(IA,IS3,T)
;

* Storage contents does not exceed upper limit (MWh):
QHSTOVOLTLIM(IA,IS3,T)$SUM(IGHSTOALL,IAGKN(IA,IGHSTOALL)) ..
* Existing or accumulated capacity
  SUM(IGHSTOALL$IAGK_Y(IA,IGHSTOALL),   IGKFX_Y(IA,IGHSTOALL)+IGKVACCTOY(IA,IGHSTOALL)     )
* New investments
  +SUM(IGHSTOALL$IAGKN(IA,IGHSTOALL),   VGKN(IA,IGHSTOALL)
   )
         =G=
  VHSTOVOLT(IA,IS3,T)
;

QESTOVOLTLIM(IA,IS3,T)$SUM(IGESTO,IAGKN(IA,IGESTO)) ..
* Existing or accumulated capacity
  SUM(IGESTO$IAGK_Y(IA,IGESTO),  IGKFX_Y(IA,IGESTO)+IGKVACCTOY(IA,IGESTO)      )
* New investments
  +SUM(IGESTO$IAGKN(IA,IGESTO),  VGKN(IA,IGESTO)   )
         =G=
  VESTOVOLT(IA,IS3,T)
;

*---------- Maximal installable capacity per fuel type is restricted (MW): ----

QKFUELC(C,FFF)$FKPOT(C,FFF)..
      SUM(IAGK_Y(IA,G)$(IGF(G,FFF) AND ICA(C,IA)), IGKVACCTOY(IA,G))
    + SUM(IAGK_Y(IA,G)$(IGF(G,FFF) AND ICA(C,IA)), IGKFXYMAX(IA,G))
    + SUM(IAGKN(IA,G)$(IGF(G,FFF) AND ICA(C,IA)),     VGKN(IA,G))
         =L=  FKPOT(C,FFF);

QKFUELR(IR,FFF)$FKPOT(IR,FFF)..

      SUM(IAGK_Y(IA,G)$(IGF(G,FFF) AND RRRAAA(IR,IA)), IGKVACCTOY(IA,G))
      + SUM(IAGK_Y(IA,G)$(IGF(G,FFF) AND RRRAAA(IR,IA)), IGKFXYMAX(IA,G))
      + SUM(IAGKN(IA,G)$(IGF(G,FFF) AND RRRAAA(IR,IA)),   VGKN(IA,G))
        =L=  FKPOT(IR,FFF);


QKFUELA(IA,FFF)$FKPOT(IA,FFF) ..
        SUM(IAGK_Y(IA,G)$IGF(G,FFF),  IGKVACCTOY(IA,G))
      + SUM(IAGK_Y(IA,G)$IGF(G,FFF), IGKFXYMAX(IA,G))
      + SUM(IAGKN(IA,G)$IGF(G,FFF),   VGKN(IA,G))
        =L=  FKPOT(IA,FFF);

* Electricity generation constraints by fuel (in energy), country
QFGEMINC(C,FFF)$FGEMIN(C,FFF)..
     SUM((IA,IS3,T)$ICA(C,IA), SUM(IGNOTETOH(IGE)$(IGF(IGE,FFF) AND IAGK_Y(IA,IGE)),  IHOURSINST(IS3,T) * VGE_T(IA,IGE,IS3,T)))
   + SUM((IA,IS3,T)$ICA(C,IA), SUM(IGNOTETOH(IGE)$(IGF(IGE,FFF) AND IAGKN(IA,IGE)),   IHOURSINST(IS3,T) * VGEN_T(IA,IGE,IS3,T)))
     =G= FGEMIN(C,FFF);

QFGEMAXC(C,FFF)$FGEMAX(C,FFF)..
    FGEMAX(C,FFF)
     =G=
     SUM((IA,IS3,T)$ICA(C,IA), SUM(IGNOTETOH(IGE)$(IGF(IGE,FFF) AND IAGK_Y(IA,IGE)),   IHOURSINST(IS3,T) * VGE_T(IA,IGE,IS3,T)))
   + SUM((IA,IS3,T)$ICA(C,IA), SUM(IGNOTETOH(IGE)$(IGF(IGE,FFF) AND IAGKN(IA,IGE)),    IHOURSINST(IS3,T) * VGEN_T(IA,IGE,IS3,T)))
;
* Electricity generation constraints by fuel (in energy), region
QFGEMINR(IR,FFF)$FGEMIN(IR,FFF)..
     SUM((IA,IS3,T)$RRRAAA(IR,IA), SUM(IGNOTETOH(IGE)$(IGF(IGE,FFF) AND IAGK_Y(IA,IGE)),  IHOURSINST(IS3,T) * VGE_T(IA,IGE,IS3,T)))
   + SUM((IA,IS3,T)$RRRAAA(IR,IA), SUM(IGNOTETOH(IGE)$(IGF(IGE,FFF) AND IAGKN(IA,IGE)),   IHOURSINST(IS3,T) * VGEN_T(IA,IGE,IS3,T)))
     =G= FGEMIN(IR,FFF);

QFGEMAXR(IR,FFF)$FGEMAX(IR,FFF)..
    FGEMAX(IR,FFF)
     =G=
     SUM((IA,IS3,T)$RRRAAA(IR,IA), SUM(IGNOTETOH(IGE)$(IGF(IGE,FFF) AND IAGK_Y(IA,IGE)),  IHOURSINST(IS3,T) * VGE_T(IA,IGE,IS3,T)))
   + SUM((IA,IS3,T)$RRRAAA(IR,IA), SUM(IGNOTETOH(IGE)$(IGF(IGE,FFF) AND IAGKN(IA,IGE)),   IHOURSINST(IS3,T) * VGEN_T(IA,IGE,IS3,T)))
;

* Electricity generation constraints by fuel (in energy), area
QFGEMINA(IA,FFF)$FGEMIN(IA,FFF)..
     SUM((IS3,T), SUM(IGNOTETOH(IGE)$(IGF(IGE,FFF) AND IAGK_Y(IA,IGE)),  IHOURSINST(IS3,T) * VGE_T(IA,IGE,IS3,T)))
   + SUM((IS3,T), SUM(IGNOTETOH(IGE)$(IGF(IGE,FFF) AND IAGKN(IA,IGE)),   IHOURSINST(IS3,T) * VGEN_T(IA,IGE,IS3,T)))
     =G= FGEMIN(IA,FFF);

QFGEMAXA(IA,FFF)$FGEMAX(IA,FFF)..
    FGEMAX(IA,FFF)
     =G=
     SUM((IS3,T), SUM(IGNOTETOH(IGE)$(IGF(IGE,FFF) AND IAGK_Y(IA,IGE)),   IHOURSINST(IS3,T) * VGE_T(IA,IGE,IS3,T)))
   + SUM((IS3,T), SUM(IGNOTETOH(IGE)$(IGF(IGE,FFF) AND IAGKN(IA,IGE)),    IHOURSINST(IS3,T) * VGEN_T(IA,IGE,IS3,T)))
;

* Fuel use constraints (in energy), area

QGMINAF(IA,FFF)$IGMINF_Y(IA,FFF)..
     SUM(G$(IAGK_Y(IA,G) AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGF_T(IA,G,IS3,T)))
    +SUM(G$(IAGKN(IA,G)  AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGFN_T(IA,G,IS3,T)))
    + VQGMINAF(IA,FFF)
    =G= IGMINF_Y(IA,FFF);

QGMAXAF(IA,FFF)$IGMAXF_Y(IA,FFF)..
    IGMAXF_Y(IA,FFF)
    + VQGMAXAF(IA,FFF)
         =G=
     SUM(G$(IAGK_Y(IA,G) AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGF_T(IA,G,IS3,T)))
    +SUM(G$(IAGKN(IA,G)  AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGFN_T(IA,G,IS3,T)))
;


QGEQAF(IA,FFF)$IGEQF_Y(IA,FFF)..
    IGEQF_Y(IA,FFF)
    + VQGEQAF(IA,FFF,'IPLUS') - VQGEQAF(IA,FFF,'IMINUS')
   =E=
     SUM(G$(IAGK_Y(IA,G) AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGF_T(IA,G,IS3,T)))
    +SUM(G$(IAGKN(IA,G)  AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGFN_T(IA,G,IS3,T)))
   ;


QGEQAF_S(IA,FFF,IS3)$IGEQF_Y_S(IA,FFF,IS3)..
    IGEQF_Y_S(IA,FFF,IS3)
     =L=
     SUM(G$(IAGK_Y(IA,G) AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM(T, IHOURSINST(IS3,T) * VGF_T(IA,G,IS3,T)))
    +SUM(G$(IAGKN(IA,G)  AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM(T, IHOURSINST(IS3,T) * VGFN_T(IA,G,IS3,T)))
   ;



* Fuel use constraints (in energy), region

QGMINRF(IR,FFF)$IGMINF_Y(IR,FFF)..
    SUM(IA$RRRAAA(IR,IA),
     SUM(G$(IAGK_Y(IA,G) AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGF_T(IA,G,IS3,T)))
    +SUM(G$(IAGKN(IA,G)  AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGFN_T(IA,G,IS3,T)))
    )
    + VQGMINRF(IR,FFF)
    =G= IGMINF_Y(IR,FFF);

QGMAXRF(IR,FFF)$IGMAXF_Y(IR,FFF)..
    IGMAXF_Y(IR,FFF)
    + VQGMAXRF(IR,FFF)
         =G=
    SUM(IA$RRRAAA(IR,IA),
     SUM(G$(IAGK_Y(IA,G) AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGF_T(IA,G,IS3,T)))
    +SUM(G$(IAGKN(IA,G)  AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGFN_T(IA,G,IS3,T)))
    );


QGEQRF(IR,FFF)$IGEQF_Y(IR,FFF)..
    IGEQF_Y(IR,FFF)
    + VQGEQRF(IR,FFF,'IPLUS') - VQGEQRF(IR,FFF,'IMINUS')
   =E=
    SUM(IA$RRRAAA(IR,IA),
    SUM(G$(IAGK_Y(IA,G) AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGF_T(IA,G,IS3,T)))
    +SUM(G$(IAGKN(IA,G)  AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGFN_T(IA,G,IS3,T)))
    );

QGEQRF_S(IR,FFF,IS3)$IGEQF_Y_S(IR,FFF,IS3)..
    IGEQF_Y_S(IR,FFF,IS3)
   =L=
    SUM(IA$RRRAAA(IR,IA),
    SUM(G$(IAGK_Y(IA,G) AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM(T, IHOURSINST(IS3,T) * VGF_T(IA,G,IS3,T)))
    +SUM(G$(IAGKN(IA,G)  AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM(T, IHOURSINST(IS3,T) * VGFN_T(IA,G,IS3,T)))
    );


* Fuel use constraints (in energy), country

QGMINCF(C,FFF)$IGMINF_Y(C,FFF)..
    SUM(IA$ICA(C,IA),
     SUM(G$(IAGK_Y(IA,G) AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGF_T(IA,G,IS3,T)))
    +SUM(G$(IAGKN(IA,G)  AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGFN_T(IA,G,IS3,T)))
    )
    + VQGMINCF(C,FFF)
    =G= IGMINF_Y(C,FFF);

QGMAXCF(C,FFF)$IGMAXF_Y(C,FFF)..
    IGMAXF_Y(C,FFF)
    + VQGMAXCF(C,FFF)
         =G=
    SUM(IA$ICA(C,IA),
     SUM(G$(IAGK_Y(IA,G) AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGF_T(IA,G,IS3,T)))
    +SUM(G$(IAGKN(IA,G)  AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGFN_T(IA,G,IS3,T)))
    );


QGEQCF(C,FFF)$IGEQF_Y(C,FFF)..
    IGEQF_Y(C,FFF)
    + VQGEQCF(C,FFF,'IPLUS') - VQGEQCF(C,FFF,'IMINUS')
   =E=
    SUM(IA$ICA(C,IA),
     SUM(G$(IAGK_Y(IA,G) AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGF_T(IA,G,IS3,T)))
    +SUM(G$(IAGKN(IA,G)  AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM((IS3,T), IHOURSINST(IS3,T) * VGFN_T(IA,G,IS3,T)))
    );

QGEQCF_S(C,FFF,IS3)$IGEQF_Y_S(C,FFF,IS3)..
    IGEQF_Y_S(C,FFF,IS3)
   =L=
    SUM(IA$ICA(C,IA),
     SUM(G$(IAGK_Y(IA,G) AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM(T, IHOURSINST(IS3,T) * VGF_T(IA,G,IS3,T)))
    +SUM(G$(IAGKN(IA,G)  AND (GDATA(G,'GDFUEL') EQ FDATA(FFF,'FDACRONYM'))), IOF3P6 * SUM(T, IHOURSINST(IS3,T) * VGFN_T(IA,G,IS3,T)))
    );


*----------- Transmission (MW):------------------------------------------------

* Electricity transmission is limited by transmission capacity:

QXK(IRE,IRI,IS3,T)$((IXKINI_Y(IRE,IRI) OR IXKN(IRI,IRE) OR IXKN(IRE,IRI)) AND (IXKINI_Y(IRE,IRI) NE INF) )..
       IXKRATE(IRE,IRI,IS3,T)*(
       IXKINI_Y(IRE,IRI)+IXKVACCTOY(IRE,IRI)
     + VXKN(IRE,IRI)$(IXKN(IRE,IRI) OR IXKN(IRI,IRE))+ VXKN(IRI,IRE)$(IXKN(IRE,IRI) OR IXKN(IRI,IRE))
     )
     + VQXK(IRE,IRI,IS3,T,'IPLUS')-VQXK(IRE,IRI,IS3,T,'IMINUS')
     =G=  VX_T(IRE,IRI,IS3,T);


QXMAXINV(IRE,IRI)$(IXKN(IRE,IRI)AND XMAXINV(IRE,IRI))..
         XMAXINV(IRE,IRI)
 =G=
         VXKN(IRE,IRI);

*------------ Emissions: ------------------------------------------------------

QLIMCO2(C)$(ILIM_CO2_Y(C) AND (ILIM_CO2_Y(C) LT INF))..

      SUM(IAGK_Y(IA,G)$ICA(C,IA), SUM((IS3,T), IHOURSINST(IS3,T) * (IM_CO2(G) * IOF0001) * IOF3P6 * VGF_T(IA,G,IS3,T)))
    + SUM(IAGKN(IA,G)$ICA(C,IA),  SUM((IS3,T), IHOURSINST(IS3,T) * (IM_CO2(G) * IOF0001) * IOF3P6 * VGFN_T(IA,G,IS3,T)))

      =L= ILIM_CO2_Y(C) ;


QLIMSO2(C)$(ILIM_SO2_Y(C) AND (ILIM_SO2_Y(C) LT INF))..

      SUM(IAGK_Y(IA,G)$ICA(C,IA), SUM((IS3,T), IHOURSINST(IS3,T) * (IM_SO2(G) * IOF0001) * IOF3P6 * VGF_T(IA,G,IS3,T)))
    + SUM(IAGKN(IA,G)$ICA(C,IA),  SUM((IS3,T), IHOURSINST(IS3,T) * (IM_SO2(G) * IOF0001) * IOF3P6 * VGFN_T(IA,G,IS3,T)))

      =L= ILIM_SO2_Y(C) ;


QLIMNOX(C)$(ILIM_NOX_Y(C) AND (ILIM_NOX_Y(C) LT INF))..

      SUM(IAGK_Y(IA,G)$ICA(C,IA), SUM((IS3,T), IHOURSINST(IS3,T) * (GDATA(G,'GDNOX') * IOF0000001) * IOF3P6 * VGF_T(IA,G,IS3,T)))
    + SUM(IAGKN(IA,G)$ICA(C,IA),  SUM((IS3,T), IHOURSINST(IS3,T) * (GDATA(G,'GDNOX') * IOF0000001) * IOF3P6 * VGFN_T(IA,G,IS3,T)))

      =L= ILIM_NOX_Y(C) ;



QFMAXINVEST(C,FFF)$(FMAXINVEST(C,FFF) > 0) ..
         FMAXINVEST(C,FFF)
 =G=
         SUM((IAGKN(IA,G))$(ICA(C,IA) and IGF(G,FFF)), VGKN(IA,G))
;


QGMAXINVEST2(C,IGKFIND)$GROWTHCAP(C,IGKFIND)..
   SUM(IA$(IAGKN(IA,IGKFIND) AND ICA(C,IA)),VGKN(IA,IGKFIND))
   =L=
   SUM(IA$(IAGKN(IA,IGKFIND) AND ICA(C,IA)),
    (IGKFX_Y_1(IA,IGKFIND)-IGKFX_Y(IA,IGKFIND)+IGKVACCEOY(IA,IGKFIND)-IGKVACCTOY(IA,IGKFIND)))
   +IGROWTHCAP(C,IGKFIND);


$ontext
*------ Begin equations used only in balbase3 ----------------------------------
* Hans: Kommer senere som nt addon
*------ End equations used only in Balbase3 ------------------------------------
$offtext

*-------------------------------------------------------------------------------
*----- Any equations for addon to be placed here: ------------------------------
*-------------------------------------------------------------------------------

*Raffaele

QONWIND(C)$INSCAPWINDON_Y(C)..
   INSCAPWINDON_Y(C)=E=SUM(IA$ICA(C,IA),(SUM(IGWNDON,VGKN(IA,IGWNDON)$(IAGKN(IA,IGWNDON))+IGKFXYMAX(IA,IGWNDON))));

QOFFWIND(C)$INSCAPWINDOFF_Y(C)..
   INSCAPWINDOFF_Y(C)=E=SUM(IA$ICA(C,IA),(SUM(IGWNDOFF,VGKN(IA,IGWNDOFF)$(IAGKN(IA,IGWNDOFF))+IGKFXYMAX(IA,IGWNDOFF))));

QSOLE(C)$INSCAPSOLE_Y(C)..
   INSCAPSOLE_Y(C)=E=SUM(IA$ICA(C,IA),(SUM(IGSOLE,VGKN(IA,IGSOLE)$(IAGKN(IA,IGSOLE))+IGKFXYMAX(IA,IGSOLE))));

QSOLH(C)$INSCAPSOLH_Y(C)..
   INSCAPSOLH_Y(C)=E=SUM(IA$ICA(C,IA),(SUM(IGSOLH,VGKN(IA,IGSOLH)$(IAGKN(IA,IGSOLH))+IGKFXYMAX(IA,IGSOLH))));

$ontext
QHP(C)$INSCAPHP_Y(C)..
   INSCAPHP_Y(C)=E=SUM(IA$ICA(C,IA),(SUM(IGHP,VGKN(IA,IGHP)$(IAGKN(IA,IGHP))+IGKFXYMAX(IA,IGHP))));
$offtext



* These add-on equations pertain to district heating.
$ifi %FV%==yes $include '../../base/addons/Fjernvarme/eq_fv.inc';

* These add-on equations pertain to combination technologies.
$ifi %COMBTECH%==yes $include '../../base/addons/combtech/combEQ.inc';

* These add-on equations pertain to price sensitive electricity exchange with
* third countries.
$ifi %X3V%==yes $include '../../base/addons/x3v/model/x3veq.inc';

* This file includes:
* [list equations]

* These add-on equations pertain to heat transmission.
$ifi %HEATTRANS%==yes $include '../../base/addons/heattrans/model/htequations.inc';

$ifi %REShareE%==yes $include  '../../base/addons/REShareE/RESEeqns.inc';
$ifi %REShareEH%==yes $include '../../base/addons/REShareEH/RESEHeqns.inc';

$ifi %AGKNDISC%==yes  $include  '../../base/addons/AGKNDISC/AGKNDISCequations.inc';

$ifi %POLICIES%==yes  $include '../../base/addons/policies/pol_eq.inc';

$ifi %SYSTEMCONST%==yes  $include '../../base/addons/SystemConst/eq_syscon.inc';

* These add-on equations pertain to hydrogen.
$ifi %H2%==yes $include '../../base/addons/Hydrogen/H2equations.inc';

*-------------------------------------------------------------------------------
*----- End: Any equations for addon to be placed here. -------------------------
*-------------------------------------------------------------------------------


*-------------------------------------------------------------------------------
*  End: Declaration and definition of EQUATIONS
*-------------------------------------------------------------------------------



*-------------------------------------------------------------------------------
* Definition of the models:
*-------------------------------------------------------------------------------

MODEL BALBASE1 'Balmorel model without endogeneous investments'
/
*--- Objective function --------------------------------------------------------
                                QOBJ
*--- Balance equations ---------------------------------------------------------
                                QEEQ
                                QHEQ
                                QGFEQ
                                QGFNEQ
*--- Feasible generation region ------------------------------------------------
                                QGCBGBPR
                                QGCBGEXT
                                QGCVGEXT
                                QGGETOH

*--- Storage restructions ------------------------------------------------------
                                QHYRSSEQ
                                QHYRSMINVOL
                                QHYRSMAXVOL
                                QESTOVOLT
                                QHSTOVOLT
*--- Transmission capacity -----------------------------------------------------
                                QXK


*--- Electricity generation restrictions by fuel -------------------------------
                                QFGEMINC
                                QFGEMAXC
                                QFGEMINR
                                QFGEMAXR
                                QFGEMINA
                                QFGEMAXA
*--- Fuel consumption restrictions ---------------------------------------------
*/*
                                QGMINCF
                                QGMAXCF
                                QGEQCF
                                QGEQCF_S
                                QGMINRF
                                QGMAXRF
                                QGEQRF
                                QGEQRF_S
                                QGMINAF
                                QGMAXAF
                                QGEQAF
                                QGEQAF_S
*--- Emissions restrictions ----------------------------------------------------
                                QLIMCO2
                                QLIMSO2
                                QLIMNOX
*--- Operational restrictions --------------------------------------------------
**/

*----- Any equations for addon to be placed here: ------------------------------
$ifi %FV%==yes $include '../../base/addons/Fjernvarme/eqN_fv.inc';
$ifi %COMBTECH%==yes $include '../../base/addons/combtech/combbb1.inc';
$ifi %X3VfxQ%==yes              QX3VBAL
$ifi %HEATTRANS%==yes $include '../../base/addons/heattrans/model/htbb1.inc';
$ifi %UnitComm%==yes      $include '../../base/addons/unitcommitment/modeladd.inc';
$ifi %POLICIES%==yes $include '../../base/addons/policies/pol_eqn.inc';

$ifi %SYSTEMCONST%==yes $include '../../base/addons/SystemConst/eqn_syscon.inc';
$ifi %H2%==yes $INCLUDE '../../base/addons/Hydrogen/H2bb1.inc';

*----- End: Any equations for addon to be placed here. -------------------------

/;

* Scale the model: only equation QOBJ is really badly scaled:
BALBASE1.SCALEOPT=1;
QOBJ.SCALE=IOF1000000;




MODEL BALBASE2  'Balmorel model with endogeneous investments'
/
*--- Objective function --------------------------------------------------------
                                QOBJ
*--- Balance equations ---------------------------------------------------------
                                QEEQ
                                QHEQ
                                QGFEQ
                                QGFNEQ
*--- Feasible generation region ------------------------------------------------
                                QGCBGBPR
                                QGCBGEXT
                                QGCVGEXT
                                QGGETOH
                                QGNCBGBPR
                                QGNCBGEXT
                                QGNCVGEXT
                                QGNGETOH
*--- Generation capacities -----------------------------------------------------
                                QGEKNT
                                QGHKNT
                                QGKNWND
                                QGKNHYRR
                                QGKNSOLE
                                QGKNSOLH
                                QGKNWAVE
*--- Storage restrictions ------------------------------------------------------
                                QHYRSSEQ
                                QHYRSMINVOL
                                QHYRSMAXVOL
                                QESTOVOLT
                                QHSTOVOLT
                                QHSTOLOADTLIM
                                QESTOLOADTLIM
                                QHSTOVOLTLIM
                                QESTOVOLTLIM
*--- Transmission capacity -----------------------------------------------------
                                QXK
*--- Fuel capacity restrictions ------------------------------------------------
                                QKFUELC
                                QKFUELR
                                QKFUELA

*--- Electricity generation restrictions by fuel -------------------------------
                                QFGEMINC
                                QFGEMAXC
                                QFGEMINR
                                QFGEMAXR
                                QFGEMINA
                                QFGEMAXA
*--- Fuel consumption restrictions ---------------------------------------------
                                QGMINCF
                                QGMAXCF
                                QGEQCF
                                QGEQCF_S
                                QGMINRF
                                QGMAXRF
                                QGEQRF
                                QGEQRF_S
                                QGMINAF
                                QGMAXAF
                                QGEQAF
                                QGEQAF_S
*--- Emissions restrictions ----------------------------------------------------
                                QLIMCO2
                                QLIMSO2
                                QLIMNOX
*--- Operational restrictions --------------------------------------------------
*Raffaele
*                                QONWIND
*                                QOFFWIND
*                                QSOLE
*                                QSOLH
*QHP
*----- Any equations for addon to be placed here: ------------------------------
*--- Capacity restrictions for decommissioning ----------------------------------
$ifi %PLANTCLOSURES%==yes           QGEKOT
$ifi %PLANTCLOSURES%==yes           QGHKOT
$ifi %PLANTCLOSURES%==yes           QGKOHYRR
$ifi %PLANTCLOSURES%==yes           QGKOWND
$ifi %PLANTCLOSURES%==yes           QGKOSOLE
$ifi %PLANTCLOSURES%==yes           QGKOWAVE
*--- Investment restricitions --------------------------------------------------
                                QXMAXINV

*----- Any equations for true addon to be placed here: -------------------------
$ifi %X3VfxQ%==yes              QX3VBAL
$ifi %COMBTECH%==yes $include '../../base/addons/combtech/combbb2.inc';
$ifi %HEATTRANS%==yes $include '../../base/addons/heattrans/model/htbb1.inc';
$ifi %UnitComm%==yes      $include '../../base/addons/unitcommitment/modeladd.inc';
$ifi %REShareE%==yes QREShareE
$ifi %REShareEH%==yes QREShareEH

$ifi %AGKNDISC%==yes  QAGKNDISCCONT
$ifi %AGKNDISC%==yes  QAGKNDISC01
$ifi %POLICIES%==yes $include '../../base/addons/policies/pol_eqn.inc';
$ifi %SYSTEMCONST%==yes $include '../../base/addons/SystemConst/eqn_syscon.inc';
$ifi %H2%==yes $INCLUDE '../../base/addons/Hydrogen/H2bb1.inc'
*----- End: Any equations for addon to be placed here. -------------------------
/;

* Scale the model: only equation QOBJ is really badly scaled:
BALBASE2.SCALEOPT=1;
QOBJ.SCALE=IOF1000000;

MODEL BALBASE3 'Balmorel model without endogeneous investments, simulating each season individually'
/
*--- Objective function --------------------------------------------------------
                                QOBJ
*--- Balance equations ---------------------------------------------------------
                                QEEQ
                                QHEQ
                                QGFEQ
                                QGFNEQ
*--- Feasible generation region ------------------------------------------------
                                QGCBGBPR
                                QGCBGEXT
                                QGCVGEXT
                                QGGETOH
*--- Storage restrictions ------------------------------------------------------
                                QESTOVOLT
                                QHSTOVOLT
*--- Transmission capacity -----------------------------------------------------
                                QXK
*--- Operational restrictions --------------------------------------------------
*--- Additional hydro restrictions ---------------------------------------------
* Hans : Kommer senere
*----- Any equations for addon to be placed here: ------------------------------
$ifi %HYRSBB123%==quant        QHYRSG
$ifi %HYRSBB123%==quantprice   QHYRSG
$ifi %HYRSBB123%==quantprice   QWATERVOLINI

$ifi %COMBTECH%==yes $include '../../base/addons/combtech/combbb1.inc';
$ifi %HEATTRANS%==yes $include '../../base/addons/heattrans/model/htbb1.inc';
$ifi %H2%==yes $INCLUDE '../../base/addons/Hydrogen/H2bb3.inc';
*----- End: Any equations for addon to be placed here. -------------------------
/;

* Scale the model: only equation QOBJ is really badly scaled:
BALBASE3.SCALEOPT=1;
QOBJ.SCALE=IOF1000000;



*-------------------------------------------------------------------------------
*----- Any further models to be defined here:
*-------------------------------------------------------------------------------


*-------------------------------------------------------------------------------
*----- End: Any further models to be defined here
*-------------------------------------------------------------------------------

*-------------------------------------------------------------------------------
*----- End of definition of models ---------------------------------------------
*-------------------------------------------------------------------------------


* The input data are subject to a certain control for 'reasonable' values.
* The errors are checked by the code in the files ERRORx.INC

$INCLUDE '../../base/logerror/logerinc/error2.inc';

*$goto endofmodel
$ifi %COMBTECH%==yes  $INCLUDE  '../../base/addons/combtech/comberr2.inc';

$ifi %POLICIES%==yes  $INCLUDE  '../../base/addons/policies/pol_err2.inc';

* The following file contains definitions of a number of temporary parameters
* and sets that may be used for printout of simulation results.
$ifi %PRINTFILES%==yes $INCLUDE '../../base/output/printout/printinc/print2.inc';


*-------------------------------------------------------------------------------
* Here the model to be solved/simulated is included:

BALBASE1.optfile = %USEOPTIONFILE%;
BALBASE2.optfile = %USEOPTIONFILE%;
BALBASE3.optfile = %USEOPTIONFILE%;





$ifi %bb1%==yes
$if EXIST 'bb123.sim' $INCLUDE 'bb123.sim';
$ifi %bb1%==yes
$if not EXIST 'bb123.sim' $INCLUDE '../../base/model/bb123.sim';

$ifi %bb2%==yes
$if EXIST 'bb123.sim' $INCLUDE 'bb123.sim';
$ifi %bb2%==yes
$if not EXIST 'bb123.sim' $INCLUDE '../../base/model/bb123.sim';

$ifi %bb3%==yes
$if EXIST 'bb123.sim' $INCLUDE 'bb123.sim';
$ifi %bb3%==yes
$if not EXIST 'bb123.sim' $INCLUDE '../../base/model/bb123.sim';


*--- Results which can be transfered between simulations are placed here: ------

$ifi %bb1%==yes execute_unload  '../../simex/HYRSG.gdx',HYRSG;
$ifi %bb1%==yes execute_unload  '../../simex/VHYRS_SL.gdx',VHYRS_SL;
$ifi %bb1%==yes execute_unload  '../../simex/WATERVAL.gdx',WATERVAL;

$ifi %bb2%==yes execute_unload  '../../simex/HYRSG.gdx',HYRSG;
$ifi %bb2%==yes execute_unload  '../../simex/VHYRS_SL.gdx',VHYRS_SL;
$ifi %bb2%==yes execute_unload  '../../simex/WATERVAL.gdx',WATERVAL;

$ifi  %bb3%==yes $ifi not %HYRSBB123%==none $include  "../../base\addons\hyrsbb123\hyrsbb123unload.inc";

$ifi %MAKEINVEST%==yes execute_unload '../../base/data/GKVACC.gdx', GKVACC;
$ifi %MAKEINVEST%==yes execute_unload '../../base/data/GKVACCDECOM.gdx', GKVACCDECOM;
$ifi %MAKEINVEST%==yes execute_unload '../../base/data/GVKGN.gdx', GVKGN;
$ifi %MAKEINVEST%==yes execute_unload '../../base/data/XKACC.gdx', XKACC;


$ifi %X3V%==yes $INCLUDE '../../base/addons/x3v/model/x3vgdx.inc';

$ifi %H2%==yes execute_unload '../../base/addons/Hydrogen/H2STOVOL_START.gdx',H2STOVOL_START;

*--- End: Results which can be transfered between simulations are placed here --


*--- Results collection and comparison -----------------------------------------
* Merge simulation years:
$ifi %MERGESAVEPOINTRESULTS%==yes  execute 'gdxmerge "%relpathModel%..\output\temp\*.gdx"';

* Rename merged.gdx to case identification.
$ifi %MERGESAVEPOINTRESULTS%==yes  execute 'move merged.gdx "%CASEID%.gdx"';
$ifi %COMPARECASE%==NONE
$ifi %MERGESAVEPOINTRESULTS%==yes  execute 'gdxmerge "%CASEID%.gdx"';
$ifi not %COMPARECASE%==NONE
$ifi %MERGESAVEPOINTRESULTS%==yes  execute 'gdxmerge "%CASEID%.gdx" "%relpathModel%..\..\%COMPAREWITH%\output\%COMPARECASE%.gdx"';
$ifi %COMPARECASE%==NONE
$ifi %MERGESAVEPOINTRESULTS%==yes  execute 'move merged.gdx "%relpathoutput%%CASEID%-Results.gdx"';
$ifi not %COMPARECASE%==NONE
$ifi %MERGESAVEPOINTRESULTS%==yes  execute 'move merged.gdx "%relpathoutput%%CASEID%-Compare.gdx"';
$ifi %MERGESAVEPOINTRESULTS%==yes  execute 'move %CASEID%.gdx "%relpathoutput%%CASEID%.gdx"';
$ifi %COMPARECASE%==NONE
$ifi %MERGEDSAVEPOINTRESULTS2MDB%==yes execute '=gdx2access "%relpathoutput%%CASEID%-Results.gdx"';
$ifi not %COMPARECASE%==NONE
$ifi %MERGEDSAVEPOINTRESULTS2MDB%==yes execute '=gdx2access "%relpathoutput%%CASEID%-Compare.gdx"';


* $ifi %comparewithbase%==yes   putclose  fileCOMPAREWITHBASEbat 'move  ' '%relpathModel%''..\..\output\BASE.gdx '  '%relpathoutput%BASE.gdx';

**$ifi %INPUTDATAGDX2MDB%==yes $INCLUDE 'aconymFix.inc';  Hans: to be implemented for GAMS version 22.7 and later, which has better support for this.


* Make an MS Access database from comparison results file.
* Irrelevant $ifi %MAKEACCESS%==yes execute '=gdx2access %CASEID%-COMPARE.gdx';



*--- End: Results collection and comparison ------------------------------------
$ifi %INPUTDATA2GDX%==yes execute 'move "%relpathModel%..\output\temp\1INPUT.gdx" "%relpathInputdata2GDX%INPUTDATAOUT.gdx"';
* Transfer inputdata a seperate Access file:
$ifi %INPUTDATAGDX2MDB%==yes execute '=GDX2ACCESS "%relpathInputdata2GDX%INPUTDATAOUT.gdx"';
* Transfer to Excel, read the identifiers to be transferred from file inputdatagdx2xls.txt:
* Note: presently not working (the relevant data is not set i file inputdatagdx2xls.txt):
* NOTWORKING (i.e. you MUST have '$setglobal INPUTDATAGDX2XLS'): Note:  GAMS version 22.7 and later have better support for this....
*$ifi %INPUTDATAGDX2XLS%==yes  execute  "GDXXRW.EXE Input=%relpathInputdata2GDX%INPUTDATAOUT.gdx  Output=%relpathInputdata2GDX%INPUTDATAOUT.xls  @%relpathModel%inputdatagdx2xls.txt";



*----- End of model:------------------------------------------------------------
$label ENDOFMODEL
*----- End of model ------------------------------------------------------------

parameter Iwindproduction(C) "Actually found wind generated electricity (TWh)" ;
Iwindproduction(C)  = SUM((IA,G,S,T)$(ica(c,IA) AND igwnd(G)), VGE_T.L(IA,G,S,T)*IHOURSINST(S,T))/1e6;
parameter IHYDROproduction(C) "Actually found hydro generated electricity (TWh)" ;
IHYDROproduction(C)  = SUM((IA,G,S,T)$(ica(c,IA) AND (igHYRR(G) OR igHYRS(G))), VGE_T.L(IA,G,S,T)*IHOURSINST(S,T))/1e6;



execute_unload "all_B303.gdx";
  /*
DISPLAY  VQEEQ.L, VQHEQ.L
VQESTOVOLT(AAA,S
VQHSTOVOLT(AAA,S
VQHYRSSEQ(AAA,S,
VQGEQCF(C,FFF,IP
VQGMINCF(C,FFF)
VQGMAXCF(C,FFF)
VQGEQRF(RRR,FFF,
VQGMAXRF(RRR,FFF
VQGMINRF(RRR,FFF
VQGEQAF(AAA,FFF,
VQGMAXAF(AAA,FFF
VQGMINAF(AAA,FFF
*/
$ontext
display IGKrate, gkrate, GEFFRATE, IDE_T_Y ,VGE_T.l , VGEN_T.l , VX_T.l, IGKFXYMAX, IAGKN ;


* save present gkrate:

parameter IGKRATE_test(aaA,ggG,SSS,TTT);
IGKRATE_test(aaa,g,s,T) =  Igkrate(aaa,g,S,T);

execute_unload "gkrate_b303.gdx", IGKRATE_test; !! Has indexes S,T
$kill IGKRATE_test


*$include ..\..\..\..\data_Odrev\var2001.inc  Hans: this include-file is substituted by the following which is relevant for the purpose:

TABLE GKDERATE_S(AAA,GGG,SSS)  'Reduction in capacity'
                                                S01       S02       S03       S04       S05       S06       S07       S08       S09       S10       S11       S12
*DK_E_Rural.DK_E_Rural_ST-E8-ORs                 0.99      0.94      0.91      0.73      0.46      0.42      0.29      0.7       0.85      0.96      0.98      0.98
*Surplus heat capacities derated to enable constant production the whole year, while keeping the right annual production according to Energiproducenttællingen
DK_CA_Esb.DK_CA_Esb_SurpHeat_FF                 0         0         0         0         0         0         0         0         0         0         0         0
DK_CA_TVIS.DK_CA_TVIS_SurpHeat_FF               0.56      0.56      0.56      0.56      0.56      0.56      0.56      0.56      0.56      0.56      0.56      0.56
DK_CA_Aal.DK_CA_Aal_SurpHeat_FF                 0.37      0.37      0.37      0.37      0.37      0.37      0.37      0.37      0.37      0.37      0.37      0.37
DK_MAM_Hobro.DK_MAM_Hobro_SurpHeat_FF           0         0         0         0         0         0         0         0         0         0         0         0
DK_MAM_Nyborg.DK_MAM_Nyborg_SurpHeat_FF         0.71      0.71      0.71      0.71      0.71      0.71      0.71      0.71      0.71      0.71      0.71      0.71
DK_MAM_Skagen.DK_MAM_Skagen_SurpHeat_FF         0.10      0.10      0.10      0.10      0.10      0.10      0.10      0.10      0.10      0.10      0.10      0.10
DK_SA_E_WO_HO.DK_SA_SurpHeat_FF                 0         0         0         0         0         0         0         0         0         0         0         0
DK_SA_W_EB.DK_SA_SurpHeat_FF                    0         0         0         0         0         0         0         0         0         0         0         0
DK_SA_W_NG_CHP.DK_SA_SurpHeat_FF                0.12      0.12      0.12      0.12      0.12      0.12      0.12      0.12      0.12      0.12      0.12      0.12
DK_SA_W_NG_HO.DK_SA_SurpHeat_FF                 0.32      0.32      0.32      0.32      0.32      0.32      0.32      0.32      0.32      0.32      0.32      0.32
DK_SA_W_WO_HO.DK_SA_SurpHeat_FF                 0.09      0.09      0.09      0.09      0.09      0.09      0.09      0.09      0.09      0.09      0.09      0.09
;

* Hans: New

* Seson values are copied to all time steps
**HansUD GKDERATE(IA,G,SSS,TTT)$GKDERATE_S(IA,G,'S01')=GKDERATE_S(IA,G,'S01');
IGKRATE_test(aaA,ggG,SSS,TTT)$GKDERATE_S(aaA,Ggg,'S01')=GKDERATE_S(aaA,ggG,'S01');
* Unless other values are assigned in the TABLE above,
* a default value 0.90 is used and assigned below:

iscalar1 = card(IGKRATE_test); display "after first assignment (transfer from S to S,T)",iscalar1 ;    !! approx 70000 elements, took 0.016 sec
**HansUD GKDERATE(IA,G,SSS,TTT)$(not GKDERATE(IA,G,SSS,TTT) and (not IGWND(G)) and (AGKN(IA,G) or IAGK(IA,G)))= 0.90;
IGKRATE_test(aaa,G,SSS,TTT)$(not IGKRATE_test(aaa,G,SSS,TTT) and (not IGWND(G)) and (AGKN(aaa,G) or IAGK(aaa,g)))= 0.90;
iscalar1 = card(IGKRATE_test); display  "after second assignment ('use 0.90 if no value for relevant')",iscalar1 ;  !! approx 2 million elements, took 1.185 sec

** hANS $include '../../%COREDATAFOLDER%/data/gkderateweek.inc';
*$include ..\..\..\..\data_Odrev\gkderateweek.inc


* hans__ overskriv! :
* PEME 02082004 HER SÆTTES GKDERATE LIG MED GKDERATEW
  LOOP(IR,
     LOOP((IA,IGE)$(RRRAAA(IR,IA) and (AGKN(IA,IGE) or IAGK(IA,IGE))),
          IGKRATE_test(IA,IGE,SSS,TTT)$(not GKDERATE_S(IA,IGE,SSS) and GKDERATEW(SSS,IR) and (not IGWND(IGE)) and (AGKN(IA,IGE) or IAGK(IA,IGE)))= GKDERATEW(SSS,IR);
     );
  );
iscalar1 = card(IGKRATE_test); display  "after third  assignment (transfer GKDERATEW)",iscalar1 ;  !! No increase in size, took 0.858 seconds - I wonder if anything was assigned?

**UD HANS GKDERATE(AIND(IA),G,SSS,TTT)$(IAGK(AIND,G) or AGKN(AIND,G))=1;
**UD Hans: NOte : AIND (individual heating areas) is empty if not option FV==yes
**UD Hans: The next will assign 2 million variables
**UDIGKRATE_test(IA,G,SSS,TTT)$(IAGK(IA,G) or AGKN(IA,G))=1;

* Lars: Scaling of GKDERATE of Nuclear power to ensure correct nuclear production:
** HANSUD GKDERATE('FI_R_Rural',IGNUC,SSS,TTT)$IAGK('FI_R_Rural',IGNUC)=GKDERATE('FI_R_Rural','FI_R_Rural_NU-C8-NU',SSS,TTT) * 8230/6767 ;
** HANSUD GKDERATE('SE_M_Rural',IGNUC,SSS,TTT)$IAGK('SE_M_Rural',IGNUC)=GKDERATE('SE_M_Rural','SE_M_Rural_NU-C8-NU',SSS,TTT) * 7249/6755 ;
** HANSUD GKDERATE('SE_S_Rural',IGNUC,SSS,TTT)$IAGK('SE_S_Rural',IGNUC)=GKDERATE('SE_S_Rural','SE_S_Rural_NU-C8-NU',SSS,TTT) * 7249/6755 ;
*  Hans: missing IGNUC, so making it
set ignuc(g) " nuclear fuel";
ignuc(g)$(gdata(g,"GDFUEL") eq 1) = Yes;
display ignuc;




IGKRATE_test('FI_R_Rural',IGNUC,SSS,TTT)$IAGK('FI_R_Rural',IGNUC)=IGKRATE_test('FI_R_Rural','FI_R_Rural_NU-C8-NU',SSS,TTT) * 8230/6767 ;
IGKRATE_test('SE_M_Rural',IGNUC,SSS,TTT)$IAGK('SE_M_Rural',IGNUC)=IGKRATE_test('SE_M_Rural','SE_M_Rural_NU-C8-NU',SSS,TTT) * 7249/6755 ;
IGKRATE_test('SE_S_Rural',IGNUC,SSS,TTT)$IAGK('SE_S_Rural',IGNUC)=IGKRATE_test('SE_S_Rural','SE_S_Rural_NU-C8-NU',SSS,TTT) * 7249/6755 ;
iscalar1 = card(IGKRATE_test); display  "after fourth  assignment (transfer GKDERATEW)",iscalar1 ;  !! No increase in size, 3 assignments together took 5.5 seconds

* LPB: Scaling of GKDERATE of Nuclear power for Russia based on data from Alex from 2006/2008
* LB: Removed to attain balance.
*GKDERATE(AAA,'KOL_NUC_PP_COND-NU',SSS,TTT)$(RRRAAA('RU_KOL',AAA)) =GKDERATE(AAA,'KOL_NUC_PP_COND-NU',SSS,TTT)$(RRRAAA('RU_KOL',AAA))*0.68;
*GKDERATE(AAA,'LEN_NUC_PP-NU',SSS,TTT)$(RRRAAA('RU_STP',AAA)) =GKDERATE(AAA,'LEN_NUC_PP-NU',SSS,TTT)$(RRRAAA('RU_STP',AAA))*0.65;


execute_unload "gkrate_FV.gdx", IGKRATE_test;


$OFFTEXT

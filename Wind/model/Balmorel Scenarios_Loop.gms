*Code created for running different scenarios (Amalia, 05-09-2016)

*Call the balmorel mode, which will be solved through a loop in order to calculate all the marginal values
$include Balmorel.gms


* Turn off the listing of the input file */
$offlisting

* Turn off the listing and cross-reference of the symbols used */
$offsymxref offsymlist

Option solprint=off;
Option sysout=off;


*step 1 - setup scenarios

Set scenarios  'Scenarios for calculation of marginal values of electricity' %semislash%
$if     EXIST '../SensitivityAnalysis/scenarios.inc' $INCLUDE      '../SensitivityAnalysis/scenarios.inc';
$if not EXIST '../SensitivityAnalysis/scenarios.inc' $INCLUDE '../../base/SensitivityAnalysis/scenarios.inc';
%semislash%;


*Change of the parameters for the different scenarios
$if     EXIST '../SensitivityAnalysis/scenarios_parameters.inc' $INCLUDE      '../SensitivityAnalysis/scenarios_parameters.inc';
$if not EXIST '../SensitivityAnalysis/scenarios_parameters.inc' $INCLUDE '../../base/SensitivityAnalysis/scenarios_parameters.inc';


*step 2 save data
$if     EXIST '../SensitivityAnalysis/saved_data.inc' $INCLUDE      '../SensitivityAnalysis/saved_data.inc';
$if not EXIST '../SensitivityAnalysis/saved_data.inc' $INCLUDE '../../base/SensitivityAnalysis/saved_data.inc';


$if     EXIST '../SensitivityAnalysis/Loop_parameters_output.inc' $INCLUDE      '../SensitivityAnalysis/Loop_parameters_output.inc';
$if not EXIST '../SensitivityAnalysis/Loop_parameters_output.inc' $INCLUDE '../../base/SensitivityAnalysis/Loop_parameters_output.inc';


loop(scenarios,

*step 3 reestablish data to base level
IGKFX_Y(IA,G)=sav_IGKFX_Y(IA,G);
IGKFXYMAX(IA,G)=sav_IGKFXYMAX(IA,G);
WNDFLH(IA)=sav_WNDFLH(IA);
SOLEFLH(IA)=sav_SOLEFLH(IA);
SOLHFLH(IA)=sav_SOLHFLH(IA);
WTRRRFLH(IA)=sav_WTRRRFLH(IA);
WTRRSFLH(IA)=sav_WTRRSFLH(IA);
IHYINF_S(IA,S)=sav_IHYINF_S(IA,S);
IFUELP_Y(IA,FFF)=sav_IFUELP_Y(IA,FFF);
IXKRATE(IRE,IRI,S,T)=sav_IXKRATE(IRE,IRI,S,T);
IXMAXINV_Y(IYALIAS,IRE,IRI)=sav_IXMAXINV_Y(IYALIAS,IRE,IRI);
GINVCOST(IA,G)=sav_GINVCOST(IA,G);

*step 4 change data to levels needed in scenario
IGKFX_Y(IA,G)$(IGKFX_Y_Scenario(scenarios,IA,G))=IGKFX_Y_Scenario(scenarios,IA,G);
IGKFXYMAX(IA,G)$(IGKFXYMAX_Scenario(scenarios,IA,G))=IGKFXYMAX_Scenario(scenarios,IA,G);
WNDFLH(IA)$(WNDFLH_Scenarios(scenarios,IA))=WNDFLH_Scenarios(scenarios,IA);
SOLEFLH(IA)$(SOLEFLH_Scenarios(scenarios,IA))=SOLEFLH_Scenarios(scenarios,IA);
SOLHFLH(IA)$(SOLHFLH_Scenarios(scenarios,IA))=SOLHFLH_Scenarios(scenarios,IA);
WTRRRFLH(IA)$(WTRRRFLH_Scenarios(scenarios,IA))=WTRRRFLH_Scenarios(scenarios,IA);
WTRRSFLH(IA)$(WTRRSFLH_Scenarios(scenarios,IA))=WTRRSFLH_Scenarios(scenarios,IA);
IHYINF_S(IA,S)$(IHYINF_S_Scenarios(scenarios,IA,S))=IHYINF_S_Scenarios(scenarios,IA,S);
IFUELP_Y(IA,FFF)$(IFUELP_Y_Scenario(scenarios,IA,FFF))=IFUELP_Y_Scenario(scenarios,IA,FFF);
IXKRATE(IRE,IRI,S,T)$(IXKRATE_Scenario(scenarios,IRE,IRI,S,T))=IXKRATE_Scenario(scenarios,IRE,IRI,S,T);
IXMAXINV_Y(IYALIAS,IRE,IRI)$(IXMAXINV_Y_Scenario(scenarios,IYALIAS,IRE,IRI))=IXMAXINV_Y_Scenario(scenarios,IYALIAS,IRE,IRI);
GINVCOST(IA,G)$(GINVCOST_Scenario(scenarios,IA,G))=GINVCOST_Scenario(scenarios,IA,G);

LOOP(Y,IXKRATE(IRE,IRI,S,T)$(XKRATE_Y(Y,IRE,IRI,S,T))=XKRATE_Y(Y,IRE,IRI,S,T););


*Update bounds
VGE_T.UP(IA,IGKE,S,T)$IAGK_Y(IA,IGKE)=(IGKVACCTOY(IA,IGKE) + IGKFX_Y(IA,IGKE))*(IGKRATE(IA,IGKE,S,T));
VGH_T.UP(IA,IGKH,S,T)$IAGK_Y(IA,IGKH)=(IGKVACCTOY(IA,IGKH) + IGKFX_Y(IA,IGKH))*(IGKRATE(IA,IGKH,S,T));

VGE_T.FX(IA,IGSOLE,S,T)$IAGK_Y(IA,IGSOLE) = ((SOLEFLH(IA) * (IGKVACCTOY(IA,IGSOLE) + IGKFX_Y(IA,IGSOLE)) * SOLE_VAR_T(IA,S,T)) / ISOLESUMST(IA))$ISOLESUMST(IA);
VGH_T.FX(IA,IGSOLH,S,T)$IAGK_Y(IA,IGSOLH) = ((SOLHFLH(IA) * (IGKVACCTOY(IA,IGSOLH) + IGKFX_Y(IA,IGSOLH)) * SOLH_VAR_T(IA,S,T)) / ISOLHSUMST(IA))$ISOLHSUMST(IA);

$ifi not %WNDSHUTDOWN%==yes VGE_T.FX(IA,IGWND,S,T)$IAGK_Y(IA,IGWND) =  ((WNDFLH(IA) * (IGKVACCTOY(IA,IGWND) + IGKFX_Y(IA,IGWND))* WND_VAR_T(IA,S,T))/IWND_SUMST(IA))$IWND_SUMST(IA);
$ifi     %WNDSHUTDOWN%==yes VGE_T.up(IA,IGWND,S,T)$IAGK_Y(IA,IGWND) =  ((WNDFLH(IA) * (IGKVACCTOY(IA,IGWND) + IGKFX_Y(IA,IGWND))* WND_VAR_T(IA,S,T))/IWND_SUMST(IA))$IWND_SUMST(IA);

VGE_T.FX(IA,IGHYRR,S,T)$IAGK_Y(IA,IGHYRR)=((WTRRRFLH(IA) * (IGKVACCTOY(IA,IGHYRR) + IGKFX_Y(IA,IGHYRR))* WTRRRVAR_T(IA,S,T)) / IWTRRRSUM(IA))$IWTRRRSUM(IA);

$ifi %bb1%==yes  IHYINF_S(IA,S)= ((WTRRSFLH(IA) * WTRRSVAR_S(IA,S) * (IOF365*WEIGHT_S(S)/IWEIGHSUMS)) / IWTRRSSUM(IA))$IWTRRSSUM(IA);
$ifi %bb2%==yes  IHYINF_S(IA,S)= ((WTRRSFLH(IA) * WTRRSVAR_S(IA,S) * (IOF365*WEIGHT_S(S)/IWEIGHSUMS)) / IWTRRSSUM(IA))$IWTRRSSUM(IA);
$ifi %bb3%==yes  IHYINF_S(IA,S)= ((WTRRSFLH(IA) * WTRRSVAR_S(IA,S) * (IOF365*WEIGHT_S(S)/IWEIGHSUMS)) / IWTRRSSUM(IA))$IWTRRSSUM(IA);

*step 5 -- solve model
$ifi NOT %UnitComm%==yes $goto notunit
$ifi not %UnitRMIP%==yes
$ifi %bb1%==yes  SOLVE BALBASE1 USING MIP MINIMIZING VOBJ;
$ifi not %UnitRMIP%==yes
$ifi %bb2%==yes  SOLVE BALBASE2 USING MIP MINIMIZING VOBJ;
$ifi not %UnitRMIP%==yes
$ifi %bb3%==yes  SOLVE BALBASE3 USING MIP MINIMIZING VOBJ;
$ifi %UnitRMIP%==yes
$ifi %bb1%==yes  SOLVE BALBASE1 USING RMIP MINIMIZING VOBJ;
$ifi %UnitRMIP%==yes
$ifi %bb2%==yes  SOLVE BALBASE2 USING RMIP MINIMIZING VOBJ;
$ifi %UnitRMIP%==yes
$ifi %bb3%==yes  SOLVE BALBASE3 USING RMIP MINIMIZING VOBJ;
$goto YESunit

$label notunit
$ifi %bb1%==yes SOLVE BALBASE1 USING LP MINIMIZING VOBJ;
$ifi not %bb2%==yes $goto not_bb2;
$ifi %SOLVEMIP%==yes SOLVE BALBASE2 USING MIP MINIMIZING VOBJ;
$ifi not %SOLVEMIP%==yes SOLVE BALBASE2 USING LP MINIMIZING VOBJ;
$label not_bb2;
$ifi %bb3%==yes  SOLVE BALBASE3 USING LP MINIMIZING VOBJ;
$LABEL YESunit
;


*step 6 cross scenario report writing

$if     EXIST '../SensitivityAnalysis/Loop_output.inc' $INCLUDE      '../SensitivityAnalysis/Loop_output.inc';
$if not EXIST '../SensitivityAnalysis/Loop_output.inc' $INCLUDE '../../base/SensitivityAnalysis/Loop_output.inc';




*step 7 end of loop
    );

*step 8 Create a Report
*step 9 compute and display final results



execute_unload "ScenarioResults.gdx"  EPrice_RST_scen, HPrice_AST_scen, EDemand_RST_scen, Edemand_CST_scen, Edemand_R_scen, Edemand_C_scen, Hdemand_AST_scen, EPrice_R_scen, EPrice_CST_scen, EPrice_C_scen, HPrice_AS_scen,
HPrice_A_scen, EProduction_RST_scen, ETrade_RST_scen, Eproduction_CST_scen, ETrade_CST_scen, Eproduction_R_scen, Eproduction_C_scen, ETrade_R_scen, ETrade_C_scen, Hproduction_AST_scen, Hproduction_AS_scen,
Hproduction_A_scen, Hproduction_CST_scen, Hproduction_CS_scen, Hproduction_C_scen, FuelConsumption_C_scen, FuelConsumption_TOTAL_scen, Elec_Capacity_scen, Heat_Capacity_scen, WNDFLH_Scenarios, LVGKN_s, MVGKN_s, LVOBJ_s,
GKFX_Scenario, IGKFX_Y_Scenario, IGKFXYMAX_Scenario, WNDFLH_Scenarios, SOLEFLH_Scenarios, SOLHFLH_Scenarios, WTRRRFLH_Scenarios, WTRRSFLH_Scenarios, IHYINF_S_Scenarios,
FUELPRICE_Scenario, IFUELP_Y_Scenario, IXKRATE_Scenario, XMAXINV_Scenario, IXMAXINV_Y_Scenario, GDATA_Scenario, GINVCOST_Scenario  ;

*End of the Balmorel scenario file


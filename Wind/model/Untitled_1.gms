
PARAMETER SOSIBUFLOW_VAR_T(AAA,PROC,FLOW,SSS,TTT) "Variation in some Source, Sink or Buffer Flows over the year"
/

/;


* Urban areas in Western Denmark use the DK_CA_Aarhus profile.


*WASTE GENERATION: data  in SOSIBUBOUND indicate that the value is on ton/h, therefore make a flat profile:

SOSIBUFLOW_VAR_T(IA,'HouseHoldWasteGen','RESIDUALWASTEHH',S,T)$(not AAATRANSIT(IA)) = 1;
SOSIBUFLOW_VAR_T(IA,'IndustrialWasteGen','INDUSTRY_MIXEDWASTE',S,T)$(not AAATRANSIT(IA)) = 1;



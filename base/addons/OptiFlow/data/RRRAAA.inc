* Specification of areas in regions:

SET RRRAAA(RRR,AAA)  'Areas in region'
/
* DK_RRR.DK_RRR_AAA
DK_E.DK_CA_KBH
UK_RRR. UK_AAA



DK_W.DK_CA_Aab
DK_W.DK_CA_Aal
DK_W.DK_CA_Aarhus
DK_W.DK_CA_Esb
DK_W.DK_CA_Hern
DK_E.DK_CA_Kal

DK_W.DK_CA_Odense
DK_W.DK_CA_Randers
DK_E.DK_CA_Roenne
DK_W.DK_CA_TVIS
DK_E.DK_CA_Vestfrb
DK_E.DK_E_DTU
DK_W.DK_MA_Grenaa
DK_E.DK_MA_Hil
DK_W.DK_MA_Hjoerring
DK_W.DK_MA_Holst
DK_W.DK_MA_Horsens
DK_E.DK_MA_NrdOstSj
DK_W.DK_MA_Silk
DK_W.DK_MA_Sndborg
DK_W.DK_MA_Viborg
DK_W.DK_MAM_Aars
DK_W.DK_MAM_Frdhavn
DK_W.DK_MAM_Had
DK_W.DK_MAM_Hammel
DK_W.DK_MAM_Hobro
DK_E.DK_MAM_Naestved
DK_E.DK_MAM_NrAlslev
DK_W.DK_MAM_Nyborg
DK_E.DK_MAM_Nyk
DK_W.DK_MAM_Skagen
DK_E.DK_MAM_Slagelse
DK_W.DK_MAM_Svend
DK_W.DK_MAM_Thisted


DK_W.DK_CA_Aab_DEC
DK_W.DK_CA_Aal_DEC
DK_W.DK_CA_Aarhus_DEC
DK_W.DK_CA_Esb_DEC
DK_W.DK_CA_Hern_DEC
DK_E.DK_CA_Kal_DEC
DK_E.DK_CA_KBH_DEC
DK_W.DK_CA_Odense_DEC
DK_W.DK_CA_Randers_DEC
DK_E.DK_CA_Roenne_DEC
DK_W.DK_CA_TVIS_DEC
DK_E.DK_CA_Vestfrb_DEC
DK_E.DK_E_DTU_DEC
DK_W.DK_MA_Grenaa_DEC
DK_E.DK_MA_Hil_DEC
DK_W.DK_MA_Hjoerring_DEC
DK_W.DK_MA_Holst_DEC
DK_W.DK_MA_Horsens_DEC
DK_E.DK_MA_NrdOstSj_DEC
DK_W.DK_MA_Silk_DEC
DK_W.DK_MA_Sndborg_DEC
DK_W.DK_MA_Viborg_DEC
DK_W.DK_MAM_Aars_DEC
DK_W.DK_MAM_Frdhavn_DEC
DK_W.DK_MAM_Had_DEC
DK_W.DK_MAM_Hammel_DEC
DK_W.DK_MAM_Hobro_DEC
DK_E.DK_MAM_Naestved_DEC
DK_E.DK_MAM_NrAlslev_DEC
DK_W.DK_MAM_Nyborg_DEC
DK_E.DK_MAM_Nyk_DEC
DK_W.DK_MAM_Skagen_DEC
DK_E.DK_MAM_Slagelse_DEC
DK_W.DK_MAM_Svend_DEC
DK_W.DK_MAM_Thisted_DEC



*We might need to define some kind of neighbouring areas in order to warranty transport of waste outside Fyn.
*DK_E.DK_JUTLAND_all
*DK_W.DK_SEALAND_all



BC_RRR. BC_AAA
PL_RRR. PL_AAA



* The following DK_LASOE does not have a natural region, so just assign arbitrarily (.. presently I think that it does not matter :) )


* DE_RRR.DE_RRR_AAA
* NO_RRR.NO_RRR_AAA
* SE_RRR.SE_RRR_AAA
/;

TABLE PROCDATA(PROC,PROCDATASET)    "Process data"

                                   PROCINVCOST   PROCKAPVARIABLE    PROCFROMYEAR   PROCLIFETIME     PROCFIXCOST
MSW_chp_small                         6.97              1              2000            20             1000
MSW_chp_medium                        6.25              1              2000            20             1000
MSW_chp_large                         5.85              1              2000            20             1000
MSW_ho                                4.81              1              2000            20             1000
RDF_chp                               6.67              1              2000            20             1000
RDF_ho                                5.13              1              2000            20             1000
RDF_storage                           0.00001           1              2000            25             1000

;


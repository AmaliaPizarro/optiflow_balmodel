SET PROC_T(PROC) "PROC that operate on high time resolution T"
/

HeatCoolDown
HeatUsage
HeatgenfromBurn

Landfill_37

WasteincinCHP_35
BiogasCHP
Biogas_Upgrading
BottomashProcessing_36


ElecBuffer
HeatBuffer

HeatGeneration

Money_buffer_T
GHG_Buffer
GHGbio_Buffer

ProcHeatGenLinkStorage

GasBuffer
*DieselBuffer
CoalBuffer
StrawBuffer
WoodBuffer
WoodWasteBuffer
WoodPelletsBuffer
*BiooilBuffer

SunHBuffer
BiogasBuffer
UpBiogasBuffer
CoalCCSBuffer
FuelOilBuffer
GasCCSBuffer
WoodPelletsCCSBuffer
SurplusHBuffer


HazardousWBuffer

MSW_Charact_chp
MSW_Charact_ho
MSW_chp_small
MSW_chp_medium
MSW_chp_large
MSW_ho
RDF_Charact_chp
RDF_Charact_ho
RDF_chp
RDF_ho
SUM_MSW_charact_chp
SUM_MSW_charact_ho
SUM_RDF_charact_chp
SUM_RDF_charact_ho


Bottom_ash_MSW_chp_small
Bottom_ash_MSW_chp_medium
Bottom_ash_MSW_chp_large

Bottom_ash_MSW_ho
Bottom_ash_RDF_chp
Bottom_ash_RDF_ho


/;


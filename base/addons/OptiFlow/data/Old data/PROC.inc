
SET PROC "All 'transformations' that we use"
/
* NB: for debugging, print and ailing memory nice to have FLOW elements in UPPERCASE og Proc elements in CamelCase.

* The following three elements are obligatory (they will be used in the code as quoted elements)

HeatCoolDown
HeatUsage
HeatgenfromBurn

Imported_HighCV_Use
RDF_StorageSplit
RDF_low-CV_Source
RDF_high-CV_Source
RDF_Export

Recycling_plastic
Recycling_iron
Recycling_nonferrous
SUM_RDF_Storage
DigestateDewatering


ProcEximTruck
ProcEximShip
ProcEximTrain

SourceSegregOF_50
SourceSegregOF_25
SourceSegregOF_No
GenerationHHW
SourceSegregOF_TOTAL

Anaerob_CoDigestion           "Anaerobic co-digestion of source segregated biowaste with Manure�(75% w./w.)+Biogas Desulfurization"
Anaerob_MoDigestion           "Anaerobic mono-digestion of the Organic Fraction of Household Waste separated in a Material Recovery Facility in a reactor type AIKAN+Biogas Desulfurization"
*BiogasCHP                    "Combined Heat and Power production from biogas burning in an engine"
*Biogas_SplitCHPorTransp      "Process for selection of biogas use: direct combustion for CHP vs. upgrading till biomethane standard"
Biogas_SumIn                  "Process for sumatorium of biogas production from anerobic mono and co-digestion"
*Biogas_Upgrading             "Biogas upgrading: CO2 removal + COmpression and transport towards the Natural Gas grid"
Bottom_ash_MSW_chp            "Treatment of bottom ash from MSW CHP incineration plant"
Bottom_ash_MSW_ho             "Treatment of bottom ash from MSW HO incineration plant"
Bottom_ash_RDF_chp            "Treatment of bottom ash from RDF CHP incineration plant"
Bottom_ash_RDF_ho             "Treatment of bottom ash from RDF HO incineration plant"
CurbsideCollectBioW           "Curbside collection of bio waste sorted out for anaerobic co-digestion with manure"
CurbsideCollectResHHW_1       "Curbside collection of mixed combustible waste - Area with high farm density"
CurbsideCollectResHHW_2       "Curbside collection of mixed combustible waste - Area with medium farm density"
CurbsideCollectResHHW_3       "Curbside collection of mixed combustible waste - Area with low farm density"
Digestateonland               "Digestate outdoor storage and spreading on field from anaerobic co-digestion, substituting fertilizer production"
DigestateStorSpread           "Digestate outdoor storage and spreading on field from anaerobic mono-digestion, non substituting fertilizer production"
HHW_Separation                "Selection of route for Mixed Combustible Waste: direct combustion vs. Material Recovery Facility "
ManureBuffer                  "Manure from pigs, cows, chicken or fur generated"
MRF_plant                     "Material Recovery Facility"
MRF_plant_costs               "Artificial process created to account total cost of the MRF refered to a total input flow"
MSW_Charact_chp               "Characterization (HV,Ash,C-bio,C-fossil) of the MSW going to CHP incineration plants"
MSW_Charact_ho                "Characterization (HV,Ash,C-bio,C-fossil) of the MSW going to HO incineration plants"
*MSW_chp                       "MSW CHP Incineration plant"
MSW_ho                        "MSW HO Incineration plant"
MSW_IncinerationType          "Decission Process between CHP or HO incineration of MSW"
MSW_Linkstorage_chp           "Link storage process between week to hour resolution: MSW storage to CHP dispatch" !!! Check that the storage is on a weekly basis
MSW_Linkstorage_ho            "Link storage process between week to hour resolution: MSW storage to HO dispatch"  !!! Check that the storage is on a weekly basis
PretreatmentSep               "Pretreatment of the source segregate biowaste prior anaerobic co-digestion with manure"
ProcBiogasCHPLinkStorage      "Link storage process between day to hour resolution: Biogas production to direct burning for CHP" !!! Check that the storage is on a daily basis
ProcBiogasUpgradeLinkStorage  "Link storage process between day to hour resolution: Biogas production to upgrading process for biomethane refining" !!! Check that the storage is on a daily basis
RDF_Charact_chp               "Characterization (HV,Ash,C-bio,C-fossil) of the RDF going to dedicated RDF CHP incineration plants"
RDF_Charact_ho                "Characterization (HV,Ash,C-bio,C-fossil) of the RDF going to dedicated RDF HO incineration plants"
RDF_chp                       "Dedicated RDF CHP incineration plant"
RDF_ho                        "Dedicated RDF HO incineration plant"
RDF_IncinerationType          "Decission Process between CHP or HO incineration of RDF"
RDF_Linkstorage_chp           "Link storage process between season to hour resolution: RDF storage to CHP dispatch" !!! Check that the storage is on a seasonal basis
RDF_Linkstorage_ho            "Link storage process between season to hour resolution: RDF storage to HO dispatch" !!! Check that the storage is on a seasonal basis
RDF_processing                "MSW processing to RDF after a MRF"
Recycling_facility            "Sink process out of the boundaries of OptiWaste, where Recyclable fractions are sent"
SourceSegreg_pre              "Process that defines the amount of biowaste source segregated at home"
Sum_MRF_plant                 "Sum of MSW amounts (ton) sent to a MRF"
SUM_MSW_charact_chp           "Sum of fractions sent to MSW CHP incineration plant"
SUM_MSW_charact_ho            "Sum of fractions sent to MSW HO incineration plant"
SUM_organic                   "Sum of the organic fraction segregated in the MRF sent to anaerobic mono-digestion"
SUM_RDF_charact_chp           "Sum of fractions sent to dedicated RDF CHP incineration plant"
SUM_RDF_charact_ho            "Sum of fractions sent to dedicated RDF HO incineration plant"
SUM_RDF_processing            "Sum of combustible waste from a MRF, which is further processing to RDF standard"
SUM_Recyclables               "Sum of recyclable fractions separated in a MRF"
SUM_refuse_MRF                "Sum of waste from a MRF sent to direct mass burning"

RDF_high-CV_Buffer        "Import of RDF with high calorif value - Buffer Process to account avoided treatment abroad"
RDF_low-CV_Buffer         "Import of RDF with low calorif value - Buffer Process to account avoided treatment abroad"


IndustrialWasteGen         "Generation of industrial waste"
IndustrySegreg_pre         "Generation type of industrial waste"
IndustryCollectBio         "Collection of the organic fraction of industrial waste"
IndustryPretreatmentSep    "Pre-treatment of the organic fraction of industrial waste prior anaerobic co-digestion with manure"
Anaerob_IndCoDigestion     "Anaerobic co-digestion of the organic fraction of industrial waste (25% w./w.) with manure"
DigestateIndStorSpread     "Digestate outdoor storage and spreading on field from anaerobic co-digestion of industrial waste with manure, substituting fertilizer production"
IndustryCollectCW          "Collection of the mixed combustible fraction of industrial waste"
IndW_Separation            "Selection of route for Industrial mixed Combustible Waste: direct combustion vs. RDF processing "
Ind_RDF_processing         "Processing of industrial mixed combustible waste to RDF"


ElecBuffer                "Electricity Buffer PROC"
HeatBuffer                "Heat (district heating, at plant level) Buffer PROC"
GasBuffer                 "Gas in regional gas system Buffer PROC"
DieselBuffer              "Diesel Buffer PROC"
*Elec_ForProcesses        "Electricity supply for processes"  UD Hans
*Heat_ForProcesses        "Heat supply for processes"         UD Hans
CoalBuffer                 "Coal Buffer PROC"
StrawBuffer                "Straw Buffer PROC"
WoodBuffer                 "Wood Buffer PROC"
WoodWasteBuffer            "Wood Waste Buffer PROC"
WoodPelletsBuffer          "Wood Pellet Buffer PROC"
BiooilBuffer               "Biooil Buffer PROC"
HazardousWBuffer           "Hazardous Waste Buffer PROC"
RDFBuffer                  "Buffer related to Refuse Derived Fuel for import/export towards other areas"

SunHBuffer                       "Thermal Solar Buffer PROC"
BiogasBuffer                     "Biogas Buffer PROC"
UpBiogasBuffer                   "Upgraded biogas Buffer PROC"
CoalCCSBuffer                    "Coal CCS Buffer PROC"
FuelOilBuffer                    "Fuel oil Buffer PROC"
GasCCSBuffer                     "Natural gas CCS Buffer PROC"
WoodPelletsCCSBuffer             "Wood pellet CCS Buffer PROC"
SurplusHBuffer                   "Surplus Heat Buffer PROC"



HouseHoldWasteGen        "Generation of household waste"
*SourceSegreg_pre         "Source segregation of household waste PRIOR to SourceSegregHHW_46_54 - artificial"
SourceSegregHHW_46_54    "Source segregation of household waste (Process ID 46 for A1 scneario and 54 for A2 scenario)"
CurbsideCollectBioW_4    "Curbside collection of bio waste sorted out for biogas production, A2 scenario"
*CurbsideCollectResHHW_2  "Curbside collection of bio and remaining waste, A2 scenario"

GJSumIn                  "Summing of thermal energy (GJ) in waste fractions into total thermal energy entering waste incineration"
NonGJSumIn               "Summing of masses (tonnes) in waste fractions into total mass entering waste incineration, later linked to bottom ash processing"
WasteincinCHP_35         "Waste incineration for CHP generation"
BottomashProcessing_36   "Bottom ash sorting for metals to recover"
Residue_TonToGJConv      "Residue going to incineration, conversion of tonne to GJ, fictive process"

PretreatmentSep_55       "Pretreatment/separation of bio waste before digestion"
*AnaerobDigestion_56      "Anaerobic digestion of bio waste yielding biogas as main output (digestate as residue), this process has been subdivided between from monodigestion or codigestion origin, as indicated below, (Amalia, October 2014)"
Biogas_SplitCHPorTransp  "Artificial process enabling the choice of using biogas for CHP or upgrade for use as fuel in vehicles"
BiogasCHP                "Biogas based CHP generation, no process ID received yet"
Biogas_Upgrading         "Upgrading of biogas, enabling use as fuel in vehicles (removal of CO2 etc.)"
*Biogas_UseInVehicle      "Use of upgraded biogas (Compressed Bio Gas, CBG) as fuel in vehicle driving"    removed 201440611, 'replaced' by GasBuffer
*Avoided_PetrolInVehicle  "Avoided use of petrol as fuel in vehicle driving"                               removed 201440611, 'replaced' by GasBuffer

Wasteincin_ElecDisplace  "Displaced electricity generation due to waste incineration at CHP plant"
Wasteincin_HeatDisplace  "Displaced heat generation due to waste incineration at CHP plant"
Biogas_ElecDisplace      "Displaced electricity generation due to biogas CHP"
Biogas_HeatDisplace      "Displaced heat generation due to biogas CHP"

*Economy of scale in incineration plants

MSW_chp_small
MSW_chp_medium
MSW_chp_large

Bottom_ash_MSW_chp_small
Bottom_ash_MSW_chp_medium
Bottom_ash_MSW_chp_large

RDF_Storage



* Manure section:  -------------------------------------------------------------
*DigestateStorSpread      "Digestate outdoor storage and spreading on field, this process has been subdivided between from monodigestion or codigestion origin, as indicated below, (Amalia, October 2014)"
*Manure_StorageandSpread  "Manure outdoor storage and spreading on field"
*Avoided_FertilizProd_N   "Avoided mineral N fertilizer production (ammonium nitrate)"
*Avoided_FertilizProd_P   "Avoided mineral P fertilizer production (diammonium phosphate)"
*Avoided_FertilizProd_K   "Avoided mineral K fertilizer production (potassium chloride)"
*Avoided_ManureStorSpread "Avoided outdoor storage and spreading of manure on field"
*FertilizProd_N           "Mineral N fertilizer production (ammonium nitrate)"
*FertilizProd_P           "Mineral P fertilizer production (diammonium phosphate)"
*FertilizProd_K           "Mineral K fertilizer production (potassium chloride)"
* New:
ManureCows          !! Source, fixed amount
NPK_split
Field_N             !! Buffer
Field_P             !! Buffer
Field_K             !! Buffer
* End Manure section:  ---------------------------------------------------------

Landfill_37              "Landfilling (dumping waste on landfill)"

Money_buffer             "Money buffer, delivers money inputs to processes and receives money outputs from processes"
Money_buffer_T           "Money buffer, delivers money inputs to processes and receives money outputs from processes"
GHG_Buffer               "Greenhouse gas emission sink from fossil origin, receives GHG emissions from processes, used in quantifying INDICSVALUES"
GHGbio_Buffer            "Greenhouse gas emission sink from biogenic origin, receives GHG emissions from processes, used in quantifying INDICSVALUES"
GHG_Buffer_T              "Greenhouse gas emission sink from fossil origin, receives GHG emissions from processes, used in quantifying INDICSVALUES"
GHGbio_Buffer_T            "Greenhouse gas emission sink from biogenic origin, receives GHG emissions from processes, used in quantifying INDICSVALUES"

* ud 20130517  CurbsideCollectResHHW_2  "Curbside collection of residual household waste, A1 scenario"

*Not used anymore, however, were used in data files
*ElecSysGen               "Electricity generation in the non-waste sector"  !! Ud Hans
*HeatSysGen               "Heat generation in the non-waste sector"         !! Ud Hans
*ElecDelivery             "Delivery of electricity to the system - will in the current Optiwaste model be credited in the form of displaced electricity generation"    !! Hans : det vil v�re klarere, hvis efterf�lgende 'Delivery' erstattes af e.g. 'Net' for ikke at antyde at der er tale om elproduktion:   !! ud, Hans
*HeatDelivery             "Delivery of heat to the system - will in the current Optiwaste model be credited in the form of displaced heat generation"                  !! Hans : det vil v�re klarere, hvis efterf�lgende 'Delivery' erstattes af e.g. 'Net' for ikke at antyde at der er tale om elproduktion:   !! ud, Hans


*New Processes introduced for selection of codigestion vs. monodigestion (Amalia, October 2014)
AnaerobDigestion_split
AnaerobDigestion_56_mono
AnaerobDigestion_56_co
BiogasSumIn
DigestateStorSpread_mono
DigestateStorSpread_co
NSumIn
PSumIn
KSumIn


* New
*ProcExim                "Connection point for exporting from and importing to and Area"

ProcElecLinkStorage
ProcWasteIncinCHPLinkStorage
*ProcBiogasUpgradeLinkStorage
*ProcBiogasCHPLinkStorage
ProcHWLinkStorage
ProcManureLinkStorage
$ifi %keep_ProcMoneyLinkStorage%==yes ProcMoneyLinkStorage
ProcGHGNonBioLinkStorage
ProcGHGBioLinkStorage
ProcHeatGenLinkStorage
ProcDieselLinkStorage

HeatGeneration

ElectricityConsump
DieselConsump

ManureTreatmentDecision

*MoneyConsump    Hans: deleted, there seems to be to 'money'buffers, MoneyConsump and Money_buffer
*GHGNonBioGen
*GHGBioGen


*Process for generation of heat and electricity

DK_CA_Aab_SteamTur_EXT_CO
DK_CA_Aab_Engine_NG
DK_CA_Aab_SteamTur-Medi-BP-WO
DK_CA_Aab_DEC_Engine_BGN
DK_CA_Aab_DEC_Engine_NG
DK_CA_Aab_DEC_GasTur-NG
DK_CA_Aal_Engine_BGN
DK_CA_Aal_SteamTur_EXT_CO
DK_CA_Aal_Engine_LO
DK_CA_Aal_SteamTur_LO
DK_CA_Aal_Engine_NG
DK_CA_Aal_DEC_Engine_BGN
DK_CA_Aal_DEC_Engine_LO
DK_CA_Aal_DEC_Engine_NG
DK_CA_Aal_DEC_GasTur-NG
DK_CA_Aarhus_Engine_BGN
DK_CA_Aarhus_SteamTur_EXT_CO
DK_CA_Aarhus_Engine_NG
DK_CA_Aarhus_DEC_Engine_BGN
DK_CA_Aarhus_DEC_Engine_LO
DK_CA_Aarhus_DEC_SteamTur_FO
DK_CA_Aarhus_DEC_Engine_NG
DK_CA_Aarhus_DEC_Biomass_Gasification
DK_CA_Esb_Engine_BGN
DK_CA_Esb_SteamTur_EXT_CO
DK_CA_Esb_Engine_LO
DK_CA_Esb_Engine_NG
DK_CA_Esb_DEC_Engine_BGN
DK_CA_Esb_DEC_Engine_LO
DK_CA_Esb_DEC_Engine_NG
DK_CA_Esb_DEC_GasTur-NG
DK_CA_Hern_Engine_BGN
DK_CA_Hern_Engine_LO
DK_CA_Hern_Engine_NG
DK_CA_Hern_SteamTur-LARGE-BP-WO
DK_CA_Hern_DEC_Engine_BGN
DK_CA_Hern_DEC_SteamTur_EXT_CO
DK_CA_Hern_DEC_Engine_LO
DK_CA_Hern_DEC_Engine_NG
DK_CA_Hern_DEC_GasTur-NG
DK_CA_Kal_SteamTur_EXT_CO
DK_CA_Kal_Engine_LO
DK_CA_Kal_Engine_NG
DK_CA_Kal_DEC_Engine_BGN
DK_CA_Kal_DEC_Engine_LO
DK_CA_Kal_DEC_Engine_NG
DK_CA_KBH_Engine_BGN
DK_CA_KBH_SteamTur_EXT_CO
DK_CA_KBH_Engine_LO
DK_CA_KBH_Engine_NG
DK_CA_KBH_GasTur-NG
DK_CA_KBH_SteamTur_EXT_WP
DK_CA_KBH_DEC_Engine_BGN
DK_CA_KBH_DEC_SteamTur_EXT_CO
DK_CA_KBH_DEC_Engine_LO
DK_CA_KBH_DEC_Engine_NG
DK_CA_KBH_DEC_GasTur-NG
DK_CA_KBH_DEC_SteamTur-Medi-BP-WO
DK_CA_Odense_Engine_BGN
DK_CA_Odense_SteamTur_EXT_CO
DK_CA_Odense_Engine_LO
DK_CA_Odense_Engine_NG
DK_CA_Odense_SteamTur_Medi_BP_ST
DK_CA_Odense_SteamTur-Medi-BP-WO
DK_CA_Odense_DEC_Engine_BGN
DK_CA_Odense_DEC_Engine_LO
DK_CA_Odense_DEC_Engine_NG
DK_CA_Odense_DEC_GasTur-NG
DK_CA_Odense_DEC_SteamTur-Medi-BP-WO
DK_CA_Randers_Engine_LO
DK_CA_Randers_Engine_NG
DK_CA_Randers_SteamTur-Medi-BP-WO
DK_CA_Randers_DEC_Engine_BGN
DK_CA_Randers_DEC_Engine_LO
DK_CA_Randers_DEC_Engine_NG
DK_CA_Randers_DEC_GasTur-NG
DK_CA_Roenne_DEC_Engine_BGN
DK_CA_Roenne_DEC_SteamTur_EXT_CO
DK_CA_Roenne_DEC_Engine_LO
DK_CA_TVIS_Engine_BGN
DK_CA_TVIS_GasTurCC-NG
DK_CA_TVIS_GasTur-NG
DK_CA_TVIS_DEC_Engine_BGN
DK_CA_TVIS_DEC_SteamTur_EXT_CO
DK_CA_TVIS_DEC_Engine_LO
DK_CA_TVIS_DEC_Engine_NG
DK_CA_Vestfrb_DEC_Engine_LO
DK_CA_Vestfrb_DEC_Engine_NG
DK_CA_Vestfrb_DEC_GasTur-NG
DK_E_DTU_GasTur-NG
DK_MA_Grenaa_SteamTur_Medi_BP_WW
DK_MA_Grenaa_DEC_Engine_BGN
DK_MA_Hil_Engine_LO
DK_MA_Hil_Engine_NG
DK_MA_Hil_GasTur-NG
DK_MA_Hil_DEC_Engine_BGN
DK_MA_Hil_DEC_Engine_CND_NG
DK_MA_Hil_DEC_Engine_LO
DK_MA_Hil_DEC_Engine_NG
DK_MA_Hil_DEC_Biomass_Gasification
DK_MA_Hjoerring_Engine_NG
DK_MA_Hjoerring_GasTur-NG
DK_MA_Hjoerring_DEC_Engine_BGN
DK_MA_Hjoerring_DEC_Engine_LO
DK_MA_Holst_Engine_BGN
DK_MA_Holst_DEC_Engine_BGN
DK_MA_Holst_DEC_Engine_LO
DK_MA_Holst_DEC_Engine_NG
DK_MA_Holst_DEC_GasTur-NG
DK_MA_Holst_DEC_SteamTur_Medi_BP_ST
DK_MA_Horsens_DEC_Engine_LO
DK_MA_Horsens_DEC_Engine_NG
DK_MA_NrdOstSj_Engine_NG
DK_MA_NrdOstSj_GasTur-NG
DK_MA_NrdOstSj_DEC_Engine_BGN
DK_MA_NrdOstSj_DEC_Engine_NG
DK_MA_Silk_Engine_BGN
DK_MA_Silk_Engine_LO
DK_MA_Silk_GasTur-NG
DK_MA_Sndborg_DEC_Engine_BGN
DK_MA_Sndborg_DEC_Engine_NG
DK_MA_Viborg_Engine_BGN
DK_MA_Viborg_Engine_NG
DK_MA_Viborg_GasTur-NG
DK_MA_Viborg_DEC_Engine_BGN
DK_MA_Viborg_DEC_Engine_LO
DK_MA_Viborg_DEC_Engine_NG
DK_MA_Viborg_DEC_Biomass_Gasification
DK_MAM_Aars_Engine_LO
DK_MAM_Aars_Engine_NG
DK_MAM_Aars_DEC_Engine_BGN
DK_MAM_Aars_DEC_Engine_LO
DK_MAM_Aars_DEC_Engine_NG
DK_MAM_Aars_DEC_GasTur-NG
DK_MAM_Frdhavn_Engine_BGN
DK_MAM_Frdhavn_Engine_LO
DK_MAM_Frdhavn_GasTur-NG
DK_MAM_Frdhavn_DEC_Engine_BGN
DK_MAM_Frdhavn_DEC_Engine_LO
DK_MAM_Frdhavn_DEC_Engine_NG
DK_MAM_Had_Engine_NG
DK_MAM_Had_DEC_Engine_BGN
DK_MAM_Had_DEC_Engine_LO
DK_MAM_Had_DEC_Engine_NG
DK_MAM_Hammel_DEC_Engine_BGN
DK_MAM_Hammel_DEC_Engine_NG
DK_MAM_Hobro_Engine_NG
DK_MAM_Hobro_DEC_Engine_BGN
DK_MAM_Hobro_DEC_Engine_LO
DK_MAM_Hobro_DEC_Engine_NG
DK_MAM_Naestved_Engine_NG
DK_MAM_Naestved_DEC_Engine_BGN
DK_MAM_Naestved_DEC_Engine_NG
DK_MAM_Naestved_DEC_SteamTur_Medi_BP_ST
DK_MAM_Nyborg_GasTur-NG
DK_MAM_Nyborg_Incinerator-HazardousWaste
DK_MAM_Nyk_SteamTur_FO
DK_MAM_Nyk_DEC_Engine_BGN
DK_MAM_Nyk_DEC_SteamTur_EXT_CO
DK_MAM_Nyk_DEC_Engine_LO
DK_MAM_Nyk_DEC_SteamTur_Medi_BP_ST
DK_MAM_Skagen_Engine_NG
DK_MAM_Skagen_GasTur-NG
DK_MAM_Slagelse_DEC_Engine_BGN
DK_MAM_Slagelse_DEC_Engine_LO
DK_MAM_Slagelse_DEC_Engine_NG
DK_MAM_Svend_Engine_NG
DK_MAM_Svend_DEC_Engine_NG
DK_MAM_Svend_DEC_SteamTur_Medi_BP_ST
DK_MAM_Svend_DEC_SteamTur-Medi-BP-WO
DK_MAM_Thisted_Engine_NG
DK_MAM_Thisted_DEC_Engine_BGN
DK_MAM_Thisted_DEC_Engine_LO
DK_MAM_Thisted_DEC_Engine_NG
Biofuel_SURPLUS_H
Boiler-NG
Boiler-ST
Boiler-WO
Boiler-WP
Central-CHP-BG-10_19
Central-CHP-BG-15
Central-CHP-BG-20_29
Central-CHP-BG-30
Central-CHP-BG-EC-15
DK_CA_Aab_Boiler_BO
DK_CA_Aab_Boiler_FO
DK_CA_Aab_Boiler_LO
DK_CA_Aab_Boiler_NG
DK_CA_Aab_Boiler_ST
DK_CA_Aab_Boiler_WO
DK_CA_Aab_Boiler_WW
DK_CA_Aab_DEC_Boiler_BG
DK_CA_Aab_DEC_Boiler_NG
DK_CA_Aab_DEC_Boiler_WO
DK_CA_Aal_Boiler_BG
DK_CA_Aal_Boiler_BO
DK_CA_Aal_Boiler_LO
DK_CA_Aal_Boiler_NG
DK_CA_Aal_DEC_Boiler_BG
DK_CA_Aal_DEC_Boiler_BO
DK_CA_Aal_DEC_Boiler_FO
DK_CA_Aal_DEC_Boiler_LO
DK_CA_Aal_DEC_Boiler_NG
DK_CA_Aal_DEC_Boiler_ST
DK_CA_Aal_DEC_Boiler_WO
DK_CA_Aal_DEC_Boiler_WP
DK_CA_Aal_DEC_El_Boiler
DK_CA_Aal_DEC_SolarDH
DK_CA_Aarhus_Boiler_FO
DK_CA_Aarhus_Boiler_LO
DK_CA_Aarhus_Boiler_NG
DK_CA_Aarhus_Boiler_ST
DK_CA_Aarhus_Boiler_WO
DK_CA_Aarhus_Boiler_WP
DK_CA_Aarhus_DEC_Boiler_BG
DK_CA_Aarhus_DEC_Boiler_FO
DK_CA_Aarhus_DEC_Boiler_LO
DK_CA_Aarhus_DEC_Boiler_NG
DK_CA_Aarhus_DEC_Boiler_ST
DK_CA_Aarhus_DEC_Boiler_WO
DK_CA_Aarhus_DEC_Boiler_WP
DK_CA_Aarhus_DEC_El_Boiler
DK_CA_Aarhus_DEC_HeatPump_El
DK_CA_Aarhus_DEC_SolarDH
DK_CA_Esb_Boiler_BO
DK_CA_Esb_Boiler_LO
DK_CA_Esb_Boiler_NG
DK_CA_Esb_DEC_Boiler_LO
DK_CA_Esb_DEC_Boiler_NG
DK_CA_Esb_DEC_Boiler_WO
DK_CA_Esb_DEC_Boiler_WP
DK_CA_Esb_DEC_El_Boiler
DK_CA_Esb_DEC_SolarDH
DK_CA_Hern_Boiler_BG
DK_CA_Hern_Boiler_BO
DK_CA_Hern_Boiler_LO
DK_CA_Hern_Boiler_NG
DK_CA_Hern_Boiler_WO
DK_CA_Hern_Boiler_WP
DK_CA_Hern_DEC_Boiler_BG
DK_CA_Hern_DEC_Boiler_BO
DK_CA_Hern_DEC_Boiler_FO
DK_CA_Hern_DEC_Boiler_LO
DK_CA_Hern_DEC_Boiler_NG
DK_CA_Hern_DEC_Boiler_WO
DK_CA_Hern_DEC_Boiler_WP
DK_CA_Hern_DEC_Boiler_WW
DK_CA_Hern_DEC_El_Boiler
DK_CA_Hern_DEC_SolarDH
DK_CA_Kal_Boiler_BG
DK_CA_Kal_Boiler_BO
DK_CA_Kal_Boiler_LO
DK_CA_Kal_Boiler_NG
DK_CA_Kal_Boiler_WO
DK_CA_Kal_Boiler_WW
DK_CA_Kal_DEC_Boiler_BG
DK_CA_Kal_DEC_Boiler_LO
DK_CA_Kal_DEC_Boiler_NG
DK_CA_Kal_DEC_Boiler_ST
DK_CA_Kal_DEC_Boiler_WO
DK_CA_Kal_DEC_Boiler_WP
DK_CA_Kal_DEC_El_Boiler
DK_CA_Kal_El_Boiler
DK_CA_Kal_SolarDH
DK_CA_KBH_Boiler_BG
DK_CA_KBH_Boiler_FO
DK_CA_KBH_Boiler_LO
DK_CA_KBH_Boiler_NG
DK_CA_KBH_Boiler_WP
DK_CA_KBH_DEC_Boiler_BO
DK_CA_KBH_DEC_Boiler_FO
DK_CA_KBH_DEC_Boiler_LO
DK_CA_KBH_DEC_Boiler_NG
DK_CA_KBH_DEC_Boiler_ST
DK_CA_KBH_DEC_Boiler_WO
DK_CA_KBH_El_Boiler
DK_CA_KBH_Geo_El_HeatPump
DK_CA_Odense_Boiler_BG
DK_CA_Odense_Boiler_LO
DK_CA_Odense_Boiler_NG
DK_CA_Odense_Boiler_ST
DK_CA_Odense_DEC_Boiler_BO
DK_CA_Odense_DEC_Boiler_CO
DK_CA_Odense_DEC_Boiler_LO
DK_CA_Odense_DEC_Boiler_NG
DK_CA_Odense_DEC_Boiler_ST
DK_CA_Odense_DEC_Boiler_WO
DK_CA_Odense_DEC_Boiler_WP
DK_CA_Odense_DEC_Boiler_WW
DK_CA_Odense_DEC_El_Boiler
DK_CA_Odense_El_Boiler
DK_CA_Odense_HeatPump_El
DK_CA_Randers_Boiler_BO
DK_CA_Randers_Boiler_LO
DK_CA_Randers_Boiler_NG
DK_CA_Randers_DEC_Boiler_LO
DK_CA_Randers_DEC_Boiler_NG
DK_CA_Randers_DEC_Boiler_ST
DK_CA_Randers_DEC_Boiler_WO
DK_CA_Randers_DEC_Boiler_WW
DK_CA_Roenne_Boiler_ST
DK_CA_Roenne_Boiler_WP
DK_CA_Roenne_DEC_Boiler_FO
DK_CA_Roenne_DEC_Boiler_LO
DK_CA_Roenne_DEC_Boiler_NG
DK_CA_Roenne_DEC_Boiler_ST
DK_CA_Roenne_DEC_Boiler_WO
DK_CA_Roenne_DEC_Boiler_WP
DK_CA_TVIS_Boiler_LO
DK_CA_TVIS_Boiler_NG
DK_CA_TVIS_Boiler_WP
DK_CA_TVIS_DEC_Boiler_BG
DK_CA_TVIS_DEC_Boiler_BO
DK_CA_TVIS_DEC_Boiler_LO
DK_CA_TVIS_DEC_Boiler_NG
DK_CA_TVIS_DEC_Boiler_WO
DK_CA_TVIS_DEC_Boiler_WP
DK_CA_TVIS_DEC_El_Boiler
DK_CA_TVIS_DEC_SolarDH
DK_CA_Vestfrb_DEC_Boiler_NG
DK_E_DTU_Boiler_NG
DK_MA_Grenaa_Boiler_FO
DK_MA_Grenaa_Boiler_LO
DK_MA_Grenaa_Boiler_NG
DK_MA_Grenaa_Boiler_WO
DK_MA_Grenaa_DEC_Boiler_FO
DK_MA_Grenaa_DEC_Boiler_LO
DK_MA_Grenaa_DEC_Boiler_ST
DK_MA_Grenaa_DEC_Boiler_WO
DK_MA_Hil_Boiler_BG
DK_MA_Hil_Boiler_NG
DK_MA_Hil_Boiler_WO
DK_MA_Hil_Boiler_WP
DK_MA_Hil_DEC_Boiler_BO
DK_MA_Hil_DEC_Boiler_LO
DK_MA_Hil_DEC_Boiler_NG
DK_MA_Hil_DEC_Boiler_WO
DK_MA_Hil_DEC_Boiler_WP
DK_MA_Hil_DEC_El_Boiler
DK_MA_Hil_SolarDH
DK_MA_Hjoerring_Boiler_NG
DK_MA_Hjoerring_Boiler_WO
DK_MA_Hjoerring_Boiler_WP
DK_MA_Hjoerring_DEC_Boiler_NG
DK_MA_Holst_Boiler_LO
DK_MA_Holst_Boiler_NG
DK_MA_Holst_Boiler_WO
DK_MA_Holst_DEC_Boiler_BG
DK_MA_Holst_DEC_Boiler_BO
DK_MA_Holst_DEC_Boiler_LO
DK_MA_Holst_DEC_Boiler_NG
DK_MA_Holst_DEC_Boiler_ST
DK_MA_Holst_DEC_Boiler_WO
DK_MA_Holst_DEC_Boiler_WP
DK_MA_Holst_DEC_Boiler_WW
DK_MA_Holst_DEC_El_Boiler
DK_MA_Holst_DEC_HeatPump_El
DK_MA_Holst_DEC_SolarDH
DK_MA_Holst_El_Boiler
DK_MA_Horsens_Boiler_NG
DK_MA_Horsens_DEC_Boiler_LO
DK_MA_Horsens_DEC_Boiler_NG
DK_MA_Horsens_DEC_Boiler_WO
DK_MA_NrdOstSj_Boiler_BO
DK_MA_NrdOstSj_Boiler_NG
DK_MA_NrdOstSj_Boiler_WO
DK_MA_NrdOstSj_Boiler_WW
DK_MA_Silk_Boiler_LO
DK_MA_Silk_Boiler_NG
DK_MA_Sndborg_Boiler_BO
DK_MA_Sndborg_Boiler_NG
DK_MA_Sndborg_Boiler_WO
DK_MA_Sndborg_DEC_Boiler_LO
DK_MA_Sndborg_DEC_Boiler_NG
DK_MA_Sndborg_DEC_El_Boiler
DK_MA_Sndborg_DEC_SolarDH
DK_MA_Sndborg_SolarDH
DK_MA_Viborg_Boiler_BG
DK_MA_Viborg_Boiler_NG
DK_MA_Viborg_DEC_Boiler_BO
DK_MA_Viborg_DEC_Boiler_NG
DK_MA_Viborg_DEC_Boiler_WO
DK_MA_Viborg_DEC_Boiler_WP
DK_MAM_Aars_Boiler_FO
DK_MAM_Aars_Boiler_NG
DK_MAM_Aars_Boiler_WP
DK_MAM_Aars_DEC_Boiler_BG
DK_MAM_Aars_DEC_Boiler_LO
DK_MAM_Aars_DEC_Boiler_NG
DK_MAM_Aars_DEC_Boiler_ST
DK_MAM_Aars_DEC_Boiler_WO
DK_MAM_Aars_DEC_Boiler_WP
DK_MAM_Aars_DEC_Boiler_WW
DK_MAM_Aars_El_Boiler
DK_MAM_Frdhavn_Boiler_NG
DK_MAM_Frdhavn_DEC_Boiler_LO
DK_MAM_Frdhavn_DEC_Boiler_NG
DK_MAM_Frdhavn_DEC_Boiler_ST
DK_MAM_Frdhavn_DEC_Boiler_WO
DK_MAM_Frdhavn_DEC_Boiler_WW
DK_MAM_Frdhavn_DEC_El_Boiler
DK_MAM_Frdhavn_DEC_SolarDH
DK_MAM_Had_Boiler_NG
DK_MAM_Had_Boiler_WO
DK_MAM_Had_DEC_Boiler_BG
DK_MAM_Had_DEC_Boiler_LO
DK_MAM_Had_DEC_Boiler_NG
DK_MAM_Had_DEC_Boiler_WO
DK_MAM_Had_DEC_Boiler_WP
DK_MAM_Had_DEC_Boiler_WW
DK_MAM_Had_DEC_El_Boiler
DK_MAM_Had_DEC_SolarDH
DK_MAM_Hammel_Boiler_BO
DK_MAM_Hammel_Boiler_WP
DK_MAM_Hammel_DEC_Boiler_LO
DK_MAM_Hammel_DEC_Boiler_NG
DK_MAM_Hammel_DEC_Boiler_WO
DK_MAM_Hammel_DEC_Boiler_WP
DK_MAM_Hammel_DEC_HeatPump_El
DK_MAM_Hobro_Boiler_BO
DK_MAM_Hobro_Boiler_NG
DK_MAM_Hobro_DEC_Boiler_LO
DK_MAM_Hobro_DEC_Boiler_NG
DK_MAM_Hobro_DEC_Boiler_ST
DK_MAM_Hobro_DEC_Boiler_WO
DK_MAM_Naestved_Boiler_NG
DK_MAM_Naestved_DEC_Boiler_NG
DK_MAM_Naestved_DEC_Boiler_ST
DK_MAM_Naestved_DEC_Boiler_WO
DK_MAM_Naestved_DEC_Boiler_WP
DK_MAM_Naestved_DEC_El_Boiler
DK_MAM_NrAlslev_Boiler_NG
DK_MAM_NrAlslev_Boiler_ST
DK_MAM_Nyborg_Boiler_BG
DK_MAM_Nyborg_Boiler_BO
DK_MAM_Nyborg_Boiler_NG
DK_MAM_Nyk_Boiler_BO
DK_MAM_Nyk_Boiler_FO
DK_MAM_Nyk_Boiler_NG
DK_MAM_Nyk_Boiler_ST
DK_MAM_Nyk_Boiler_WO
DK_MAM_Nyk_DEC_Boiler_BG
DK_MAM_Nyk_DEC_Boiler_CO
DK_MAM_Nyk_DEC_Boiler_FO
DK_MAM_Nyk_DEC_Boiler_LO
DK_MAM_Nyk_DEC_Boiler_NG
DK_MAM_Nyk_DEC_Boiler_ST
DK_MAM_Nyk_DEC_Boiler_WO
DK_MAM_Nyk_DEC_Boiler_WP
DK_MAM_Nyk_DEC_Boiler_WW
DK_MAM_Nyk_DEC_SolarDH
DK_MAM_Skagen_Boiler_NG
DK_MAM_Skagen_El_Boiler
DK_MAM_Slagelse_Boiler_NG
DK_MAM_Slagelse_DEC_Boiler_BG
DK_MAM_Slagelse_DEC_Boiler_FO
DK_MAM_Slagelse_DEC_Boiler_LO
DK_MAM_Slagelse_DEC_Boiler_NG
DK_MAM_Slagelse_DEC_Boiler_WO
DK_MAM_Slagelse_DEC_Boiler_WP
DK_MAM_Svend_Boiler_BO
DK_MAM_Svend_Boiler_NG
DK_MAM_Svend_DEC_Boiler_BO
DK_MAM_Svend_DEC_Boiler_LO
DK_MAM_Svend_DEC_Boiler_NG
DK_MAM_Svend_DEC_Boiler_ST
DK_MAM_Svend_DEC_Boiler_WP
DK_MAM_Svend_DEC_HeatPump_El
DK_MAM_Svend_DEC_SolarDH
DK_MAM_Thisted_Boiler_NG
DK_MAM_Thisted_Boiler_ST
DK_MAM_Thisted_Boiler_WP
DK_MAM_Thisted_DEC_Boiler_BO
DK_MAM_Thisted_DEC_Boiler_LO
DK_MAM_Thisted_DEC_Boiler_NG
DK_MAM_Thisted_DEC_Boiler_WO
DK_MAM_Thisted_DEC_Boiler_WP
DK_MAM_Thisted_DEC_El_Boiler
DK_MAM_Thisted_El_Boiler
DK_MAM_Thisted_Geo_El_HeatPump
El-Boiler
Engine-BGn-10_19
Engine-BGn-20_29
Engine-BGn-30_49
Engine-BGn-50
Engine-BGn-EC-10_19
Engine-BGn-EC-20_29
Engine-BGn-EC-30_49
Engine-BGn-EC-50
Engine-NG-10_19
Engine-NG-20_29
Engine-NG-30_49
Engine-NG-50
GasTurCC-BP-BGn-10_19
GasTurCC-BP-BGn-20
GasTurCC-BP-BGn-EC-10_19
GasTurCC-BP-BGn-EC-20
GasTurCC-BP-NG-10_19
GasTurCC-BP-NG-20
GasTurCC-EXT-BGn-10_19
GasTurCC-EXT-BGn-20_29
GasTurCC-EXT-BGn-30_49
GasTurCC-EXT-BGn-50
GasTurCC-EXT-BGn-EC-10_19
GasTurCC-EXT-BGn-EC-20_29
GasTurCC-EXT-BGn-EC-30_49
GasTurCC-EXT-BGn-EC-50
GasTurCC-EXT-NG-10_19
GasTurCC-EXT-NG-20_29
GasTurCC-EXT-NG-30_49
GasTurCC-EXT-NG-50
GasTurCC-EXT-NGccs-30_50
GasTur-EXT-BGn-10_19
GasTur-EXT-BGn-20
GasTur-EXT-BGn-EC-10_19
GasTur-EXT-BGn-EC-20
GasTur-EXT-NG-10_19
GasTur-EXT-NG-20
Geo_EL_HeatPump-10_19
Geo_EL_HeatPump-20
HeatPump-EL-10_19
HeatPump-EL-20_29
HeatPump-EL-30_49
HeatPump-EL-50
SolarDH-10_19
SolarDH-20_29
SolarDH-30
SteamTur-EXT-CO-10_19
SteamTur-EXT-CO-20_29
SteamTur-EXT-CO-30_49
SteamTur-EXT-CO-50
SteamTur-EXT-COccs-30_50
SteamTur-EXT-NG-10_19
SteamTur-EXT-NG-20_50
SteamTur-EXT-WP-10_19
SteamTur-EXT-WP-20_29
SteamTur-EXT-WP-30_49
SteamTur-EXT-WP-50
SteamTur-EXT-WPccs-30_50
SteamTur-LARGE-BP-WO-10_19
SteamTur-LARGE-BP-WO-20_29
SteamTur-LARGE-BP-WO-30
SteamTur-LARGE-EXT-WO-10_19
SteamTur-LARGE-EXT-WO-20_29
SteamTur-LARGE-EXT-WO-30
SteamTur-Medi-BP-ST-10
SteamTur-Medi-BP-WO-10
SteamTur-Medi-BP-WW-10
SteamTur-Small-BP-ST-10_19
SteamTur-Small-BP-ST-20
SteamTur-Small-BP-WO-10_19
SteamTur-Small-BP-WO-20
SteamTur-Small-BP-WW-10_19
SteamTur-Small-BP-WW-20
SurpHeat_FF


*----------------------------------------------------------------------------------------------------------------------------------------------------


/;

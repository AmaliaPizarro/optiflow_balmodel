PARAMETER RAMPUPCOEFF(IPROCFROM,IPROCTO,FLOW)
/
SUM_MSW_charact_chp .  MSW_chp_large   .   MSW_FUELCHP_LARGE_TON     0.15
SUM_MSW_charact_chp .  MSW_chp_medium  .   MSW_FUELCHP_MEDIUM_TON    0.15
SUM_MSW_charact_chp .  MSW_chp_small   .   MSW_FUELCHP_SMALL_TON     0.15
SUM_MSW_charact_chp .  MSW_ho          .   MSW_FUELHO_TON            0.15
SUM_MSW_charact_chp .  RDF_chp         .   RDF_FUELCHP_TON           0.15
SUM_MSW_charact_chp .  RDF_ho          .   RDF_FUELHO_TON            0.15
/
;

!! NOTE: OVERLOADED AS SOSIBU, SOSIBU_AST and SOSIBU_RST ?
* Rules probably should be:
*   for any index combination at most one element in has values.
*   If there is a SOSIBUFLOW_VAR_T/SOSIBUBOUND_VAR_T then SOSIBUBOUND
*   (and only that, not _AST or _RST) has value for the corresponding index combination.
*   Code in the .gms file will then combine all to aone or a few ...

!! TODO: RECONSIERE: MAYBE THIS IS ONLY FOR SOURCE (E.G. ANNUAL VALUES OF WASTE GENERATED)  - IN THIS CASE, CAHNGE NAME
*It would be conveniant to have this parameter as a function of the year
TABLE SOSIBUBOUND(AAA,PROC,FLOW,iLOUPFXSET) "Bounds on Source, Sink and Buffer Process Flows - Annual values"
* Convention for entering data:
*   Enter at most one data item for each PROC,
*   If nothing or 0 is entered then the default values corresponding to the declaration of the variable (positive, negative or free) apply.
*   If EPS is entered then the corresponding value will be zero (CHECK IT).
* The unit for the data may differ between (PROC,FLOW).

*Unit: Flow Units/year (as defined the model 17th June 2015)
*Values for the year 2012 - It would be very conveniant to define this parameter as a function of the year (YYY)
* So that waste generation and heat demand (between others) could be defined in one folder for the different simulations
*Currently manual modification of the parameter required

                                                                                    ILOUPFX_LO           ILOUPFX_UP             ILOUPFX_FX

DK_CA_Aab                . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             90717
DK_CA_Aal                . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             305231
DK_CA_Aarhus             . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             571006
DK_CA_Esb                . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             176256
DK_CA_Hern               . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             192165
DK_CA_Kal                . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             208239
DK_CA_KBH                . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             1833251
DK_CA_Odense             . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             396975
DK_CA_Randers            . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             116885
DK_CA_Roenne             . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             33615
DK_CA_TVIS               . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             340949
*DK_CA_Vestfrb            . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             127743
DK_E_DTU                 . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             60949
DK_MA_Grenaa             . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             40457
DK_MA_Hil                . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             191425
DK_MA_Hjoerring          . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             60457
DK_MA_Holst              . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             143625
DK_MA_Horsens            . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             162617
DK_MA_NrdOstSj           . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             131373
DK_MA_Silk               . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             104511
DK_MA_Sndborg            . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             74027
DK_MA_Viborg             . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             104808
DK_MAM_Aars              . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             34135
DK_MAM_Frdhavn           . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             54392
DK_MAM_Had               . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             55041
DK_MAM_Hammel            . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             55278
DK_MAM_Hobro             . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             72098
DK_MAM_Naestved          . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             149848
DK_MAM_NrAlslev          . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_Nyborg            . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             30333
DK_MAM_Nyk               . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             86261
DK_MAM_Skagen            . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_Slagelse          . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             74539
DK_MAM_Svend             . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             75199
DK_MAM_Thisted           . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             58136
DK_CA_Aab_DEC            . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_CA_Aal_DEC            . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_CA_Aarhus_DEC         . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_CA_Esb_DEC            . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_CA_Hern_DEC           . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_CA_Kal_DEC            . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_CA_KBH_DEC            . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_CA_Odense_DEC         . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_CA_Randers_DEC        . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_CA_Roenne_DEC         . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_CA_TVIS_DEC           . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_CA_Vestfrb_DEC        . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_E_DTU_DEC             . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MA_Grenaa_DEC         . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MA_Hil_DEC            . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MA_Hjoerring_DEC      . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MA_Holst_DEC          . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MA_Horsens_DEC        . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MA_NrdOstSj_DEC       . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MA_Silk_DEC           . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MA_Sndborg_DEC        . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MA_Viborg_DEC         . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_Aars_DEC          . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_Frdhavn_DEC       . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_Had_DEC           . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_Hammel_DEC        . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_Hobro_DEC         . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_Naestved_DEC      . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_NrAlslev_DEC      . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_Nyborg_DEC        . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_Nyk_DEC           . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_Skagen_DEC        . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_Slagelse_DEC      . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_Svend_DEC         . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS
DK_MAM_Thisted_DEC       . HouseHoldWasteGen        .  RESIDUALWASTEHH                                                             EPS

*                                                                                   ILOUPFX_LO           ILOUPFX_UP             ILOUPFX_FX
DK_CA_Aab                . IndustrialWasteGen        .  INDUSTRYWASTE                                                             32793
DK_CA_Aal                . IndustrialWasteGen        .  INDUSTRYWASTE                                                             110337
DK_CA_Aarhus             . IndustrialWasteGen        .  INDUSTRYWASTE                                                             206410
DK_CA_Esb                . IndustrialWasteGen        .  INDUSTRYWASTE                                                             63714
DK_CA_Hern               . IndustrialWasteGen        .  INDUSTRYWASTE                                                             69465
DK_CA_Kal                . IndustrialWasteGen        .  INDUSTRYWASTE                                                             75275
DK_CA_KBH                . IndustrialWasteGen        .  INDUSTRYWASTE                                                             662694
DK_CA_Odense             . IndustrialWasteGen        .  INDUSTRYWASTE                                                             143501
DK_CA_Randers            . IndustrialWasteGen        .  INDUSTRYWASTE                                                             42252
DK_CA_Roenne             . IndustrialWasteGen        .  INDUSTRYWASTE                                                             12151
DK_CA_TVIS               . IndustrialWasteGen        .  INDUSTRYWASTE                                                             123248
*DK_CA_Vestfrb            . IndustrialWasteGen        .  INDUSTRYWASTE                                                             46177
DK_E_DTU                 . IndustrialWasteGen        .  INDUSTRYWASTE                                                             22032
DK_MA_Grenaa             . IndustrialWasteGen        .  INDUSTRYWASTE                                                             14625
DK_MA_Hil                . IndustrialWasteGen        .  INDUSTRYWASTE                                                             69197
DK_MA_Hjoerring          . IndustrialWasteGen        .  INDUSTRYWASTE                                                             21854
DK_MA_Holst              . IndustrialWasteGen        .  INDUSTRYWASTE                                                             51918
DK_MA_Horsens            . IndustrialWasteGen        .  INDUSTRYWASTE                                                             58784
DK_MA_NrdOstSj           . IndustrialWasteGen        .  INDUSTRYWASTE                                                             47489
DK_MA_Silk               . IndustrialWasteGen        .  INDUSTRYWASTE                                                             37779
DK_MA_Sndborg            . IndustrialWasteGen        .  INDUSTRYWASTE                                                             26760
DK_MA_Viborg             . IndustrialWasteGen        .  INDUSTRYWASTE                                                             37887
DK_MAM_Aars              . IndustrialWasteGen        .  INDUSTRYWASTE                                                             12339
DK_MAM_Frdhavn           . IndustrialWasteGen        .  INDUSTRYWASTE                                                             19662
DK_MAM_Had               . IndustrialWasteGen        .  INDUSTRYWASTE                                                             19897
DK_MAM_Hammel            . IndustrialWasteGen        .  INDUSTRYWASTE                                                             19982
DK_MAM_Hobro             . IndustrialWasteGen        .  INDUSTRYWASTE                                                             26062
DK_MAM_Naestved          . IndustrialWasteGen        .  INDUSTRYWASTE                                                             54168
DK_MAM_NrAlslev          . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_Nyborg            . IndustrialWasteGen        .  INDUSTRYWASTE                                                             10965
DK_MAM_Nyk               . IndustrialWasteGen        .  INDUSTRYWASTE                                                             31182
DK_MAM_Skagen            . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_Slagelse          . IndustrialWasteGen        .  INDUSTRYWASTE                                                             26945
DK_MAM_Svend             . IndustrialWasteGen        .  INDUSTRYWASTE                                                             27183
DK_MAM_Thisted           . IndustrialWasteGen        .  INDUSTRYWASTE                                                             21015
DK_CA_Aab_DEC            . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_CA_Aal_DEC            . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_CA_Aarhus_DEC         . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_CA_Esb_DEC            . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_CA_Hern_DEC           . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_CA_Kal_DEC            . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_CA_KBH_DEC            . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_CA_Odense_DEC         . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_CA_Randers_DEC        . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_CA_Roenne_DEC         . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_CA_TVIS_DEC           . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_CA_Vestfrb_DEC        . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_E_DTU_DEC             . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MA_Grenaa_DEC         . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MA_Hil_DEC            . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MA_Hjoerring_DEC      . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MA_Holst_DEC          . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MA_Horsens_DEC        . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MA_NrdOstSj_DEC       . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MA_Silk_DEC           . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MA_Sndborg_DEC        . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MA_Viborg_DEC         . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_Aars_DEC          . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_Frdhavn_DEC       . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_Had_DEC           . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_Hammel_DEC        . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_Hobro_DEC         . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_Naestved_DEC      . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_NrAlslev_DEC      . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_Nyborg_DEC        . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_Nyk_DEC           . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_Skagen_DEC        . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_Slagelse_DEC      . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_Svend_DEC         . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS
DK_MAM_Thisted_DEC       . IndustrialWasteGen        .  INDUSTRYWASTE                                                             EPS

$ontext
*                                                                                   ILOUPFX_LO           ILOUPFX_UP             ILOUPFX_FX

DK_CA_Aab                .  HeatBuffer        .  HEATFLOW                                                                         238418
DK_CA_Aal                .  HeatBuffer        .  HEATFLOW                                                                         1374258
DK_CA_Aarhus             .  HeatBuffer        .  HEATFLOW                                                                         2426020
DK_CA_Esb                .  HeatBuffer        .  HEATFLOW                                                                         921912
DK_CA_Hern               .  HeatBuffer        .  HEATFLOW                                                                         605022
DK_CA_Kal                .  HeatBuffer        .  HEATFLOW                                                                         574298
DK_CA_KBH                .  HeatBuffer        .  HEATFLOW                                                                         6780012
DK_CA_Odense             .  HeatBuffer        .  HEATFLOW                                                                         2029348
DK_CA_Randers            .  HeatBuffer        .  HEATFLOW                                                                         426266
DK_CA_Roenne             .  HeatBuffer        .  HEATFLOW                                                                         17837
DK_CA_TVIS               .  HeatBuffer        .  HEATFLOW                                                                         1301521
DK_CA_Vestfrb            .  HeatBuffer        .  HEATFLOW                                                                         EPS
DK_E_DTU                 .  HeatBuffer        .  HEATFLOW                                                                         89091
DK_MA_Grenaa             .  HeatBuffer        .  HEATFLOW                                                                         218702
DK_MA_Hil                .  HeatBuffer        .  HEATFLOW                                                                         335945
DK_MA_Hjoerring          .  HeatBuffer        .  HEATFLOW                                                                         264828
DK_MA_Holst              .  HeatBuffer        .  HEATFLOW                                                                         395068
DK_MA_Horsens            .  HeatBuffer        .  HEATFLOW                                                                         213327
DK_MA_NrdOstSj           .  HeatBuffer        .  HEATFLOW                                                                         411986
DK_MA_Silk               .  HeatBuffer        .  HEATFLOW                                                                         283457
DK_MA_Sndborg            .  HeatBuffer        .  HEATFLOW                                                                         230342
DK_MA_Viborg             .  HeatBuffer        .  HEATFLOW                                                                         263507
DK_MAM_Aars              .  HeatBuffer        .  HEATFLOW                                                                         79869
DK_MAM_Frdhavn           .  HeatBuffer        .  HEATFLOW                                                                         157399
DK_MAM_Had               .  HeatBuffer        .  HEATFLOW                                                                         116140
DK_MAM_Hammel            .  HeatBuffer        .  HEATFLOW                                                                         66561
DK_MAM_Hobro             .  HeatBuffer        .  HEATFLOW                                                                         47166
DK_MAM_Naestved          .  HeatBuffer        .  HEATFLOW                                                                         191446
DK_MAM_NrAlslev          .  HeatBuffer        .  HEATFLOW                                                                         17186
DK_MAM_Nyborg            .  HeatBuffer        .  HEATFLOW                                                                         146459
DK_MAM_Nyk               .  HeatBuffer        .  HEATFLOW                                                                         205054
DK_MAM_Skagen            .  HeatBuffer        .  HEATFLOW                                                                         58764
DK_MAM_Slagelse          .  HeatBuffer        .  HEATFLOW                                                                         131777
DK_MAM_Svend             .  HeatBuffer        .  HEATFLOW                                                                         135432
DK_MAM_Thisted           .  HeatBuffer        .  HEATFLOW                                                                         158529
DK_CA_Aab_DEC            .  HeatBuffer        .  HEATFLOW                                                                         127245
DK_CA_Aal_DEC            .  HeatBuffer        .  HEATFLOW                                                                         591923
DK_CA_Aarhus_DEC         .  HeatBuffer        .  HEATFLOW                                                                         513987
DK_CA_Esb_DEC            .  HeatBuffer        .  HEATFLOW                                                                         85845
DK_CA_Hern_DEC           .  HeatBuffer        .  HEATFLOW                                                                         532878
DK_CA_Kal_DEC            .  HeatBuffer        .  HEATFLOW                                                                         200017
DK_CA_KBH_DEC            .  HeatBuffer        .  HEATFLOW                                                                         199927
DK_CA_Odense_DEC         .  HeatBuffer        .  HEATFLOW                                                                         340750
DK_CA_Randers_DEC        .  HeatBuffer        .  HEATFLOW                                                                         95377
DK_CA_Roenne_DEC         .  HeatBuffer        .  HEATFLOW                                                                         173253
DK_CA_TVIS_DEC           .  HeatBuffer        .  HEATFLOW                                                                         252760
DK_CA_Vestfrb_DEC        .  HeatBuffer        .  HEATFLOW                                                                         24774
DK_E_DTU_DEC             .  HeatBuffer        .  HEATFLOW                                                                         EPS
DK_MA_Grenaa_DEC         .  HeatBuffer        .  HEATFLOW                                                                         41258
DK_MA_Hil_DEC            .  HeatBuffer        .  HEATFLOW                                                                         234661
DK_MA_Hjoerring_DEC      .  HeatBuffer        .  HEATFLOW                                                                         EPS
DK_MA_Holst_DEC          .  HeatBuffer        .  HEATFLOW                                                                         337248
DK_MA_Horsens_DEC        .  HeatBuffer        .  HEATFLOW                                                                         71957
DK_MA_NrdOstSj_DEC       .  HeatBuffer        .  HEATFLOW                                                                         3961
DK_MA_Silk_DEC           .  HeatBuffer        .  HEATFLOW                                                                         EPS
DK_MA_Sndborg_DEC        .  HeatBuffer        .  HEATFLOW                                                                         82730
DK_MA_Viborg_DEC         .  HeatBuffer        .  HEATFLOW                                                                         232001
DK_MAM_Aars_DEC          .  HeatBuffer        .  HEATFLOW                                                                         144745
DK_MAM_Frdhavn_DEC       .  HeatBuffer        .  HEATFLOW                                                                         128673
DK_MAM_Had_DEC           .  HeatBuffer        .  HEATFLOW                                                                         212974
DK_MAM_Hammel_DEC        .  HeatBuffer        .  HEATFLOW                                                                         95805
DK_MAM_Hobro_DEC         .  HeatBuffer        .  HEATFLOW                                                                         103807
DK_MAM_Naestved_DEC      .  HeatBuffer        .  HEATFLOW                                                                         295184
DK_MAM_NrAlslev_DEC      .  HeatBuffer        .  HEATFLOW                                                                         EPS
DK_MAM_Nyborg_DEC        .  HeatBuffer        .  HEATFLOW                                                                         EPS
DK_MAM_Nyk_DEC           .  HeatBuffer        .  HEATFLOW                                                                         338738
DK_MAM_Skagen_DEC        .  HeatBuffer        .  HEATFLOW                                                                         EPS
DK_MAM_Slagelse_DEC      .  HeatBuffer        .  HEATFLOW                                                                         74906
DK_MAM_Svend_DEC         .  HeatBuffer        .  HEATFLOW                                                                         127170
DK_MAM_Thisted_DEC       .  HeatBuffer        .  HEATFLOW                                                                         143075
$offtext

*                                                                                   ILOUPFX_LO           ILOUPFX_UP             ILOUPFX_FX

DK_CA_Aab_DEC         . ManureBuffer     . MANURE                                   -1776303
DK_CA_Aal_DEC         . ManureBuffer     . MANURE                                   -3230020
DK_CA_Aarhus_DEC      . ManureBuffer     . MANURE                                   -1178759
DK_CA_Esb_DEC         . ManureBuffer     . MANURE                                   -1570127
DK_CA_Hern_DEC        . ManureBuffer     . MANURE                                   -3584459
DK_CA_Kal_DEC         . ManureBuffer     . MANURE                                   -642703
*DK_CA_KBH_DEC         . ManureBuffer     . MANURE                                   -330410
DK_CA_Odense_DEC      . ManureBuffer     . MANURE                                   -2034101
DK_CA_Randers_DEC     . ManureBuffer     . MANURE                                   -827436
DK_CA_Roenne_DEC      . ManureBuffer     . MANURE                                   -407804
DK_CA_TVIS_DEC        . ManureBuffer     . MANURE                                   -1617454
DK_CA_Vestfrb_DEC     . ManureBuffer     . MANURE                                   -966
DK_E_DTU_DEC          . ManureBuffer     . MANURE                                    EPS
DK_MA_Grenaa_DEC      . ManureBuffer     . MANURE                                   -354164
DK_MA_Hil_DEC         . ManureBuffer     . MANURE                                   -101615
DK_MA_Hjoerring_DEC   . ManureBuffer     . MANURE                                   -370098
DK_MA_Holst_DEC       . ManureBuffer     . MANURE                                   -2929668
DK_MA_Horsens_DEC     . ManureBuffer     . MANURE                                   -941150
DK_MA_NrdOstSj_DEC    . ManureBuffer     . MANURE                                   -777
DK_MA_Silk_DEC        . ManureBuffer     . MANURE                                   -35861
DK_MA_Sndborg_DEC     . ManureBuffer     . MANURE                                   -433779
DK_MA_Viborg_DEC      . ManureBuffer     . MANURE                                   -931667
DK_MAM_Aars_DEC       . ManureBuffer     . MANURE                                   -1728794
DK_MAM_Frdhavn_DEC    . ManureBuffer     . MANURE                                   -251355
DK_MAM_Had_DEC        . ManureBuffer     . MANURE                                   -2582819
DK_MAM_Hammel_DEC     . ManureBuffer     . MANURE                                   -251987
DK_MAM_Hobro_DEC      . ManureBuffer     . MANURE                                   -264248
DK_MAM_Naestved_DEC   . ManureBuffer     . MANURE                                   -425394
DK_MAM_NrAlslev_DEC   . ManureBuffer     . MANURE                                    EPS
DK_MAM_Nyborg_DEC     . ManureBuffer     . MANURE                                   -96223
DK_MAM_Nyk_DEC        . ManureBuffer     . MANURE                                   -487724
*DK_MAM_Skagen_DEC    . ManureBuffer     . MANURE                                    EPS
DK_MAM_Slagelse_DEC   . ManureBuffer     . MANURE                                   -61114
DK_MAM_Svend_DEC      . ManureBuffer     . MANURE                                   -291810
DK_MAM_Thisted_DEC    . ManureBuffer     . MANURE                                   -1290069

DK_CA_Aab            . ManureBuffer     . MANURE                                                                                       EPS
DK_CA_Aal            . ManureBuffer     . MANURE                                                                                       EPS
DK_CA_Aarhus         . ManureBuffer     . MANURE                                                                                       EPS
DK_CA_Esb            . ManureBuffer     . MANURE                                                                                       EPS
DK_CA_Hern           . ManureBuffer     . MANURE                                                                                       EPS
DK_CA_Kal            . ManureBuffer     . MANURE                                                                                       EPS
DK_CA_KBH            . ManureBuffer     . MANURE                                                                                       EPS
DK_CA_Odense         . ManureBuffer     . MANURE                                                                                       EPS
DK_CA_Randers        . ManureBuffer     . MANURE                                                                                       EPS
DK_CA_Roenne         . ManureBuffer     . MANURE                                                                                       EPS
DK_CA_TVIS           . ManureBuffer     . MANURE                                                                                       EPS
*DK_CA_Vestfrb        . ManureBuffer     . MANURE                                                                                       EPS
DK_E_DTU             . ManureBuffer     . MANURE                                                                                       EPS
DK_MA_Grenaa         . ManureBuffer     . MANURE                                                                                       EPS
DK_MA_Hil            . ManureBuffer     . MANURE                                                                                       EPS
DK_MA_Hjoerring      . ManureBuffer     . MANURE                                                                                       EPS
DK_MA_Holst          . ManureBuffer     . MANURE                                                                                       EPS
DK_MA_Horsens        . ManureBuffer     . MANURE                                                                                       EPS
DK_MA_NrdOstSj       . ManureBuffer     . MANURE                                                                                       EPS
DK_MA_Silk           . ManureBuffer     . MANURE                                                                                       EPS
DK_MA_Sndborg        . ManureBuffer     . MANURE                                                                                       EPS
DK_MA_Viborg         . ManureBuffer     . MANURE                                                                                       EPS
DK_MAM_Aars          . ManureBuffer     . MANURE                                                                                       EPS
DK_MAM_Frdhavn       . ManureBuffer     . MANURE                                                                                       EPS
DK_MAM_Had           . ManureBuffer     . MANURE                                                                                       EPS
DK_MAM_Hammel        . ManureBuffer     . MANURE                                                                                       EPS
DK_MAM_Hobro         . ManureBuffer     . MANURE                                                                                       EPS
DK_MAM_Naestved      . ManureBuffer     . MANURE                                                                                       EPS
DK_MAM_NrAlslev      . ManureBuffer     . MANURE                                                                                       EPS
DK_MAM_Nyborg        . ManureBuffer     . MANURE                                                                                       EPS
DK_MAM_Nyk           . ManureBuffer     . MANURE                                                                                       EPS
*DK_MAM_Skagen       . ManureBuffer     . MANURE                                                                                       EPS
DK_MAM_Slagelse      . ManureBuffer     . MANURE                                                                                       EPS
DK_MAM_Svend         . ManureBuffer     . MANURE                                                                                       EPS
DK_MAM_Thisted       . ManureBuffer     . MANURE                                                                                       EPS

;

*SOSIBUBOUND(AAA,PROC,FLOW,iLOUPFXSET)$(NOT AAAIMPORT(AAA))=SOSIBUBOUND("DK_CA_KBH","RDF_LOW-CV_Buffer","RDF_LOW-CV",iLOUPFXSET);
*SOSIBUBOUND(AAA,PROC,FLOW,iLOUPFXSET)$(NOT AAAIMPORT(AAA))=SOSIBUBOUND("DK_CA_KBH","RDF_HIGH-CV_Buffer","RDF_HIGH-CV",iLOUPFXSET);

SOSIBUBOUND('UK_AAA','RDF_low-CV_Source','RDF_LOW-CV','ILOUPFX_UP')=EPS;
SOSIBUBOUND('UK_AAA','RDF_high-CV_Source','RDF_HIGH-CV','ILOUPFX_UP')=EPS;


/*   revideres efter indf�relse af index FLOW, men virkede f�r det.
* Test: only Source, Sink and Buffer Processes can be entered:
LOOP(IPROCINTERIOR$SUM(iLOUPFXSET, ABS(SOSIBUBOUND(IPROCINTERIOR,iLOUPFXSET)) GT 0),
   DISPLAY "Error: in SOSIBUBOUND - values should be entered only for Source, Sink of Buffer Processes.";
);
* Test: enter at most one data item for each PROC:
LOOP(PROC$(NOT IPROCINTERIOR(PROC)),
  IF((SUM(iLOUPFXSET$SOSIBUBOUND(PROC,iLOUPFXSET),1) GT 1),
    DISPLAY "Error: in SOSIBUBOUND - at most one data item should be entered for each PROC.";
  );
);   */


!! TESTER:  SOSIBUBOUND(AAA,PROC,FLOW,iLOUPFXSET) = 10*SOSIBUBOUND(AAA,PROC,FLOW,iLOUPFXSET) + 1;

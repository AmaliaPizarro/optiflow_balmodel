SET PROCBUFFER(PROC) "Buffer processes (trace entering and/or leaving)"
/
* BottomashProcessing_36 !! not buffer, will from now be internal
* PretreatmentSep_55     !! not buffer, will from now be internal

*Buffer Processes related to Fuels
ElecBuffer
HeatBuffer
GasBuffer
DieselBuffer
CoalBuffer
StrawBuffer
WoodBuffer
WoodWasteBuffer
WoodPelletsBuffer
BiooilBuffer
HazardousWBuffer
SunHBuffer
BiogasBuffer
UpBiogasBuffer
CoalCCSBuffer
FuelOilBuffer
GasCCSBuffer
WoodPelletsCCSBuffer
SurplusHBuffer

ManureBuffer
RDF_high-CV_Buffer
RDF_low-CV_Buffer

ProcElecLinkStorage
$ifi %keep_ProcMoneyLinkStorage%==yes ProcMoneyLinkStorage
ProcGHGNonBioLinkStorage
ProcGHGBioLinkStorage
ProcDieselLinkStorage

HeatGeneration

ElectricityConsump
DieselConsump
*MoneyConsump  Hans: deled, cf. Money_buffer
*GHGNonBioGen
*GHGBioGen

RDFBuffer

*Buffer Processes related to nutrients (fertilizers)
Field_N
Field_P
Field_K

*Buffer Processes related to flow indicators
Money_buffer
Money_buffer_T
GHG_Buffer
GHGbio_Buffer
GHG_Buffer_T
GHGbio_Buffer_T



*----------------------------------------------------------------------------------------------------------------------------------------------------

/;

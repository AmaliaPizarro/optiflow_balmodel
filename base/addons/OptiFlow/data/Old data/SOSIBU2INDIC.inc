
* NOTE: OVERLOADED AS SOSIBU2INDIC, SOSIBU2INDIC_AST and SOSIBU2INDIC_RST

$ONTEXT
TABLE SOSIBU2INDIC(PROC,FLOWINDIC) "Coefficients for transformation of VSOURCE, VSINK and VBUFFER flows to FLOWINDIC values"
* Note: there is also  parallel tables SOSIBU2INDIC_xxx that contain more indexes. For PROC for which this is relevant, enter 0 here (or omit the corresponding row).
* Amalia: CHECK! CHECK! CHECK! values


                             OPERATIONCOST   GHGEMISSION_nonbio    GHGEMISSION_bio
*                            Cost (Money)    GHG emission (ton  CO2 equiv)
 HouseHoldWasteGen            0               0               !! Source
 Landfill_37                  0.00             0.00             !! !! Sink  - decided not to apply any numbers on this, it is not essential presently
*ElecBuffer                                                   !! Will have indexes (RRR,SSS,TTT) therefore not here, cf. TABLE SOSIBU2INDIC_(PROC,FLOWINDIC,RRR,SSS,TTT) therefore not here
*HeatBuffer                                                   !! Will have indexes (AAA,SSS,TTT) therefore not here, cf. TABLE SOSIBU2INDIC_(PROC,FLOWINDIC,AAA,SSS,TTT)
 GasBuffer                    62.5           0.0568           !! Will have indexes maybe   ... but not presently
Field_N                    4500              1.340
 Field_P                   7550              0.209
 Field_K                   6500              0.206
 Money_buffer                 1               0               !! NB: one-to-one in a single position
 GHG_Buffer                   0               1               !! NB: one-to-one in a single position
;
$OFFTEXT

                  !! PROC ER EN SOSIBU

**Version for Biowaste Seminar (October,2014): Biogenic emissions are not considered due to a lack of time. This must be considered subsequently (Amalia,19th October)
TABLE SOSIBU2INDIC(YYY,PROC,FLOW,FLOWINDIC) "Coefficients for transformation of VSOURCE, VSINK and VBUFFER flows to FLOWINDIC values"
                                                           OPERATIONCOST   GHGEMISSION_nonbio         GHGEMISSION_bio
*                                                         Cost (Money)    GHG emission (ton  CO2 equiv)
2000. HouseHoldWasteGen.RESIDUALWASTEHH                           0               0                      0           !! Source
2000. IndustrialWasteGen.  INDUSTRYWASTE                          0               0                      0
2000. HazardousWBuffer .HAZARDOUSWASTEFLOW                      0.00               0.00                      0.00
2000.  Field_N          .N_TOFIELD                              596.3            -5.737                    0.00
2000. Field_P          .P_TOFIELD                               206.7            -1.431                     0.00
2000. Field_K          .K_TOFIELD                               82.7             -0.490                     0.00
2000. Money_Buffer     .OPERATIONCOST                            1                 0.00                    0.00
2000.Money_Buffer_T     .OPERATIONCOST                            1                0.00                    0.00
2000.GHG_Buffer       .GHGEMISSION_nonbio                       0.00                 1                      0.00
2000. GHGbio_Buffer    .GHGEMISSION_bio                          0.00               0.00                      1
2000.GHG_Buffer_T       .GHGEMISSION_nonbio                       0.00                 1                    0.00
2000.GHGbio_Buffer_T    .GHGEMISSION_bio                          0.00                0.00                      1
2000. ManureBuffer     . MANURE                                 0.00                  0.00                     0.00
2000. Bottom_ash_MSW_chp_small  . ASH_MSW_CHP_SMALL              0.00                  0.00                     0.00
2000. Bottom_ash_MSW_chp_medium . ASH_MSW_CHP_MEDIUM             0.00                  0.00                     0.00
2000. Bottom_ash_MSW_chp_large  . ASH_MSW_CHP_LARGE              0.00                  0.00                     0.00
2000. Bottom_ash_MSW_ho   . ASH_MSW_HO                          0.00                  0.00                     0.00
2000. Bottom_ash_RDF_chp  . ASH_RDF_CHP                          0.00                  0.00                     0.00
2000. Bottom_ash_RDF_ho   . ASH_RDF_HO                          0.00                  0.00                     0.00
2000. HeatCoolDown        . HEATCOOLFLOW                         -2.000                0.00                      0.00   

2012.Recycling_plastic .MRF_RECYCLABLES_PLASTIC_TOTAL          0.00               -6.024                   0.00
2012.Recycling_iron .  MRF_RECYCLABLES_IRON_TOTAL             340.2                -3.183                    0.00
2012.Recycling_nonferrous  .  MRF_RECYCLABLES_NF_TOTAL        1242.7               -26.128                  0.00

*2000.   RDF_high-CV_Buffer. RDF_HIGH-CV                          0.00                  0.00                     0.00
*2000.   RDF_low-CV_Buffer . RDF_LOW-CV                           0.00                  0.00                     0.00

2012. GasBuffer        .GASFLOW                                 6.5576              -0.0568                   0.00
2012. DieselBuffer     .DIESELFLOW                              14.5154             -0.074                    0.00
2012. CoalBuffer       .COALFLOW                                2.8493              -0.095                    0.00
2012. StrawBuffer      .STRAWFLOW                               5.5019               0.00                      -0.011
2012. WoodBuffer       .WOODFLOW                                6.1020               0.00                      0.00
2012. WoodWasteBuffer  .WOODWASTEFLOW                           0.6724               0.00                      0.00
2012. WoodPelletsBuffer .WOODPELLETSFLOW                        8.3024               0.00                      -0.001
2012. BiooilBuffer      .BIOOILFLOW                             21.1402              0.00                      -0.001
2012. SunHBuffer       .SUNHFLOW                                 0.00                 0.00                      0.0774
2012. BiogasBuffer     .BIOGASFLOW                              19.0591              0.00                      0.0774
2012. UpBiogasBuffer    .UPBIOGASFLOW                           22.1000              0.00                      0.00
2012. CoalCCSBuffer      .COALCCSFLOW                           2.8493              -0.095                    0.00
2012. FuelOilBuffer      .FUELOILFLOW                           11.0004             -0.078                    0.00
2012. GasCCSBuffer       .GASCCSFLOW                            6.5576              -0.086                    0.00
2012. WoodPelletsCCSBuffer .WOODPELLETSCCSFLOW                  8.3024               0.00                      0.07975
2012. SurplusHBuffer      .SURPLUSHFLOW                          0.00                 0.00                      0.00



2025. GasBuffer        .GASFLOW                                  7.8771            -0.0568                   0.00
2025. DieselBuffer     .DIESELFLOW                               16.8856           -0.074                    0.00
2025. CoalBuffer       .COALFLOW                                 2.4274            -0.095                    0.00
2025. StrawBuffer      .STRAWFLOW                                6.1020             0.00                      -0.011
2025. WoodBuffer       .WOODFLOW                                 6.8030             0.00                      0.00
2025. WoodWasteBuffer  .WOODWASTEFLOW                            0.6724             0.00                      0.00
2025. WoodPelletsBuffer .WOODPELLETSFLOW                         8.7025             0.00                      -0.001
2025. BiooilBuffer      .BIOOILFLOW                              21.3368            0.00                      -0.001
2025. SunHBuffer       .SUNHFLOW                                 0.00                0.00                      0.0774
2025. BiogasBuffer     .BIOGASFLOW                               19.1129            0.00                      0.0774
2025. UpBiogasBuffer    .UPBIOGASFLOW                            22.5656            0.00                      0.00
2025. CoalCCSBuffer      .COALCCSFLOW                            2.4274            -0.095                    0.00
2025. FuelOilBuffer      .FUELOILFLOW                            12.7957           -0.078                    0.00
2025. GasCCSBuffer       .GASCCSFLOW                             7.8771            -0.086                    0.00
2025. WoodPelletsCCSBuffer .WOODPELLETSCCSFLOW                   8.7025             0.00                      0.07975
2025. SurplusHBuffer      .SURPLUSHFLOW                          0.00                0.00                      0.00


2035. GasBuffer        .GASFLOW                                  7.5376            -0.0568                   0.00
2035. DieselBuffer     .DIESELFLOW                               16.4905           -0.074                    0.00
2035. CoalBuffer       .COALFLOW                                 2.0021            -0.095                    0.00
2035. StrawBuffer      .STRAWFLOW                                5.30               0.00                      -0.011
2035. WoodBuffer       .WOODFLOW                                 5.90               0.00                      0.00
2035. WoodWasteBuffer  .WOODWASTEFLOW                            0.6724             0.00                      0.00
2035. WoodPelletsBuffer .WOODPELLETSFLOW                         7.70               0.00                      -0.001
2035. BiooilBuffer      .BIOOILFLOW                              19.60              0.00                      -0.001
2035. SunHBuffer       .SUNHFLOW                                 0.00                0.00                      0.0774
2035. BiogasBuffer     .BIOGASFLOW                               18.98               0.00                      0.0774
2035. UpBiogasBuffer    .UPBIOGASFLOW                            22.69              0.00                      0.00
2035. CoalCCSBuffer      .COALCCSFLOW                            2.0021            -0.095                    0.00
2035. FuelOilBuffer      .FUELOILFLOW                            12.4965           -0.078                    0.00
2035. GasCCSBuffer       .GASCCSFLOW                             7.5376            -0.086                    0.00
2035. WoodPelletsCCSBuffer .WOODPELLETSCCSFLOW                   7.70               0.00                      0.07975
2035. SurplusHBuffer      .SURPLUSHFLOW                          0.00                0.00                      0.00


2050. GasBuffer        .GASFLOW                                  6.8131           -0.0568                   0.00
2050. DieselBuffer     .DIESELFLOW                               15.1088          -0.074                    0.00
2050. CoalBuffer       .COALFLOW                                 1.7886           -0.095                    0.00
2050. StrawBuffer      .STRAWFLOW                                7.4031            0.00                      -0.011
2050. WoodBuffer       .WOODFLOW                                 8.2032            0.00                      0.00
2050. WoodWasteBuffer  .WOODWASTEFLOW                            0.6724            0.00                      0.00
2050. WoodPelletsBuffer .WOODPELLETSFLOW                         9.6035            0.00                      -0.001
2050. BiooilBuffer      .BIOOILFLOW                              19.29           0.00                      -0.001
2050. SunHBuffer       .SUNHFLOW                                 0.00               0.00                      0.0774
2050. BiogasBuffer     .BIOGASFLOW                               18.98           0.00                      0.0774
2050. UpBiogasBuffer    .UPBIOGASFLOW                            22.86           0.00                      0.00
2050. CoalCCSBuffer      .COALCCSFLOW                            1.7886           -0.095                    0.00
2050. FuelOilBuffer      .FUELOILFLOW                            11.4492          -0.078                    0.00
2050. GasCCSBuffer       .GASCCSFLOW                             6.8131           -0.086                    0.00
2050. WoodPelletsCCSBuffer .WOODPELLETSCCSFLOW                   9.6035            0.00                      0.07975
2050. SurplusHBuffer      .SURPLUSHFLOW                          0.00               0.00                      0.00




2035. RDF_high-CV_Source. RDF_HIGH-CV                            EPS
2035. RDF_low-CV_Source . RDF_LOW-CV                             EPS
;




SOSIBU2INDIC(Y,PROC,FLOW,FLOWINDIC)$(SOSIBU2INDIC(Y,PROC,FLOW,FLOWINDIC)EQ 0) = SOSIBU2INDIC('2000',PROC,FLOW,FLOWINDIC);


*Very rought estimation of material prices with oil price
SOSIBU2INDIC(Y,'Recycling_plastic','MRF_RECYCLABLES_PLASTIC_TOTAL','OPERATIONCOST')= SOSIBU2INDIC('2012','Recycling_plastic','MRF_RECYCLABLES_PLASTIC_TOTAL','OPERATIONCOST')*SOSIBU2INDIC(Y,'DieselBuffer','DIESELFLOW','OPERATIONCOST')/SOSIBU2INDIC('2012','DieselBuffer','DIESELFLOW','OPERATIONCOST');
SOSIBU2INDIC(Y,'Recycling_iron','MRF_RECYCLABLES_IRON_TOTAL','OPERATIONCOST')= SOSIBU2INDIC('2012','Recycling_iron','MRF_RECYCLABLES_IRON_TOTAL','OPERATIONCOST')*SOSIBU2INDIC(Y,'DieselBuffer','DIESELFLOW','OPERATIONCOST')/SOSIBU2INDIC('2012','DieselBuffer','DIESELFLOW','OPERATIONCOST');
SOSIBU2INDIC(Y,'Recycling_nonferrous','MRF_RECYCLABLES_NF_TOTAL','OPERATIONCOST')= SOSIBU2INDIC('2012','Recycling_nonferrous','MRF_RECYCLABLES_NF_TOTAL','OPERATIONCOST')*SOSIBU2INDIC(Y,'DieselBuffer','DIESELFLOW','OPERATIONCOST')/SOSIBU2INDIC('2012','DieselBuffer','DIESELFLOW','OPERATIONCOST');





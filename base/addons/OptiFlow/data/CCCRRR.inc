* Specification of regions in countries:

SET CCCRRR(CCC,RRR)  'Regions in countries'
/
*   DENMARK  .(DK_RRR)
DENMARK  .(DK_E,DK_W)
UK.(UK_RRR)
BC.(BC_RRR)
PL.(PL_RRR)
*  GERMANY  .(DE_RRR)
*  NORWAY   .(NO_RRR)
*  SWEDEN   .(SE_RRR)
/;

SET APROCKAPNEW(AAA,PROC)     'Areas for possible location of new Proc or capacity expansion of existing Proc'
*This is stablished according to economy of scale, where large plants can only be located when heat demand is high


/

DK_CA_KBH          .   Gasification_ST


DK_CA_KBH          .   MSW_chp_large

DK_CA_KBH          .   MSW_ho_large

/
;

APROCKAPNEW(AREASURBAN,PROC)=APROCKAPNEW('DK_CA_KBH',PROC);

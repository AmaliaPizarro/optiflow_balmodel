TABLE PROCDATA(PROC,PROCDATASET)    "Process data"

                                   PROCINVCOST   PROCKAPVARIABLE    PROCFROMYEAR   PROCLIFETIME     PROCFIXCOST
Gasification_ST                       1.00              1              2015            30             10000

MSW_chp_large                         5.55              1              2019            30             141745
MSW_chp_medium                        6.53              1              2019            30             199732
MSW_chp_small                         7.56              1              2019            30             293154
MSW_chp_verysmall                     8.44              1              2019            30             364027
MSW_ho_large                          4.21              1              2019            30             77315
MSW_ho_medium                         4.62              1              2019            30             115973
MSW_ho_small                          5.01              1              2019            30             177181
MSW_ho_verysmall                      5.33              1              2019            30             222282
MSW_chp_AmagerBakke                   0.00              1              2019            30             141745

;

PROCDATA(PROC,'PROCINVCOST')=PROCDATA(PROC,'PROCINVCOST');

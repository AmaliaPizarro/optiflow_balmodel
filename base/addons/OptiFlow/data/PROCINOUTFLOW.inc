* Revision wrt ELEC, HEAT, GAS, buffers, (Linkstorage)
* - removed all that was originally commented out, see original at botton of file
* - assumed tht %quickandverydirty% is not yes, then removed accordingly
* - removed all original comments, see original at botton of file
* Real Changes:
* - Buffer nodes
* - Linkstorage nodes: observe, that they are NOT effecting ANYTHING here i this file, but they MUST be introduced in file FLOWFROMTOPROC.inc.

* The essential information is the same as in Sardiania paper PROCINOUTFLOW.inc
* Modification on additional information:
*   Index IPROCINOUTRELATION  added
*   VARIOUTSUM replaced by 1 (but may differ from 1)
*   VARIinSUM  replaced by 1 (but may differ from 1)
*   Reciprocal values permitted (Karsten request), entered a couple of places, not everywhere, seems to work

$ifi %rollback%==yes $goto olderversionstart

PARAMETER PROCINOUTFLOW(AAA,PROC,IFLOWIN,IFLOWOUT,IPROCINOUTRELATION)  "Relationship at PROC between coming (index 3) and leaving (index 4) FLOW; interior PROC only"
/
DK_CA_KBH          .    ProcEximTruck                  .    RESIDUALWASTEHH               .     RESIDUALWASTEHH               .   IONEONE      1
DK_CA_KBH          .    MSW_IncinerationType           .    RESIDUALWASTEHH               .     RESIDUALWASTEHH_CHP           .   IONEMANY     1
DK_CA_KBH          .    MSW_IncinerationType           .    RESIDUALWASTEHH               .     RESIDUALWASTEHH_HO            .   IONEMANY     1
DK_CA_KBH          .    MSW_Linkstorage_chp            .    RESIDUALWASTEHH_CHP           .     RESIDUALWASTEHH_CHP           .   IONEONE      1
DK_CA_KBH          .    MSW_Linkstorage_ho             .    RESIDUALWASTEHH_HO            .     RESIDUALWASTEHH_HO            .   IONEONE      1
DK_CA_KBH          .    MSW_Charact                    .    RESIDUALWASTEHH_CHP           .     RESIDUALWASTEHH_CHP_TON       .   IONEONE      1
DK_CA_KBH          .    MSW_Charact                    .    RESIDUALWASTEHH_HO            .     RESIDUALWASTEHH_HO_TON        .   IONEONE      1
DK_CA_KBH          .    SUM_MSW_charact_chp            .    RESIDUALWASTEHH_CHP_TON       .     MSW_FUELCHP_TON               .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_chp            .    RESIDUALWASTEHH_CHP_GJ        .     MSW_FUELCHP_GJ                .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_chp            .    RESIDUALWASTEHH_CHP_ASH       .     MSW_FUELCHP_ASH               .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_ho             .    RESIDUALWASTEHH_HO_TON        .     MSW_FUELHO_TON                .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_ho             .    RESIDUALWASTEHH_HO_GJ         .     MSW_FUELHO_GJ                 .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_ho             .    RESIDUALWASTEHH_HO_ASH        .     MSW_FUELHO_ASH                .   IMANYONE     1

DK_CA_KBH          .    ProcEximTruck                  .    INDUSTRY_MIXEDWASTE           .     INDUSTRY_MIXEDWASTE           .   IONEONE      1
DK_CA_KBH          .    IndustryW_StorageMAX           .    INDUSTRY_MIXEDWASTE           .     INDUSTRY_MW_STOR              .   IONEMANY     1
DK_CA_KBH          .    IndustryW_StorageMAX           .    INDUSTRY_MIXEDWASTE           .     INDUSTRY_MW_nonSTOR           .   IONEMANY     1
DK_CA_KBH          .    RDF_Storage_Ind                .    INDUSTRY_MW_STOR              .     INDUSTRY_MW_STOR              .   IONEONE      1
DK_CA_KBH          .    RDF_Storage_Ind                .    INDUSTRY_MW_STOR              .     OPERATIONCOST                 .   IONEONE      -4.7
DK_CA_KBH          .    RDF_Storage_Ind                .    INDUSTRY_MW_STOR              .     DIESELFLOW                    .   IONEONE      -0.2
DK_CA_KBH          .    MSW_IncinerationType           .    INDUSTRY_MW_nonSTOR           .     INDUSTRY_MW_nonSTOR_CHP       .   IONEMANY     1
DK_CA_KBH          .    MSW_IncinerationType           .    INDUSTRY_MW_nonSTOR           .     INDUSTRY_MW_nonSTOR_HO        .   IONEMANY     1
DK_CA_KBH          .    MSW_IncinerationType           .    INDUSTRY_MW_STOR              .     INDUSTRY_MW_STOR_CHP          .   IONEMANY     1
DK_CA_KBH          .    MSW_IncinerationType           .    INDUSTRY_MW_STOR              .     INDUSTRY_MW_STOR_HO           .   IONEMANY     1
DK_CA_KBH          .    MSW_Linkstorage_chp            .    INDUSTRY_MW_nonSTOR_CHP       .     INDUSTRY_MW_nonSTOR_CHP       .   IONEONE      1
DK_CA_KBH          .    MSW_Linkstorage_ho             .    INDUSTRY_MW_nonSTOR_HO        .     INDUSTRY_MW_nonSTOR_HO        .   IONEONE      1
DK_CA_KBH          .    MSW_Linkstorage_chp            .    INDUSTRY_MW_STOR_CHP          .     INDUSTRY_MW_STOR_CHP          .   IONEONE      1
DK_CA_KBH          .    MSW_Linkstorage_ho             .    INDUSTRY_MW_STOR_HO           .     INDUSTRY_MW_STOR_HO           .   IONEONE      1
DK_CA_KBH          .    MSW_Charact                    .    INDUSTRY_MW_nonSTOR_CHP       .     INDUSTRY_MW_nonSTOR_CHP_TON   .   IONEONE      1
DK_CA_KBH          .    MSW_Charact                    .    INDUSTRY_MW_STOR_CHP          .     INDUSTRY_MW_STOR_CHP_TON      .   IONEONE      1
DK_CA_KBH          .    MSW_Charact                    .    INDUSTRY_MW_nonSTOR_HO        .     INDUSTRY_MW_nonSTOR_HO_TON    .   IONEONE      1
DK_CA_KBH          .    MSW_Charact                    .    INDUSTRY_MW_STOR_HO           .     INDUSTRY_MW_STOR_HO_TON       .   IONEONE      1
DK_CA_KBH          .    SUM_MSW_charact_chp            .    INDUSTRY_MW_nonSTOR_CHP_TON   .     MSW_FUELCHP_TON               .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_chp            .    INDUSTRY_MW_nonSTOR_CHP_GJ    .     MSW_FUELCHP_GJ                .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_chp            .    INDUSTRY_MW_nonSTOR_CHP_ASH   .     MSW_FUELCHP_ASH               .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_chp            .    INDUSTRY_MW_STOR_CHP_TON      .     MSW_FUELCHP_TON               .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_chp            .    INDUSTRY_MW_STOR_CHP_GJ       .     MSW_FUELCHP_GJ                .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_chp            .    INDUSTRY_MW_STOR_CHP_ASH      .     MSW_FUELCHP_ASH               .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_ho             .    INDUSTRY_MW_nonSTOR_HO_TON    .     MSW_FUELHO_TON                .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_ho             .    INDUSTRY_MW_nonSTOR_HO_GJ     .     MSW_FUELHO_GJ                 .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_ho             .    INDUSTRY_MW_nonSTOR_HO_ASH    .     MSW_FUELHO_ASH                .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_ho             .    INDUSTRY_MW_STOR_HO_TON       .     MSW_FUELHO_TON                .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_ho             .    INDUSTRY_MW_STOR_HO_GJ        .     MSW_FUELHO_GJ                 .   IMANYONE     1
DK_CA_KBH          .    SUM_MSW_charact_ho             .    INDUSTRY_MW_STOR_HO_ASH       .     MSW_FUELHO_ASH                .   IMANYONE     1

DK_CA_KBH .    MSW_Charact .    RESIDUALWASTEHH_CHP     .    RESIDUALWASTEHH_CHP_GJ      .    IONEONE 10.6112312925609
DK_CA_KBH .    MSW_Charact .    RESIDUALWASTEHH_CHP     .    RESIDUALWASTEHH_CHP_ASH     .    IONEONE 0.0742403387374266
DK_CA_KBH .    MSW_Charact .    RESIDUALWASTEHH_HO      .    RESIDUALWASTEHH_HO_GJ       .    IONEONE 10.6112312925609
DK_CA_KBH .    MSW_Charact .    RESIDUALWASTEHH_HO      .    RESIDUALWASTEHH_HO_ASH      .    IONEONE 0.0742403387374266
DK_CA_KBH .    MSW_Charact .    INDUSTRY_MW_nonSTOR_CHP .    INDUSTRY_MW_nonSTOR_CHP_GJ  .    IONEONE 10.8130078956593
DK_CA_KBH .    MSW_Charact .    INDUSTRY_MW_nonSTOR_CHP .    INDUSTRY_MW_nonSTOR_CHP_ASH .    IONEONE 0.0474459145854694
DK_CA_KBH .    MSW_Charact .    INDUSTRY_MW_STOR_CHP    .    INDUSTRY_MW_STOR_CHP_GJ     .    IONEONE 10.8130078956593
DK_CA_KBH .    MSW_Charact .    INDUSTRY_MW_STOR_CHP    .    INDUSTRY_MW_STOR_CHP_ASH    .    IONEONE 0.0474459145854694
DK_CA_KBH .    MSW_Charact .    INDUSTRY_MW_nonSTOR_HO  .    INDUSTRY_MW_nonSTOR_HO_GJ   .    IONEONE 10.8130078956593
DK_CA_KBH .    MSW_Charact .    INDUSTRY_MW_nonSTOR_HO  .    INDUSTRY_MW_nonSTOR_HO_ASH  .    IONEONE 0.0474459145854694
DK_CA_KBH .    MSW_Charact .    INDUSTRY_MW_STOR_HO     .    INDUSTRY_MW_STOR_HO_GJ      .    IONEONE 10.8130078956593
DK_CA_KBH .    MSW_Charact .    INDUSTRY_MW_STOR_HO     .    INDUSTRY_MW_STOR_HO_ASH     .    IONEONE 0.0474459145854694


DK_CA_KBH          .    MSW_chp_large                  .    MSW_FUELCHP_GJ                .     ELECFLOW                      .   IONEMANY     12.312
DK_CA_KBH          .    MSW_chp_large                  .    MSW_FUELCHP_GJ                .     HEATFLOW                      .   IONEMANY     12.312
DK_CA_KBH          .    MSW_chp_large                  .    MSW_FUELCHP_TON               .     OPERATIONCOST                 .   IONEONE      -17.45

DK_CA_KBH          .    MSW_ho_large                   .    MSW_FUELHO_GJ                 .     HEATFLOW                      .   IONEONE      0.95
DK_CA_KBH          .    MSW_ho_large                   .    MSW_FUELHO_TON                .     OPERATIONCOST                 .   IONEONE      -17.45
DK_CA_KBH          .    MSW_ho_large                   .    MSW_FUELHO_ASH                .     ASH_MSW                       .   IONEONE      1

DK_CA_KBH          .   ProcEximTruck                   .    STRAWFLOW                      .     STRAWFLOW                    .   IONEONE       1
DK_CA_KBH          .   MSW_Linkstorage_gasif           .    STRAWFLOW                      .     STRAWFLOW                    .   IONEONE       1
DK_CA_KBH          .    Gasification_ST                .    STRAWFLOW                      .     SYNGASFLOW                   .   IONEONE       0.9
DK_CA_KBH          .    Gasification_ST                .    STRAWFLOW                      .     HEATFLOW                     .   IONEONE       0.1
DK_CA_KBH          .    Gasification_ST                .    STRAWFLOW                      .     OPERATIONCOST                .   IONEONE      -17.45



/;

* For convenience assign initially same data to all other areas (then overwrite where needed):




PROCINOUTFLOW(AREASURBAN,PROC,IFLOWIN,IFLOWOUT,IPROCINOUTRELATION)$(PROCINOUTFLOW(AREASURBAN,PROC,IFLOWIN,IFLOWOUT,IPROCINOUTRELATION) EQ 0) =PROCINOUTFLOW('DK_CA_KBH',PROC,IFLOWIN,IFLOWOUT,IPROCINOUTRELATION);



* Here inserted almost-nonsense information in relation to international trade - simple 'other countries'

* Information for trading with other countries


* For convenience assign initially same data to all other areas for import of waste (then overwrite where needed):
PROCINOUTFLOW(IA,PROC,IFLOWIN,IFLOWOUT,IPROCINOUTRELATION)$(AAAIMPORT(IA))= PROCINOUTFLOW('UK_AAA',PROC,IFLOWIN,IFLOWOUT,IPROCINOUTRELATION);

*This new parameter is defined for incineration plants in the base year, where incineration plants are identified as MSW_chp_medium, in order to make easier the code (no investments are allowed)
*In a further stage it could be good to define a process like existing incineration plants



*  Change in units for Electricity and Heat production flows from GJ to MWh:

PROCINOUTFLOW(IA,PROC,IFLOWIN,'ELECFLOW',IPROCINOUTRELATION)$(PROCINOUTFLOW(IA,PROC,IFLOWIN,'ELECFLOW',IPROCINOUTRELATION))=PROCINOUTFLOW(IA,PROC,IFLOWIN,'ELECFLOW',IPROCINOUTRELATION)/IOF3P6;
PROCINOUTFLOW(IA,PROC,IFLOWIN,'HEATFLOW',IPROCINOUTRELATION)$(PROCINOUTFLOW(IA,PROC,IFLOWIN,'HEATFLOW',IPROCINOUTRELATION))=PROCINOUTFLOW(IA,PROC,IFLOWIN,'HEATFLOW',IPROCINOUTRELATION)/IOF3P6;
PROCINOUTFLOW(IA,PROC,'ELECFLOW',IFLOWOUT,IPROCINOUTRELATION)$(PROCINOUTFLOW(IA,PROC,'ELECFLOW',IFLOWOUT,IPROCINOUTRELATION))=PROCINOUTFLOW(IA,PROC,'ELECFLOW',IFLOWOUT,IPROCINOUTRELATION)*IOF3P6;
PROCINOUTFLOW(IA,PROC,'HEATFLOW',IFLOWOUT,IPROCINOUTRELATION)$(PROCINOUTFLOW(IA,PROC,'HEATFLOW',IFLOWOUT,IPROCINOUTRELATION))=PROCINOUTFLOW(IA,PROC,'HEATFLOW',IFLOWOUT,IPROCINOUTRELATION)*IOF3P6;




!! NOTE: OVERLOADED AS SOSIBU, SOSIBU_AST and SOSIBU_RST ?
* Rules probably should be:
*   for any index combination at most one element in has values.
*   If there is a SOSIBUFLOW_VAR_T/SOSIBUBOUND_VAR_T then SOSIBUBOUND
*   (and only that, not _AST or _RST) has value for the corresponding index combination.
*   Code in the .gms file will then combine all to aone or a few ...

!! TODO: RECONSIERE: MAYBE THIS IS ONLY FOR SOURCE (E.G. ANNUAL VALUES OF WASTE GENERATED)  - IN THIS CASE, CAHNGE NAME
*It would be conveniant to have this parameter as a function of the year
PARAMETER SOSIBUBOUND(AAA,PROC,FLOW,iLOUPFXSET) "Bounds on Source, Sink and Buffer Process Flows - Annual values"

/
DK_CA_Aab         .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     143172.2328
DK_CA_Aal         .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     410903.658
DK_CA_Aarhus      .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     659460.6527
DK_CA_Esb         .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     247831.6195
DK_CA_Hern        .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     271703.9997
DK_CA_Kal         .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     328599.4205
DK_CA_KBH         .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     2170466.052
DK_CA_Odense      .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     557076.3737
DK_CA_Randers     .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     141677.162
DK_CA_Roenne      .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     59245.87306
DK_CA_TVIS        .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     469051.8654
DK_CA_Vestfrb     .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     169216.3067
DK_E_DTU          .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     80989.40247
DK_MA_Grenaa      .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     55716.50142
DK_MA_Hil         .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     268690.2206
DK_MA_Hjoerring   .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     96408.4279
DK_MA_Holst       .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     216477.976
DK_MA_Horsens     .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     193486.092
DK_MA_NrdOstSj    .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     184475.7791
DK_MA_Silk        .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     131736.1232
DK_MA_Sndborg     .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     111045.9391
DK_MA_Viborg      .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     138948.5101
DK_MAM_Aars       .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     55279.20799
DK_MAM_Frdhavn    .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     91806.50549
DK_MAM_Had        .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     82509.58807
DK_MAM_Hammel     .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     69503.06321
DK_MAM_Hobro      .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     104581.0875
DK_MAM_Naestved   .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     238104.7953
DK_MAM_NrAlslev   .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     EPS
DK_MAM_Nyborg     .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     46190.59589
DK_MAM_Nyk        .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     153680.5712
DK_MAM_Skagen     .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     EPS
DK_MAM_Slagelse   .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     112910.3455
DK_MAM_Svend      .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     113244.225
DK_MAM_Thisted    .   StrawGen  . STRAWFLOW   .   ILOUPFX_UP     96182.39448

/



;
* Convention for entering data:
*   Enter at most one data item for each PROC,
*   If nothing or 0 is entered then the default values corresponding to the declaration of the variable (positive, negative or free) apply.
*   If EPS is entered then the corresponding value will be zero (CHECK IT).
* The unit for the data may differ between (PROC,FLOW).

*Unit: Flow Units/year (as defined the model 17th June 2015)
*Values for the year 2012 - It would be very conveniant to define this parameter as a function of the year (YYY)
* So that waste generation and heat demand (between others) could be defined in one folder for the different simulations
*Currently manual modification of the parameter required

TABLE SOSIBUBOUND_Y(YYY,AAA,PROC,FLOW,iLOUPFXSET) "Bounds on Source, Sink and Buffer Process Flows - for each year"

$if     EXIST "data\SOSIBUBOUND_Y.xlsx" $call =xls2gms r=A1:J600 I="data\SOSIBUBOUND_Y.xlsx" O="data\SOSIBUBOUND_Y.inc"
$if     EXIST "data\SOSIBUBOUND_Y.xlsx" $include "data\SOSIBUBOUND_Y.inc"

;

LOOP(Y,SOSIBUBOUND(IA,PROC,FLOW,iLOUPFXSET)$(SOSIBUBOUND_Y(Y,IA,PROC,FLOW,iLOUPFXSET))=SOSIBUBOUND_Y(Y,IA,PROC,FLOW,iLOUPFXSET););

SOSIBUBOUND('UK_AAA','Imported_RDF','IMPORTEDRDFFLOW','ILOUPFX_UP')=EPS;



/*   revideres efter indf�relse af index FLOW, men virkede f�r det.
* Test: only Source, Sink and Buffer Processes can be entered:
LOOP(IPROCINTERIOR$SUM(iLOUPFXSET, ABS(SOSIBUBOUND(IPROCINTERIOR,iLOUPFXSET)) GT 0),
   DISPLAY "Error: in SOSIBUBOUND - values should be entered only for Source, Sink of Buffer Processes.";
);
* Test: enter at most one data item for each PROC:
LOOP(PROC$(NOT IPROCINTERIOR(PROC)),
  IF((SUM(iLOUPFXSET$SOSIBUBOUND(PROC,iLOUPFXSET),1) GT 1),
    DISPLAY "Error: in SOSIBUBOUND - at most one data item should be entered for each PROC.";
  );
);   */


!! TESTER:  SOSIBUBOUND(AAA,PROC,FLOW,iLOUPFXSET) = 10*SOSIBUBOUND(AAA,PROC,FLOW,iLOUPFXSET) + 1;

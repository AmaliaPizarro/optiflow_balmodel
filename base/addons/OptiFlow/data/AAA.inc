* Areas:
* This new set of areas has been defined specifically for the Municipalities of Fyn (16th October, Amalia)

SET AAA(CCCRRRAAA)  'All areas'
/
*DK_RRR_AAA


DK_CA_Aab
DK_CA_Aal
DK_CA_Aarhus
DK_CA_Esb
DK_CA_Hern
DK_CA_Kal
DK_CA_KBH
DK_CA_Odense
DK_CA_Randers
DK_CA_Roenne
DK_CA_TVIS
DK_CA_Vestfrb
DK_E_DTU
DK_MA_Grenaa
DK_MA_Hil
DK_MA_Hjoerring
DK_MA_Holst
DK_MA_Horsens
DK_MA_NrdOstSj
DK_MA_Silk
DK_MA_Sndborg
DK_MA_Viborg
DK_MAM_Aars
DK_MAM_Frdhavn
DK_MAM_Had
DK_MAM_Hammel
DK_MAM_Hobro
DK_MAM_Naestved
DK_MAM_NrAlslev
DK_MAM_Nyborg
DK_MAM_Nyk
DK_MAM_Skagen
DK_MAM_Slagelse
DK_MAM_Svend
DK_MAM_Thisted
DK_CA_Aab_DEC
DK_CA_Aal_DEC
DK_CA_Aarhus_DEC
DK_CA_Esb_DEC
DK_CA_Hern_DEC
DK_CA_Kal_DEC
DK_CA_KBH_DEC
DK_CA_Odense_DEC
DK_CA_Randers_DEC
DK_CA_Roenne_DEC
DK_CA_TVIS_DEC
DK_CA_Vestfrb_DEC
DK_E_DTU_DEC
DK_MA_Grenaa_DEC
DK_MA_Hil_DEC
DK_MA_Hjoerring_DEC
DK_MA_Holst_DEC
DK_MA_Horsens_DEC
DK_MA_NrdOstSj_DEC
DK_MA_Silk_DEC
DK_MA_Sndborg_DEC
DK_MA_Viborg_DEC
DK_MAM_Aars_DEC
DK_MAM_Frdhavn_DEC
DK_MAM_Had_DEC
DK_MAM_Hammel_DEC
DK_MAM_Hobro_DEC
DK_MAM_Naestved_DEC
DK_MAM_NrAlslev_DEC
DK_MAM_Nyborg_DEC
DK_MAM_Nyk_DEC
DK_MAM_Skagen_DEC
DK_MAM_Slagelse_DEC
DK_MAM_Svend_DEC
DK_MAM_Thisted_DEC


*We might need to define some kind of neighbouring areas in order to warranty transport of waste outside Fyn.
*DK_JUTLAND_all
*DK_SEALAND_all
UK_AAA
BC_AAA
PL_AAA

* DE_RRR_AAA
* NO_RRR_AAA
* SE_RRR_AAA

/;

SET AREASRURAL(AAA)

/
DK_CA_Aab_DEC
DK_CA_Aal_DEC
DK_CA_Aarhus_DEC
DK_CA_Esb_DEC
DK_CA_Hern_DEC
DK_CA_Kal_DEC
DK_CA_KBH_DEC
DK_CA_Odense_DEC
DK_CA_Randers_DEC
DK_CA_Roenne_DEC
DK_CA_TVIS_DEC
DK_CA_Vestfrb_DEC
DK_E_DTU_DEC
DK_MA_Grenaa_DEC
DK_MA_Hil_DEC
DK_MA_Hjoerring_DEC
DK_MA_Holst_DEC
DK_MA_Horsens_DEC
DK_MA_NrdOstSj_DEC
DK_MA_Silk_DEC
DK_MA_Sndborg_DEC
DK_MA_Viborg_DEC
DK_MAM_Aars_DEC
DK_MAM_Frdhavn_DEC
DK_MAM_Had_DEC
DK_MAM_Hammel_DEC
DK_MAM_Hobro_DEC
DK_MAM_Naestved_DEC
DK_MAM_NrAlslev_DEC
DK_MAM_Nyborg_DEC
DK_MAM_Nyk_DEC
DK_MAM_Skagen_DEC
DK_MAM_Slagelse_DEC
DK_MAM_Svend_DEC
DK_MAM_Thisted_DEC
/
;

SET AREASURBAN(AAA)
/
DK_CA_Aab
DK_CA_Aal
DK_CA_Aarhus
DK_CA_Esb
DK_CA_Hern
DK_CA_Kal
DK_CA_KBH
DK_CA_Odense
DK_CA_Randers
DK_CA_Roenne
DK_CA_TVIS
*DK_CA_Vestfrb
DK_E_DTU
DK_MA_Grenaa
DK_MA_Hil
DK_MA_Hjoerring
DK_MA_Holst
DK_MA_Horsens
DK_MA_NrdOstSj
DK_MA_Silk
DK_MA_Sndborg
DK_MA_Viborg
DK_MAM_Aars
DK_MAM_Frdhavn
DK_MAM_Had
DK_MAM_Hammel
DK_MAM_Hobro
DK_MAM_Naestved
DK_MAM_NrAlslev
DK_MAM_Nyborg
DK_MAM_Nyk
DK_MAM_Skagen
DK_MAM_Slagelse
DK_MAM_Svend
DK_MAM_Thisted
/;









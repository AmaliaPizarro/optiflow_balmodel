SET PROC_T(PROC) "PROC that operate on high time resolution T"
/

Gasification_ST
SYNGASBuffer

ElecBuffer
HeatBuffer
Money_buffer_T
GHG_Buffer
GHGbio_Buffer
GasBuffer

MSW_Charact
SUM_MSW_charact_chp
SUM_MSW_charact_chp_AmagerBakke
SUM_MSW_charact_ho
SUM_MSW_charact_WtEBoiler_1_14
SUM_MSW_charact_WtEBoiler_1_20
SUM_MSW_charact_WtEBoiler_2_14
SUM_MSW_charact_WtEBoiler_2_20
SUM_MSW_charact_WtEBoiler_2_25
SUM_MSW_charact_WtEBoiler_2_30
SUM_MSW_charact_WtEBoiler_3_14
SUM_MSW_charact_WtEBoiler_3_20
SUM_MSW_charact_WtEBoiler_3_25
SUM_MSW_charact_WtEBoiler_3_30
SUM_MSW_charact_WtEBoiler_4_14
SUM_MSW_charact_WtEBoiler_5_14
SUM_MSW_charact_WtEBoiler_5_20
SUM_MSW_charact_WtEBoiler_6_14
SUM_MSW_charact_WtECHP_1_14
SUM_MSW_charact_WtECHP_1_20
SUM_MSW_charact_WtECHP_1_25
SUM_MSW_charact_WtECHP_1_30
SUM_MSW_charact_WtECHP_10_14
SUM_MSW_charact_WtECHP_10_20
SUM_MSW_charact_WtECHP_10_25
SUM_MSW_charact_WtECHP_10_30
SUM_MSW_charact_WtECHP_11_14
SUM_MSW_charact_WtECHP_11_20
SUM_MSW_charact_WtECHP_11_25
SUM_MSW_charact_WtECHP_12_14
SUM_MSW_charact_WtECHP_12_20
SUM_MSW_charact_WtECHP_12_25
SUM_MSW_charact_WtECHP_13_14
SUM_MSW_charact_WtECHP_13_20
SUM_MSW_charact_WtECHP_14_14
SUM_MSW_charact_WtECHP_14_20
SUM_MSW_charact_WtECHP_15_14
SUM_MSW_charact_WtECHP_15_20
SUM_MSW_charact_WtECHP_15_25
SUM_MSW_charact_WtECHP_16_14
SUM_MSW_charact_WtECHP_16_20
SUM_MSW_charact_WtECHP_17_14
SUM_MSW_charact_WtECHP_17_20
SUM_MSW_charact_WtECHP_17_25
SUM_MSW_charact_WtECHP_18_14
SUM_MSW_charact_WtECHP_18_20
SUM_MSW_charact_WtECHP_19_14
SUM_MSW_charact_WtECHP_19_20
SUM_MSW_charact_WtECHP_19_25
SUM_MSW_charact_WtECHP_2_14
SUM_MSW_charact_WtECHP_2_20
SUM_MSW_charact_WtECHP_20_14
SUM_MSW_charact_WtECHP_20_20
SUM_MSW_charact_WtECHP_3_14
SUM_MSW_charact_WtECHP_3_20
SUM_MSW_charact_WtECHP_3_25
SUM_MSW_charact_WtECHP_3_30
SUM_MSW_charact_WtECHP_3_35
SUM_MSW_charact_WtECHP_3_40
SUM_MSW_charact_WtECHP_4_14
SUM_MSW_charact_WtECHP_4_20
SUM_MSW_charact_WtECHP_4_25
SUM_MSW_charact_WtECHP_4_30
SUM_MSW_charact_WtECHP_5_14
SUM_MSW_charact_WtECHP_5_20
SUM_MSW_charact_WtECHP_5_25
SUM_MSW_charact_WtECHP_5_30
SUM_MSW_charact_WtECHP_6_14
SUM_MSW_charact_WtECHP_6_20
SUM_MSW_charact_WtECHP_6_25
SUM_MSW_charact_WtECHP_6_30
SUM_MSW_charact_WtECHP_7_14
SUM_MSW_charact_WtECHP_7_20
SUM_MSW_charact_WtECHP_7_25
SUM_MSW_charact_WtECHP_7_30
SUM_MSW_charact_WtECHP_7_35
SUM_MSW_charact_WtECHP_8_14
SUM_MSW_charact_WtECHP_8_20
SUM_MSW_charact_WtECHP_8_25
SUM_MSW_charact_WtECHP_8_30
SUM_MSW_charact_WtECHP_8_35
SUM_MSW_charact_WtECHP_9_14
SUM_MSW_charact_WtECHP_9_20
SUM_MSW_charact_WtECHP_9_25
MSW_chp_AmagerBakke
MSW_chp_large
MSW_chp_medium
MSW_chp_small
MSW_chp_verysmall
MSW_ho_large
MSW_ho_medium
MSW_ho_small
MSW_ho_verysmall
MSW_WtEBoiler_1_14
MSW_WtEBoiler_1_20
MSW_WtEBoiler_2_14
MSW_WtEBoiler_2_20
MSW_WtEBoiler_2_25
MSW_WtEBoiler_2_30
MSW_WtEBoiler_3_14
MSW_WtEBoiler_3_20
MSW_WtEBoiler_3_25
MSW_WtEBoiler_3_30
MSW_WtEBoiler_4_14
MSW_WtEBoiler_5_14
MSW_WtEBoiler_5_20
MSW_WtEBoiler_6_14
MSW_WtECHP_1_14
MSW_WtECHP_1_20
MSW_WtECHP_1_25
MSW_WtECHP_1_30
MSW_WtECHP_10_14
MSW_WtECHP_10_20
MSW_WtECHP_10_25
MSW_WtECHP_10_30
MSW_WtECHP_11_14
MSW_WtECHP_11_20
MSW_WtECHP_11_25
MSW_WtECHP_12_14
MSW_WtECHP_12_20
MSW_WtECHP_12_25
MSW_WtECHP_13_14
MSW_WtECHP_13_20
MSW_WtECHP_14_14
MSW_WtECHP_14_20
MSW_WtECHP_15_14
MSW_WtECHP_15_20
MSW_WtECHP_15_25
MSW_WtECHP_16_14
MSW_WtECHP_16_20
MSW_WtECHP_17_14
MSW_WtECHP_17_20
MSW_WtECHP_17_25
MSW_WtECHP_18_14
MSW_WtECHP_18_20
MSW_WtECHP_19_14
MSW_WtECHP_19_20
MSW_WtECHP_19_25
MSW_WtECHP_2_14
MSW_WtECHP_2_20
MSW_WtECHP_20_14
MSW_WtECHP_20_20
MSW_WtECHP_3_14
MSW_WtECHP_3_20
MSW_WtECHP_3_25
MSW_WtECHP_3_30
MSW_WtECHP_3_35
MSW_WtECHP_3_40
MSW_WtECHP_4_14
MSW_WtECHP_4_20
MSW_WtECHP_4_25
MSW_WtECHP_4_30
MSW_WtECHP_5_14
MSW_WtECHP_5_20
MSW_WtECHP_5_25
MSW_WtECHP_5_30
MSW_WtECHP_6_14
MSW_WtECHP_6_20
MSW_WtECHP_6_25
MSW_WtECHP_6_30
MSW_WtECHP_7_14
MSW_WtECHP_7_20
MSW_WtECHP_7_25
MSW_WtECHP_7_30
MSW_WtECHP_7_35
MSW_WtECHP_8_14
MSW_WtECHP_8_20
MSW_WtECHP_8_25
MSW_WtECHP_8_30
MSW_WtECHP_8_35
MSW_WtECHP_9_14
MSW_WtECHP_9_20
MSW_WtECHP_9_25
Bottom_ash



/;


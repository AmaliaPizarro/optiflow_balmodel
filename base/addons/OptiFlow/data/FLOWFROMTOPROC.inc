* Revision wrt ELEC, HEAT, GAS, buffers
* - removed all that was originally commented out, see original at botton of file
* - assumed tht %quickandverydirty% is not yes, then removed accordingly
* - removed all original comments, see original at botton of file
* Real Changes:
* - Electricity:
*     PROCTO  Wasteincin_ElecDisplace is replaced by ElecBuffer and  FLOW    OPERATIONCOST and GHGEMISSION_nonbio related to that deleted (these two will be applied on the buffer variables)
*     Similarly for Biogas_ElecDisplace
*     Similarly for Wasteincin_HeatDisplace that is replaced by HeatBuffer, etc
*     Similarly for

* DK_W_AAA        NonGJSumIn        WasteincinCHP_35        NONTHERMALENERGY_TON        ins2      This one is strange in gdxdiff!
* -Import of waste to Denmark is allowed, compared with the cost and GHG emissions in the origin countries. (AMALIA)
* Note: if a flow entered in a line here in FLOWFROMTOPROC does not have corresponding information in PROCINOUTFLOW
* then the variable VPROCESFLOW corresponding to that line will be unrestricted, which is an error.


SET FLOWFROMTOPROC(AAA,IPROCFROM,IPROCTO,FLOW) "FLOW from PROC (index 2) to PROC (index 3) in AREA"
/
DK_CA_KBH                .  HouseHoldWasteGen               .   ProcEximTruck                       .   RESIDUALWASTEHH
DK_CA_KBH                .  ProcEximTruck                   .   MSW_IncinerationType                .   RESIDUALWASTEHH
DK_CA_KBH                .  MSW_IncinerationType            .   MSW_Linkstorage_chp                 .   RESIDUALWASTEHH_CHP
DK_CA_KBH                .  MSW_IncinerationType            .   MSW_Linkstorage_ho                  .   RESIDUALWASTEHH_HO
DK_CA_KBH                .  MSW_Linkstorage_chp             .   MSW_Charact                         .   RESIDUALWASTEHH_CHP
DK_CA_KBH                .  MSW_Linkstorage_ho              .   MSW_Charact                         .   RESIDUALWASTEHH_HO
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_chp                 .   RESIDUALWASTEHH_CHP_TON
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_chp                 .   RESIDUALWASTEHH_CHP_GJ
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_chp                 .   RESIDUALWASTEHH_CHP_ASH
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_ho                  .   RESIDUALWASTEHH_HO_TON
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_ho                  .   RESIDUALWASTEHH_HO_GJ
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_ho                  .   RESIDUALWASTEHH_HO_ASH

DK_CA_KBH                .  IndustrialWasteGen              .   ProcEximTruck                       .   INDUSTRY_MIXEDWASTE
DK_CA_KBH                .  ProcEximTruck                   .   IndustryW_StorageMAX                .   INDUSTRY_MIXEDWASTE
DK_CA_KBH                .  IndustryW_StorageMAX            .   RDF_Storage_Ind                     .   INDUSTRY_MW_STOR
DK_CA_KBH                .  IndustryW_StorageMAX            .   MSW_IncinerationType                .   INDUSTRY_MW_nonSTOR
DK_CA_KBH                .  Money_buffer                    .   RDF_Storage_Ind                     .   OPERATIONCOST
DK_CA_KBH                .  DieselBuffer                    .   RDF_Storage_Ind                     .   DIESELFLOW
DK_CA_KBH                .  RDF_Storage_Ind                 .   MSW_IncinerationType                .   INDUSTRY_MW_STOR
DK_CA_KBH                .  MSW_IncinerationType            .   MSW_Linkstorage_chp                 .   INDUSTRY_MW_nonSTOR_CHP
DK_CA_KBH                .  MSW_IncinerationType            .   MSW_Linkstorage_ho                  .   INDUSTRY_MW_nonSTOR_HO
DK_CA_KBH                .  MSW_Linkstorage_chp             .   MSW_Charact                         .   INDUSTRY_MW_nonSTOR_CHP
DK_CA_KBH                .  MSW_Linkstorage_ho              .   MSW_Charact                         .   INDUSTRY_MW_nonSTOR_HO
DK_CA_KBH                .  MSW_IncinerationType            .   MSW_Linkstorage_chp                 .   INDUSTRY_MW_STOR_CHP
DK_CA_KBH                .  MSW_IncinerationType            .   MSW_Linkstorage_ho                  .   INDUSTRY_MW_STOR_HO
DK_CA_KBH                .  MSW_Linkstorage_chp             .   MSW_Charact                         .   INDUSTRY_MW_STOR_CHP
DK_CA_KBH                .  MSW_Linkstorage_ho              .   MSW_Charact                         .   INDUSTRY_MW_STOR_HO
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_chp                 .   INDUSTRY_MW_nonSTOR_CHP_TON
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_chp                 .   INDUSTRY_MW_nonSTOR_CHP_GJ
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_chp                 .   INDUSTRY_MW_nonSTOR_CHP_ASH
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_chp                 .   INDUSTRY_MW_STOR_CHP_TON
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_chp                 .   INDUSTRY_MW_STOR_CHP_GJ
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_chp                 .   INDUSTRY_MW_STOR_CHP_ASH
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_ho                  .   INDUSTRY_MW_nonSTOR_HO_TON
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_ho                  .   INDUSTRY_MW_nonSTOR_HO_GJ
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_ho                  .   INDUSTRY_MW_nonSTOR_HO_ASH
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_ho                  .   INDUSTRY_MW_STOR_HO_TON
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_ho                  .   INDUSTRY_MW_STOR_HO_GJ
DK_CA_KBH                .  MSW_Charact                     .   SUM_MSW_charact_ho                  .   INDUSTRY_MW_STOR_HO_ASH

DK_CA_KBH                .  SUM_MSW_charact_chp             .   MSW_chp_large                       .   MSW_FUELCHP_TON
DK_CA_KBH                .  SUM_MSW_charact_chp             .   MSW_chp_large                       .   MSW_FUELCHP_GJ
DK_CA_KBH                .  SUM_MSW_charact_chp             .   MSW_chp_large                       .   MSW_FUELCHP_ASH
DK_CA_KBH                .  SUM_MSW_charact_ho              .   MSW_ho_large                        .   MSW_FUELHO_TON
DK_CA_KBH                .  SUM_MSW_charact_ho              .   MSW_ho_large                        .   MSW_FUELHO_GJ
DK_CA_KBH                .  SUM_MSW_charact_ho              .   MSW_ho_large                        .   MSW_FUELHO_ASH
DK_CA_KBH                .  MSW_chp_large                   .   ElecBuffer                          .   ELECFLOW
DK_CA_KBH                .  MSW_chp_large                   .   HeatBuffer                          .   HEATFLOW
DK_CA_KBH                .  Money_buffer_T                  .   MSW_chp_large                       .   OPERATIONCOST
DK_CA_KBH                .  MSW_chp_large                   .   Bottom_ash                          .   ASH_MSW
DK_CA_KBH                .  MSW_ho_large                    .   HeatBuffer                          .   HEATFLOW
DK_CA_KBH                .  Money_buffer_T                  .   MSW_ho_large                        .   OPERATIONCOST
DK_CA_KBH                .  MSW_ho_large                    .   Bottom_ash                          .   ASH_MSW

DK_CA_KBH                .  StrawGen                        .   ProcEximTruck                       .   STRAWFLOW
DK_CA_KBH                .  ProcEximTruck                   .   MSW_Linkstorage_gasif               .   STRAWFLOW
DK_CA_KBH                .  MSW_Linkstorage_gasif           .   Gasification_ST                     .   STRAWFLOW
DK_CA_KBH                .  Gasification_ST                 .   SYNGASBuffer                        .   SYNGASFLOW
DK_CA_KBH                .  Gasification_ST                 .   HeatBuffer                          .   HEATFLOW
DK_CA_KBH                .  Money_buffer_T                  .   Gasification_ST                     .   OPERATIONCOST


/;

* For convinience assigning same data to all other areas (then overwriting where needed).
* However, note that areas that are exclusively for transit with respect to transport should not be part of this:


FLOWFROMTOPROC(AREASURBAN,IPROCFROM,IPROCTO,FLOW)$(NOT FLOWFROMTOPROC(AREASURBAN,IPROCFROM,IPROCTO,FLOW)) =FLOWFROMTOPROC('DK_CA_KBH',IPROCFROM,IPROCTO,FLOW);


* Information for trading with other countries


* For convinience assigning same data to all other exporting areas (then overwriting where needed).
FLOWFROMTOPROC(IA,IPROCFROM,IPROCTO,FLOW)$(AAAIMPORT(IA))=FLOWFROMTOPROC('UK_AAA',IPROCFROM,IPROCTO,FLOW);


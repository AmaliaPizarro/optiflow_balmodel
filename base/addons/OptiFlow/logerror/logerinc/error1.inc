* File error1.inc

* This file is part of the OptiWaste model, version XX (August 2014).

* File last time modified



***Hallo:
***ved samk�ring med B.gms skal du overveje, om der skal skrives til forskellige foldere og/eller filnavne!


* This file initiates check on the data and solution of the model.


* Output related to errors in data and solution.
* This file will contain information about errors in input data, if any:
FILE ERRORFILE         /..\LogError\errors.out/;
if ((ERRORFILE.pw LT 32000),
ERRORFILE.pw = 32000;
PUT ERRORFILE;
PUT "File errors.out: Detailed description of OptiWaste errors."/;
PUT "Version:         " SYSTEM.TITLE /;
PUT "File created:    " SYSTEM.DATE " " SYSTEM.TIME //;
);

* This file will contain a summary of the information in the file ERRORS.OUT,
* and about the solver status for each year of the simulation
* including the objective function value:

FILE logfile          /..\LogError\logfile.out/;
if ((logfile.pw LT 32000),
logfile.pw = 32000;
PUT logfile;
PUT "File logfile.out."/;
PUT "Version:         " SYSTEM.TITLE /;
PUT "Log created:     " SYSTEM.DATE " " SYSTEM.TIME //;
);


* Initialisation of auxiliary variables:
* (No errcount1)
$if not declared IERRCOUNT2 SCALAR IERRCOUNT2 "Error count 2" /0/;
$if not declared IERRCOUNT3 SCALAR IERRCOUNT3 "Error count 3" /0/;
$if not declared IERRCOUNT4 SCALAR IERRCOUNT4 "Error count 4" /0/;


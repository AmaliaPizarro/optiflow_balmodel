* File optiwastew.mss
*-------------------------------------------------------------------------------

* This file is part of the OptiWaste model, version XX (August 2014).
* File last time modified


* This file will print the model and solver status, the objective value, etc.,
* for the model OptiWasteW for each year solved



Put logfile;

* Note: The below text: "Some OptiWaste errors" is a keyword for communication with the BUI. Do not change.

IF ((IERRCOUNT2 EQ 0),
PUT "No obvious OptiWaste errors detected in the input data before simulation." //;
ELSE
PUT "Some OptiWaste errors were detected in the input data before simulation." " See the file errors.out." //;
)



PUT "Solver status of the OptiWasteW model" //;
IF((CARD(Y) EQ 1), PUT "One year in the model."  //;
ELSE      PUT CARD(Y):4:0 " years in the model."  //;   );
PUT "Model status returned      Solver status returned          Obj. fct. value  Iter. used  No of infeas No of nonopt" /;



* Note: The below text: "Some OptiWaste errors" is a keyword for communication with the BUI. Do not change.
IF((IERRCOUNT3 GT 0),
PUT "       Some OptiWaste errors were detected before optimisation. See the file errors.out." /;
);


PUT$(OPTIWASTEW.MODELSTAT EQ 1)  " Optimal                     ";
PUT$(OPTIWASTEW.MODELSTAT EQ 2)  " Locally optimal             ";
PUT$(OPTIWASTEW.MODELSTAT EQ 3)  " Unbounded                   ";
PUT$(OPTIWASTEW.MODELSTAT EQ 4)  " Infeasible                  ";
PUT$(OPTIWASTEW.MODELSTAT EQ 5)  " Locally infeasible          ";
PUT$(OPTIWASTEW.MODELSTAT EQ 6)  " Intermediate infeasible     ";
PUT$(OPTIWASTEW.MODELSTAT EQ 7)  " Intermediate nonoptimal     ";
PUT$(OPTIWASTEW.MODELSTAT EQ 8)  " Integer solution            ";
PUT$(OPTIWASTEW.MODELSTAT EQ 9)  " Intermediate non-integer    ";
PUT$(OPTIWASTEW.MODELSTAT EQ 10) " Integer infeasible          ";
PUT$(OPTIWASTEW.MODELSTAT EQ 12) " Error unknown               ";
PUT$(OPTIWASTEW.MODELSTAT EQ 13) " Error no solution           ";
PUT$(OPTIWASTEW.MODELSTAT EQ 14) " No solution returned        ";
PUT$(OPTIWASTEW.MODELSTAT EQ 15) " Solved unique               ";
PUT$(OPTIWASTEW.MODELSTAT EQ 16) " Solved                      ";
PUT$(OPTIWASTEW.MODELSTAT EQ 17) " Solved singular             ";
PUT$(OPTIWASTEW.MODELSTAT EQ 18) " Unbounded no solution       ";
PUT$(OPTIWASTEW.MODELSTAT EQ 19) " Infeasible no solution      ";
PUT$(OPTIWASTEW.MODELSTAT GE 20) " Error no. " OPTIWASTEW.MODELSTAT:3:0 " ";

PUT$(OPTIWASTEW.SOLVESTAT EQ 1)  " Normal completion           ";
PUT$(OPTIWASTEW.SOLVESTAT EQ 2)  " Max iterations reached      ";
PUT$(OPTIWASTEW.SOLVESTAT EQ 3)  " Resource limit reached      ";
PUT$(OPTIWASTEW.SOLVESTAT GE 4)  " Non optimal - error no.  " OPTIWASTEW.SOLVESTAT:3:0
;

PUT VOBJW.L:16:2 " ";
PUT OPTIWASTEW.ITERUSD:11:0 " " ;
PUT OPTIWASTEW.NUMINFES:13:0 " " ;
PUT OPTIWASTEW.NUMNOPT:12:0 /;



* Note: The below text: "Some OptiWaste errors" is a keyword for communication with the BUI. Do not change.
IF((IERRCOUNT4 GT 0),
PUT "       Some OptiWaste errors were detected after optimisation. See the file errors.out." /;
);



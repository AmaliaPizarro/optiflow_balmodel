* File error4.inc

* This file is part of the Balmorel model, version 2.13 Alpha (xxx 2006).
* File last time modified 20080414(hr)


* This file contains check on the solution of the optimisation.
* The checks are primarily intended as help to detect reasons for infeasibility.

* Check the code below to see the exact meaning of this.


PUT ERRORFILE;

PUT /"================================================================================"/;
PUT "The output was investigated for obvious errors after optimisation:" /;
PUT  "================================================================================"/;

IERRCOUNT4=0;

    /*
*POSITIVE VARIABLE VQNODEBALANCE(AAA,PROC,IFLOWIN,IFLOWOUT,S,T,IPLUSMINUS) "Feasibility-ensuring variable with high penalty cost - values are all zero in an OK model instance (U/h)";
* QOBJ:    +(-VQNODEBALANCE(IA,PROC,IFLOWIN,IFLOWOUT,S,ITWWT,'IPLUS')+VQNODEBALANCE(IA,PROC,IFLOWIN,IFLOWOUT,S,ITWWT,'IMINUS'))$(IPROCINOUTFLOW2(IA,PROC,IFLOWIN,IFLOWOUT))

   */


* Check whether the articifial slack variables on equations have non-zero values.
* If they have, this indicates an error.

$ifi not %useQNODEslack%==yes  $goto notuseQNODEslack
* VQNODEBALANCE:
LOOP((Y,IA,PROC,IFLOWIN,IFLOWOUT,S,T)$(IPROCINOUTFLOW(IA,PROC,IFLOWIN,IFLOWOUT)  AND
                                         (IPRIOVARIOUTSUM(IA,PROC,IFLOWIN,IFLOWOUT) LE 1) AND
                                         (IPRIOVARIINSUM(IA,PROC,IFLOWIN,IFLOWOUT) LE 1)  AND
                                         (NOT PROCLINKSTORAGE(PROC))),
  LOOP(IPLUSMINUS$VQNODEBALANCE.L(Y,IA,PROC,IFLOWIN,IFLOWOUT,S,T,IPLUSMINUS),
    PUT "Error: Artificial slack variable VQNODEBALANCE has a value, indicating infeasibility: ";
    PUT  IA.TL:OLTWCRA PROC.TL:OLTWP IFLOWIN.TL:OLTWF  IFLOWOUT.TL:OLTWF   S.TL:OLTWYST  T.TL:OLTWYST   " Value: " VQNODEBALANCE.L(Y,IA,PROC,IFLOWIN,IFLOWOUT,S,T,IPLUSMINUS):10:3  "  for " IPLUSMINUS.TL:8 /;
    IERRCOUNT4 = IERRCOUNT4 + 1;
  );
);

$label notuseQNODEslack

$ifi not %useSSBslack%==yes  $goto notuseSSBslack
* VQSOURCEBALANCE:
LOOP((Y,IA,PROCSOURCE,FLOW,ILOUPFXSET)$(SUM(IPROCTO$FLOWFROMTOPROC(IA,PROCSOURCE,IPROCTO,FLOW),1) AND SOSIBUBOUND(IA,PROCSOURCE,FLOW,ILOUPFXSET)),
  LOOP((S,T,IPLUSMINUS)$VQSOURCEBALANCE.L(Y,IA,PROCSOURCE,FLOW,S,T,IPLUSMINUS),
  PUT "Error: Artificial slack variable VQSOURCEBALANCE has a value, indicating infeasibility: ";
  PUT  IA.TL:OLTWCRA PROCSOURCE.TL:OLTWP FLOW.TL:OLTWF  S.TL:OLTWYST  T.TL:OLTWYST   " Value: " VQSOURCEBALANCE.L(Y,IA,PROCSOURCE,FLOW,S,T,IPLUSMINUS):10:3  "  for " IPLUSMINUS.TL:8 /;
  IERRCOUNT4 = IERRCOUNT4 + 1;
););

* VQSINKBALANCE:
LOOP((Y,IA,PROCSINK,FLOW,ILOUPFXSET)$(SUM(IPROCFROM$FLOWFROMTOPROC(IA,IPROCFROM,PROCSINK,FLOW),1) AND SOSIBUBOUND(IA,PROCSINK,FLOW,ILOUPFXSET)),
  LOOP((S,T,IPLUSMINUS)$VQSINKBALANCE.L(Y,IA,PROCSINK,FLOW,S,T,IPLUSMINUS),
  PUT "Error: Artificial slack variable VQSINKBALANCE has a value, indicating infeasibility: ";
  PUT  IA.TL:OLTWCRA PROCSINK.TL:OLTWP FLOW.TL:OLTWF  S.TL:OLTWYST  T.TL:OLTWYST   " Value: " VQSINKBALANCE.L(Y,IA,PROCSINK,FLOW,S,T,IPLUSMINUS):10:3  "  for " IPLUSMINUS.TL:8 /;
  IERRCOUNT4 = IERRCOUNT4 + 1;
););

* VQBUFFERBALANCE:
LOOP((Y,IA,PROCBUFFER,FLOW,ILOUPFXSET)$((SUM(IPROCFROM$FLOWFROMTOPROC(IA,IPROCFROM,PROCBUFFER,FLOW),1) OR SUM(IPROCTO$FLOWFROMTOPROC(IA,PROCBUFFER,IPROCTO,FLOW),1)) AND SOSIBUBOUND(IA,PROCBUFFER,FLOW,ILOUPFXSET)),
  LOOP((S,T,IPLUSMINUS)$VQBUFFERBALANCE.L(Y,IA,PROCBUFFER,FLOW,S,T,IPLUSMINUS),
  PUT "Error: Artificial slack variable VQBUFFERBALANCE has a value, indicating infeasibility: ";
  PUT  IA.TL:OLTWCRA PROCBUFFER.TL:OLTWP FLOW.TL:OLTWF S.TL:OLTWYST  T.TL:OLTWYST  " Value: " VQBUFFERBALANCE.L(Y,IA,PROCBUFFER,FLOW,S,T,IPLUSMINUS):10:3   "  for " IPLUSMINUS.TL:8   /;
  IERRCOUNT4 = IERRCOUNT4 + 1;
););
$label notuseSSBslack

$ifi not %useShareSlack%==yes  $goto notuseShareSlack

     /*
POSITIVE VARIABLE VQFLOWBOUNDSHAREOUTUP(AAA,PROC,IFLOWIN,IFLOWOUT,S,T,ILOUPFXSET,IPLUSMINUS)         "Feasibility-ensuring variable with high penalty cost - values are all zero in an OK model instance (U/h)";


EQUATION QFLOWBOUNDSHAREOUTUP(AAA,PROC,IFLOWIN,IFLOWOUT,S,T) "Maximum shares of split FLOWs leaving PROC (U/h)";   QFLOWBOUNDSHAREOUTUP(IA,PROC,IFLOWIN,IFLOWOUT,S,ITWWT)$(IPROCINOUTFLOW(IA,PROC,IFLOWIN,IFLOWOUT) AND PROCINOUTFLOW(IA,PROC,IFLOWIN,IFLOWOUT,"IONEMANY")).
EQUATION QFLOWBOUNDSHAREOUTLO(AAA,PROC,IFLOWIN,IFLOWOUT,S,T) "Minimum shares of split FLOWs leaving PROC (U/h)";
EQUATION QFLOWBOUNDSHAREOUTFX(AAA,PROC,IFLOWIN,IFLOWOUT,S,T) "Fixed shares of split FLOWs leaving PROC (U/h)";
EQUATION QFLOWBOUNDSHAREINUP(AAA,PROC,IFLOWIN,IFLOWOUT,S,T)  "Maximum shares of sum of inflow to PROC (U/h)";
EQUATION QFLOWBOUNDSHAREINLO(AAA,PROC,IFLOWIN,IFLOWOUT,S,T)  "Minimum shares of sum of inflow to PROC (U/h)";
EQUATION QFLOWBOUNDSHAREINFX(AAA,PROC,IFLOWIN,IFLOWOUT,S,T)  "Fixed shares of sum of inflow to PROC (U/h)";

     */


$label notuseShareSlack
*------------------------------------------------------------------------------
*------------------------------------------------------------------------------
* The conclusion is printed:




IF ((IERRCOUNT4 EQ 0),
PUT "No obvious errors detected after optimisation."/;
ELSE
IF ((IERRCOUNT4 EQ 1),
PUT " An error was detected after optimisation." /;
ELSE
PUT IERRCOUNT4:5:0 " errors were detected after optimisation." /;
);
PUT "See the file error4.inc for details about errors."
PUT //;
);

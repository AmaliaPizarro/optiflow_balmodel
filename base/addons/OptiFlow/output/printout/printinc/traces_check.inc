* File traces.inc for the OptiWaste model.

* Testing phase only  

PUT tracesfile system.date "  " system.time / /;
PUT  "Overview of found flows" / / ;


* Source:
* Note that there is only one quality of Source flow for each Source process.
PUT "From Source processes:"/;
LOOP(PROCESSSOURCE,
  PUT "   " PROCESSSOURCE.TL:32 ":"/;
  LOOP(TRACES$VSOURCE.L(PROCESSSOURCE,TRACES),
     PUT "      " VSOURCE.L(PROCESSSOURCE,TRACES):12:3  "  flow   "  TRACES.TL:3 "  " TRACES.TS /;
  )
);
PUT //;

* Sink:
* Note that there may be more than one quality Sink flows for each Sink process.
PUT "To Sink processes:"/;
LOOP(PROCESSSINK,
  PUT "   " PROCESSSINK.TL:32 ":"/;
  LOOP(TRACES$VSINK.L(PROCESSSINK,TRACES),
     PUT "      " VSINK.L(PROCESSSINK,TRACES):12:3  "  flow   "  TRACES.TL:32 "  " TRACES.TS/;  ;
  )
);
PUT //;


PUT "To (+?) or from (-?) Buffer processes:"/;
LOOP(PROCESSBUFFER,
  PUT "   " PROCESSBUFFER.TL:32 ":"/;
  LOOP(TRACES$VBUFFER.L(PROCESSBUFFER,TRACES),
     PUT "      " VBUFFER.L(PROCESSBUFFER,TRACES):12:3  "  flow   "  TRACES.TL:32  "  " TRACES.TE(TRACES)/;
  )
);
PUT //;


PUT "Internal processes:"/;
LOOP(IPROCESSESINTERNAL,
  PUT "   " IPROCESSESINTERNAL.TL:32 ":"/;
  LOOP(TRACES$IOUTFROMPROCESS(IPROCESSESINTERNAL,TRACES),
     LOOP(PROCESSTO$VPROCESTRACE.L(IPROCESSESINTERNAL,PROCESSTO,TRACES),
        PUT "      " VPROCESTRACE.L(IPROCESSESINTERNAL,PROCESSTO,TRACES):12:3  "  flow   "  TRACES.TL:32  "  " TRACES.TS/;
     )
  )
);
PUT //;

* File flow.inc for the OptiWaste model.

* Testing phase only

IF(((OPTIWASTEW.MODELSTAT EQ 1) OR (OPTIWASTEW.SOLVESTAT EQ 1)),


PUT flowfile system.date "  " system.time / /;
PUT  "Overview of found flows and (certain) traces" / / ;
* ==============================================================================
PUT  "Overview of found flows" / / ;


* Source:
* Note that there is only one quality of Source flow for each Source PROC.
PUT "From Source PROCes:"/;
LOOP(PROCSOURCE,
  PUT "   " PROCSOURCE.TL:32 ":"/;
  LOOP(FLOW$VSOURCE.L(PROCSOURCE,FLOW),
     PUT "      " VSOURCE.L(PROCSOURCE,FLOW):12:3  "  flow   "  FLOW.TL:32 "  " FLOW.TE(FLOW)/;
  )
);
PUT //;

* Sink:
* Note that there may be more than one quality Sink flows for each Sink PROC.
PUT "To Sink PROCes:"/;
LOOP(PROCSINK,
  PUT "   " PROCSINK.TL:32 ":"/;
  LOOP(FLOW$VSINK.L(PROCSINK,FLOW),
     PUT "      " VSINK.L(PROCSINK,FLOW):12:3  "  flow   "  FLOW.TL:32 "  " FLOW.TE(FLOW)/;  ;
  )
);
PUT //;


PUT "To (+?) or from (-?) Buffer PROCes:"/;
LOOP(PROCBUFFER,
  PUT "   " PROCBUFFER.TL:32 ":"/;
  LOOP(FLOW$VBUFFER.L(PROCBUFFER,FLOW),
     PUT "      " VBUFFER.L(PROCBUFFER,FLOW):12:3  "  flow   "  FLOW.TL:32  "  " FLOW.TE(FLOW)/;
  )
);
PUT //;


PUT "Internal PROCes:"/;
LOOP(IPROCINTERIOR,
  PUT "   " IPROCINTERIOR.TL:32 ":"/;
  LOOP(FLOW$ILEAVEPROC(IPROCINTERIOR,FLOW),
     LOOP(PROCTO$VPROCESFLOW.L(IPROCINTERIOR,PROCTO,FLOW),
        PUT "      " VPROCESFLOW.L(IPROCINTERIOR,PROCTO,FLOW):12:3  "  flow   "  FLOW.TL:32  "  " FLOW.TE(FLOW)/;
     )
  )
);
PUT //;

); !! IF((OPTIWASTEW.MODELSTAT EQ 1) AND (OPTIWASTEW.SOLVESTAT EQ 1)),...)

* ==============================================================================
*hr: Outcommented to ensure they dont affect the case study in focus
$ontext
PUT  "Overview of found traces:" / / ;


* Source:
* Note that there is only one quality of Source flow for each Source PROC.
PUT "From Source PROCes:"/;
LOOP(PROCSOURCE,
  LOOP(FLOW$VSOURCE.L(PROCSOURCE,FLOW),
     LOOP(TRACE$COMPOSITION(FLOW,TRACE),
     PUT "   " PROCSOURCE.TL:32 ":"/;
        PUT "Trace:   " TRACE.TL:32  (VSOURCE.L(PROCSOURCE,FLOW) * COMPOSITION(FLOW,TRACE)):12:3  TRACE.TE(TRACE) /;
     )
  )
);
PUT //;


* VERSION 1
* Sink:
* Note that there may be more than one quality Sink flows for each Sink PROC.
PUT "To Sink PROCes:"/;
LOOP(PROCSINK,
  PUT "   " PROCSINK.TL:32 ":"/;
  LOOP(FLOW$VSINK.L(PROCSINK,FLOW),
     LOOP(TRACE$COMPOSITION(FLOW,TRACE),
        PUT "      Trace:   " TRACE.TL:32  (SUM(TRACE2, VSINK.L(PROCSINK,FLOW) * COMPOSITION(FLOW,TRACE2))):12:3  TRACE.TE(TRACE) /;
     )
  )
);
PUT //;


* VERSION 2
* Sink:
* Note that there may be more than one quality Sink flows for each Sink PROC.
PUT "To Sink PROCes:"/;
LOOP(PROCSINK,
  PUT "   " PROCSINK.TL:32 ":"/;
  LOOP(TRACE,
     ISCALAR1 = SUM(FLOW$VSINK.L(PROCSINK,FLOW),  VSINK.L(PROCSINK,FLOW) * COMPOSITION(FLOW,TRACE));
     IF(ISCALAR1,  PUT "      Trace:   " TRACE.TL:32 ISCALAR1:12:3  TRACE.TE(TRACE) /;);
  )
);
PUT //;
$offtext


PUT "To (+?) or from (-?) Buffer PROCes:"/;
PUT "TODO" /;

PUT //;


put "TESTER  QNODEBALANCE" //;

loop((PROC,FLOWIN,FLOWOUT)$(PROCINOUTFLOW(PROC,FLOWIN,FLOWOUT) AND (IPRIOVARIOUTSUM(PROC,FLOWIN,FLOWOUT) LE 1) AND (IPRIOVARIINSUM(PROC,FLOWIN,FLOWOUT) LE 1)),

  loop(PROCFROM$(FLOWFROMTOPROC(PROCFROM,PROC,FLOWOUT) AND (PROCINOUTFLOW(PROC,FLOWIN,FLOWOUT) NE VARIINSUM) AND (PROCINOUTFLOW(PROC,FLOWIN,FLOWOUT) NE VARIOUTSUM)),
* Two inFLOW: THIS IS THE 'secondary'!


put$(    INEGPROCININFLOW(PROC,FLOWIN,FLOWOUT))  PROCFROM.tl:30 "   "   PROC.tl:30 "  "     FLOWOUT.tl:30 /;


));

/*
QNODEBALANCE(PROC,FLOWIN,FLOWOUT)$(PROCINOUTFLOW(PROC,FLOWIN,FLOWOUT) AND (IPRIOVARIOUTSUM(PROC,FLOWIN,FLOWOUT) LE 1) AND (IPRIOVARIINSUM(PROC,FLOWIN,FLOWOUT) LE 1))..
* Into PROC:


* This is the 'secondary:   MISSED IT!
*+ sum(procto$(FLOWFROMTOPROC(PROC,PROCTO,FLOWOUT) and INEGPROCININFLOW(PROC,FLOWIN,FLOWOUT)), VPROCESFLOW(PROC,PROCTO,FLOWOUT))
* The following 'FLOWOUT' is actually an entering FLOW:
  +SUM(PROCFROM$(FLOWFROMTOPROC(PROCFROM,PROC,FLOWOUT) AND (PROCINOUTFLOW(PROC,FLOWIN,FLOWOUT) NE VARIINSUM) AND (PROCINOUTFLOW(PROC,FLOWIN,FLOWOUT) NE VARIOUTSUM)),
* Two inFLOW: THIS IS THE 'secondary'!
 -VPROCESFLOW(PROCFROM,PROC,FLOWOUT)$(    INEGPROCININFLOW(PROC,FLOWIN,FLOWOUT))
      )

*/
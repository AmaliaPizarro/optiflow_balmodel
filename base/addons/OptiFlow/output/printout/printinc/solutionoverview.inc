* File solutionoverview.inc for the OptiWaste model.

* Testing phase only


PUT solviewfile "* File solutionoverview.out"  system.date "  " system.time /;
PUT "Overview of the solution of the OptiWaste model."//;

PARAMETER FLOWVALUE(AAA,FLOW) "Optimal value (magnitude) of FLOW";   !! And this makes me think that ...

IF(((OPTIWASTEW.MODELSTAT EQ 1) OR (OPTIWASTEW.SOLVESTAT EQ 1)),

LOOP(IA,

PUT "Area " IA.TL ": -------------------------------------------------------------"/;

FLOWVALUE(IA,FLOW) = SUM((PROCFROM,PROCTO)$FLOWFROMTOPROC(IA,PROCFROM,PROCTO,FLOW), VPROCESFLOW.L(IA,PROCFROM,PROCTO,FLOW));



ISCALAR1 = 0;
LOOP((PROC,FLOWIN,FLOWOUT),
IF((VQNODEBALANCE.L(IA,PROC,FLOWIN,FLOWOUT,'IPLUS') NE 0),
   PUT "Nodebalance could not be satisfied at node:  "  PROC.tl:32 FLOWIN.tl:32  FLOWOUT.tl:32 ", 'IPLUS' direction is non-zero, value is  " VQNODEBALANCE.L(IA,PROC,FLOWIN,FLOWOUT,'IPLUS'):10:3 /;
   ISCALAR1 = ISCALAR1 + 1;
ELSE
IF((VQNODEBALANCE.L(IA,PROC,FLOWIN,FLOWOUT,'IMINUS') NE 0),
   PUT "Nodebalance could not be satisfied at node:  "  PROC.tl:32 FLOWIN.tl:32  FLOWOUT.tl:32 ", 'IMINUS' direction is non-zero, value is " VQNODEBALANCE.L(IA,PROC,FLOWIN,FLOWOUT,'IMINUS'):10:3 /;
   ISCALAR1 = ISCALAR1 + 1;
))
);
IF((ISCALAR1 EQ 0), PUT "All nodebalances could be satisfied."/; );
PUT / /;

PUT "List of PROC with indication of Source, Sink (BUFFER: TODO) and Exim:"/;
LOOP(PROC,
PUT PROC.TL:32;
    IF(PROCSOURCE(PROC), PUT "  Source  ";
  ELSE
IF(PROCSINK(PROC), PUT "  Sink    ";
      ELSE
        IF(PROCEXIM(PROC),  PUT "  Exim    ";
 ELSE PUT "          ";
))); PUT PROC.TE(PROC)  /;
);
PUT / /;


PUT "List of FLOW with indication of Cons, Indics, and with the optimal values:"/;
LOOP(FLOW,
PUT FLOW.TL:32;  IF(CONSUMPTIONFLOW(FLOW), PUT "  Cons    "; ELSE IF(FLOWINDIC(FLOW), PUT "  Indic   "; ELSE PUT "          "));
PUT FLOWVALUE(IA,FLOW):8:3;

PUT "   "  FLOW.TE(FLOW)  /;
);
PUT / /;

PUT "List of Source flows with optimal values:"/;
PUT "PROC                            FLOW                              Value"/;
LOOP((PROCSOURCE,FLOW)$(SUM((PROCTO)$FLOWFROMTOPROC(IA,PROCSOURCE,PROCTO,FLOW),1)),
PUT PROCSOURCE.tl:32 FLOW.tl:32   VSOURCE.L(IA,PROCSOURCE,FLOW):8:3 /;
);
PUT / /;

PUT "List of Sink flows with optimal values:"/;
PUT "PROC                            FLOW                              Value"/;
LOOP((PROCSINK,FLOW)$(SUM((PROCFROM)$FLOWFROMTOPROC(IA,PROCFROM,PROCSINK,FLOW),1)),
PUT PROCSINK.tl:32 FLOW.tl:32   VSINK.L(IA,PROCSINK,FLOW):8:3 /;
);
PUT / /;

PUT "List of Buffer flows with optimal values:"/;
PUT "PROC                            FLOW                              Value"/;
LOOP((PROCBUFFER,FLOW)$(SUM((PROCFROM)$FLOWFROMTOPROC(IA,PROCFROM,PROCBUFFER,FLOW),1)),
PUT PROCBUFFER.tl:32 FLOW.tl:32   VBUFFER.L(IA,PROCBUFFER,FLOW):8:3 /;
);
LOOP((PROCBUFFER,FLOW)$(SUM((PROCTO)$FLOWFROMTOPROC(IA,PROCBUFFER,PROCTO,FLOW),1)),
PUT PROCBUFFER.tl:32 FLOW.tl:32   VBUFFER.L(IA,PROCBUFFER,FLOW):8:3 /;
);

PUT / /;

PUT "List of Exim flows with optimal values:"/;
put "TODO------------------------TODO"/;
/*
PUT "PROC                            FLOW                              Value"/;
LOOP((PROCBUFFER,FLOW)$(SUM((PROCFROM)$FLOWFROMTOPROC(IA,PROCFROM,PROCBUFFER,FLOW),1)),
PUT PROCBUFFER.tl:32 FLOW.tl:32   VBUFFER.L(IA,PROCBUFFER,FLOW):8:3 /;
);
LOOP((PROCBUFFER,FLOW)$(SUM((PROCTO)$FLOWFROMTOPROC(IA,PROCBUFFER,PROCTO,FLOW),1)),
PUT PROCBUFFER.tl:32 FLOW.tl:32   VBUFFER.L(IA,PROCBUFFER,FLOW):8:3 /;
);
*/
PUT / /;


$ontext    DEBUGGING ATTEMPTSs
put "================================= debug QNODEBALANCE =================================="//;
loop((PROC,FLOWIN,FLOWOUT)$(PROCINOUTFLOW(PROC,FLOWIN,FLOWOUT) AND (IPRIOVARIOUTSUM(PROC,FLOWIN,FLOWOUT) LE 1) AND (IPRIOVARIINSUM(PROC,FLOWIN,FLOWOUT) LE 1)),
put "Passing first line:  (PROC,FLOWIN,FLOWOUT)= " PROC.tl:32 FLOWIN.tl:32 FLOWOUT.tl:32  /;
);

put //;
put "=== Left hand side Into PROC: ===" /;
loop((PROC,FLOWIN,FLOWOUT)$(PROCINOUTFLOW(PROC,FLOWIN,FLOWOUT) AND (IPRIOVARIOUTSUM(PROC,FLOWIN,FLOWOUT) LE 1) AND (IPRIOVARIINSUM(PROC,FLOWIN,FLOWOUT) LE 1)),
put "Passing first line:  (PROC,FLOWIN,FLOWOUT)= " PROC.tl:32 FLOWIN.tl:32 FLOWOUT.tl:32  /;
   loop(PROCFROM$(FLOWFROMTOPROC(PROCFROM,PROC,FLOWIN) AND (PROCINOUTFLOW(PROC,FLOWIN,FLOWOUT) NE VARIOUTSUM) AND (IPRIOVARIINSUM(PROC,FLOWIN,FLOWOUT) EQ 0)),                                             !! summer for at f� identificeret PROCFROM, men kun �t element identificeres per  PROC,FLOWIN,FLOWOUT
*      VPROCESFLOW(PROCFROM,PROC,FLOWIN)*PROCINOUTFLOW(PROC,FLOWIN,FLOWOUT))
       put "    Passing Into PROC: and Not VARIOUTSUM: variinsum??  VPROCESFLOW('PROCFROM',PROC,FLOWIN): "   PROCFROM.tl:32 PROC.tl:32 FLOWIN.tl:32  /;
    )

  loop(PROCFROM$(FLOWFROMTOPROC(PROCFROM,PROC,FLOWIN) AND (PROCINOUTFLOW(PROC,FLOWIN,FLOWOUT) EQ VARIOUTSUM)),
       put "    Passing Into PROC  and Is VARIOUTSUM                VPROCESFLOW(FLOWIN,'PROCFROM',PROC): "   PROCFROM.tl:32 PROC.tl:32 FLOWIN.tl:32  /;
   )


);


put //;
put "=== Right hand side Out from PROC: ===" /;
$offtext

PUT "Area " IA.TL " end  ---------------------------------------------------------"/;

); !! LOOP(IA,...)
); !! IF(((OPTIWASTEW.MODELSTAT EQ 1) OR (OPTIWASTEW.SOLVESTAT EQ 1)),...)

put /"==============TODO: TRANS details ==============" /;
put "==============TODO: TRANS details ==============" /;
put "==============TODO: TRANS details ==============" /;
put "==============TODO: TRANS details ==============" /;
put "==============TODO: TRANS details ==============" /;
put "==============TODO: TRANS details ==============" /;

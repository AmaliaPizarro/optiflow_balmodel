* File print1.inc for the OptiWaste model.
* This file holds declarations in relation to print files.

file helpfile /..\output\printout\helpfile.out/;

file owinputoutfile /..\output\printout\inputout.out/;
owinputoutfile.pw = 32000;

file solviewfile /..\output\printout\solutionoverview.out/;
solviewfile.pw = 32000;

file flowfile /..\output\printout\flowfile.out/;
flowfile.pw = 32000;


$title OptiWaste model 2016 -  Date 20161007 (last rev. 20161007 - Amalia)

$ontext
Hans: 20150402+;
Observation with version 20150402 (reduced data set, using data files from Amalia):
 - QFROMSHAREBOUNDOUTFX, -LO, -UP: Too many equations generated, expressions was revised.
 - QSOURCENODEBALANCE, qnodebalance, QFROMSHAREBOUNDOUTFX, -LO, -UP: too many variabels used in QOBJ, expressions TO BE revised: TODO
 - added option addUP in option file, search %addUP% in optiwaste.gms
 - NPK_split appears in in APROCKAPNES, but not in model. OK?

$offtext

$ontext
THESE ARE MAIN ASSUMPTIONS PRESENTLY:
*  SOSIBUBOUND, SOSIBUBOUND_ST  etc. are general formats
   They are used in code for equation definitions and may be used for input data.
   For some specific input data that are subsets of the SOSIBUs there have been defined additional input parameters,
   any value here will be inserted into (i.e., overwrites) the existing values in the SOSIBUs by code in this file.
   (??: These specific input data  parametes are:
   - DH
   - DH_VAR_T
   - FLOWMAXTOENERGYPLANT (ugly name!) inserted to FLOWBOUND??)

*  Note that due to the generality of the network many flows and equations have a mixture of units, e.g 'ton/h', 'Money/h' where only the '/h' is common
   and units like e.g. 'ton' and 'Money' have nothing in common except the non-existing '/h'.
   For such flows and equation the units will be indicated as '(U/h)' and '(U)', respectively.

*  Flows will have units 'U/h' e.g. 'ton/h', 'MW', 'Money/h', whenever this is possible, which means for most Flows.
   It is not possible where storages are involved, there the units will be e.g. 'ton', 'MWh', 'Money' (VTIMELINKSTOVOL, QTIMESTORAGEBALANCE, (plus future real storages)).
   It is not possible in the objective function, there the units will be e.g. 'ton', 'MWh', 'Money' (QOBJW, QOBJQ, QOBJWQ).
   It is not possible in some other specific IDs, there the units will be e.g. 'ton', 'MWh', 'Money' (VFLOWINDICVALUES, QINDICQUANT, QINDICINDICMIN/-MAX(?), ).
   It is not possible in some other specific IDs, there the units will be pure numbers or shares, indicated as '~',
      e.g. WEIGHT_S (number), IWEIGHTSUMS (number), FLOWSHAREBOUNDOUT (share) - maybe distinguish and use '~' and (share)?
   It is not always that the units are of one kind only for a parameter, e.g. PROCDATA, SPSIBU2INDIC, this is indicated by '*'

* Capacity: see above

*  Note that the following data entry convention is used in a number of data entries (possibly inconsistent!):
   missing input value or 0 input value means 'no input'; the intended input value 0 is to be entered as EPS.
   Further, this is (to be) extended to appliction of default values (1 in most cases), which is used for entries with 'no input'.
$offtext

$ontext
Types of infeasibilities
-        Topological
-        .FX
-        .LO, .UP
Reasoning:
-        "Topological": e.g.: if all Flows goes into an Interior Process, this will permit only Flows values of zero (and it does not make sense in the model)
-        ".FX": Two or more .FX (that are mutually dependent) are inconsistent
-        ".LO, .UP":  As above, but more difficult (?)
How to identify:
-        "Topological": error2.inc
-        ".FX": make only VQ on those Flows that are fixed. If no other bounds (".LO, .UP) then maybe the values and signs of VQ will indicate 'how much'?
-        ".LO, .UP": �
IDs with bounds are characterized by being indexed over ILOUPFXSET. I see the following:
-        PARAMETER SOSIBUBOUND(AAA,PROC,FLOW,ILOUPFXSET) "Bounds on Source, Sink and Buffer Process Flows"
-        PARAMETER SOSIBUBOUND_AS(AAA,PROC,FLOW,SSS,ILOUPFXSET) "Bounds on Source, Sink and Buffer Process Flows"
-        PARAMETER SOSIBUBOUND_AST(AAA,PROC,FLOW,SSS,TTT,ILOUPFXSET) "Bounds on Source, Sink and Buffer Process Flows"
-        PARAMETER FLOWBOUND(AAA,IPROCFROM,IPROCTO,FLOW/*,SSS,TTT*/,ILOUPFXSET) "Bounds on individual interior FLOWs []"
-        PARAMETER FLOWBOUNDSHAREOUT(AAA,IPROCFROM,IPROCTO,FLOW/*,SSS,TTT*/,ILOUPFXSET) "Bounds on shares of variable splits of outflow from PROC []"
-        PARAMETER FLOWBOUNDSHAREIN(AAA,IPROCFROM,IPROCTO,FLOW/*,SSS,TTT*/,ILOUPFXSET) "Bounds on shares of sum of inflow to PROC []"
$offtext


SCALAR IOPTIFLOWVERSION "This version of OptiFlow" /100.20161007 /;

* GAMS options are $included from file optiflowgams.opt
* In order to make them apply globally, the option $ONGLOBAL will first be set here
$ONGLOBAL

* The optiflowgams file holds control settings for GAMS.
$if     EXIST 'optiflowgams.opt' $INCLUDE                  'optiflowgams.opt';

* The optiflowopt file holds control settings for the OptiFlow model.
$if     EXIST 'optiflowopt.opt' $INCLUDE                  'optiflowopt.opt';


* Various items for reporting, error checking and other use

* Convenient constant factors, typically relating Input and Output:
SCALAR IOF1000    'Multiplier 1000'       /1000/;
SCALAR IOF1000000 'Multiplier 1000000'    /1000000/;
SCALAR IOF0001    'Multiplier 0.001'      /0.001/;
SCALAR IOF0000001 'Multiplier 0.000001'   /0.000001/;
SCALAR IOF3P6     'Multiplier 3.6'        /3.6/;
SCALAR IOF24      'Multiplier 24'         /24/;
SCALAR IOF8760    'Multiplier 8760'       /8760/;
SCALAR IOF8784    'Multiplier 8784'       /8784/;
SCALAR IOF365     'Multiplier 365'        /365/;
SCALAR IOF05      'Multiplier 0.5'        /0.5/;
SCALAR IOF1_      'Multiplier 1 (special, posssibly to disappear in future versions'  /1/;
* Scalars for occational use, their meaning will be context dependent :
SCALAR ISCALAR1 "(Context dependent)";
SCALAR ISCALAR2 "(Context dependent)";
SCALAR ISCALAR3 "(Context dependent)";
* Convenient set for use with GDXDIF:
SET IGDXDIFSET /DIF1, DIF2, INS1, INS2/;

$include 'output/printout/printinc/print1.inc';


*-------------------------------------------------------------------------------
* GEOGRAPHY:
*-------------------------------------------------------------------------------

* SETS:
* The following sets, except AAATRANSIT, are common between OptiWaste and Balmorel.
SET CCCRRRAAA         'All geographical entities (CCC + RRR + AAA)';
SET CCC(CCCRRRAAA)    'All Countries';
SET RRR(CCCRRRAAA)    'All regions';
SET AAA(CCCRRRAAA)    'All areas';
SET CCCRRR(CCC,RRR)   'Regions in countries';
SET RRRAAA(RRR,AAA)   'Areas in regions';
SET C(CCC)            'Countries in the simulation';
* Internal sets and aliases in relation to geography, may be used for data assignments:
ALIAS (RRR,IRRRE,IRRRI);
ALIAS (AAA,IAAAE,IAAAI);
SET IR(RRR)            'Regions in the simulation';
SET IA(AAA)            'Areas in the simulation';
SET ICA(CCC,AAA)       'Relation of areas to countries in the simulation';
ALIAS (IR,IRE,IRI);
ALIAS (IA,IAE,IAI);


*-------------------------------------------------------------------------------
*----- Definitions of SETS and ACRONYMS that are given in the $included files:
*-------------------------------------------------------------------------------
* Printing of data to the list file controlled by %ONOFFDATALISTING% and %ONOFFCODELISTING%
* (see file optiwastegams.opt):
%ONOFFDATALISTING%
* TODO: not completely implemented, may wait

SET CCCRRRAAA          'All geographical entities (CCC + RRR + AAA)' %semislash%
$if     EXIST 'data/CCCRRRAAA.inc' $INCLUDE      'data/CCCRRRAAA.inc';
%semislash%;

SET CCC(CCCRRRAAA)       'All Countries' %semislash%
$if     EXIST 'data/CCC.inc' $INCLUDE         'data/CCC.inc';
%semislash%;

SET RRR(CCCRRRAAA)       'All regions' %semislash%
$if     EXIST 'data/RRR.inc' $INCLUDE         'data/RRR.inc';
%semislash%;

SET AAA(CCCRRRAAA)       'All areas' %semislash%
$if     EXIST 'data/AAA.inc' $INCLUDE         'data/AAA.inc';
%semislash%;

SET CCCRRR(CCC,RRR)      'Regions in countries' %semislash%
$if     EXIST 'data/CCCRRR.inc' $INCLUDE         'data/CCCRRR.inc';
%semislash%;

SET RRRAAA(RRR,AAA)      'Areas in regions' %semislash%
$if     EXIST 'data/RRRAAA.inc' $INCLUDE         'data/RRRAAA.inc';
%semislash%;

SET C(CCC)    'Countries in the simulation'  %semislash%
$if     EXIST 'data/C.inc' $INCLUDE         'data/C.inc';
%semislash%;


*-------------------------------------------------------------------------------
* TIME:
*-------------------------------------------------------------------------------


SET YYY    "Years with data"    %semislash%
$if     EXIST 'data/YYY.inc' $INCLUDE         'data/YYY.inc';
%semislash%;

SET Y(YYY) "Years in simulation" %semislash%
$if     EXIST 'data/Y.inc' $INCLUDE         'data/Y.inc';
%semislash%;

SET SSS                'All seasons' %semislash%
$if     EXIST 'data/SSS.inc' $INCLUDE      'data/SSS.inc';
%semislash%;

SET TTT                'All time periods' %semislash%
$if     EXIST 'data/TTT.inc' $INCLUDE      'data/TTT.inc';
%semislash%;

SET S(SSS)    'Seasons in the simulation'   %semislash%
$if     EXIST 'data/S.inc' $INCLUDE      'data/S.inc';
%semislash%;

SET T(TTT)    'Time periods within the season in the simulation'  %semislash%
$if     EXIST 'data/T.inc' $INCLUDE         'data/T.inc';
%semislash%;


PARAMETER WEIGHT_S(SSS) 'Weight (relative length) of each season (~)'    %semislash%
$if     EXIST 'data/WEIGHT_S.inc' $INCLUDE      'data/WEIGHT_S.inc';
%semislash%;

PARAMETER WEIGHT_T(TTT)  'Weight (relative length) of each time period (~)'   %semislash%
$if     EXIST 'data/WEIGHT_T.inc' $INCLUDE      'data/WEIGHT_T.inc';
%semislash%;

%ONOFFCODELISTING%
PARAMETER IHOURSINST(SSS,T)   'Length of time ST-segments (hours)';
SCALAR IWEIGHSUMS 'Sum of weight relations of the time of each S-segment in WEIGHT_S (hours)';
SCALAR IWEIGHSUMT 'Sum of weight relations of the time of each T-segment in WEIGHT_T';
IWEIGHSUMS = SUM(S, WEIGHT_S(S));
IWEIGHSUMT = SUM(T, WEIGHT_T(T));
IHOURSINST(S,T) = IOF8760 * (WEIGHT_S(S)/IWEIGHSUMS)*(WEIGHT_T(T)/IWEIGHSUMT);

PARAMETER TIMELENGHT(TTT);
TIMELENGHT(TTT)=1;

PARAMETER CYCLES(TTT);
CYCLES(T)=CARD(TTT)/(CARD(T)*TIMELENGHT(T));


*------------------------------------------------------------------------------
* Declaration of internal sets:
*------------------------------------------------------------------------------
* Internal sets and aliases in relation to geography.
* Note: although it is possible, you should generally not (i.e., don't!) use
* the following internal sets for data entry or data assignments
* (it may be impossible in future versions).
SET IR(RRR)            'Regions in the simulation';
SET IA(AAA)            'Areas in the simulation';
SET ICA(CCC,AAA)       'Assignment of areas to countries in the simulation';
* The simple way to limit the geographical scope of the model is to specify in the input data
* the set C as a proper subset of the set CCC,
* because the following mechanism will automatically exclude all regions
* and areas not in any country in C.
ICA(CCC,AAA) = YES$(SUM(RRR$ (RRRAAA(RRR,AAA) AND CCCRRR(CCC,RRR)),1) GT 0);
IR(RRR) = YES$(SUM(C,CCCRRR(C,RRR)));
IA(AAA) = YES$(SUM(C,ICA(C,AAA)));

SET IS3(S)   'Present season simulated in balbase3';


*-------------------------------------------------------------------------------
*--- End: Definitions of SETS and ACRONYMS that are given in the $included files
*-------------------------------------------------------------------------------

*-------------------------------------------------------------------------------
*----- Definitions of ALIASES that are needed for data entry:
*-------------------------------------------------------------------------------

* Duplication of sets for describing electricity exchange between regions:
ALIAS (RRR,IRRRE,IRRRI);
ALIAS (IR,IRE,IRI);
* Duplication of sets for describing heat exchange between areas: /*varmetrans*/
ALIAS (AAA,IAAAE,IAAAI);
ALIAS (IA,IAE,IAI);
* Duplication of sets for fuel.

ALIAS(S,IS3LOOPSET);

* Duplication of sets for seasons and hours:
ALIAS (YYY,IYALIAS);
ALIAS (S,ISALIAS);
ALIAS (T,ITALIAS);


*Load of parameters, only characteristics of OptiFlow
$if     EXIST 'OptiFlow_parameters_NoBal.inc' $INCLUDE         'OptiFlow_parameters_NoBal.inc';


* Debugging aids:
* Note: when placed on the left hand side of the relation sign (=L=, =E= or =G=) the sign to the �IPLUS�and �IMINUS� terms should be �+� and �-�, respectively.
SET    IPLUSMINUS "Violation of equation - positive or negative direction, respectively" /IPLUS, IMINUS/;

* ---------------------------------
%ONOFFCODELISTING%
* This file contains initialisations of printing of log and error messages:
$INCLUDE 'logerror/logerinc/error1.inc';

* The input data are subject to a certain control for 'reasonable' values.
* The errors are checked by the code in the files ERRORx.INC
$INCLUDE 'logerror/logerinc/error2.inc';

$include "output\printout\printinc\inputout.inc";

* ---------------------------------

*-------------------------------------------------------------------------------
*----- Any declarations of variables
*-------------------------------------------------------------------------------
$if     EXIST 'OptiFlow_variables.inc' $INCLUDE         'OptiFlow_variables.inc';

*-------------------------------------------------------------------------------
*----- Any declarations of equations
*-------------------------------------------------------------------------------
$if     EXIST 'OptiFlow_deceq.inc' $INCLUDE         'OptiFlow_deceq.inc';

*-------------------------------------------------------------------------------
*----- Equations description
*-------------------------------------------------------------------------------

$if     EXIST 'OptiFlow_equations.inc' $INCLUDE         'OptiFlow_equations.inc';


*-------------------------------------------------------------------------------
*----- Bounds to equations
*-------------------------------------------------------------------------------
$if     EXIST 'OptiFlow_bounds.inc' $INCLUDE         'OptiFlow_bounds.inc';


MODEL OPTIWASTEW  "OptiWaste with weighted objective values"
/
QOBJW
QNODEBALANCE_S
QNODEBALANCE_T
QSOURCENODEBALANCE
QSINKNODEBALANCE
QBUFFERNODEBALANCE_S
QBUFFERNODEBALANCE_T
QSTORAGEBALANCE
QSTORAGEBALANCE_Y
QTRANSITNODEBALANCE
QFLOWSHAREOUT2INUP
QFLOWSHAREOUT2INLO
QFLOWSHAREOUT2INFX
QINDICQUANT
QFLOWSHAREOUT2OUTLO_T
QFLOWSHAREOUT2OUTUP_T
QFLOWSHAREOUT2OUTFX_T
QRAMPBALANCE
QRAMPCONSTRAINT
QFLOWTBALANCE

*$ifi %inv%==yes       QPROCKAPACCUMNET        !! Has weakness - but used anyway
$ifi %inv%==yes       QPROCKAP_UP
$ifi %inv%==yes       QSTOVOLTLIM_T
$ifi %inv%==yes       QSTOVOLTLIM
$ifi %APKNDISC%==yes  QAPKNDISCEQUALCONT
/;

MODEL OPTIWASTEQ  "OptiWaste with objective function value assuming parameterised quantities technique - this model is sketched only"
/
QOBJQ
QNODEBALANCE_S
QNODEBALANCE_T
QSOURCENODEBALANCE
QSINKNODEBALANCE
QINDICQUANT
QINDICINDICMIN
QINDICINDICMAX
/;

MODEL OPTIWASTEWQ  "OptiWaste with objective function value assuming weighted objective values and parameterised quantities technique - this model is sketched only"
/
QOBJWQ
QNODEBALANCE_S
QNODEBALANCE_T
QSOURCENODEBALANCE
QSINKNODEBALANCE
QINDICQUANT
QINDICINDICMIN
QINDICINDICMAX
/;


* ------------------------------------------------------------------------------
*$ifi not %uniquesol%==yes SOLVE OPTIWASTEW  Maximizing  VOBJW  USING LP;     %uniquesol% TODO
*$ifi %uniquesol%==yes     SOLVE OPTIWASTEW  MaxIMIZING  VOBJW  USING QCP;    %uniquesol% TODO
$ifi not %APKNdisc%==yes SOLVE OPTIWASTEW USING LP Maximizing VOBJW ;       !! USING   QCP if uniquesol
$ifi     %APKNdisc%==yes SOLVE OPTIWASTEW  Maximizing VOBJW USING MIP;      !! USING MIQCP if uniquesol

* ------------------------------------------------------------------------------

* For debug it may be convenient ot have zero susbtituted by eps at relevant positions:
$ifi "%addEPStoL%"=="yes"   VFLOW.L(IA,IPROCFROM,IPROCTO,FLOW,S,T)$FLOWFROMTOPROC(IA,IPROCFROM,IPROCTO,FLOW) =  VFLOW.L(IA,IPROCFROM,IPROCTO,FLOW,S,T) + EPS;
$ifi "%addEPStoL%"=="yes"   VSOURCE.L(IA,PROCSOURCE,FLOW,S,T)$(SUM((IPROCTO)$FLOWFROMTOPROC(IA,PROCSOURCE,IPROCTO,FLOW),1)) = VSOURCE.L(IA,PROCSOURCE,FLOW,S,T) + EPS;
$ifi "%addEPStoL%"=="yes"   VSINK.L(IA,PROCSINK,FLOW,S,T)$(SUM((IPROCFROM)$FLOWFROMTOPROC(IA,IPROCFROM,PROCSINK,FLOW),1))  =  VSINK.L(IA,PROCSINK,FLOW,S,T) + EPS;
$ifi "%addEPStoL%"=="yes"   VBUFFER.L(IA,PROCBUFFER,FLOW,S,T)$(SUM((IPROCFROM)$FLOWFROMTOPROC(IA,IPROCFROM,PROCBUFFER,FLOW),1) OR SUM((IPROCTO)$FLOWFROMTOPROC(IA,PROCBUFFER,IPROCTO,FLOW),1)) = VBUFFER.L(IA,PROCBUFFER,FLOW,S,T) + EPS;
$ifi "%addEPStoL%"=="yes"   VTRANS.L(IAE,IAI,FLOW,S,T)$ITRANSFLOWFROMTO(IAE,IAI,FLOW) = VTRANS.L(IAE,IAI,FLOW,S,T) + eps;

$include logerror\logerinc\optiwastew.mss

* The results are checked to some extent:
$INCLUDE 'logerror/logerinc/error4.inc';

$LABEL ENDOFMODEL

* TESTING some gams facility:
*acronym npom; vFLOWsink.l(Y,ia,procsink, flow,s,t)$(NOT PROC_T(procsink) AND (NOT ITWWT(T))) = npom; !! tesT: use e.g. NA or use acronym: legal.


execute_unload "all.gdx";




PARAMETER RAMPUP(FFF) "Ramp-up constraint of technologies (% of the nominal capacity)"
/
MUNI_WASTE  0.1
NUCLEAR     0.1
COAL        0.1
LIGNITE     0.1
PEAT        0.1

STRAW       0.1
WOOD        0.1
WOOD_WASTE  0.1
*WOOD_PELLETS 0.2

/;

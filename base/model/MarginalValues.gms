*Code created for calculation of the marginals for electricity (Amalia, 13-07-2015)

*Call the balmorel mode, which will be solved through a loop in order to calculate all the marginal values
$include Balmorel.gms


* Turn off the listing of the input file */
$offlisting

* Turn off the listing and cross-reference of the symbols used */
$offsymxref offsymlist

Option solprint=off;
Option sysout=off;


*step 1 - setup scenarios_E

SET MAR_E_R(RRR) 'Select regions for calculation of electricity marginals'
/DK_W/;

SET MAR_E_S(S)  'Select weeks for calculation of electricity marginals'
/S08/;


SET MAR_E_T(T)  'Select hours for calculation of electricity marginals'
/T001*T003/;


Set scenarios_E  'Scenarios for calculation of marginal values of electricity' %semislash%
$if     EXIST '../data/scenarios_E.inc' $INCLUDE      '../data/scenarios_E.inc';
$if not EXIST '../data/scenarios_E.inc' $INCLUDE '../../base/data/scenarios_E.inc';
%semislash%;

Parameter scen_Delta_E(RRR,SSS,TTT,scenarios_E) ' Marginal decrease in electricity production ' %semislash%
$if     EXIST '../data/scen_Delta_E.inc' $INCLUDE      '../data/scen_Delta_E.inc';
$if not EXIST '../data/scen_Delta_E.inc' $INCLUDE '../../base/data/scen_Delta_E.inc';
%semislash%;


scen_Delta_E(IR,S,T,scenarios_E)$((NOT MAR_E_R(IR)) OR (NOT MAR_E_S(S)) OR (NOT MAR_E_T(T)) OR (NOT scen_Delta_E(IR,S,T,scenarios_E)))=NO;

Parameter M_IDE_T_Y(RRR,S,T,scenarios_E) Comparative Summary;
Parameter MOBJ(scenarios_E) Comparative Summary;
Parameter MGE_T(AAA,G,S,T,scenarios_E) Comparative Summary;
Parameter MGEN_T(AAA,G,S,T,scenarios_E) Comparative Summary;
Parameter MGH_T(AAA,G,S,T,scenarios_E) Comparative Summary;
Parameter MGF_T(AAA,G,S,T,scenarios_E) Comparative Summary;
Parameter MX_T(IRRRE,IRRRI,S,T,scenarios_E) Comparative Summary;
Parameter MGHN_T(AAA,G,S,T,scenarios_E)  Comparative Summary;
Parameter MGFN_T(AAA,G,S,T,scenarios_E)  Comparative Summary;
Parameter MGKN(AAA,G,scenarios_E)  Comparative Summary;

*step 2 save data
parameter sav_IDE(RRR,SSS,TTT) saved primary commodity prices;
    sav_IDE(IR,S,T)=IDE_T_Y(IR,S,T);


*step 1 - setup scenarios_H
SET MAR_H_A(AAA) 'Select regions for calculation of electricity marginals'
/DK_CA_Kal/;

SET MAR_H_S(S)  'Select weeks for calculation of electricity marginals'
/S08/;


SET MAR_H_T(T)  'Select hours for calculation of electricity marginals'
/T001*T024/;


Set scenarios_H  'Scenarios for calculation of marginal values of electricity' %semislash%
$if     EXIST '../data/scenarios_H.inc' $INCLUDE      '../data/scenarios_H.inc';
$if not EXIST '../data/scenarios_H.inc' $INCLUDE '../../base/data/scenarios_H.inc';
%semislash%;

Parameter scen_Delta_H(AAA,SSS,TTT,scenarios_H) ' Marginal decrease in electricity production ' %semislash%
$if     EXIST '../data/scen_Delta_H.inc' $INCLUDE      '../data/scen_Delta_H.inc';
$if not EXIST '../data/scen_Delta_H.inc' $INCLUDE '../../base/data/scen_Delta_H.inc';
%semislash%;

scen_Delta_H(IA,S,T,scenarios_H)$(scen_Delta_H(IA,S,T,scenarios_H) AND (NOT scenarios_H('BASE')))=0.5;

scen_Delta_H(IA,S,T,scenarios_H)$((NOT MAR_H_A(IA)) OR (NOT MAR_H_S(S)) OR (NOT MAR_H_T(T)) OR (NOT scen_Delta_H(IA,S,T,scenarios_H)))=NO;

Parameter M_IDH_T_Y(AAA,S,T,scenarios_H) Comparative Summary;
Parameter MHOBJ(scenarios_H) Comparative Summary;
Parameter MHGE_T(AAA,G,S,T,scenarios_H) Comparative Summary;
Parameter MHGEN_T(AAA,G,S,T,scenarios_H) Comparative Summary;
Parameter MHGH_T(AAA,G,S,T,scenarios_H) Comparative Summary;
Parameter MHGF_T(AAA,G,S,T,scenarios_H) Comparative Summary;
Parameter MHX_T(IRRRE,IRRRI,S,T,scenarios_H) Comparative Summary;
Parameter MHGHN_T(AAA,G,S,T,scenarios_H)  Comparative Summary;
Parameter MHGFN_T(AAA,G,S,T,scenarios_H)  Comparative Summary;
Parameter MHGKN(AAA,G,scenarios_H)  Comparative Summary;

*step 2 save data
parameter sav_IDH(AAA,SSS,TTT) saved primary commodity prices;
    sav_IDH(IA,S,T)=IDH_T_Y(IA,S,T);


SET TECHTYPE  'Type of technologies for calculation of marginals' %semislash%
$if     EXIST '../data/TECHTYPE.inc' $INCLUDE      '../data/TECHTYPE.inc';
$if not EXIST '../data/TECHTYPE.inc' $INCLUDE '../../base/data/TECHTYPE.inc';
%semislash%;

SET GTECHTYPE(GGG,TECHTYPE) 'Type of technology aggrupated'%semislash%
$if     EXIST '../data/GTECHTYPE.inc' $INCLUDE      '../data/GTECHTYPE.inc';
$if not EXIST '../data/GTECHTYPE.inc' $INCLUDE '../../base/data/GTECHTYPE.inc';
%semislash%;

PARAMETER TECH_CO2(TECHTYPE) 'Emissions associated to every type of technology' %semislash%
$if     EXIST '../data/TECH_CO2.inc' $INCLUDE      '../data/TECH_CO2.inc';
$if not EXIST '../data/TECH_CO2.inc' $INCLUDE '../../base/data/TECH_CO2.inc';
%semislash%;



loop(scenarios_E$sum((IR,S,T),scen_Delta_E(IR,S,T,scenarios_E)),
*step 3 reestablish data to base level
    M_IDE_T_Y(IR,S,T,scenarios_E)=sav_IDE(IR,S,T)+scen_Delta_E(IR,S,T,scenarios_E)/5;

*step 4 change data to levels needed in scenario
    IDE_T_Y(IR,S,T)$(M_IDE_T_Y(IR,S,T,scenarios_E))=M_IDE_T_Y(IR,S,T,scenarios_E);



*step 5 -- solve model
$ifi NOT %UnitComm%==yes $goto notunit
$ifi not %UnitRMIP%==yes
$ifi %bb1%==yes  SOLVE BALBASE1 USING MIP MINIMIZING VOBJ;
$ifi not %UnitRMIP%==yes
$ifi %bb2%==yes  SOLVE BALBASE2 USING MIP MINIMIZING VOBJ;
$ifi not %UnitRMIP%==yes
$ifi %bb3%==yes  SOLVE BALBASE3 USING MIP MINIMIZING VOBJ;
$ifi %UnitRMIP%==yes
$ifi %bb1%==yes  SOLVE BALBASE1 USING RMIP MINIMIZING VOBJ;
$ifi %UnitRMIP%==yes
$ifi %bb2%==yes  SOLVE BALBASE2 USING RMIP MINIMIZING VOBJ;
$ifi %UnitRMIP%==yes
$ifi %bb3%==yes  SOLVE BALBASE3 USING RMIP MINIMIZING VOBJ;
$goto YESunit

$label notunit
$ifi %bb1%==yes SOLVE BALBASE1 USING LP MINIMIZING VOBJ;
$ifi not %bb2%==yes $goto not_bb2;
$ifi %SOLVEMIP%==yes SOLVE BALBASE2 USING MIP MINIMIZING VOBJ;
$ifi not %SOLVEMIP%==yes SOLVE BALBASE2 USING LP MINIMIZING VOBJ;
$label not_bb2;
$ifi %bb3%==yes  SOLVE BALBASE3 USING LP MINIMIZING VOBJ;
$LABEL YESunit
;


*step 6 cross scenario report writing
     M_IDE_T_Y(RRR,S,T,scenarios_E)=IDE_T_Y(RRR,S,T);
     MOBJ(scenarios_E)=VOBJ.L;
*     MGE_T(AAA,G,S,T,scenarios_E)=VGE_T.L(AAA,G,S,T);
*     MGEN_T(AAA,G,S,T,scenarios_E)=VGEN_T.L(AAA,G,S,T);
*     MGH_T(AAA,G,S,T,scenarios_E)=VGH_T.L(AAA,G,S,T) ;
     MGF_T(AAA,G,S,T,scenarios_E)=VGF_T.L(AAA,G,S,T) ;
     MX_T(IRRRE,IRRRI,S,T,scenarios_E)=VX_T.L(IRRRE,IRRRI,S,T) ;
*     MGHN_T(AAA,G,S,T,scenarios_E)=VGHN_T.L(AAA,G,S,T) ;
     MGFN_T(AAA,G,S,T,scenarios_E)=VGFN_T.L(AAA,G,S,T) ;
     MGKN(AAA,G,scenarios_E)=VGKN.L(AAA,G)  ;

*step 7 end of loop
    );

*step 8 Create a marginal Report
Parameter MARGINAL_G(GGG,scenarios_E) 'Calculation of the marginal technology';
MARGINAL_G(G,scenarios_E)$(
(SUM((IA,S,T),MGF_T(IA,G,S,T,scenarios_E)) OR (SUM((IA,S,T),MGFN_T(IA,G,S,T,scenarios_E))) OR (SUM((IA,S,T),MGF_T(IA,G,S,T,'BASE')))  OR (SUM((IA,S,T),MGFN_T(IA,G,S,T,'BASE'))))
 AND (((SUM((IA,S,T),MGF_T(IA,G,S,T,scenarios_E)+MGFN_T(IA,G,S,T,scenarios_E))-SUM((IA,S,T),MGF_T(IA,G,S,T,'BASE')+MGFN_T(IA,G,S,T,'BASE'))) GT 0.0001)
 OR  ((SUM((IA,S,T),MGF_T(IA,G,S,T,scenarios_E)+MGFN_T(IA,G,S,T,scenarios_E))-SUM((IA,S,T),MGF_T(IA,G,S,T,'BASE')+MGFN_T(IA,G,S,T,'BASE'))) LE (-0.0001))
))
=SUM((IA,S,T),MGF_T(IA,G,S,T,scenarios_E)+MGFN_T(IA,G,S,T,scenarios_E))-SUM((IA,S,T),MGF_T(IA,G,S,T,'BASE')+MGFN_T(IA,G,S,T,'BASE'));


Parameter MARGINAL_TECH(TECHTYPE,scenarios_E) 'Calculation of the marginal technology type in every scenario';
MARGINAL_TECH(TECHTYPE,scenarios_E)$(
((SUM(G$GTECHTYPE(G,TECHTYPE),MARGINAL_G(G,scenarios_E))) GT 0.01 )
OR
((SUM(G$GTECHTYPE(G,TECHTYPE),MARGINAL_G(G,scenarios_E))) LT (-0.01))
)
=SUM(G$GTECHTYPE(G,TECHTYPE),MARGINAL_G(G,scenarios_E))/(SUM((IR,S,T),scen_Delta_E(IR,S,T,scenarios_E)));



PARAMETER MARGINAL_CO2(scenarios_E) 'Marginal emissions associated to the change delta in Electricity';
MARGINAL_CO2(scenarios_E)=SUM(TECHTYPE,MARGINAL_TECH(TECHTYPE,scenarios_E)*TECH_CO2(TECHTYPE));


Parameter MARGINAL_KN(GGG,scenarios_E) 'Calculation of long-term affected installed capacity';
MARGINAL_KN(G,scenarios_E)$((SUM(IA,MGKN(IA,G,scenarios_E)) OR (SUM(IA,MGKN(IA,G,'BASE'))))
AND (((SUM(IA,MGKN(IA,G,scenarios_E))-SUM(IA,MGKN(IA,G,'BASE'))) GT 0.001)
OR ((SUM(IA,MGKN(IA,G,scenarios_E))-SUM(IA,MGKN(IA,G,'BASE'))) LT (-0.0001))
))
=SUM(IA,MGKN(IA,G,scenarios_E))-SUM(IA,MGKN(IA,G,'BASE'));

*step 9 compute and display final results


$ontext

loop(scenarios_H$sum((IA,S,T),scen_Delta_H(IA,S,T,scenarios_H)),
*step 3 reestablish data to base level
    M_IDH_T_Y(IA,S,T,scenarios_H)=sav_IDH(IA,S,T)+scen_Delta_H(IA,S,T,scenarios_H)/5;

*step 4 change data to levels needed in scenario
    IDH_T_Y(IA,S,T)$(M_IDH_T_Y(IA,S,T,scenarios_H))=M_IDH_T_Y(IA,S,T,scenarios_H);



*step 5 -- solve model
$ifi NOT %UnitComm%==yes $goto notunit
$ifi not %UnitRMIP%==yes
$ifi %bb1%==yes  SOLVE BALBASE1 USING MIP MINIMIZING VOBJ;
$ifi not %UnitRMIP%==yes
$ifi %bb2%==yes  SOLVE BALBASE2 USING MIP MINIMIZING VOBJ;
$ifi not %UnitRMIP%==yes
$ifi %bb3%==yes  SOLVE BALBASE3 USING MIP MINIMIZING VOBJ;
$ifi %UnitRMIP%==yes
$ifi %bb1%==yes  SOLVE BALBASE1 USING RMIP MINIMIZING VOBJ;
$ifi %UnitRMIP%==yes
$ifi %bb2%==yes  SOLVE BALBASE2 USING RMIP MINIMIZING VOBJ;
$ifi %UnitRMIP%==yes
$ifi %bb3%==yes  SOLVE BALBASE3 USING RMIP MINIMIZING VOBJ;
$goto YESunit

$label notunit
$ifi %bb1%==yes SOLVE BALBASE1 USING LP MINIMIZING VOBJ;
$ifi not %bb2%==yes $goto not_bb2;
$ifi %SOLVEMIP%==yes SOLVE BALBASE2 USING MIP MINIMIZING VOBJ;
$ifi not %SOLVEMIP%==yes SOLVE BALBASE2 USING LP MINIMIZING VOBJ;
$label not_bb2;
$ifi %bb3%==yes  SOLVE BALBASE3 USING LP MINIMIZING VOBJ;
$LABEL YESunit
;


*step 6 cross scenario report writing
     M_IDH_T_Y(AAA,S,T,scenarios_H)=IDH_T_Y(AAA,S,T);
     MHOBJ(scenarios_H)=VOBJ.L;
*     MGE_T(AAA,G,S,T,scenarios_H)=VGE_T.L(AAA,G,S,T);
*     MGEN_T(AAA,G,S,T,scenarios_H)=VGEN_T.L(AAA,G,S,T);
*     MGH_T(AAA,G,S,T,scenarios_H)=VGH_T.L(AAA,G,S,T) ;
     MHGF_T(AAA,G,S,T,scenarios_H)=VGF_T.L(AAA,G,S,T) ;
     MHX_T(IRRRE,IRRRI,S,T,scenarios_H)=VX_T.L(IRRRE,IRRRI,S,T) ;
*     MGHN_T(AAA,G,S,T,scenarios_H)=VGHN_T.L(AAA,G,S,T) ;
     MHGFN_T(AAA,G,S,T,scenarios_H)=VGFN_T.L(AAA,G,S,T) ;
     MHGKN(AAA,G,scenarios_H)=VGKN.L(AAA,G)  ;

*step 7 end of loop
    );

*step 8 Create a marginal Report
Parameter MARGINALH_G(GGG,scenarios_H) 'Calculation of the marginal technology';
MARGINALH_G(G,scenarios_H)$(
(SUM((IA,S,T),MHGF_T(IA,G,S,T,scenarios_H)) OR (SUM((IA,S,T),MHGFN_T(IA,G,S,T,scenarios_H))) OR (SUM((IA,S,T),MHGFN_T(IA,G,S,T,'BASE')))  OR (SUM((IA,S,T),MHGFN_T(IA,G,S,T,'BASE'))))
 AND (((SUM((IA,S,T),MHGF_T(IA,G,S,T,scenarios_H)+MHGFN_T(IA,G,S,T,scenarios_H))-SUM((IA,S,T),MHGF_T(IA,G,S,T,'BASE')+MHGFN_T(IA,G,S,T,'BASE'))) GT 0.0001)
 OR  ((SUM((IA,S,T),MHGF_T(IA,G,S,T,scenarios_H)+MHGFN_T(IA,G,S,T,scenarios_H))-SUM((IA,S,T),MHGF_T(IA,G,S,T,'BASE')+MHGFN_T(IA,G,S,T,'BASE'))) LE (-0.0001))
))
=SUM((IA,S,T),MHGF_T(IA,G,S,T,scenarios_H)+MHGFN_T(IA,G,S,T,scenarios_H))-SUM((IA,S,T),MHGF_T(IA,G,S,T,'BASE')+MHGFN_T(IA,G,S,T,'BASE'));


Parameter MARGINALH_TECH(TECHTYPE,scenarios_H) 'Calculation of the marginal technology type in every scenario';
MARGINALH_TECH(TECHTYPE,scenarios_H)$(
((SUM(G$GTECHTYPE(G,TECHTYPE),MARGINALH_G(G,scenarios_H))) GT 0.01 )
OR
((SUM(G$GTECHTYPE(G,TECHTYPE),MARGINALH_G(G,scenarios_H))) LT (-0.01))
)
=SUM(G$GTECHTYPE(G,TECHTYPE),MARGINALH_G(G,scenarios_H))/(SUM((IA,S,T),scen_Delta_H(IA,S,T,scenarios_H)));


PARAMETER MARGINALH_CO2(scenarios_H) 'Marginal emissions associated to the change delta in Electricity';
MARGINALH_CO2(scenarios_H)=SUM(TECHTYPE,MARGINALH_TECH(TECHTYPE,scenarios_H)*TECH_CO2(TECHTYPE));



*step 9 compute and display final results


$offtext


execute_unload "marginalresults.gdx";




